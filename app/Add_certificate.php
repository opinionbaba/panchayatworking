<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Add_certificate extends Model
{
    protected $table='certificate_type';
	protected $fillable =['cer_name', 'cer_desc', 'aadhar_card', 'ration_card', 'passport', 'voter_card', 'pan_card', 'health_card', 'birth_certi', 'driving_lic', 'created_at', 'updated_at'];
}
