<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Admin extends Model
{
    protected $table = 'admin';
	
	 protected $fillable = [
        'user_id', 'full_name', 'email','password','address','dob','image','contact','gender',
		'country', 'created_on', 'status','user_type','created_at'
		];
}
