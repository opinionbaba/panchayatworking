<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Asset extends Model
{
   protected $table='asset';
   protected $fillable = ['user_id','asset', 'created_at', 'updated_at'];
}
