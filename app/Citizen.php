<?php

namespace App;
use Illuminate\Database\Eloquent\Model;

class Citizen extends Model
{
   protected $table = 'citizen';
   protected $fillable = ['citizen_type', 'f_name', 'm_name', 'l_name', 'otp','password','ward_number', 'mobile', 'land_number', 'family_number', 'email', 'house_number', 'area', 'locality', 'village', 'city', 'state', 'pin_code', 'created_at', 'updated_at'];
}
