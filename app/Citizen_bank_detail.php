<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Citizen_bank_detail extends Model
{
    protected $table='citizen_bank_details';
	protected $fillable =['user_id', 'ac_number', 'ifsc', 'bank_name', 'branch_name', 'upi_id', 'ph_pay_number', 'goo_pay_number', 'pytm_number', 'status', 'created_at', 'updated_at'];
}
