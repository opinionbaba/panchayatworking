<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Citizen_document extends Model
{
   protected $table ='citizen_doc';
   protected $fillable = ['user_id', 'panc_id','aadhar_num', 'aadhar_front', 'aadhar_back', 'ration_num', 'ration_front', 'ration_back', 'pass_num', 'pass_front', 'pass_back', 'voter_num', 'voter_card_front', 'voter_card_back', 'health_card_num', 'pan_num','pan_image','health_file', 'b_certi_num', 'b_file', 'driving_num', 'driving_file', 'created_at', 'updated_at'];
}
