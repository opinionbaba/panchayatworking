<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Citizen_feedback extends Model
{
     protected $table='citizen_feedback';
	protected $fillable =['user_id', 'subject', 'description', 'created_at', 'updated_at'];
}
