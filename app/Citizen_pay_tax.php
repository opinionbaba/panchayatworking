<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Citizen_pay_tax extends Model
{
    protected $table='citizen_pay_tax';
	protected $fillable = [ 'user_id', 'panc_id', 'asset_name', 'amount', 'payment_id', 'status'];
}
