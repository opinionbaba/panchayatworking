<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Event extends Model
{
	protected $table = 'events';
	
	 protected $fillable = [
        'event_name', 'event_cat', 'base_path','file','created_at','updated_at'
		];
}
