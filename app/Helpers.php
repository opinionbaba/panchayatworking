<?php

//https://documentation.cpanel.net/display/DD/Guide+to+API+Authentication
//https://hostname.example.com:2083/cpsess###########/json-api/cpanel?cpanel_jsonapi_user=user&cpanel_jsonapi_apiversion=2&cpanel_jsonapi_module=AddonDomain&cpanel_jsonapi_func=addaddondomain&dir=addondomain

function cpanel_reginster_new_domain($DomainUser,$defaultDomainContactId_Live,$defaultNameServer,$defaultIPAddress) {
    
    $RegistrantContactId = dm_create_domain_contact_to_registrant(dm_create_domain_contact([
        'FirstName' => $DomainUser["store_first_name"],
        'LastName' => $DomainUser["store_last_name"],
        'Address' => $DomainUser["store_address"],
        'City' => $DomainUser["store_city"],
        'Country' => 'IN',
        'State' => $DomainUser["store_state"],
        'PostCode' => $DomainUser["post_code"],
        'CountryCode' => '91',
        'Phone' => $DomainUser["store_mobile_no"],
        'Mobile' => $DomainUser["store_mobile_no"],
        'Email' => $DomainUser["store_email_id"],
        'AccountType' => 'Personal'
    ]));

    //below code require when new admin, tech creation
    /*$defaultDomainContactId_Live = dm_create_domain_contact_to_admin(dm_create_domain_contact([
        'FirstName' => 'Asif',
        'LastName' => 'Inamdar',
        'Address' => "Royal Enclave Front Gate Srirampura, Jakkur",
        'City' => "Bangalore",
        'Country' => 'IN',
        'State' => "Karnataka",
        'PostCode' => "560064",
        'CountryCode' => '91',
        'Phone' => "+8459581549",
        'Mobile' => "8459581549",
        'Email' => "tasksmith@yahoo.com",
        'AccountType' => 'Business',
        'BusinessName' => 'FeeSeva',
        'BusinessNumberType' => 'ABN',
        'BusinessNumber' => '9673713168',
    ]));
    
    print_r( $defaultDomainContactId_Live);
    die;*/

    $DomainResponse = dm_register_domain([
        'DomainName' => $DomainUser["store_domain"],
        'RegistrantContactIdentifier' => $RegistrantContactId,
        'AdminContactIdentifier' => $defaultDomainContactId_Live,
        'BillingContactIdentifier' => $defaultDomainContactId_Live,
        'TechContactIdentifier' => $defaultDomainContactId_Live,
        'RegistrationPeriod' => 1,
        'NameServers' => [
            ['Host' => $defaultNameServer[1], 'IP' => $defaultIPAddress],
            ['Host' => $defaultNameServer[2], 'IP' => $defaultIPAddress]
        ],
    ]);
        
    return $DomainResponse;
    if (isset($DomainResponse->APIResponse->DomainDetails)) {
        $Response["Domain"] = $DomainResponse->APIResponse->DomainDetails;
    }
    

    return $Response["Domain"];
}


    function cpanel_register_addondomain($PanchayatData, $defaultCpanelCredentials = '') {
        $Rootdomain = $PanchayatData["defaultDomainName"];
        $Subdomain = $PanchayatData["domain_name"];
        $Newdomain = $PanchayatData["domain_name"];
        
        //https://hostname.example.com:2083/cpsess###########/json-api/cpanel?cpanel_jsonapi_user=user&cpanel_jsonapi_apiversion=2&cpanel_jsonapi_module=AddonDomain&cpanel_jsonapi_func=addaddondomain&dir=addondomain%2Fhome%2Fdir&newdomain=addondomain.com&subdomain=subdomain.example.com
        $Query = "https://kbeesolutions.in:2083/cpsess1808886913/json-api/cpanel?cpanel_jsonapi_user=user&cpanel_jsonapi_apiversion=2&cpanel_jsonapi_module=AddonDomain&cpanel_jsonapi_func=addaddondomain&dir=public_html%2F{$Subdomain}&newdomain={$Newdomain}&subdomain={$Subdomain}";
        
        $curl = curl_init();
        curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, 0);
        curl_setopt($curl, CURLOPT_SSL_VERIFYHOST, 0);
        curl_setopt($curl, CURLOPT_HEADER, 0);
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, 1);

        $user = $PanchayatData["UserNameCredentials"];
        //$token = $PanchayatData["token"];
        $token = $PanchayatData["token"];

        $header[0] = "Authorization: cpanel $user:$token";
        curl_setopt($curl,CURLOPT_HTTPHEADER,$header);
        curl_setopt($curl, CURLOPT_URL, $Query);

        $result = curl_exec($curl);
        if ($result == false) {
            error_log("curl_exec threw error \"" . curl_error($curl) . "\" for $Query");
        }
        curl_close($curl);
        //header('Content-Type: application/json');
        return $result;
    }



//if (!function_exists('cpanel_register_subdomain')) {

    function cpanel_register_subdomain($PanchayatData, $cPanelCredentials = '') {
        $Rootdomain = $PanchayatData["defaultDomainName"];
        $Subdomain = $PanchayatData["domain_name"];

        //$Query = 'https://kbeesolutions.in:2083/cpsess1239112023/json-api/cpanel?cpanel_jsonapi_user=user&cpanel_jsonapi_apiversion=2&cpanel_jsonapi_module=SubDomain&cpanel_jsonapi_func=addsubdomain&domain=' . $Subdomain . '&rootdomain='.$Rootdomain.'&dir=%2Fpublic_html%2F' . $Subdomain . '&disallowdot=1';
        
        $Query = "https://kbeesolutions.in:2083/cpsess1808886913/execute/SubDomain/addsubdomain?domain={$Subdomain}&rootdomain={$Rootdomain}&dir=%2Fpublic_html%2F{$Subdomain}&disallowdot=1";
        $curl = curl_init();
        curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, 0);
        curl_setopt($curl, CURLOPT_SSL_VERIFYHOST, 0);
        curl_setopt($curl, CURLOPT_HEADER, 0);
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, 1);

        $user = $PanchayatData["UserNameCredentials"];
        $token = $PanchayatData["token"];

        $header[0] = "Authorization: cpanel $user:$token";
        curl_setopt($curl,CURLOPT_HTTPHEADER,$header);
        curl_setopt($curl, CURLOPT_URL, $Query);

        //$header[0] = "Authorization: cpanel " . base64_encode($cPanelCredentials) . "\n\r";
        //curl_setopt($curl, CURLOPT_HTTPHEADER, $header);
        //curl_setopt($curl, CURLOPT_URL, $Query);

        $result = curl_exec($curl);
        if ($result == false) {
            error_log("curl_exec threw error \"" . curl_error($curl) . "\" for $Query");
        }
        curl_close($curl);
        //header('Content-Type: application/json');
        return $result;
    }

//}

//if (!function_exists('cpanel_upload_zip_file')) {

    function cpanel_upload_zip_file($PanchayatData, $cPanelCredentials = '', $IsSkipDomain = 0) {
        $query = "https://kbeesolutions.in:2083/execute/Fileman/upload_files";
        $upload_file = realpath($PanchayatData["panchyat_zip"]);
        if (function_exists('curl_file_create')) {
            $cf = curl_file_create($upload_file);
        } else {
            $cf = "@/" . $upload_file;
        }
        if ($IsSkipDomain == 1) {
            $payload = array('dir' => "public_html/", 'file-1' => $cf);
        } else {
            $payload = array('dir' => "public_html/" . $PanchayatData["domain_name"], 'file-1' => $cf);
        }
        $ch = curl_init($query);
        curl_setopt($ch, CURLOPT_HTTPAUTH, CURLAUTH_BASIC);
        curl_setopt($ch, CURLOPT_USERPWD, $cPanelCredentials);
        curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, false);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
        curl_setopt($ch, CURLOPT_POST, true);
        curl_setopt($ch, CURLOPT_POSTFIELDS, $payload);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        $curl_response = curl_exec($ch);
        if ($curl_response == false) {
            error_log("curl_exec threw error \"" . curl_error($ch) . "\" for $query");
        }
        curl_close($ch);
        ////header('Content-Type: application/json');
        return $curl_response;
    }

//}

//if (!function_exists('cpanel_create_db')) {

    function cpanel_create_db($PanchayatData, $cPanelCredentials = '', $IsSubdomainCase = 1) {

        if(isset($PanchayatData['hosting_info']['store_mysql_dbname']) && $PanchayatData['hosting_info']['store_mysql_dbname'] != '') {
            $dbname = $PanchayatData['hosting_info']['store_mysql_dbname'];
        }else {
            if ($IsSubdomainCase == 1) {
                $dbname = "kbeesolution_db" . $PanchayatData["panchyat_id"];
            } else {
                $dbname = $PanchayatData["cpanel_username"] . "_" . mt_rand(111111, 999999);
            }
        }
        
        $query = "https://kbeesolutions.in:2083/cpsess1808886913/execute/Mysql/create_database?name=" . $dbname;
        
        
        $curl = curl_init();
        curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, 0);
        curl_setopt($curl, CURLOPT_SSL_VERIFYHOST, 0);
        curl_setopt($curl, CURLOPT_HEADER, 0);
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, 1);

        $user = $PanchayatData["UserNameCredentials"];
        $token = $PanchayatData["token"];

        $header[0] = "Authorization: cpanel $user:$token";
        curl_setopt($curl,CURLOPT_HTTPHEADER,$header);
        curl_setopt($curl, CURLOPT_URL, $query);

        //$header[0] = "Authorization: cpanel " . base64_encode($cPanelCredentials) . "\n\r";
        //curl_setopt($curl, CURLOPT_HTTPHEADER, $header);
        //curl_setopt($curl, CURLOPT_URL, $query);
        $result = curl_exec($curl);
        if ($result == false) {
            error_log("curl_exec threw error \"" . curl_error($curl) . "\" for $query");
        }
        curl_close($curl);
        //header('Content-Type: application/json');
        return ["service_response" => $result, "dbname" => $dbname];
    }

//}

//if (!function_exists('cpanel_create_db_user')) {

    function cpanel_create_db_user($PanchayatData, $cPanelCredentials = '', $IsSubdomainCase = 1) {
        if(isset($PanchayatData['hosting_info']['store_mysql_dbusername']) && $PanchayatData['hosting_info']['store_mysql_dbusername'] != '') {
            $dbusername = $PanchayatData['hosting_info']['store_mysql_dbusername'];
            $dbpassword = $PanchayatData['hosting_info']['store_mysql_dbpassword'];
        }else {
            if ($IsSubdomainCase == 1) {
                $dbusername = substr("kbeesolution_" . $PanchayatData["store_id"] , 0, 16);
            } else {
                $dbusername = substr($PanchayatData["cpanel_username"] . "_" . "user", 0, 12);
            }

            $dbpassword = uniqid() . mt_rand(111, 999);
     
        }
        

        //$query = 'https://kbeesolutions.in:2083/cpsess1239112023/json-api/cpanel?cpanel_jsonapi_user=user&cpanel_jsonapi_apiversion=2&cpanel_jsonapi_module=MysqlFE&cpanel_jsonapi_func=createdbuser&dbuser=' . $dbusername . '&password=' . $dbpassword;
        
        $query = "https://kbeesolutions.in:2083/cpsess1808886913/execute/Mysql/create_user?name=" . $dbusername . "&password=" . $dbpassword;
        
        $curl = curl_init();
        curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, 0);
        curl_setopt($curl, CURLOPT_SSL_VERIFYHOST, 0);
        curl_setopt($curl, CURLOPT_HEADER, 0);
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, 1);
        $user = $PanchayatData["UserNameCredentials"];
        $token = $PanchayatData["token"];
        $header[0] = "Authorization: cpanel $user:$token";
        curl_setopt($curl,CURLOPT_HTTPHEADER,$header);
        curl_setopt($curl, CURLOPT_URL, $query);

        //$header[0] = "Authorization: cpanel " . base64_encode($cPanelCredentials) . "\n\r";
        //curl_setopt($curl, CURLOPT_HTTPHEADER, $header);
        //curl_setopt($curl, CURLOPT_URL, $query);

        $result = curl_exec($curl);
        if ($result == false) {
            error_log("curl_exec threw error \"" . curl_error($curl) . "\" for $query");
        }
        curl_close($curl);
        //header('Content-Type: application/json');
        return ["service_response" => $result, "dbusername" => $dbusername, "dbpassword" => $dbpassword];
    }

//}

//if (!function_exists('cpanel_assign_privilege_db')) {

    function cpanel_assign_privilege_db($dbname, $dbusername, $cPanelCredentials = '') {
        //$query = 'https://kbeesolutions.in:2083/cpsess1239112023/json-api/cpanel?cpanel_jsonapi_user=user&cpanel_jsonapi_apiversion=2&cpanel_jsonapi_module=MysqlFE&cpanel_jsonapi_func=setdbuserprivileges&db='.$dbname . '&dbuser=' . $dbusername . '&privileges=UPDATE%2CALTER';
        
        $query = "https://kbeesolutions.in:2083/cpsess1808886913/execute/Mysql/set_privileges_on_database?user=$dbusername&database=$dbname&privileges=ALL PRIVILEGES";    
        $query = urldecode($query);
        $curl = curl_init();
        curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, 0);
        curl_setopt($curl, CURLOPT_SSL_VERIFYHOST, 0);
        curl_setopt($curl, CURLOPT_HEADER, 0);
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, 1);

        $user = $PanchayatData["UserNameCredentials"];
        $token = $PanchayatData["token"];

        $header[0] = "Authorization: cpanel $user:$token";
        curl_setopt($curl,CURLOPT_HTTPHEADER,$header);
        curl_setopt($curl, CURLOPT_URL, $query);

        //$header[0] = "Authorization: cpanel " . base64_encode($cPanelCredentials) . "\n\r";
        //curl_setopt($curl, CURLOPT_HTTPHEADER, $header);
        //curl_setopt($curl, CURLOPT_URL, $query);
        $result = curl_exec($curl);
        if ($result == false) {
            error_log("curl_exec threw error \"" . curl_error($curl) . "\" for $query");
        }
        curl_close($curl);
        //header('Content-Type: application/json');
        return $result;
    }

//}


//if (!function_exists('cpanel_upload_custom_file')) {

    function cpanel_upload_custom_file($ToUploadFile, $DomainName = '', $cPanelCredentials = '', $IsSkipDomain = 0) {
        $query = "https://kbeesolutions.in:2083/execute/Fileman/upload_files";
        $upload_file = realpath($ToUploadFile);
        if (function_exists('curl_file_create')) {
            $cf = curl_file_create($upload_file);
        } else {
            $cf = "@/" . $upload_file;
        }
        if ($IsSkipDomain == 1) {
            $payload = array('dir' => "public_html/", 'file-1' => $cf);
        } else {
            $payload = array('dir' => "public_html/" . $DomainName, 'file-1' => $cf);
        }
        $ch = curl_init($query);
        curl_setopt($ch, CURLOPT_HTTPAUTH, CURLAUTH_BASIC);
        curl_setopt($ch, CURLOPT_USERPWD, $cPanelCredentials);
        curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, false);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
        curl_setopt($ch, CURLOPT_POST, true);
        curl_setopt($ch, CURLOPT_POSTFIELDS, $payload);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        $curl_response = curl_exec($ch);
        if ($curl_response == false) {
            error_log("curl_exec threw error \"" . curl_error($ch) . "\" for $query");
        }
        curl_close($ch);
        //header('Content-Type: application/json');
        return $curl_response;
    }

//}

//if (!function_exists('whm_create_cpanel_account')) {

    function whm_create_cpanel_account($PanchayatData, $Credentials = "") {
        $WHMParam = [];
        $Url = explode("&", "username=&domain=&bwlimit=unlimited&cgi=1&contactemail=&cpmod=paper_lantern&customip=&dkim=1&featurelist=feature_list&forcedns=0&frontpage=0&gid=&hasshell=1&hasuseregns=1&homedir=&ip=n&language=en&owner=pstlservices&max_defer_fail_percentage=0&max_email_per_hour=0&max_emailacct_quota=1024&maxaddon=0&maxftp=1&maxlst=0&maxpark=0&maxpop=0&maxsql=1&maxsub=0&mxcheck=local&owner=pstlservices&password=&pkgname=0&plan=pstlservices_basic_pack&quota=500&reseller=0&savepkg=0&spamassassin=1&spf=1&spambox=y&uid=&useregns=0");
        foreach ($Url as $value) {
            $value = explode("=", $value);
            $WHMParam[$value[0]] = $value[1];
        }
        $WHMParam["username"] = $PanchayatData["cpanel_username"];
        $WHMParam["domain"] = $PanchayatData["domain_name"];
        $WHMParam["contactemail"] = $PanchayatData["email"];
        $WHMParam["password"] = uniqid() . mt_rand(111, 999);
        $query = "http://kbeesolutions.in:2083/json-api/createacct?api.version=1&" . http_build_query($WHMParam);
        $curl = curl_init();
        curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, 0);
        curl_setopt($curl, CURLOPT_SSL_VERIFYHOST, 0);
        curl_setopt($curl, CURLOPT_HEADER, 0);
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, 1);
        $header[0] = "Authorization: Basic " . base64_encode($Credentials) . "\n\r";
        curl_setopt($curl, CURLOPT_HTTPHEADER, $header);
        curl_setopt($curl, CURLOPT_URL, $query);
        $result = curl_exec($curl);
        if ($result == false) {
            error_log("curl_exec threw error \"" . curl_error($curl) . "\" for $query");
        }
        curl_close($curl);
        //header('Content-Type: application/json');
        return [
            "service_response" => $result,
            "whm_credentials" => $WHMParam
        ];
    }

//}

//if (!function_exists('cpanel_webmail_create_account')) {

    function cpanel_webmail_create_account($emailCred = [], $Credentials = "") {
        $query = "http://kbeesolutions.in:2083/execute/Email/add_pop?email={$emailCred["username"]}&password={$emailCred["password"]}{O&quota=1024&domain={$emailCred["domain"]}&skip_update_db=1&";
        $curl = curl_init();
        curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, 0);
        curl_setopt($curl, CURLOPT_SSL_VERIFYHOST, 0);
        curl_setopt($curl, CURLOPT_HEADER, 0);
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, 1);
        $header[0] = "Authorization: Basic " . base64_encode($Credentials) . "\n\r";
        curl_setopt($curl, CURLOPT_HTTPHEADER, $header);
        curl_setopt($curl, CURLOPT_URL, $query);
        $result = curl_exec($curl);
        if ($result == false) {
            error_log("curl_exec threw error \"" . curl_error($curl) . "\" for $query");
        }
        curl_close($curl);
        //header('Content-Type: application/json');
        return [
            "service_response" => $result,
            "email_credentails" => $emailCred
        ];
    }

//}

//if (!function_exists('cpanel_webmail_list_accounts')) {

    function cpanel_webmail_list_accounts($Credentials = "") {
        $query = "http://kbeesolutions.in:2083/execute/Email/list_pops_with_disk?domain=kbeesolutions.in";
        //execute/Email/list_pops_with_disk?domain=&maxaccounts=&no_validate=&get_restrictions=&regex=&email=&infinitylang=
        $curl = curl_init();
        curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, 0);
        curl_setopt($curl, CURLOPT_SSL_VERIFYHOST, 0);
        curl_setopt($curl, CURLOPT_HEADER, 0);
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, 1);

        $header[0] = "Authorization: whm $user:$token";
        curl_setopt($curl,CURLOPT_HTTPHEADER,$header);
        curl_setopt($curl, CURLOPT_URL, $query);

        $header[0] = "Authorization: Basic " . base64_encode($Credentials) . "\n\r";
        curl_setopt($curl, CURLOPT_HTTPHEADER, $header);
        curl_setopt($curl, CURLOPT_URL, $query);
        $result = curl_exec($curl);
        /*   if ($result == false) {
          error_log("curl_exec threw error \"" . curl_error($curl) . "\" for $query");
          }
          curl_close($curl);
          //header('Content-Type: application/json'); */
        return ["service_response" => $result];
        //return ["service_response" => '{"data":[{"login":"admin@kbeesolutions.in","suspended_incoming":0,"suspended_login":0,"email":"admin@kbeesolutions.in"},{"email":"geetanjali_dimension@kbeesolutions.in","suspended_login":0,"suspended_incoming":0,"login":"geetanjali_dimension@kbeesolutions.in"},{"suspended_incoming":0,"suspended_login":0,"email":"kbeesolutions","login":"Main Account"}],"messages":null,"errors":null,"metadata":{"transformed":1},"warnings":null,"status":1}'];
    }

//}


//if (!function_exists('cpanel_webmail_delete_account')) {

    function cpanel_webmail_delete_account($emailAccount = [], $Credentials = "") {
        $query = "http://kbeesolutions.in:2083/execute/Email/delete_pop?email={$emailAccount["email"]}&domain={$emailAccount["domain"]}";
        $curl = curl_init();
        curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, 0);
        curl_setopt($curl, CURLOPT_SSL_VERIFYHOST, 0);
        curl_setopt($curl, CURLOPT_HEADER, 0);
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, 1);
        $header[0] = "Authorization: Basic " . base64_encode($Credentials) . "\n\r";
        curl_setopt($curl, CURLOPT_HTTPHEADER, $header);
        curl_setopt($curl, CURLOPT_URL, $query);
        $result = curl_exec($curl);
        if ($result == false) {
            error_log("curl_exec threw error \"" . curl_error($curl) . "\" for $query");
        }
        curl_close($curl);
        //header('Content-Type: application/json');
        return ["service_response" => $result];
    }

//}

//if (!function_exists('cpanel_webmail_dispatch_settings')) {

    function cpanel_webmail_dispatch_settings($emailAccount = [], $Credentials = "") {
        $query = "http://kbeesolutions.in:2083/execute/Email/dispatch_client_settings?to={$emailAccount["send_mail_to"]}&account={$emailAccount["send_mail_for"]}";
        $curl = curl_init();
        curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, 0);
        curl_setopt($curl, CURLOPT_SSL_VERIFYHOST, 0);
        curl_setopt($curl, CURLOPT_HEADER, 0);
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, 1);
        $header[0] = "Authorization: Basic " . base64_encode($Credentials) . "\n\r";
        curl_setopt($curl, CURLOPT_HTTPHEADER, $header);
        curl_setopt($curl, CURLOPT_URL, $query);
        $result = curl_exec($curl);
        if ($result == false) {
            error_log("curl_exec threw error \"" . curl_error($curl) . "\" for $query");
        }
        curl_close($curl);
        ////header('Content-Type: application/json');
        return ["service_response" => $result];
    }

//}

//if (!function_exists('cpanel_webmail_change_password')) {

    function cpanel_webmail_change_password($emailAccount = [], $Credentials = "") {
        $query = "http://kbeesolutions.in:2083/execute/Email/passwd_pop?email={$emailAccount["email"]}&password={$emailAccount["new_password"]}&domain={$emailAccount["domain"]}";
        $curl = curl_init();
        curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, 0);
        curl_setopt($curl, CURLOPT_SSL_VERIFYHOST, 0);
        curl_setopt($curl, CURLOPT_HEADER, 0);
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, 1);
        $header[0] = "Authorization: Basic " . base64_encode($Credentials) . "\n\r";
        curl_setopt($curl, CURLOPT_HTTPHEADER, $header);
        curl_setopt($curl, CURLOPT_URL, $query);
        $result = curl_exec($curl);
        if ($result == false) {
            error_log("curl_exec threw error \"" . curl_error($curl) . "\" for $query");
        }
        curl_close($curl);
        ////header('Content-Type: application/json');
        return ["service_response" => $result];
    }

//}

//if (!function_exists('cpanel_webmail_client_setting')) {

    function cpanel_webmail_client_setting($emailAccount = [], $Credentials = "") {
        $query = "http://kbeesolutions.in:2083/execute/Email/get_client_settings?account=" . $emailAccount["email"];
        $curl = curl_init();
        curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, 0);
        curl_setopt($curl, CURLOPT_SSL_VERIFYHOST, 0);
        curl_setopt($curl, CURLOPT_HEADER, 0);
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, 1);
        $header[0] = "Authorization: Basic " . base64_encode($Credentials) . "\n\r";
        curl_setopt($curl, CURLOPT_HTTPHEADER, $header);
        curl_setopt($curl, CURLOPT_URL, $query);
        $result = curl_exec($curl);
        if ($result == false) {
            error_log("curl_exec threw error \"" . curl_error($curl) . "\" for $query");
        }
        curl_close($curl);
        ////header('Content-Type: application/json');
        return ["service_response" => $result];
    }

//}

//if (!function_exists('whm_webmail_email_autologin')) {

    function whm_webmail_email_autologin($emailAccount = [], $Credentials = "") {
        $query = "http://kbeesolutions.in:2083/json-api/create_user_session?api.version=1&user={$emailAccount["account"]}&service=webmaild&locale=en&app=Email_Accounts&preferred_domain={$emailAccount["domain"]}";
        $curl = curl_init();
        curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, 0);
        curl_setopt($curl, CURLOPT_SSL_VERIFYHOST, 0);
        curl_setopt($curl, CURLOPT_HEADER, 0);
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, 1);
        $header[0] = "Authorization: Basic " . base64_encode($Credentials) . "\n\r";
        curl_setopt($curl, CURLOPT_HTTPHEADER, $header);
        curl_setopt($curl, CURLOPT_URL, $query);
        $result = curl_exec($curl);
        if ($result == false) {
            error_log("curl_exec threw error \"" . curl_error($curl) . "\" for $query");
        }
        curl_close($curl);
        //header('Content-Type: application/json');
        return ["service_response" => $result];
    }

//}

//if (!function_exists('whm_cpanel_suspend_account')) {

    function whm_cpanel_suspend_account($AccountArray = [], $Credentials = "") {
        $query = "http://kbeesolutions.in:2083/json-api/suspendacct?api.version=1&user={$AccountArray["username"]}&reason=PolicyViolations";
        $curl = curl_init();
        curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, 0);
        curl_setopt($curl, CURLOPT_SSL_VERIFYHOST, 0);
        curl_setopt($curl, CURLOPT_HEADER, 0);
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, 1);
        $header[0] = "Authorization: Basic " . base64_encode($Credentials) . "\n\r";
        curl_setopt($curl, CURLOPT_HTTPHEADER, $header);
        curl_setopt($curl, CURLOPT_URL, $query);
        $result = curl_exec($curl);
        if ($result == false) {
            error_log("curl_exec threw error \"" . curl_error($curl) . "\" for $query");
        }
        curl_close($curl);
        ////header('Content-Type: application/json');
        return ["service_response" => $result];
    }

//}

//if (!function_exists('whm_cpanel_restore_account')) {

    function whm_cpanel_restore_account($AccountArray = [], $Credentials = "") {
        $query = "http://kbeesolutions.in:2083/json-api/unsuspendacct?api.version=1&user=" . $AccountArray["username"];
        $curl = curl_init();
        curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, 0);
        curl_setopt($curl, CURLOPT_SSL_VERIFYHOST, 0);
        curl_setopt($curl, CURLOPT_HEADER, 0);
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, 1);
        $header[0] = "Authorization: Basic " . base64_encode($Credentials) . "\n\r";
        curl_setopt($curl, CURLOPT_HTTPHEADER, $header);
        curl_setopt($curl, CURLOPT_URL, $query);
        $result = curl_exec($curl);
        if ($result == false) {
            error_log("curl_exec threw error \"" . curl_error($curl) . "\" for $query");
        }
        curl_close($curl);
        ////header('Content-Type: application/json');
        return ["service_response" => $result];
    }

//}

//if (!function_exists('whm_cpanel_account_summary')) {

    function whm_cpanel_account_summary($AccountArray = [], $Credentials = "") {
        $query = "http://kbeesolutions.in:2083/json-api/accountsummary?api.version=1&user=" . $AccountArray["username"];
        $curl = curl_init();
        curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, 0);
        curl_setopt($curl, CURLOPT_SSL_VERIFYHOST, 0);
        curl_setopt($curl, CURLOPT_HEADER, 0);
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, 1);
        $header[0] = "Authorization: Basic " . base64_encode($Credentials) . "\n\r";
        curl_setopt($curl, CURLOPT_HTTPHEADER, $header);
        curl_setopt($curl, CURLOPT_URL, $query);
        $result = curl_exec($curl);
        if ($result == false) {
            error_log("curl_exec threw error \"" . curl_error($curl) . "\" for $query");
        }
        curl_close($curl);
        ////header('Content-Type: application/json');
        return ["service_response" => $result];
    }

//}

//if (!function_exists('whm_list_cpanel_account')) {

    function whm_list_cpanel_account($AccountArray = [], $Credentials = "") {
        $query = "http://kbeesolutions.in:2083/json-api/listaccts?api.version=1&search=&searchtype=user";
        $curl = curl_init();
        curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, 0);
        curl_setopt($curl, CURLOPT_SSL_VERIFYHOST, 0);
        curl_setopt($curl, CURLOPT_HEADER, 0);
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, 1);
        $header[0] = "Authorization: cpanel " . base64_encode($Credentials) . "\n\r";
        curl_setopt($curl, CURLOPT_HTTPHEADER, $header);
        curl_setopt($curl, CURLOPT_URL, $query);
        $result = curl_exec($curl);
        if ($result == false) {
            error_log("curl_exec threw error \"" . curl_error($curl) . "\" for $query");
        }
        curl_close($curl);
        ////header('Content-Type: application/json');
        return ["service_response" => $result];
    }

//}

//if (!function_exists('cpanel_list_subdomain')) {

    function cpanel_list_subdomain($AccountArray = [], $Credentials = "") {
        $query = "http://kbeesolutions.in:2083/execute/DomainInfo/list_domains";
        $curl = curl_init();
        curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, 0);
        curl_setopt($curl, CURLOPT_SSL_VERIFYHOST, 0);
        curl_setopt($curl, CURLOPT_HEADER, 0);
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, 1);
        $header[0] = "Authorization: Basic " . base64_encode($Credentials) . "\n\r";
        curl_setopt($curl, CURLOPT_HTTPHEADER, $header);
        curl_setopt($curl, CURLOPT_URL, $query);
        $result = curl_exec($curl);
        if ($result == false) {
            error_log("curl_exec threw error \"" . curl_error($curl) . "\" for $query");
        }
        curl_close($curl);
        ////header('Content-Type: application/json');
        return ["service_response" => $result];
    }

//}


function curl_get($url, $fields) {
    $ch = curl_init();
    
    print "<br/>";
    print $url = $url . "?" . $fields;
    curl_setopt($ch, CURLOPT_URL, $url);
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
    $output = curl_exec($ch);
    curl_close($ch);
    return $output;

}


function curl_post($url, $fields_string, $field_count) {
    
    rtrim($fields_string, '&');
    
    $ch = curl_init();
    curl_setopt($ch,CURLOPT_URL, $url);
    curl_setopt($ch,CURLOPT_POST, $field_count);
    curl_setopt($ch,CURLOPT_POSTFIELDS, $fields_string);
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
    //execute post
    $result = curl_exec($ch);
    
    //close connection
    curl_close($ch);
    
    return $result;
        
}

/*
if (!function_exists('whm_cpanel_account_terminate')) {

    function whm_cpanel_account_terminate($AccountArray = [], $Credentials = "") {
        ///json-api/removeacct?user=username
    }

}*/


function addondomain($host, $ownername, $passw, $domain, $tld) {
	$dir = "public_html/". trim($domain);
	//the user is the first element of the $domain, as requested
	$user = trim($domain);
	//put the domain back together and trim whitespace.
    $dom = trim($domain). " ." . trim($tld);
    $addonpass = "luQman786!@#";
    
	//create the cpanel request.
	$request = "/cpsess1808886913/frontend/paper_lantern/addon/doadddomain.html?domain=$dom&user=$user&dir=$dir&pass=$addonpass";
	//process the request with addondomain below
	//open a connection
	$sock = @fsockopen($host,2082);
	if(!$sock) {
		print('Socket error');
		exit();
	}
	//authenticate the connection
	$authstr = "$ownername:$passw";
	//make the passphrase slightly more difficult to decipher
	$pass = base64_encode($authstr);
	$in = "GET $request\r\n";
	$in .= "HTTP/1.0\r\n";
	$in .= "Host:$host\r\n";
	$in .= "Authorization: Basic $pass\r\n";
	$in .= "\r\n";
	//process
	fputs($sock, $in);
	while (!feof($sock)) {
		$result .= fgets ($sock,128);
	}
	fclose( $sock );
	return $result;
}
