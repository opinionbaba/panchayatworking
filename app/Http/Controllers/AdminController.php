<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Admin;
use App\Temdocument;
use App\Notice;
use App\Contact;
use App\Subscriber;
use App\Sub_category;
use App\List_account;
use App\Main_category;
use App\Citizen;
use App\Asset;
use App\Panchyat;
use App\Panchayat_user;
use App\Citizen_bank_detail;
use App\Citizen_document;
use App\Citizen_feedback;
use App\Ticket_reply;
use App\Role;
use App\Validator;
use App\Ticket;
use App\User;
use App\Enabled_widget;
use App\Panc_home_page;
use Auth;
Use Alert;
use DB;
use App\Event;
class AdminController extends Controller
{

	public function index()
	{
		return view('index');
	}

//For Audit Functionality
	
    public function allaudit()
	{
		return view('admin.all-audit');
	}
	 public function addaudit()
	{
	
		return view('admin.add-audit');
	}
	
//For Document Functionality
		
	public function save_document(Request $request)
	{ 
		$form_name = $request->input('name');
		$date= date('y-m-d');
		if ($request->hasFile('doc')) {
			$image = $request->file('doc');
			$name = time().'.'.$image->getClientOriginalExtension();
			$destinationPath = public_path('upload/form_temp/');
			$image->move($destinationPath, $name);
			
			Temdocument::Create([
			'doc_name' => $form_name,
			'file' => $name,
			'base_path' => $destinationPath,
			'created_at' => $date
		]);
			return redirect()->back()->with('success','Your From Upload Successfully...');
		  }
	}
	
     public function deletedoc($id)
		{
			$data=Temdocument::where('id',$id)->delete();
			if($data)
			{
				return redirect()->back()->with('delete','Deleted Successfully!');
			}else
			{
				return redirect()->back()->with('failure','Not Deleted!');
			}
		}
    public function alldocument()
	{
		$data['all'] = Temdocument::all();
		return view('admin.all-doc',$data);
	}
	public function add_document()
	{
	
		return view('admin.add-doc');
	}

//For Gallery Functionality
		
    public function allgallery()
	{
		return view('admin.all-gallery');
	}
	public function add_gallery()
	{
	
		return view('admin.add-gallery');
	}
	
		
//For Event Functionality
	public function save_event(Request $request)
	{
		$event_name = $request->input('event_name');
		$event_cat = $request->input('event_cat');
		$date= date('y-m-d');
		if ($request->hasFile('event_file')) {
			$image = $request->file('event_file');
			$name = time().'.'.$image->getClientOriginalExtension();
			$destinationPath = public_path('upload/event/');
			$image->move($destinationPath, $name);
			
			Event::Create([
			'event_name' => $event_name,
			'file' => $name,
			'base_path' => $destinationPath,
			'event_cat' => $event_cat,
			'created_at' => $date
		]);
			return redirect()->back()->with('success','Your From Upload Successfully...');
		  }
	}
	
	public function edit_event(Request $request)
	{
		// print_r($_POST);
		// die;
		$newname = $request->input('name');
		$id = $request->input('id');
		//$update_cat = $request->input('update_cat');
		$date= date('y-m-d');
		if ($request->hasFile('doc'))
			{
				$image = $request->file('doc');
				$name = time().'.'.$image->getClientOriginalExtension();
				$destinationPath = public_path('upload/event/');
				$image->move($destinationPath, $name);
				
				Event::where('id', $id)->update(['event_name' => $newname,'file' => $name,'base_path' => $destinationPath,'created_at' => $date,'updated_at'=>$date]);
				
			}else
			{
			 	Event::where('id', $id)->update(['event_name' => $newname,'created_at' => $date,'updated_at'=>$date]);
		    }
		  	return redirect()->back()->with('success','Your Event Update Successfully...');
	}
    public function allevent()
	{
		$data['event'] = Event::all();
		return view('admin.list-group',$data);
	}
	public function full_event($id)
	{
		$data['parti_event'] = Event::where('event_cat',$id)->get();
		// print_r($data['parti_event']);
		// die;
		return view('admin.all-event',$data);
	}
	public function add_event()
	{
		return view('admin.add-event');
	}
	
  public function delete_event($id)
	{
		$data= Event ::where('id',$id)->delete();
		if($data)
		{
			return redirect()->back()->with('delete','Deleted Successfully!');
		}else
		{
			return redirect()->back()->with('failure','Not Deleted!');
		}
	}
	
//For Notice Functionality
		public function save_notice(Request $request)
		{
		 $cat_id = $request->input('cat_id');
		 $notice = $request->input('notice');
		 $editor1 = $request->input('editor1');
		 $date= date('y-m-d');
		
		if ($request->hasFile('image'))
			{
				$image = $request->file('image');
				$name = time().'.'.$image->getClientOriginalExtension();
				$destinationPath = public_path('upload/notice/');
				$image->move($destinationPath, $name);
				
			Notice::Create([
			'cat_id' => $cat_id,
			'notice' => $notice,
			'description' => $editor1,
			'file' => $name,
			'created_at' => $date,
			'updated_at' => $date
				]);
				
				return redirect()->back()->with('success','Your Notice Add Successfully...');
			}
		}
    public function allnotice()
	{
		$data['all'] = Notice::all();
		return view('admin.notice_cat',$data);
	}
	public function add_notice()
	{
		return view('admin.add-notice');
	}
	
	public function parti_notice($id)
	{
		$data['single'] = Notice::where('cat_id',$id)->get();
		return view ('admin.all-notice',$data);
	}
	
	public function delete_notice($id)
	{
		$data= Notice ::where('id',$id)->delete();
		if($data)
		{
			return redirect()->back()->with('delete','Deleted Successfully!');
		}else
		{
			return redirect()->back()->with('failure','Not Deleted!');
		}	
	}
	
	public function edit_notice(Request $request)
	{
		$notice_name = $request->input('notice_name');
		$id = $request->input('id');
		$editor1 = $request->input('editor1');
		$date= date('y-m-d');
		if ($request->hasFile('doc'))
			{
				$image = $request->file('doc');
				$name = time().'.'.$image->getClientOriginalExtension();
				$destinationPath = public_path('upload/notice/');
				$image->move($destinationPath, $name);
				
				Notice::where('id', $id)->update(['notice' => $notice_name,'description'=>$editor1,'file' => $name,'base_path' => $destinationPath,'created_at' => $date,'updated_at'=>$date]);
				
			}else
			{
			 	Notice::where('id', $id)->update(['notice' => $notice_name,'description'=>$editor1,'created_at' => $date,'updated_at'=>$date]);
		    }
		  	return redirect()->back()->with('success','Your Notice Update Successfully...');
	}
//Contact and subscribe Functionality
	
	public function contact_us()
	{
		$data['contact'] = Contact::all();
		return view('admin.contact',$data);
	}
	public function contact_delete($id)
	{
		$data= Contact ::where('id',$id)->delete();
		if($data)
		{
			return redirect()->back()->with('delete','Deleted Successfully!');
		}else
		{
			return redirect()->back()->with('failure','Not Deleted!');
		}	
	}
	public function subscriber()
	{
		
		$data['all'] = Subscriber::all();
		
		return view('admin.subscribe',$data);
	}
	
	public function subs_delete($id)
	{
		$data= Subscriber ::where('id',$id)->delete();
		if($data)
		{
			return redirect()->back()->with('delete','Deleted Successfully!');
		}else
		{
			return redirect()->back()->with('failure','Not Deleted!');
		}	
	}

//All Category Functions

	public function maincat()
	{
		$data['all'] = Main_category::all();
		return view('admin.category',$data);
	}
	
	public function add_maincat(Request $request)
	{
		$name = $request->input('name');
		$date = date('y-m-d');
		if(!empty($name))
		{
			Main_category::Create([
			'name' => $name,
			'created_at' => $date,
			'updated_at' => $date
				]);
				
				return redirect()->back()->with('success','Main Category Add Successfully...');
		}
	}
	
	// public function all_maincat()
	// {
		// $data['all'] = Main_category::all();
		// return view('admin.category',$data);
	// }
	public function sub_cat($id)
	{
		$data['id'] = $id;
		$data['all'] = Sub_category::where('main_cat_id',$id)->get();
		return view('admin.sub_category',$data);
		
	}
	
	public function add_sub_cat(Request $request )
	{
		$sub_name = $request->input('sub_name');
		$id = $request->input('id');
		$date = date('y-m-d');
		if(!empty($sub_name))
		{
			Sub_category::Create([
			'name' => $sub_name,
			'main_cat_id' => $id,
			'created_at' => $date,
			'updated_at' => $date
				]);
				
				return redirect()->back()->with('success','Sub Category Add Successfully...');
		}
		
	}
	
	public function delete_main_cat($id)
	{
		$data = Main_category::where('id',$id)->delete();
		if($data)
		{
			return redirect()->back()->with('delete','Main Category Delete Successfully...');
		}else{
			return redirect()->back()->with('failure','Not Deleted!');
		}
	}
	
	public function delete_sub_cat($id)
	{
		$data = Sub_category::where('id',$id)->delete();
		if($data)
		{
			return redirect()->back()->with('delete','Sub Category Delete Successfully...');
		}else{
			return redirect()->back()->with('failure','Not Deleted!');
		}
	}
	
//Panchyat Function start here
	public function add_account()
	{
 		$data['all'] = Panchyat::all();
		return view('admin.all-account',$data);
	}
	public function add_panchayat(Request $request)
	{
		$panc_name = $request->input('panc_name');
		$panc_number = $request->input('panc_number');
		$panc_address = $request->input('panc_address');
		$panc_email = $request->input('panc_email');
		$panc_domain = $request->input('panc_domain');
		$panc_lat = $request->input('panc_lat');
		$panc_long = $request->input('panc_long');
		$date = date('y-m-d');
		if ($request->hasFile('panc_logo'))
			{
				$image = $request->file('panc_logo');
				$panc_logo = time().'.'.$image->getClientOriginalExtension();
				$destinationPath = public_path('/upload/panchayat/panchayat1/logo/');
				$image->move($destinationPath, $panc_logo);
				
			}else{
				$panc_logo="None";
			}
			if ($request->hasFile('panc_app_logo'))
			{
				$image = $request->file('panc_app_logo');
				$panc_app_logo = time().'.'.$image->getClientOriginalExtension();
				$destinationPath = public_path('upload/panchayat/panchayat1/logo/');
				$image->move($destinationPath, $panc_app_logo);
				
			}else{
				$panc_app_logo="None";
			}
			
			$panchayat = Panchyat::Create([
			'panc_name' => $panc_name,
			'panc_number' => $panc_number,
			'panc_address' => $panc_address,
			'panc_email' => $panc_email,
			'panc_domain' => $panc_domain,
			'panc_lat' => $panc_lat,
			'panc_long' => $panc_long,
			'panc_logo' => $panc_logo,
			'panc_app_logo' => $panc_app_logo,
			'created_at' => $date,
			'updated_at' => $date
				]);
				if($panchayat)
				{
					return redirect()->back()->with('add','Panchyat Add Successfully...');
				}else{
					return redirect()->back()->with('failure','Not Addedd!');
				}
		// print_r($_POST);
		// die;
	}
	
	public function show_panchayat($id)
	{
		$data = array();
		$data['all'] = Panchyat::where('id',$id)->get();
		$data['Panchayat_user'] = Panchayat_user::where('pn_id',$id)->get();
		$data['staff'] = Panchayat_user::where('pn_id',$id)->get();
		
		return view('admin.view-panchayat',$data);
	}
	
	public function edit_panchayat(Request $request)
	{
		$panc_name = $request->input('panc_name');
		$panc_number = $request->input('panc_number');
		$panc_address = $request->input('panc_address');
		$panc_email = $request->input('panc_email');
		$panc_domain = $request->input('panc_domain');
		$panc_lat = $request->input('panc_lat');
		$panc_long = $request->input('panc_long');
		$date = date('y-m-d');
		if ($request->hasFile('panc_logo'))
			{
				$image = $request->file('panc_logo');
				$panc_logo = time().'.'.$image->getClientOriginalExtension();
				$destinationPath = public_path('/upload/panchayat/panchayat1/logo/');
				$image->move($destinationPath, $panc_logo);
			}
			if ($request->hasFile('panc_app_logo'))
			{
				$image = $request->file('panc_app_logo');
				$panc_app_logo = time().'.'.$image->getClientOriginalExtension();
				$destinationPath = public_path('upload/panchayat/panchayat1/logo/');
				$image->move($destinationPath, $panc_app_logo);
			}
			if(!empty($panc_logo))
			{
				
				
			}
			
			$panchayat = Panchyat::Create([
			'panc_name' => $panc_name,
			'panc_number' => $panc_number,
			'panc_address' => $panc_address,
			'panc_email' => $panc_email,
			'panc_domain' => $panc_domain,
			'panc_lat' => $panc_lat,
			'panc_long' => $panc_long,
			'panc_logo' => $panc_logo,
			'panc_app_logo' => $panc_app_logo,
			'created_at' => $date,
			'updated_at' => $date
				]);
				if($panchayat)
				{
					return redirect()->back()->with('add','Panchyat Add Successfully...');
				}else{
					return redirect()->back()->with('failure','Not Addedd!');
				}
	}
	
	public function delete_panchayat($id)
	{
		$data = Panchyat::where('id',$id)->delete();
			if($data)
			{
				return redirect()->back()->with('delete','Deleted Successfully!');
			}else
			{
				return redirect()->back()->with('failure','Not Deleted!');
			}
	}
	
//Users Function start here
	public function users()
	{
		$data['panchyat'] = Panchyat:: all();
		$data['role'] = Role:: all();
		$data['Panchayat_user'] = DB::table('panchayat_user')->select('panchayat_user.*', 			'panchyat.panc_name')->join('panchyat','panchyat.id','=','panchayat_user.pn_id')->get();
						
		return view('admin.all-user',$data);
	}
	public function add_user(Request $request)
	{
		$name=$request->input('name');
		$mobile=$request->input('mobile');
		$email=$request->input('email');
		$role_id=$request->input('role_id');
		$ward_number=$request->input('ward_number');
		$pn_id=$request->input('pn_id');
		$date = date('y-m-d');
			if ($request->hasFile('profile'))
			{
				$image = $request->file('profile');
				$pro_logo = time().'.'.$image->getClientOriginalExtension();
				$destinationPath = public_path('/upload/panchayat/panchayat1/role/LDC/');
				$image->move($destinationPath, $pro_logo);
			}
			$panchayat = Panchayat_user::Create([
			'name' => $name,
			'mobile' => $mobile,
			'email' => $email,
			'role_id' => $role_id,
			'pn_id' => $pn_id,
			'profile' => $pro_logo,
			'ward_number' => $ward_number,
			'name_show' => $request->input('name_show'),
			'email_show' => $request->input('email_show'),
			'mobile_show' => $request->input('mobile_show'),
			'profile_show' => $request->input('profile_show'),
			'ward_show' => $request->input('ward_show'),
			'role_show' => $request->input('role_show'),
			'created_at' => $date,
			'updated_at' => $date
				]);
				if($panchayat)
				{
					return redirect()->back()->with('add','Panchayat User Add Successfully...');
				}else{
					return redirect()->back()->with('failure','Not Addedd!');
				}

	}
	public function view_user($id)
	{
		$data['view_user'] = DB::table('panchayat_user')->select('panchayat_user.*','panchyat.panc_name','role.role_name')
		->join('role','role.id','=','panchayat_user.role_id')
		->join('panchyat','panchyat.id','=','panchayat_user.pn_id')->where('panchayat_user.id',$id)->get();
		// print_r($data['view_user']);
		// die;
		return view('admin.view-panc-user',$data);
	}
	
	public function edit_user(Request $request ,$id)
	{
		// echo "fdfd0";
		// die;
		$name=$request->input('name');
		$mobile=$request->input('mobile');
		$email=$request->input('email');
		$role_id=$request->input('role_id');
		$ward_number=$request->input('ward_number');
		$pn_id=$request->input('pn_id');
		$date = date('y-m-d');
			if ($request->hasFile('profile'))
			{
				$image = $request->file('profile');
				$pro_logo = time().'.'.$image->getClientOriginalExtension();
				$destinationPath = public_path('/upload/panchayat/panchayat1/role/LDC/');
				$image->move($destinationPath, $pro_logo);
				$panchayat = Panchayat_user::where('id', $id)->update([
				'name' => $name,
				'mobile' => $mobile,
				'email' => $email,
				'role_id' => $role_id,
				'pn_id' => $pn_id,
				'profile' => $pro_logo,
				'ward_number' => $ward_number,
				'created_at' => $date,
				'updated_at' => $date
					]);
					if($panchayat)
					{
						return redirect()->back()->with('edit','User Updated Successfully...');
					}else{
						return redirect()->back()->with('failure','Not Addedd!');
					}
			}
			$panchayat = Panchayat_user::where('id', $id)->update([
			'name' => $name,
			'mobile' => $mobile,
			'email' => $email,
			'role_id' => $role_id,
			'pn_id' => $pn_id,
			'ward_number' => $ward_number,
			'created_at' => $date,
			'updated_at' => $date
				]);
				if($panchayat)
				{
					return redirect()->back()->with('edit','User Updated Successfully...');
				}else{
					return redirect()->back()->with('failure','Not Addedd!');
				}

			
	}
	public function delete_user($id)
	{
		$data = Panchayat_user::where('id',$id)->delete();
			if($data)
			{
				return redirect()->back()->with('delete','Deleted Successfully!');
			}else
			{
				return redirect()->back()->with('failure','Not Deleted!');
			}
	}
	
//Role Functions..
	public function role()
	{
		$data['all'] = Role::all();
		return view('admin.role-list',$data);
	}
	public function add_role(Request $request)
	{
		$date = date('y-m-d');
		$role = Role::Create([
			'role_name' => $request->input('name'),
			'description' => $request->input('desc'),
			'created_at' => $date,
			'updated_at' => $date
				]);
				if($role)
				{
					return redirect()->back()->with('insrt','Role Add Successfully...');
				}else{
					return redirect()->back()->with('failure','Not Addedd!');
				}
	}
	
	public function edit_role(Request $request,$id)
	{
		$date = date('y-m-d');
		$role = Role::where('id',$id)->update([
			'role_name' => $request->input('name'),
			'description' => $request->input('desc'),
			'created_at' => $date,
			'updated_at' => $date
				]);
				if($role)
				{
					return redirect()->back()->with('edit','Role Edit Successfully...');
				}else{
					return redirect()->back()->with('failure','Not Edit!');
				}
	}
	
	public function delete_role($id)
	{
		$data = Role::where('id',$id)->delete();
		if($data)
		{
			return redirect()->back()->with('delete','Your role delete successfully...');
		}else{
			return redirect()->back()->with('not','Not delete!');
		}
	}
	
	public function citizens()
	{
		$data['all'] = Citizen::all();
		return view('admin.citizens',$data);
	}
	
	public function view_citizen($id)
	{
		$data['view_citizen'] = Citizen::where('id',$id)->get();
		$data['asset'] = Asset::where('user_id',$id)->get();
		$data['Citizen_document'] = Citizen_document::where('user_id',$id)->get();
		$data['Citizen_bank_detail'] = Citizen_bank_detail::where('user_id',$id)->get();
		$data['Citizen_feedback'] = Citizen_feedback::where('user_id',$id)->get();
		
		return view('admin.view_citizen',$data);
	}
	
	public function view_secondry_mem()
	{
		// $data['view_citizen'] = Citizen::where('id',$id)->get();
		// $data['asset'] = Asset::where('user_id',$id)->get();
		// $data['Citizen_document'] = Citizen_document::where('user_id',$id)->get();
		// $data['Citizen_bank_detail'] = Citizen_bank_detail::where('user_id',$id)->get();
		// $data['Citizen_feedback'] = Citizen_feedback::where('user_id',$id)->get();
		// print_r($data['Citizen_bank_detail']);
		// die;
		return view('admin.view_sec_citizen');
	}
	
//Attendence function
	public function attendence()
	{
		$data['panchyat'] = Panchyat::all();
		return view ('admin.attendence.index',$data);
	}
	
	public function sec_attendence()
	{
		return view ('admin.attendence.attendence');
	}
//Panchyat funtion
	public function status_update(Request $request)
    {
	   $data['panchyat'] = Panchyat::all();
       $panchayat_id=$request->status;
	   $data['panchyat_att'] = Panchayat_user::where('pn_id',$panchayat_id)->get();
	   $data['sarpanch'] = Panchayat_user::where('pn_id',$panchayat_id)->orWhere('role_id',3)->get();
	   // print_r($data['panchyat_att']);
		 // die;
	   return view ('admin.attendence.index',$data);
	   
    }
	
	public function staff()
	{
		return view('frontend.dashboard.index');
	}
	
	public function tickets()
	{
		$data['ticket'] = Ticket::all();
		return view('admin.view_tickets',$data);
	}
	public function ticket_reply($id)
	{
		$data = Ticket::where('ticket_id',$id)->first();
		$user_id = $data->user_id;
		$citizen = User::where('id',$user_id)->first();
		return view('admin.ticket-reply',compact('data','citizen'));
	}
	
	public function reply_to_citizen(Request $request ) 
	{
		if ($request->hasFile('reply_file'))
		{
			$image = $request->file('reply_file');
			$reply_file = time().$image->getClientOriginalExtension();
			$destinationPath = public_path().'/panchayat/';
			$image->move($destinationPath, $reply_file);
		}else{
			$reply_file=0;
		} 
		$data = Ticket_reply::Create([
			'panc_desc' => $request->input('panc_desc'),
			'ticket_id' => $request->input('ticket_id'),
			'panc_user_id' => $request->input('panc_user_id'),
			'reply_status' => $request->input('reply_status'),
			'panc_file' => $reply_file

				]);
		
		if($data)
		{
			return redirect()->route('admin.tickets');
		}else{
			return "not";
		}
	}
	public function email()
	{
		return view('admin.email');
	}
	public function show_email()
	{
		return view('admin.show-email');
	}
	public function compose_email()
	{
		return view('admin.compose-email');
	}
	public function timeline()
	{
		return view('admin.timeline');
	}
	public function sms_credit()
	{
		return view('admin.sms-credit');
	}
	public function file_manager()
	{
		return view('admin.file-manager');
	}
	public function sallery_pdf()
	{
		return view('admin.sallery');
	}
	public function ads()
	{
		return view('admin.sallery');
	}
	public function home()
	{

		$data['widgets'] = Enabled_widget::where('panchayat_id', '=', Auth::user()->panc_id)->get();
		return view('admin.website.index',$data);
	}
	public function widget_status($status,$widget)
	{
		$affectedRows = Enabled_widget::where('panchayat_id', '=', Auth::user()->panc_id)->update(array($widget => $status));
		if($affectedRows)
			echo json_encode(array('status' => true , 'message'=>'Success'));
		else echo json_encode(array('status' => false , 'message'=>'Error'));
	}
	public function update_bg_image(Request $request){

		if($request->file('bg_img')!==null){
			$validatedData = $request->validate([
			    'bg_img' => 'mimes:jpeg,png,bmp,gif,svg'
			]);
			$path = $request->file('bg_img')->store('/img/panchayat/'.Auth::user()->panc_id);
			$path = "storage/app/".$path; 
			$affectedRows = Panc_home_page::where('panc_id', '=', Auth::user()->panc_id)->update(array('bg_img' => $path));
			if($affectedRows)
				echo json_encode(array('status' => true , 'message'=>'Success'));
			else echo json_encode(array('status' => false , 'message'=>'Error'));
			if(!isset($path))
				echo json_encode(array('status' => false , 'message'=>'File type not matched'));
		}else echo json_encode(array('status' => false , 'message'=>'File Error'));
		
	}
		
}
