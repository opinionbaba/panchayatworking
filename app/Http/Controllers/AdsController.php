<?php

namespace App\Http\Controllers;
use Session;
use Illuminate\Http\Request;
use App\Admin;
use App\Temdocument;
use App\Notice;
use App\Contact;
use App\Subscriber;
use App\Sub_category;
use App\List_account;
use App\Main_category;
use App\Citizen;
use App\Asset;
use App\Panchyat;
use App\Panchayat_user;
use App\Citizen_bank_detail;
use App\Citizen_document;
use App\Citizen_feedback;
use App\Role;
use App\Validator;
use App\User;
use Auth;
Use Alert;
use DB;
use App\Event;
class AdsController extends Controller
{

	function __construct(){
		//$this->user_id = Auth::id();
		$this->user_id = 1;
	}
	public function ads(){	
		$data['ads'] = DB::table('ads_category')
		->select('ads_category.*','users.name as uname')
		->join('users','ads_category.created_by','=','users.id')
		->get();
		$data['price'] = DB::table('ads_price')
		->select('ads_price.*','ads_category.name','ads_category.height','ads_category.width','panchyat.panc_name')
		->join('ads_category','ads_price.ads_category','=','ads_category.id')
		->join('panchyat','ads_price.panchayat_id','=','panchyat.id')
		->where('ads_category.status',1)
		->get();
		return view('admin.ads.index',$data);
	}
	
	public function ads_update(Request $request){
		if($request['ads_id']>0){
			$result = DB::table('ads_category')->where(['id'=>$request['ads_id']])->update([
			'name' =>$request['name'],
			'height' =>$request['height'],
			'width' => $request['width']
			]);
		}else{
			$result = DB::table('ads_category')->insert([
			'name' =>$request['name'],
			'height' =>$request['height'],
			'width' => $request['width'],
			'created_by' => $this->user_id
			]);
		}
		$this->response_message($result);
	}
	public function ads_price(Request $request){
		$data  = array('ads_category' => $request['ads_category'],'price'=> $request['price'],'price_type'=> $request['price_type'],'created_by'=>Auth::id());
		if($request['pid']>0){
			$result = DB::table('ads_price')->where(['id'=>$request['pid']])->update($data);
		}else{
			$data['panchayat_id'] = Auth::user()->panchayat_id > 0 ? Auth::user()->panchayat_id : '0';
			$result = DB::table('ads_price')->insert($data);
		}
		$this->response_message($result);
	}
	public function ads_delete($id){
		$result = DB::table('ads_category')->where('id',$id)->delete();
		$this->response_message($result);
	}
	public function ads_status($status,$id){
		$result = DB::table('ads_category')->where('id',$id)->update(['status'=>$status]);
		$this->response_message($result);
	}
	public function price_delete($id){
		$result = DB::table('ads_price')->where('id',$id)->delete();
		$this->response_message($result);
	}
	public function price_status($status,$id){
		$result = DB::table('ads_price')->where('id',$id)->update(['status'=>$status]);
		$this->response_message($result);
	}
	/*

	advertise page 

	*/
	public function advertise()
	{
		$data['price'] = DB::table('ads_price')
			->select('ads_price.*','ads_category.name','ads_category.height','ads_category.width','panchyat.panc_name')
			->join('panchyat','ads_price.panchayat_id','=','panchyat.id')
			->join('ads_category','ads_price.ads_category','=','ads_category.id')
			->where(['ads_category.status'=>1,'ads_price.status'=>1,'panchyat.status'=>1])
			->get();
		$data['advertises'] = DB::table('ads_sold')
			->select('ads_sold.from_date','ads_sold.last_date','ads_sold.status','ads_sold.ads_url','ads_sold.ads_url','ads_category.date','ads_price.price','ads_price.price_type','ads_category.name','ads_category.height','ads_category.width','panchyat.panc_name')
			->join('ads_price','ads_sold.price_id','=','ads_price.id')
			->join('panchyat','ads_price.panchayat_id','=','panchyat.id')
			->join('ads_category','ads_price.ads_category','=','ads_category.id')
			->where('ads_user',$this->user_id)
			->get();
		return view('citizen.advertise',$data);
	}
	
	public function new_ad()
	{
		$data['ads'] = DB::table('ads_category')
		->select('ads_category.*','users.name as uname')
		->join('users','ads_category.created_by','=','users.id')
		->where('ads_category.status',1)
		->get();
		
		$data['panchayat'] = Panchyat::where('status', 1)->get();
		$data['panchyat_ads'] = DB::table('ads_price')
		->select('ads_price.*','ads_category.height','ads_category.width')
		->join('ads_category','ads_price.ads_category','=','ads_category.id')
		->where(['ads_price.status'=>1,'ads_category.status'=>1])
		->get();
		return view('citizen.new-ad',$data);
	}
	public function adsrequest(Request $request){
		$response = 1;
		$result =1;
		$slots = $request->input('slot');
		$panchyats = $request->input('panchayat');
		if(is_countable($slots) && is_countable($panchyats))
		foreach ($slots as $slot) {
			$path = $request->file('ads_img_'.$slot)->store('advertise/img/'.$this->user_id);
			foreach ($panchyats as $panchayat) {
				$price_id = $this->get_price($slot,$panchayat);
				if($this->check_duplicate($slot,$panchayat)<1 && $price_id>0){
					$result = DB::table('ads_sold')->insert([
						'ads_category'=> $slot,
						'price_id'=> $price_id,
						'ads_user'=> $this->user_id,
						'panchayat_id'=> $panchayat,
						'from_date' =>'2020-04-06 18:24:20',//$request->input(''),
						'last_date' =>'2020-04-06 18:24:20',//$request->input(''),
						'days'=> $request->input('ads_date_'.$slot),
						'ads_url'=> $request->input('ads_url_'.$slot),
						'ads_img'=> 'storage/app/'.$path,
						'created_by' => $this->user_id

					]);
					if($result == 0) $response =0;
				}
			}
		}
		$this->response_message($response);
	}
	/*------------------end---------------*/
	function response_message($result, $success = 'Success', $error = 'Error!!'){
		if($result){
			echo json_encode([ 'status'=> true, 'message' =>$success]);
		}
		else echo json_encode([ 'status'=> false, 'message' =>$error]);
	}
	private function get_price($slot,$panchayat){
		$result = DB::table('ads_price')->select('id')
		->where(['ads_category'=>$slot,'panchayat_id'=>$panchayat])
		->get();
		if(isset($result[0]->id))
			return $result[0]->id;
		else return 0;
	}
	private function check_duplicate($slot,$panchayat){

		$result = DB::table('ads_sold')->select('id')
		->where(['ads_category'=>$slot,'panchayat_id'=>$panchayat,'ads_user'=>$this->user_id])
		->get();
		return count($result);
	}
	
}
