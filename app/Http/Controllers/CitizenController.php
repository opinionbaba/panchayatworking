<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use Image;
use Storage;
use File;
use App\Admin;
use App\Temdocument;
use App\Notice;
use App\Contact;
use App\Subscriber;
use App\Sub_category;
use App\List_account;
use App\Main_category;
use App\Citizen;
use App\Asset;
use App\Panchyat;
use App\Panchayat_user;
use App\Citizen_bank_detail;
use App\Citizen_document;
use App\Citizen_feedback;
use App\Add_certificate;
use App\Role;
use App\Ticket;
use App\Validator;
use App\Citizen_pay_tax;
use App\User;
use Auth;
Use Alert;
use DB;
use App\Event;
class CitizenController extends Controller
{
	
	public function index()
	{
	 return view('citizen.index');
	}  
	
	public function document()
	{
		return view('citizen.document');
	} 
	
	public function save_document(Request $request)
	{
		// $panc_name = $request->input('panc_name');
		 $path = public_path().'/panchayat/vp_curti/';
		// Make Dynamic Folder using this code : File::makeDirectory($path, $mode = 0777, true, true);
		$date = date('y-m-d');
			if ($request->hasFile('aadhar_front'))
			{
				$image = $request->file('aadhar_front');
				$aadhar_front = time().'f.'.$image->getClientOriginalExtension();
				$destinationPath = $path;
				$image->move($destinationPath, $aadhar_front);
			}else{
				$aadhar_front=0;
			}	
			
			if ($request->hasFile('aadhar_back'))
			{
				$image = $request->file('aadhar_back');
				$aadhar_back = time().'b.'.$image->getClientOriginalExtension();
				$destinationPath = $path;
				$image->move($destinationPath, $aadhar_back);
			}else{
				$aadhar_back=0;
			}
			
			if ($request->hasFile('ration_front'))
			{
				$image = $request->file('ration_front');
				$ration_front = time().'ra.'.$image->getClientOriginalExtension();
				$destinationPath = $path;
				$image->move($destinationPath, $ration_front);
			}else{
				$ration_front=0;
			}	
				
			if ($request->hasFile('ration_back'))
			{
				$image = $request->file('ration_back');
				$ration_back = time().'rb.'.$image->getClientOriginalExtension();
				$destinationPath = $path;
				$image->move($destinationPath, $ration_back);
			}else{
				$ration_back=0;
			}	
			
			if ($request->hasFile('passport_front'))
			{
				$image = $request->file('passport_front');
				$passport_front = time().'pa.'.$image->getClientOriginalExtension();
				$destinationPath = $path;
				$image->move($destinationPath, $passport_front);
			}else{
				$passport_front=0;
			}
			
			if ($request->hasFile('passport_back'))
			{
				$image = $request->file('passport_back');
				$passport_back = time().'pb.'.$image->getClientOriginalExtension();
				$destinationPath = $path;
				$image->move($destinationPath, $passport_back);
			}else{
				$passport_back=0;
			}	
			
			if ($request->hasFile('voter_front'))
			{
				$image = $request->file('voter_front');
				$voter_front = time().'va.'.$image->getClientOriginalExtension();
				$destinationPath =$path;
				$image->move($destinationPath, $voter_front);
			}else{
				$voter_front=0;
			}
			
			if ($request->hasFile('voter_back'))
			{
				$image = $request->file('voter_back');
				$voter_back = time().'vb.'.$image->getClientOriginalExtension();
				$destinationPath = $path;
				$image->move($destinationPath, $voter_back);
			}else{
				$voter_back=0;
			}	
			
			if ($request->hasFile('pan_image'))
			{
				$image = $request->file('pan_image');
				$pan_image = time().'pa.'.$image->getClientOriginalExtension();
				$destinationPath = $path;
				$image->move($destinationPath, $pan_image);
			}else{
				$pan_image=0;
			}	
			
			if ($request->hasFile('birth_image'))
			{
				$image = $request->file('birth_image');
				$birth_image = time().'ba.'.$image->getClientOriginalExtension();
				$destinationPath = $path;
				$image->move($destinationPath, $birth_image);
			}else{
				$birth_image=0;
			}	
			
			if ($request->hasFile('health_image'))
			{
				$image = $request->file('health_image');
				$health_image = time().'ha.'.$image->getClientOriginalExtension();
				$destinationPath = $path;
				$image->move($destinationPath, $health_image);
			}else{
				$health_image=0;
			}	
			if ($request->hasFile('driving_image'))
			{
				$image = $request->file('driving_image');
				$driving_image = time().'da.'.$image->getClientOriginalExtension();
				$destinationPath = $path;
				$image->move($destinationPath, $driving_image);
			}else{
				$driving_image=0;
			}	
				
				$panchayat = Citizen_document::Create([
				
				'panc_id' => 3,
				'user_id' => 3,
				'aadhar_num'        => $request->input('aadhar_num'),
				'ration_num'        => $request->input('ration_card'),
				'pass_num'   		=> $request->input('passport'),
				'voter_num'   		=> $request->input('voter_id'),
				'health_card_num'   => $request->input('health_card'),
				'b_certi_num'   	=> $request->input('birth_num'),
				'driving_num'   	=> $request->input('driving'),
				'pan_num'   		=> $request->input('pan_num'),
				'aadhar_front' 		=> $aadhar_front,
				'aadhar_back'  		=> $aadhar_back,
				'ration_front' 		=> $ration_front,
				'ration_back'  		=> $ration_back,
				'pass_front'   		=> $passport_front,
				'pass_back'    		=> $passport_back,
				'voter_card_front'  => $voter_front,
				'voter_card_back'   => $voter_back,
				'health_file'    	=> $health_image,
				'b_file'    		=> $birth_image,
				'driving_file'    	=> $driving_image,
				'pan_image'    		=> $pan_image,
				
				'created_at' 		=> $date,
				'updated_at' 		=> $date
					]);
					
					if($panchayat)
					{
						return "add";
						//return redirect()->back()->with('edit','User Updated Successfully...');
					}else{
						return "Not add";
						//return redirect()->back()->with('failure','Not Addedd!');
					}
			}
			
	public function get_otp(Request $request)
	{
	 $phone=$request->input('phone');
	 $otp = rand(1000,99999);
	 $data = array('phone'=>$phone,'otp'=>$otp);
	 $authkey="163447AG3aZJ2tJ75e85847bP1";
	 $senderid="PANCHA";
	 $message = "Welcome to Panchayat: OTP : $otp";
	 $sendotp="https://api.msg91.com/api/sendhttp.php?mobiles=$phone&authkey=$authkey&route=4&sender=$senderid&message=$message&country=91";
	 $smoutPut=file_get_contents($sendotp);
	}
			 
	 public function add_citizen(Request $request)
	 {
		// print_r($_POST);
		// die;
		$date = date('y-m-d');
		 $data = citizen::Create([
			'f_name'        => $request->input('f_name'),
			'm_name'        => $request->input('m_name'),
			'l_name'   		=> $request->input('l_name'),
			'otp'   		=> $request->input('otp'),
			'password'      => Hash::make($request->input('password')),
			'mobile'   		=> $request->input('moblie'),
			'email'   		=> $request->input('email'),
			'created_at'   	=> $date,
			'updated_at'   	=> $date,
		 ]);
		 if($data)
		 {
			 $citizen = User::Create([
				'username'     => $request->input('f_name'),
				'name'         => $request->input('f_name'),
				'email'        => $request->input('email'),
				'mobile'       => $request->input('moblie'),
				'password'	   => Hash::make($request->input('password')),
				'pass_int'     => $request->input('password'),
				'role'     	   => 9,
				'status'       => 1,
				'panc_id' => 1,
			 ]);
			 $d = Auth::loginUsingId($citizen->id);
			 // echo $d;
			 // die;
			 return redirect()->action('CitizenController@index');
		 }
	 }
			 

	public function bank_details()
	{
		return view('citizen.bank-details');
	}
	
	public function save_details(Request $request)
	{
		// print_r($_POST);
		// die;
		$date = date('y-m-d');
		$insert = Citizen_bank_detail:: Create([
		   'user_id'      => 1,
		   'ac_number'    => $request->input('ac_num'),
		   'ifsc'    	  => $request->input('ifsc'),
		   'bank_name'    => $request->input('bank_name'),
		   'branch_name'  => $request->input('br_name'),
		   'upi_id'    	  => $request->input('upi'),
		   'ph_pay_number'=> $request->input('p_pay'),
		   'goo_pay_number'=> $request->input('g_pay'),
		   'pytm_number'   => $request->input('paytm_num'),
		   'created_at'    => $date,
		   'created_at'    => $date,
		   
		]);
		if($insert)
		{
			return "Ok";
		  //return redirect()->back()->with('Success');
		}else{
			return "Not";
		}
	}
	
	public function all_ertificate()
	{
		return view('citizen.all-certificate');
	}
	public function certificate_request()
	{
		
		$data['certificate'] = Add_certificate::all();
		return view('citizen.certificate',$data);
	}
	
	public function get_certi(Request $request)
	{
		$id = $request->status;
		$check = Add_certificate::select('aadhar_card', 'ration_card', 'passport',  'voter_card', 'pan_card', 'health_card', 'birth_certi', 'driving_lic')->where([['aadhar_card', '=', 1],['ration_card','=', 1],['passport','=', 1],['voter_card','=', 1],['pan_card','=', 1],['health_card','=', 1],['birth_certi','=', 1],['driving_lic','=', 1]])->orWhere('id',$id)->first();
		  
		print_r($data['a_card']);
		die;
		return view('citizen.certificate',$data);
	}
	public function profile()
	{
		//$data['certificate'] = Add_certificate::all();
		return view('citizen.profile');
	}
	public function ad_certificate(Request $request)
	{
		return view('citizen.certificate');
	}
	public function check_certificate(Request $request)
	{
		$id= $request->input('certi');
		$data['check'] = Add_certificate::select('aadhar_card', 'ration_card', 'passport', 'voter_card', 'pan_card', 'health_card', 'birth_certi', 'driving_lic')->where([['aadhar_card', '=', 1],['ration_card','=', 1],['passport','=', 1],['voter_card','=', 1],['pan_card','=', 1],['health_card','=', 1],['birth_certi','=', 1],['driving_lic','=', 1]])->orWhere('id',$id)->get();
		
		$data['certificate'] = Add_certificate::all();
		return view('citizen.certificate',$data);
	}
	public function general()
	{
		$data['citizen'] = Citizen::where('id',1)->get();
		// print_r($data['citizen']);
		// die;
		return view('citizen.general',$data);
	}
    public function bank_detail()
	{
		$data['bank'] = Citizen_bank_detail::where('user_id',1)->get();
		return view('citizen.show-detail',$data);
	}
   public function assets()
	{
		return view('citizen.asset');
	}
  public function tax()
	{
		return view('citizen.pay-tax');
	}
   public function web_rti()
	{
		return view('frontend.rti');
		
	}
  public function notification()
	{
		return view('citizen.notification');
	} 
	public function ticket()
	{
		$data['ticket'] = Ticket::all();
		return view('citizen.ticket',$data);
	}
  public function other()
	{
		return view('citizen.other');
	}
  public function pay_tax()
	{
		return view('citizen.citizenpay_tax');
	}
	public function tax_details()
	{
		return view('citizen.tax-details');
	}
	public function new_rti()
	{
		return view('citizen.new-rti');
	}
  public function new_ticket()
	{
		return view('citizen.new-ticket');
	} 

	public function ad_ticket(Request $request)
	{
	  $ticket_id = rand(00000,99999);
	  if ($request->hasFile('attach'))
		{
			$image = $request->file('attach');
			$attach = time().'da.'.$image->getClientOriginalExtension();
			$destinationPath = public_path().'/panchayat/';
			$image->move($destinationPath, $attach);
		}else{
			$attach=0;
		}
		$data = Ticket::Create([
		 'user_id' => $request->input('user_id'),
		 'panc_id' => $request->input('panc_id'),
		 'subject' => $request->input('subject'),
		 'category'=> $request->input('category'),
		 'priority'=> $request->input('priority'),
		 'desc'    => $request->input('desc'),
		 'ticket_id' => $ticket_id,
		 'file'    => $attach,
		 'status'    => 0,
		]);
		if($data)
		{
			return redirect()->route('ticket');
		}else{
			return "not";
		}
	} 
	
	public function update_tax(Request $request)
	{
		$id = $request->status;
		$update = Citizen_pay_tax::where('user_id',$id)->update(['status' => 1]);
		if($update){
			return 1;
		}else{
			return 0;
		}
		
	}
	
    public function show_reply($id)
	{
		$data['show_reply'] = Ticket ::where('ticket_id',$id)->get();
		// print_r($data['show_reply']);
		// die;
		return view('citizen.show-reply');
		
	} 
	
	public function about_us()
	{
		return view('frontend.about-us');
	}

}
