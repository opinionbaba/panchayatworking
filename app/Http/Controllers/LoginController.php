<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use App\Admin;
use App\Temdocument;
use App\Notice;
use App\Contact;
use App\Subscriber;
use App\Sub_category;
use App\List_account;
use App\Main_category;
use App\Citizen;
use App\Asset;
use App\Panchyat;
use App\Panchayat_user;
use App\Citizen_bank_detail;
use App\Citizen_document;
use App\Citizen_feedback;
use App\Ticket_reply;
use App\Role;
use App\Validator;
use App\Ticket;
use App\User;
use Auth;
Use Alert;
use DB;
use App\Event;
class LoginController extends Controller
{
	/*---Login For Super Admin---*/
    public function login_superadmin(Request $request)
	{
		
		$email=$request->input('email');
		$password=sha1($request->input('password'));
		$role= 1;
		$login_result=User::where([['email', '=', $email],['password','=', $password],['role','=', $role]])->get();	
		
		if(count($login_result)>0)
		{
			Auth::loginUsingId($login_result[0]->id, true);
			return redirect('/dashboard');
		}
		else
		{
			return "Wrong";		
		}
	}
	
	public function logout()
	{
	    Auth::logout();
		return redirect('/');
	}
	
	/*---Login For Citizen---*/
	public function login(Request $request)
	 {
		 $email=$request->input('email');
		 $hashed =$request->input('password');
		 $login_result=User::where([['email', '=', $email],['pass_int','=', $hashed]])->get();
		if(count($login_result)>0)
		{
			Auth::loginUsingId($login_result[0]->id, true);
			if($login_result[0]->role == 1)
			 	return redirect('/dashboard');
			else return view('citizen.index');
		}
		else
		{
			echo "Wrong Username or Password";		
		}
	 }
	 
	 protected function guard_citizen()
    {
        return auth()->guard('citizen');
    }
			 
	public function citizen_logout()
	{
		Auth::logout();
		return redirect('/');
	}
}
