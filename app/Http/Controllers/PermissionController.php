<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Role;

class PermissionController extends Controller
{
   public function index()
   {
	   $role=Role::all();
	   
	   return view('admin.set_permission',compact('role'));
   }
   
   public function add_permission()
   {
	   return view('admin.add_permission');
   }
}
