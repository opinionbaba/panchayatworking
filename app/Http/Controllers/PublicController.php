<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;
use App\Panchyat;
use App\Panc_home_page;
use App\Enabled_widget;
class PublicController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index(Request $request){ 
        $domain = $request->getHost();
        $data['panchayat'] = Panchyat::where('panc_domain','=',$domain)->get();
        $panc_id = $data['panchayat'][0]['id'];
        $data['panchayat_home_page'] = Panc_home_page::where('panc_id',$panc_id)->get();
        $data['widgets'] = Enabled_widget::where('panchayat_id',$panc_id)->get();
        $data['advertises'] = DB::table('ads_sold')
        ->select('ads_img','ads_url')
        ->where(['panchayat_id' => '4','status'=>1,'ads_category'=>14])
        ->get();
        
        return view('frontend.index',$data);
    }
}
