<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Admin;
use App\Temdocument;
use App\Notice;
use App\Contact;
use App\Subscriber;
use App\Sub_category;
use App\List_account;
use App\Main_category;
use App\Citizen;
use App\Asset;
use App\Panchyat;
use App\Panchayat_user;
use App\Citizen_bank_detail;
use App\Citizen_document;
use App\Citizen_feedback;
use App\Role;
use App\Add_certificate;
use App\Panc_home_page;
use Auth;
Use Alert;
use DB;
use App\Event;
class WebsiteController extends Controller
{
	 // function __construct(){
        // parent::__construct();
        // $test = $this->load->database();
        
    // }
   
    public function website()
	{
		return view ('admin.website.index');
	} 
	
	public function request()
	{
		  $data['panchyat'] = Panchyat::all();
		return view ('admin.website.request',$data);
	}
	public function panc_data(Request $request)
	{
		 $id=$request->status;
		 $data['panchyat'] = Panchyat::all();
		 $data['panchyat_user'] = Panchayat_user::where('pn_id',$id)->get();
		 // print_r($data['panchyat_user']);
		 // die;
		return view ('admin.website.request',$data);
	}
	
	public function certificate()
	{
		$data['certificate'] = Add_certificate::all();
		return view('admin.certificate.index',$data);
	}
	public function view_certificate($id)
	{
		$data['certificate'] = Add_certificate::where('id',$id)->get();
		return view('admin.certificate.view_certificate',$data);
	}
	
	public function add_certificate(Request $request)
	{
		$date = date('y-m-d');
		$add_certi= Add_certificate::Create([
			'cer_name' => $request->input('cer_name'),
			'cer_desc' => $request->input('cer_description'),
			'aadhar_card' => $request->input('aadhar_card'),
			'ration_card' => $request->input('ration_card'),
			'passport' => $request->input('passport'),
			'voter_card' => $request->input('v_id'),
			'pan_card' => $request->input('pan_card'),
			'health_card' => $request->input('health_card'),
			'birth_certi' => $request->input('birth_certi'),
			'driving_lic' => $request->input('d_l'),
			'created_at' => $date,
			'updated_at' => $date
			
			]);
		if($add_certi)
		{
			return redirect()->back()->with('insrt','Certificate Add Successfully...');
		}else{
			return redirect()->back()->with('failure','Not Addedd!');
		}
		return view('admin.certificate.index');
	}
	
	public function delete_certificate($id)
	{
		$data = Add_certificate::where('id',$id)->delete();
		if($data)
		{
			return redirect()->back()->with('delete','Your certificate delete successfully...');
		}else{
			return redirect()->back()->with('not','Not delete!');
		}
	}
	
	public function add_home_detail(Request $request)
	{
		$date = date('y-m-d');
		$add_certi= Panc_home_page::Create([
			'panc_id' => 1,
			'title' => $request->input('title'),
			'population' => $request->input('total_popu'),
			'area' => $request->input('area'),
			'house_holder' => $request->input('house_hold'),
			'male_popu' => $request->input('male_popu'),
			'female_popu' => $request->input('female_popu'),
			'voter' => $request->input('voter'),
			'created_at' => $date,
			'updated_at' => $date
			
			]);
		if($add_certi)
		{
			return redirect()->back()->with('insrt','Details Add Successfully...');
		}else{
			return redirect()->back()->with('failure','Not Addedd!');
		}
	}
	
	public function home_forms()
	{
		$data['notice'] = Temdocument::all();
		return view('frontend.forms',$data);
	}
	
	public function gallery()
	{
		return view('frontend.gallery');
	}
	public function audit()
	{
		return view('frontend.audit');
	}
	public function certi_request()
	{
		$data['certi'] = Add_certificate::all();
		return view('frontend.certi_request',$data);
	}
	public function contact_us()
	{
		return view('frontend.contact_us');
	}
	public function acts()
	{
		return view('frontend.acts');
	}
	public function rti()
	{
		return view('citizen.rti');
	}
	public function scheme()
	{
		return view('frontend.scheme');
	}
	public function setting()
	{
		return view('admin.setting.index');
	}
	public function admin_about_us()
	{
		return view('admin.website.about-us');
	}
	
	
}
