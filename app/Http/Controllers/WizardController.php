<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Admin;
use App\List_account;
use App\Main_category;
use App\Panchyat;
use App\Panchayat_user;
use App\Validator;
use App\User;
use Auth;
Use Alert;
use DB;
use App\Event;
use Redirect;
use App\Helpers;
use ZipArchive;

class WizardController extends Controller
{
	private $defaultDomainName = "kbeesolutions.in";
	private $defaultCpanelUserNameCredentials = "kbees";
    // private $defaultDomainContactId_Test = "C-009589263-SN";
    // private $defaultDomainContactId_Live = "C-009589263-SN";
    private $defaultIPAddress = "173.212.231.106";
	private $defaultCpanelCredentials = "kbees:10DWCUUW6WMBCHHB5HEQZIWGG29PQSHW";
	private $token = "10DWCUUW6WMBCHHB5HEQZIWGG29PQSHW";
    private $defaultWHMCredentials = "root:Shabana786!@#";
    //public $defaultNameServer = [1 => "ns1.moofth.com", 2 => "ns2.moofth.com"];
	private $RecieverUrl="/home/kbees/public_html/panel/";
	private $SenderUrl="/home/kbees/public_html/";
	private $DefaultImageUrl="/home/kbees/public_html/public/assets/images/";
	private $ManualImageUrl="/home/kbees/public_html/public/panchayat/";
	
		
	public function heloo($panc_id)
	{
		echo "success";
		die;
	}
	
	public function hi($panc_id)
	{
		echo "success1";
		die;
	}
		
	public function get_step_first()
	{
		return view('admin.wizard.panchayat_config');
	}
	
	public function post_step_first(Request $request)
	{
		$this->validate($request,[
        'panc_name' => 'required',
        'panc_number' => 'required',
        'panc_address' => 'required',
        'panc_city' => 'required',
        'panc_email' => 'required',
        'panc_domain' => 'required'

        
		]);
		$panc_name = $request->input('panc_name');
		$panc_pincode = $request->input('panc_pincode');
		$panc_number = $request->input('panc_number');
		$panc_address = $request->input('panc_address');
		$panc_city = $request->input('panc_city');
		$panc_email = $request->input('panc_email');
		$panc_domain = $request->input('panc_domain');
		$date = date('Y-m-d h:i:s');

		$PanchayatData = (['name' => $panc_name,'mobile' => $panc_number,'address' => $panc_address,'panc_pincode'=>$panc_pincode,'panc_city' => $panc_city,'panc_email' => $panc_email,'domain_name' => $panc_domain,'defaultDomainName' => $this->defaultDomainName,'token' => $this->token,'UserNameCredentials' => $this->defaultCpanelUserNameCredentials]);
		
		$domain=$this->addondomain($PanchayatData);
		
		if($domain == 'error')
		{
			return redirect()->back()->with('msg','Duplicate Entry...!');
		}
		if($domain == 'success')
		{
			$PanchayatInsertedData = (['panc_name' => $panc_name,'panc_number' => $panc_number,'panc_pincode'=>$panc_pincode,'panc_address' => $panc_address,'panc_city' => $panc_city,'panc_email' => $panc_email,'panc_domain' => $panc_domain,'created_at' => $date,'updated_at' => $date]);
			
			$PanchyatId= DB::table('panchyat')->insertGetId($PanchayatInsertedData);
			if($PanchyatId)
			{
				$text = $panc_name;
				$PanchyatFolderName=$this->panchyatname($text);
				if ($request->hasFile('panc_logo'))
					{
						
						$image = $request->file('panc_logo');
						$panc_logo = time().'.'.$image->getClientOriginalExtension();
						$destinationPath = $this->ManualImageUrl . $PanchyatFolderName .'/logo/';
						$image->move($destinationPath, $panc_logo);
						
					}else{
						$panc_logo="default.png";
					}
					if ($request->hasFile('panc_app_logo'))
					{
						
						$image = $request->file('panc_app_logo');
						$panc_app_logo = time().'.'.$image->getClientOriginalExtension();
						$destinationPath = $this->ManualImageUrl . $PanchyatFolderName .'/logo/applogo/';
						$image->move($destinationPath, $panc_app_logo);
						
					}else{
						$panc_app_logo="app_default.png";
					}
				$result=DB::table('panchyat')->where(['id'=>$PanchyatId])->update(['panc_logo'=>$panc_logo,'panc_app_logo'=>$panc_app_logo,'panc_asset'=>$PanchyatFolderName,'status'=>1]);
				if($result){
					return redirect()->action('AdminController@add_account')->with('add','Panchayat Created Successfully..');
				}
				else{
					return redirect()->back()->with('msg','Not Addedd!');
				}
			}
			return redirect()->back()->with('msg','Not Addedd!');
		}
		else{
			return redirect()->back()->with('msg','Somthing went wrong!');
		}
		//die;
		
	}
	
	
	public function addondomain($PanchayatData)
	{
		$Response["Domain"] = (array) json_decode(cpanel_register_addondomain($PanchayatData, $this->defaultCpanelCredentials));
		
		if(empty($Response['Domain']['cpanelresult']->error))
		{
			$src = $this->RecieverUrl . "panchyat_zip.zip";

            $target = $this->SenderUrl . $PanchayatData['domain_name'] . '/panchyat_zip.zip';
            
            copy($src, $target);
			$zip = new ZipArchive;
            $res = $zip->open($target);
            if ($res === TRUE) {
                $zip->extractTo($this->SenderUrl . $PanchayatData['domain_name'] );
                $zip->close();
				unlink($target);
            }
			else
			{
				return "error";
			}
			/*-----Panchyat asset----------*/
			$src = $this->RecieverUrl . "panchyat_asset.zip";
            $target = $this->SenderUrl . "public/panchayat/panchyat_asset.zip";
            copy($src, $target);
			$text=$PanchayatData['name'];
			$Panchyat_Name=$this->panchyatname($text);
			
			$zip = new ZipArchive;
            $res = $zip->open($target);
            if ($res === TRUE) {    
                $zip->extractTo($this->SenderUrl . "public/panchayat");
                $zip->close();
				$oldpanchyat = $this->SenderUrl . "public/panchayat/panchyat_asset";
				$newpanchyat = $this->SenderUrl . "public/panchayat/".$Panchyat_Name;
				rename($oldpanchyat,$newpanchyat);
				$result=unlink($target);
				if($result)
				{
					return "success";
				}
				else
				{
					return "error";
				}
			}
		}
		else{
			return "error";
		}
		
	}
	
	public static function panchyatname($text)
	{
	  // replace non letter or digits by -
	  $text = preg_replace('~[^\pL\d]+~u', '-', $text);

	  // transliterate
	  $text = iconv('utf-8', 'us-ascii//TRANSLIT', $text);

	  // remove unwanted characters
	  $text = preg_replace('~[^-\w]+~', '', $text);

	  // trim
	  $text = trim($text, '-');

	  // remove duplicate -
	  $text = preg_replace('~-+~', '-', $text);

	  // lowercase
	  $text = strtolower($text);

	  if (empty($text)) {
		return 'n-a';
	  }

	  return $text;
	}
	
			
}
