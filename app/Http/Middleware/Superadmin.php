<?php

namespace App\Http\Middleware;

use Closure;
use Auth;

class Superadmin
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    // now there is only two role super admin and citizen
    public function handle($request, Closure $next)
    {
       if(Auth::user() && Auth::user()->role == 1)
        {
            return $next($request);
        }
        return redirect("/");
       
    }
}
