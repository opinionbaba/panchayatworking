<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class List_account extends Model
{
   protected $table = 'list_account';
   protected $fillable = ['panchyat_name', 'number', 'address', 'logo', 'email', 'domain', 'latitude', 'longitude', 'app_logo', 'created_at', 'updated_at', 'status'];
}
