<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Main_category extends Model
{
    protected $table = 'main_cat';
	protected $fillable = ['name','created_at','updated_at'];
}
