<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Notice extends Model
{
    protected $table = 'notice';
	
	 protected $fillable = [ 'cat_id', 'notice', 'base_path','description', 'file', 'created_at','updated_at'];
}
