<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Panc_home_page extends Model
{
   protected $table='panc_home_page';
   protected $fillable = ['panc_id', 'title', 'population', 'area', 'house_holder', 'male_popu', 'female_popu', 'voter', 'created_at', 'updated_at'];
}
