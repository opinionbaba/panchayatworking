<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Panchayat_user extends Model
{
    protected $table ='panchayat_user';
	protected $fillable = ['name', 'email', 'mobile', 'role_id', 'pn_id', 'ward_number', 'profile', 'status', 'created_at', 'updated_at','name_show', 'email_show', 'number_show', 'role_show', 'profile_show', 'ward_show'];
}
