<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Panchyat extends Model
{
    protected $table ='panchyat';
	protected $fillable = ['panc_name', 'panc_address', 'panc_number', 'panc_email', 'panc_domain', 'panc_lat','panc_long', 'panc_logo', 'panc_app_logo', 'created_at', 'updated_at'];
	
	public function ads_price()
    {
        return $this->hasMany('App\Ads_price','panchayat_id','id');
    }
}
