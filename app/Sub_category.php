<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Sub_category extends Model
{
    protected $table = 'sub_category';
	protected $fillable = ['main_cat_id','name','created_at','updated_at'];
}
