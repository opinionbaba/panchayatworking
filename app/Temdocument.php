<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Temdocument extends Model
{
	 protected $table = 'tem_document';
	
	 protected $fillable = [ 'doc_name', 'file','base_path', 'created_at'];
          
}
