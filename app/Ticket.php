<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Ticket extends Model
{
    protected $table='ticket';
	protected $fillable=[ 'user_id', 'panc_id','priority', 'category','ticket_id', 'subject', 'file', 'desc', 'status'];
}
