<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Ticket_reply extends Model
{
   protected $table= 'ticket_reply';
   protected $fillable = ['ticket_id', 'panc_user_id', 'reply_status', 'panc_desc', 'panc_file'];
}
