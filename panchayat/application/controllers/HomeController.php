<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class HomeController extends CI_Controller {

	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see https://codeigniter.com/user_guide/general/urls.html
	 */
	 
	/**
	 * Index Page
	 */
	public function index()
	{
		
		$data['title'] = "Home";
		$this->load->view('layouts/homeheader');
		$this->load->view('home',$data);
		$this->load->view('layouts/homefooter');
	}
	
	public function aboutus()
	{		
		$data['title'] = "About";
		$this->load->view('layouts/homeheader');
		$this->load->view('aboutus',$data);
		$this->load->view('layouts/homefooter');
	}
	
	public function schemes()
	{	
		$data['title'] = "Schemes";
		$this->load->view('layouts/homeheader');
		$this->load->view('schemes',$data);
		$this->load->view('layouts/homefooter');
	}
	
	public function acts()
	{	
		$data['title'] = "Acts";
		$this->load->view('layouts/homeheader');
		$this->load->view('acts',$data);
		$this->load->view('layouts/homefooter');
	}
	
	public function rti()
	{	
		$data['title'] = "RTI";
		$this->load->view('layouts/homeheader');
		$this->load->view('rti',$data);
		$this->load->view('layouts/homefooter');
	}
	
	public function forms()
	{	
		$data['title'] = "Forms";
		$this->load->view('layouts/homeheader');
		$this->load->view('forms',$data);
		$this->load->view('layouts/homefooter');
	}
	
	public function photo_gallery()
	{	
		$data['title'] = "Photo Gallery";
		$this->load->view('layouts/homeheader');
		$this->load->view('photo_gallery',$data);
		$this->load->view('layouts/homefooter');
	}
	
	public function audit()
	{	
		$data['title'] = "Audit";
		$this->load->view('layouts/homeheader');
		$this->load->view('audit',$data);
		$this->load->view('layouts/homefooter');
	}
	
	
	
	public function certificates()
	{	
		$data['title'] = "Certificates";
		$this->load->view('layouts/homeheader');
		$this->load->view('certificates',$data);
		$this->load->view('layouts/homefooter');
	}
	
	public function contact()
	{	
		$data['title'] = "Contact";
		$this->load->view('layouts/homeheader');
		$this->load->view('contact',$data);
		$this->load->view('layouts/homefooter');
	}
	
	// public function index()
	// {
		// $this->load->view('welcome_message');
	// }
}
