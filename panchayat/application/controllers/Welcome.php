<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Welcome extends CI_Controller {
 die;
	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see https://codeigniter.com/user_guide/general/urls.html
	 */
	 
	/**
	 * Index Page
	 */
	public function index()
	{
		$data = array();
		$this->template->set('title', 'Home');
		$this->template->load('template', 'contents' , 'home', $data);
	}
	
	public function aboutus()
	{		
		$data = array();
		$this->template->set('title', 'About us');
		$this->template->load('template', 'contents' , 'aboutus', $data);
	}
	
	public function blog()
	{	
		$data = array();
		$this->template->set('title', 'Blog');
		$this->template->load('template', 'contents' , 'blog', $data);
	}
	
	// public function index()
	// {
		// $this->load->view('welcome_message');
	// }
}
