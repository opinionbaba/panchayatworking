<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?>


		<!-- CORE : begin -->
		<div id="core">
			<div class="c-container">
				<div class="row">

					<!-- MIDDLE COLUMN : begin -->
					<div class="middle-column col-md-6 col-md-push-3">

						<!-- PAGE HEADER : begin -->
						<div id="page-header" class="m-has-breadcrumbs">

							<!-- PAGE TITLE : begin -->
							<div class="page-title">
								<h1>About Us</h1>
							</div>
							<!-- PAGE TITLE : end -->

							<!-- BREADCRUMBS : begin -->
							<div class="breadcrumbs">
								<ul>
									<li class="home"><a href="<?=base_url();?>">Home</a></li>
									<li>About Us</li>
								</ul>
							</div>
							<!-- BREADCRUMBS : end -->

						</div>
						<!-- PAGE HEADER : end -->

						<!-- PAGE CONTENT : begin -->
						<div id="page-content">
							<div class="page-content-inner">

								<!-- NOTICE LIST PAGE : begin -->
								<div class="notice-list-page notice-page">

									<!-- NOTICE : begin -->
									<article class="notice">
										<div class="notice-inner c-content-box m-no-padding">

											<!-- NOTICE CORE : begin -->
											<div class="notice-core">

												<!-- NOTICE TITLE : begin -->
												<h2 class="notice-title"><a href="notice-detail.html">Annual Marathon Registration</a></h2>
												<!-- NOTICE TITLE : end -->

												<!-- NOTICE CONTENT : begin -->
												<div class="notice-content">
													<p>The marathon is a long-distance running event with an official distance of 42.195 kilometres (26 miles and 385 yards), usually run as a road race. The event was instituted in commemoration of the fabled run of the Greek soldier Pheidippides, a messenger from the Battle of Marathon to Athens.</p>
												</div>
												<!-- NOTICE CONTENT : end -->

											</div>
											<!-- NOTICE CORE : end -->

											<!-- NOTICE FOOTER : begin -->
											<div class="notice-footer">
												<div class="notice-footer-inner">

													<!-- NOTICE DATE : begin -->
													<div class="notice-date">
														<i class="ico tp tp-clock2"></i>November 16, 2015 in <a href="notice-list.html">Sport</a>
													</div>
													<!-- NOTICE DATE : end -->

												</div>
											</div>
											<!-- NOTICE FOOTER : end -->

										</div>
									</article>
									<!-- NOTICE : end -->

									<!-- NOTICE : begin -->
									<article class="notice">
										<div class="notice-inner c-content-box m-no-padding">

											<!-- NOTICE CORE : begin -->
											<div class="notice-core">

												<!-- NOTICE TITLE : begin -->
												<h2 class="notice-title"><a href="notice-detail.html">Traffic Safety Notice</a></h2>
												<!-- NOTICE TITLE : end -->

												<!-- NOTICE CONTENT : begin -->
												<div class="notice-content">
													<p>Road traffic safety refers to methods and measures for reducing the risk of a person using the road network being killed or seriously injured. The users of a road include pedestrians, cyclists, motorists, their passengers, and passengers of on-road public transport, mainly buses and trams.</p>
												</div>
												<!-- NOTICE CONTENT : end -->

											</div>
											<!-- NOTICE CORE : end -->

											<!-- NOTICE FOOTER : begin -->
											<div class="notice-footer">
												<div class="notice-footer-inner">

													<!-- NOTICE DATE : begin -->
													<div class="notice-date">
														<i class="ico tp tp-clock2"></i>October 5, 2015 in <a href="notice-list.html">Community</a>
													</div>
													<!-- NOTICE DATE : end -->

												</div>
											</div>
											<!-- NOTICE FOOTER : end -->

										</div>
									</article>
									<!-- NOTICE : end -->

									<!-- NOTICE : begin -->
									<article class="notice">
										<div class="notice-inner c-content-box m-no-padding">

											<!-- NOTICE CORE : begin -->
											<div class="notice-core">

												<!-- NOTICE TITLE : begin -->
												<h2 class="notice-title"><a href="notice-detail.html">Municipal Waste</a></h2>
												<!-- NOTICE TITLE : end -->

												<!-- NOTICE CONTENT : begin -->
												<div class="notice-content">
													<p>Waste disposal may be restricted entirely via a disposal ban. The most common and widespread such standard is a prohibition on littering. Where a jurisdiction has authorized a specific place or system for trash collection, deposition or abandonment of trash elsewhere may be subject to civil or criminal penalties.</p>
												</div>
												<!-- NOTICE CONTENT : end -->

											</div>
											<!-- NOTICE CORE : end -->

											<!-- NOTICE FOOTER : begin -->
											<div class="notice-footer">
												<div class="notice-footer-inner">

													<!-- NOTICE DATE : begin -->
													<div class="notice-date">
														<i class="ico tp tp-clock2"></i>September 24, 2015 in <a href="notice-list.html">Enviroment</a>
													</div>
													<!-- NOTICE DATE : end -->

												</div>
											</div>
											<!-- NOTICE FOOTER : end -->

										</div>
									</article>
									<!-- NOTICE : end -->

									<!-- NOTICE : begin -->
									<article class="notice">
										<div class="notice-inner c-content-box m-no-padding">

											<!-- NOTICE CORE : begin -->
											<div class="notice-core">

												<!-- NOTICE TITLE : begin -->
												<h2 class="notice-title"><a href="notice-detail.html">Upcoming Public Meetings</a></h2>
												<!-- NOTICE TITLE : end -->

												<!-- NOTICE CONTENT : begin -->
												<div class="notice-content">
													<p>A town hall meeting is an American term given to an informal public meeting, function, or event derived from the traditional town meetings of New England. Typically open to everybody in a town community and held at the local municipal building.</p>
												</div>
												<!-- NOTICE CONTENT : end -->

											</div>
											<!-- NOTICE CORE : end -->

											<!-- NOTICE FOOTER : begin -->
											<div class="notice-footer">
												<div class="notice-footer-inner">

													<!-- NOTICE DATE : begin -->
													<div class="notice-date">
														<i class="ico tp tp-clock2"></i>August 24, 2015 in <a href="notice-list.html">Community</a>
													</div>
													<!-- NOTICE DATE : end -->

												</div>
											</div>
											<!-- NOTICE FOOTER : end -->

										</div>
									</article>
									<!-- NOTICE : end -->

								</div>
								<!-- NOTICE LIST PAGE : begin -->

								<!-- PAGINATION : begin -->
								<div class="c-pagination">
									<ul>
										<li class="m-active"><a href="post-list.html">1</a></li>
										<li><a href="post-list.html">2</a></li>
										<li><a href="post-list.html">3</a></li>
									</ul>
								</div>
								<!-- PAGINATION : end -->

							</div>
						</div>
						<!-- PAGE CONTENT : end -->

						<hr class="c-separator m-margin-top-small m-margin-bottom-small m-transparent hidden-lg hidden-md">

					</div>
					<!-- MIDDLE COLUMN : end -->

					<!-- LEFT COLUMN : begin -->
					<div class="left-column col-md-3 col-md-pull-6">

						<?php include('layouts/left_menu.php');?>

						<!-- LEFT SIDEBAR : begin -->
						<aside class="sidebar">
							<div class="widget-list">

								<?php include('layouts/gallery_widget.php');?>
								<?php include('layouts/document_widget.php');?>

							</div>
						</aside>
						<!-- LEFT SIDEBAR : end -->

					</div>
					<!-- LEFT COLUMN : end -->

					<!-- RIGHT COLUMN : begin -->
					<div class="right-column col-md-3">

						<!-- RIGHT SIDEBAR : begin -->
						<aside class="sidebar">
							<div class="widget-list">

								<?php include('layouts/right_category.php');?>

							<?php include('layouts/right_image.php');?>

							</div>
						</aside>
						<!-- RIGHT SIDEBAR : end -->

					</div>
					<!-- RIGHT COLUMN : end -->

				</div>
			</div>
		</div>
		<!-- CORE : end -->
