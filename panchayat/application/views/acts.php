<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?>


		<!-- CORE : begin -->
		<div id="core">
			<div class="c-container">
				<div class="row">

					<!-- MIDDLE COLUMN : begin -->
					<div class="middle-column col-md-6 col-md-push-3">

						<!-- PAGE HEADER : begin -->
						<div id="page-header" class="m-has-breadcrumbs">

							<!-- PAGE TITLE : begin -->
							<div class="page-title">
								<h1>Acts</h1>
							</div>
							<!-- PAGE TITLE : end -->

							<!-- BREADCRUMBS : begin -->
							<div class="breadcrumbs">
								<ul>
									<li class="home"><a href="{{('/')}}">Home</a></li>
									<li>Acts</li>
								</ul>
							</div>
							<!-- BREADCRUMBS : end -->

						</div>
						<!-- PAGE HEADER : end -->

						<!-- PAGE CONTENT : begin -->
						<div id="page-content">
							<div class="page-content-inner">

								<!-- NOTICE LIST PAGE : begin -->
								<div class="notice-list-page notice-page">

									<!-- NOTICE : begin -->
									<article class="notice">
										<div class="notice-inner c-content-box m-no-padding">

											<!-- NOTICE CORE : begin -->
											
											<div class="notice-core">
												<div class="row">
												  <div class="col-sm-10">
													<!-- NOTICE TITLE : begin -->
													<h2 class="notice-title">The Goa Panchayat Raj Ordinance 1994</h2>
													<!-- NOTICE TITLE : end -->
													</div>
													<div class="col-sm-2">
													  <a href="<?=base_url();?>assets/acts/The Right To Information Act, 2005.pdf" target="_blank" ><i class="fa fa-download"></i></a>
													</div>
												</div>
											</div>
											<div class="notice-core">
												<div class="row">
												  <div class="col-sm-10">
													<!-- NOTICE TITLE : begin -->
													<h2 class="notice-title">The Goa Panchayat Raj Act, 1994.pdf</h2>
													<!-- NOTICE TITLE : end -->
													</div>
													<div class="col-sm-2">
													  <a href="<?=base_url();?>assets/acts/The Right To Information Act, 2005.pdf" target="_blank" ><i class="fa fa-download"></i></a>
													</div>
												</div>
											</div>
											<div class="notice-core">
												<div class="row">
												  <div class="col-sm-10">
													<!-- NOTICE TITLE : begin -->
													<h2 class="notice-title">The Goa, Daman and Diu Mundkars Act, 1975 and Rules, 1977.pdf</h2>
													<!-- NOTICE TITLE : end -->
													</div>
													<div class="col-sm-2">
													  <a href="<?=base_url();?>assets/acts/The Right To Information Act, 2005.pdf" target="_blank" ><i class="fa fa-download"></i></a>
													</div>
												</div>
											</div>
											<div class="notice-core">
												<div class="row">
												  <div class="col-sm-10">
													<!-- NOTICE TITLE : begin -->
													<h2 class="notice-title">The Goa Regulation of Land Development and Building Construction Act, 2008.pdf</h2>
													<!-- NOTICE TITLE : end -->
													</div>
													<div class="col-sm-2">
													  <a href="<?=base_url();?>assets/acts/The Right To Information Act, 2005.pdf" target="_blank" ><i class="fa fa-download"></i></a>
													</div>
												</div>
											</div>
											<div class="notice-core">
												<div class="row">
												  <div class="col-sm-10">
													<!-- NOTICE TITLE : begin -->
													<h2 class="notice-title">The Goa, Daman and Diu Public Health Act, 1985 and Rules, 1987.pdf</h2>
													<!-- NOTICE TITLE : end -->
													</div>
													<div class="col-sm-2">
													  <a href="<?=base_url();?>assets/acts/The Right To Information Act, 2005.pdf" target="_blank" ><i class="fa fa-download"></i></a>
													</div>
												</div>
											</div>
											<div class="notice-core">
												<div class="row">
												  <div class="col-sm-10">
													<!-- NOTICE TITLE : begin -->
													<h2 class="notice-title">The Goa, Daman and Diu Town and Country Planning Act, 1974 and Rules, 1976.pdf</h2>
													<!-- NOTICE TITLE : end -->
													</div>
													<div class="col-sm-2">
													  <a href="<?=base_url();?>assets/acts/The Right To Information Act, 2005.pdf" target="_blank" ><i class="fa fa-download"></i></a>
													</div>
												</div>
											</div>
											<div class="notice-core">
												<div class="row">
												  <div class="col-sm-10">
													<!-- NOTICE TITLE : begin -->
													<h2 class="notice-title">The Registration Of Births And Deaths Act, 1969.pdf</h2>
													<!-- NOTICE TITLE : end -->
													</div>
													<div class="col-sm-2">
													  <a href="<?=base_url();?>assets/acts/The Right To Information Act, 2005.pdf" target="_blank" ><i class="fa fa-download"></i></a>
													</div>
												</div>
											</div>
											<div class="notice-core">
												<div class="row">
												  <div class="col-sm-10">
													<!-- NOTICE TITLE : begin -->
													<h2 class="notice-title">The Right To Information Act, 2005.pdf</h2>
													<!-- NOTICE TILE : end -->
													</div>
													<div class="col-sm-2">
													  <a href="<?=base_url();?>assets/acts/The Right To Information Act, 2005.pdf" target="_blank" ><i class="fa fa-download"></i></a>
													</div>
												</div>
											</div>
											
											<!-- NOTICE CORE : end -->
										</div>
									</article>
									<!-- NOTICE : end -->
								</div>
								<!-- NOTICE LIST PAGE : begin -->
							</div>
						</div>
						<!-- PAGE CONTENT : end -->



						<hr class="c-separator m-margin-top-small m-margin-bottom-small m-transparent hidden-lg hidden-md">

					</div>
					<!-- MIDDLE COLUMN : end -->

					<!-- LEFT COLUMN : begin -->
					<div class="left-column col-md-3 col-md-pull-6">

						<?php include('layouts/left_menu.php');?>

						<!-- LEFT SIDEBAR : begin -->
						<aside class="sidebar">
							<div class="widget-list">

								<?php include('layouts/gallery_widget.php');?>
								<?php include('layouts/document_widget.php');?>

							</div>
						</aside>
						<!-- LEFT SIDEBAR : end -->

					</div>
					<!-- LEFT COLUMN : end -->

					<!-- RIGHT COLUMN : begin -->
					<div class="right-column col-md-3">

						<!-- RIGHT SIDEBAR : begin -->
						<aside class="sidebar">
							<div class="widget-list">

								<?php include('layouts/right_category.php');?>

							<?php include('layouts/right_image.php');?>

							</div>
						</aside>
						<!-- RIGHT SIDEBAR : end -->

					</div>
					<!-- RIGHT COLUMN : end -->

				</div>
			</div>
		</div>
		<!-- CORE : end -->
