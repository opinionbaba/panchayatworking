<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?>


		<!-- CORE : begin -->
		<div id="core">
			<div class="c-container">
				<div class="row">

					<!-- MIDDLE COLUMN : begin -->
					<div class="middle-column col-md-6 col-md-push-3">

						<!-- PAGE HEADER : begin -->
						<div id="page-header" class="m-has-breadcrumbs">

							<!-- PAGE TITLE : begin -->
							<div class="page-title">
								<h1>Certificates</h1>
							</div>
							<!-- PAGE TITLE : end -->

							<!-- BREADCRUMBS : begin -->
							<div class="breadcrumbs">
								<ul>
									<li class="home"><a href="{{('/')}}">Home</a></li>
									<li>Certificates</li>
								</ul>
							</div>
							<!-- BREADCRUMBS : end -->

						</div>
						<!-- PAGE HEADER : end -->

						<!-- PAGE CONTENT : begin -->
						<div id="page-content">
							<div class="page-content-inner">

								<!-- NOTICE LIST PAGE : begin -->
								<div class="notice-list-page notice-page">

									<!-- NOTICE : begin -->
									<article class="notice">
										<div class="notice-inner c-content-box m-no-padding">

											<!-- NOTICE CORE : begin -->
										
											<div class="notice-core">
											<input class="form-control" id="myInput" type="text" placeholder="Search..">
													  <br>
													  <table class="table table-bordered table-striped">
														<thead>
														  <tr>
															<th>Firstname</th>
															<th>Lastname</th>
															<th>Email</th>
														  </tr>
														</thead>
														<tbody id="myTable">
														  <tr>
															<td>John</td>
															<td>Doe</td>
															<td>john@example.com</td>
														  </tr>
														  <tr>
															<td>Mary</td>
															<td>Moe</td>
															<td>mary@mail.com</td>
														  </tr>
														  <tr>
															<td>July</td>
															<td>Dooley</td>
															<td>july@greatstuff.com</td>
														  </tr>
														  <tr>
															<td>Anja</td>
															<td>Ravendale</td>
															<td>a_r@test.com</td>
														  </tr>
														</tbody>
													  </table>
											</div>
<script>
$(document).ready(function(){
  $("#myInput").on("keyup", function() {
    var value = $(this).val().toLowerCase();
    $("#myTable tr").filter(function() {
      $(this).toggle($(this).text().toLowerCase().indexOf(value) > -1)
    });
  });
});
</script>
											<!-- NOTICE CORE : end -->
										</div>
									</article>
									<!-- NOTICE : end -->
								</div>
								<!-- NOTICE LIST PAGE : begin -->
							</div>
						</div>
						<!-- PAGE CONTENT : end -->



						<hr class="c-separator m-margin-top-small m-margin-bottom-small m-transparent hidden-lg hidden-md">

					</div>
					<!-- MIDDLE COLUMN : end -->

					<!-- LEFT COLUMN : begin -->
					<div class="left-column col-md-3 col-md-pull-6">

						<?php include('layouts/left_menu.php');?>

						<!-- LEFT SIDEBAR : begin -->
						<aside class="sidebar">
							<div class="widget-list">

								<?php include('layouts/gallery_widget.php');?>
								<?php include('layouts/document_widget.php');?>

							</div>
						</aside>
						<!-- LEFT SIDEBAR : end -->

					</div>
					<!-- LEFT COLUMN : end -->

					<!-- RIGHT COLUMN : begin -->
					<div class="right-column col-md-3">

						<!-- RIGHT SIDEBAR : begin -->
						<aside class="sidebar">
							<div class="widget-list">

								<?php include('layouts/right_category.php');?>

							<?php include('layouts/right_image.php');?>

							</div>
						</aside>
						<!-- RIGHT SIDEBAR : end -->

					</div>
					<!-- RIGHT COLUMN : end -->

				</div>
			</div>
		</div>
		<!-- CORE : end -->
