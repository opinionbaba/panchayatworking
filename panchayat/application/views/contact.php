<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?>


		<!-- CORE : begin -->
		<div id="core">
			<div class="c-container">
				<div class="row">
				<div class="header-gmap">
					<div class="gmap-canvas"
						data-google-api-key="AIzaSyCHctnM3xR2JUhnaQ5gLrIveomGIIbCS2w"
						data-address="Jakkur, Bengaluru, Karnataka "
						data-zoom="17"
						data-maptype="hybrid"
						data-enable-mousewheel="true">
					</div>
				</div>
					<!-- MIDDLE COLUMN : begin -->
					<div class="middle-column col-md-6 col-md-push-3">

						<!-- PAGE HEADER : begin -->
						<div id="page-header" class="m-has-breadcrumbs">

							<!-- PAGE TITLE : begin -->
							<div class="page-title">
								<h1>Contact</h1>
							</div>
							<!-- PAGE TITLE : end -->

							<!-- BREADCRUMBS : begin -->
							<div class="breadcrumbs">
								<ul>
									<li class="home"><a href="{{('/')}}">Home</a></li>
									<li>Contact</li>
								</ul>
							</div>
							<!-- BREADCRUMBS : end -->

						</div>
						<!-- PAGE HEADER : end -->

						<!-- PAGE CONTENT : begin -->
						<div id="page-content">
							<div class="page-content-inner">

								<!-- NOTICE LIST PAGE : begin -->
								<div class="notice-list-page notice-page">

									<!-- NOTICE : begin -->
									<article class="notice">
										<div class="notice-inner c-content-box m-no-padding">

											<!-- NOTICE CORE : begin -->
											
											<div class="notice-core">
											<div class="row">
												<iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d15387.085631550828!2d74.01845376464023!3d15.388861517348836!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x3bbfa55468fd10ff%3A0xddc81bf090e32acd!2sBethora%20Industrial%20Estate%2C%20Betora%2C%20Goa!5e0!3m2!1sen!2sin!4v1584615078941!5m2!1sen!2sin" width="600" height="300" frameborder="0" style="border:0;" allowfullscreen="" aria-hidden="false" tabindex="0"></iframe>
												</div>
												<div class="row">
												 <div class="col-md-8">
												<form action="add_contact" method="Post">
												<div class="form-group">
												  <label for="name">Name:</label>
												  <input type="text" class="form-control" id="name" placeholder="Enter your name" name="name" required="name">
												</div>
												<div class="form-group">
												  <label for="number">Mobile No:</label>
												  <input type="text" class="form-control" id="mobile_no" placeholder="Enter your number" name="mobile_no" required="mobile_no">
												</div>
												 <div class="form-group">
												  <label for="email">Email:</label>
												  <input type="email" class="form-control" id="email" placeholder="Enter your email" name="email" required="email">
												</div>
												<div class="form-group">
												  <label for="subject">subject:</label>
												  <input type="text" class="form-control" id="subject" placeholder="Enter subject" name="subject" required="subject">
												</div>
												 <div class="form-group">
												  <label for="Message">Message:</label>
												<textarea id="message" rows="2" cols="50"  placeholder="Write something here..." name="message" required="message">
												</textarea>
												</div>
												<button type="submit" class="submit-btn">Send</button>
												</form>

												</div>
												<div class="col-md-4">
													<center>
													<img src="{{asset('public/frontend/images/dummy.png')}}" height="250px" width="170px" alt="name"><br>
													<br>
													<p>Office Village Panchayat,<br>
													Bethora, NH-4A,<br>
													Ponda, Goa(403409)<br>
													vpbethora@gmail.com<br>
													 +91 777777777</p>
													</center>
												</div>
												</div>
											</div>
											
											<!-- NOTICE CORE : end -->
										</div>
									</article>
									<!-- NOTICE : end -->
								</div>
								<!-- NOTICE LIST PAGE : begin -->
							</div>
						</div>
						<!-- PAGE CONTENT : end -->



						<hr class="c-separator m-margin-top-small m-margin-bottom-small m-transparent hidden-lg hidden-md">

					</div>
					<!-- MIDDLE COLUMN : end -->

					<!-- LEFT COLUMN : begin -->
					<div class="left-column col-md-3 col-md-pull-6">

						<?php include('layouts/left_menu.php');?>

						<!-- LEFT SIDEBAR : begin -->
						<aside class="sidebar">
							<div class="widget-list">

								<?php include('layouts/gallery_widget.php');?>
								<?php include('layouts/document_widget.php');?>

							</div>
						</aside>
						<!-- LEFT SIDEBAR : end -->

					</div>
					<!-- LEFT COLUMN : end -->

					<!-- RIGHT COLUMN : begin -->
					<div class="right-column col-md-3">

						<!-- RIGHT SIDEBAR : begin -->
						<aside class="sidebar">
							<div class="widget-list">

								<?php include('layouts/right_category.php');?>

							<?php include('layouts/right_image.php');?>

							</div>
						</aside>
						<!-- RIGHT SIDEBAR : end -->

					</div>
					<!-- RIGHT COLUMN : end -->

				</div>
			</div>
		</div>
		<!-- CORE : end -->
