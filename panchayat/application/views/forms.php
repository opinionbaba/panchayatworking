<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?>


		<!-- CORE : begin -->
		<div id="core">
			<div class="c-container">
				<div class="row">

					<!-- MIDDLE COLUMN : begin -->
					<div class="middle-column col-md-6 col-md-push-3">

						<!-- PAGE HEADER : begin -->
						<div id="page-header" class="m-has-breadcrumbs">

							<!-- PAGE TITLE : begin -->
							<div class="page-title">
								<h1>Forms</h1>
							</div>
							<!-- PAGE TITLE : end -->

							<!-- BREADCRUMBS : begin -->
							<div class="breadcrumbs">
								<ul>
									<li class="home"><a href="{{('/')}}">Home</a></li>
									<li>Forms</li>
								</ul>
							</div>
							<!-- BREADCRUMBS : end -->

						</div>
						<!-- PAGE HEADER : end -->

						<!-- PAGE CONTENT : begin -->
						<div id="page-content">
							<div class="page-content-inner">
								<div class="c-content-box">

									<h2>VILLAGE PANCHAYAT</h2>
									<p>The Right to Information Act (RTI) is an Act of the Parliament of India “to provide for setting out the practical regime of right to information for citizens” and replaces the erstwhile Freedom of information Act, 2002. The Act applies to all States and Union Territories of India except Jammu & Kashmir. Under the provisions of the Act, any citizen may request information from a “public authority” (a body of Government or “instrumentality of State”) which is required to reply expeditiously or within thirty days. The Act also requires every public authority to computerise their records for wide dissemination and to proactively certain categories of information so that the citizens need minimum recourse to request for information formally. This law was passed by Parliament on 15 June 2005 and came fully into force on 12 October 2005, which was Vijayadashmi. The first application was given to a Pune police station. Information disclosure in India was restricted by the Official Secrets Act 1923 and various other special laws, which the new RTI Act relaxes. It codifies a fundamental right of citizens.</p>
								<blockquote style="font-size: 17px !important;">

								Name Of PIO: Gokuldas Kudalkar<br>
							    Designation: VP Secetary
								</blockquote>

									<!-- TABS : begin
									<div class="c-tabs">
										<div class="c-content-box">
											<div class="tabs-inner">
												<ul class="tab-list">
													<li class="tab m-active">
														<span class="tab-label">From 1731 to 1734</span>
													</li>
													<li class="tab">
														<span class="tab-label">Through the 1763</span>
													</li>
													<li class="tab">
														<span class="tab-label">On March 20, 1764</span>
													</li>
												</ul>
												<ul class="content-list">
													<li class="tab-content m-active">
														<p>From 1731 to 1734, the French constructed Fort St. Frédéric, which gave the French control of the New France/Vermont frontier region in the Lake Champlain Valley. With the outbreak of the French and Indian War in 1754, the North American front of the Seven Years’ War between the French 8and English, the French began construction of Fort Carillon at present-day Ticonderoga, New York in 1755. The British failed to take Fort St. Frédéric or Fort Carillon between 1755 and 1758. In 1759, a combined force of 12,000 British regular and provincial troops under Sir Jeffery Amherst captured Carillon, after which the French abandoned Fort St. Frédéric. Amherst constructed Fort Crown Point next to the remains of the Fort St. Frédéric, securing British control over the area.</p>
													</li>
													<li class="tab-content">
														<p>Following France’s loss in the French and Indian War, through the 1763 Treaty of Paris they ceded control of the land to the British. Colonial settlement was limited by the Crown to lands east of the Appalachians, in order to try to end encroachment on Native American lands. The territory of Vermont was divided nearly in half in a jagged line running from Fort William Henry in Lake George diagonally north-eastward to Lake Memphremagog. With the end of the war, new settlers arrived in Vermont. Ultimately, Massachusetts, New Hampshire and New York all claimed this frontier area.</p>
													</li>
													<li class="tab-content">
														<p>On March 20, 1764, King George III established the boundary between New Hampshire and New York along the west bank of the Connecticut River, north of Massachusetts, and south of 45 degrees north latitude. In 1770, Ethan Allen, his brothers Ira and Levi, and Seth Warner, recruited an informal militia known as the Green Mountain Boys to protect the interests of the original New Hampshire settlers against newcomers from New York.</p>
													</li>
												</ul>
											</div>
										</div>
									</div-->
								</div>
							</div>
						</div>
						<!-- PAGE CONTENT : end -->



						<hr class="c-separator m-margin-top-small m-margin-bottom-small m-transparent hidden-lg hidden-md">

					</div>
					<!-- MIDDLE COLUMN : end -->

					<!-- LEFT COLUMN : begin -->
					<div class="left-column col-md-3 col-md-pull-6">

						<?php include('layouts/left_menu.php');?>

						<!-- LEFT SIDEBAR : begin -->
						<aside class="sidebar">
							<div class="widget-list">

								<?php include('layouts/gallery_widget.php');?>
								<?php include('layouts/document_widget.php');?>

							</div>
						</aside>
						<!-- LEFT SIDEBAR : end -->

					</div>
					<!-- LEFT COLUMN : end -->

					<!-- RIGHT COLUMN : begin -->
					<div class="right-column col-md-3">

						<!-- RIGHT SIDEBAR : begin -->
						<aside class="sidebar">
							<div class="widget-list">

								<?php include('layouts/right_category.php');?>

							<?php include('layouts/right_image.php');?>

							</div>
						</aside>
						<!-- RIGHT SIDEBAR : end -->

					</div>
					<!-- RIGHT COLUMN : end -->

				</div>
			</div>
		</div>
		<!-- CORE : end -->
