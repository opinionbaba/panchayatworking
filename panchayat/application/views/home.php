<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?>



<!-- CORE : begin -->
		<div id="core">
			<div class="c-container">
				<div class="row">

					<!-- MIDDLE COLUMN : begin -->
					<div class="middle-column col-md-6 col-md-push-3">

						<!-- PAGE HEADER : begin -->
						<div id="page-header">

							<!-- PAGE TITLE : begin -->
							<div class="page-title">
								<h1>Welcome to TownPress,<br><em>the Most Exciting Town of Northeast!</em></h1>
							</div>
							<!-- PAGE TITLE : end -->

						</div>
						<!-- PAGE HEADER : end -->

						<!-- PAGE CONTENT : begin -->
						<div id="page-content">
							<div class="page-content-inner">

								<!-- DIRECTORY : begin -->
								<!-- You can choose to have 2, 3 or 4 columns in Directory element.
								To change the number of columns, change the class in the following element from m-3-columns to m-2-columns or m-4-columns -->
								<div class="c-directory m-has-icon m-3-columns">
									<div class="c-content-box">
										<div class="directory-inner">
											<i class="ico-shadow tp tp-road-sign"></i>
											<h2 class="directory-title"><i class="ico tp tp-road-sign"></i>Choose <strong>Your Interest</strong></h2>
											<div class="directory-content">
												<nav class="directory-menu">
													<ul>
														<li><a href="town-hall.php">Government</a>
															<ul>
																<li><a href="town-hall.php">Town Hall</a></li>
																<li><a href="town-council.php">Town Council</a></li>
																<li><a href="home-2.php">Alternative Home</a></li>
															</ul>
														</li>
														<li><a href="post-list.php">Community</a>
															<ul>
																<li><a href="post-list.php">TownPress News</a></li>
																<li><a href="post-list.php">Public Notices</a></li>
																<li><a href="document-list.php">Town Documents</a></li>
															</ul>
														</li>
														<li><a href="statistics.php">About Our Town</a>
															<ul>
																<li><a href="statistics.php">Statistics</a></li>
																<li><a href="town-history.php">Town History</a></li>
																<li><a href="virtual-tour.php">Virtual Tour</a></li>
															</ul>
														</li>
														<li><a href="event-list.php">Relax</a>
															<ul>
																<li><a href="event-list.php">Upcoming Events</a></li>
																<li><a href="gallery-list-html.php">Photo Galleries</a></li>
																<li><a href="#">Facebook Page</a></li>
															</ul>
														</li>
														<li><a href="contact.php">Get In Touch</a>
															<ul>
																<li><a href="contact.php">Write To Mayor</a></li>
																<li><a href="phone-numbers-2.php">Phone Numbers</a></li>
																<li><a href="#">Twitter Profile</a></li>
															</ul>
														</li>
														<li><a href="http://themeforest.net/user/LSVRthemes/portfolio">TownPress Template</a>
															<ul>
																<li><a href="http://themeforest.net/user/LSVRthemes/portfolio">Purchase TownPress</a></li>
																<li><a href="http://themeforest.net/user/LSVRthemes/portfolio">WordPress Version</a></li>
																<li><a href="http://demos.lsvr.sk/townpress.php/documentation/">Documentation</a></li>
															</ul>
														</li>
													</ul>
												</nav>
											</div>
										</div>
									</div>
								</div>
								<!-- DIRECTORY : end -->

								<!-- POST LIST : begin -->
								<div class="c-post-list m-has-icon">
									<div class="c-content-box">
										<div class="post-list-inner">
											<i class="ico-shadow tp tp-reading"></i>
											<h2 class="post-list-title"><i class="ico tp tp-reading"></i><a href="post-list.php">TownPress <strong>News</strong></a></h2>
											<div class="post-list-content">

												<!-- FEATURED POST : begin -->
												<article class="featured-post m-has-thumb">
													<div class="post-image">
														<a href="post-detail.php"><img src="<?=base_url();?>/assets/images/post-01-cropped.jpg" alt=""></a>
													</div>
													<div class="post-core">
														<h3 class="post-title">
															<a href="post-detail.php">Report from Friendly Soccer Match between TownPress and PressTown</a>
														</h3>
														<div class="post-date"><i class="ico tp tp-clock2"></i>August 23, 2015</div>
														<div class="post-excerpt">
															<p>Football refers to a number of sports that involve, to varying degrees, kicking a ball with the foot to score a goal. Unqualified, the word football is understood to refer to whichever form of football is the most popular in the regional context in which the word appears.</p>
														</div>
													</div>
												</article>
												<!-- FEATURED POST : end -->

												<!-- POST : begin -->
												<article class="post">
													<h3 class="post-title">
														<a href="post-detail.php">New Housing Complex in TownPress Nearly Complete</a>
													</h3>
													<div class="post-date">July 6, 2015</div>
												</article>
												<!-- POST : end -->

												<!-- POST : begin -->
												<article class="post">
													<h3 class="post-title">
														<a href="post-detail.php">This Year’s Summer Rock Festival Draws More Than 1000 Fans</a>
													</h3>
													<div class="post-date">June 15, 2015</div>
												</article>
												<!-- POST : end -->

												<!-- POST : begin -->
												<article class="post">
													<h3 class="post-title">
														<a href="post-detail.php">10 Things You Didn’t Know About our Town</a>
													</h3>
													<div class="post-date">June 2, 2015</div>
												</article>
												<!-- POST : end -->

												<!-- POST : begin -->
												<article class="post">
													<h3 class="post-title">
														<a href="post-detail.php">Report From Monday’s Financial Town Meeting</a>
													</h3>
													<div class="post-date">May 14, 2015</div>
												</article>
												<!-- POST : end -->

												<p class="more-btn-holder"><a href="post-list.php">Read All Posts</a></p>

											</div>
										</div>
									</div>
								</div>
								<!-- POST LIST : end -->

							</div>
						</div>
						<!-- PAGE CONTENT : end -->

						<hr class="c-separator m-margin-top-small m-margin-bottom-small m-transparent hidden-lg hidden-md">

					</div>
					<!-- MIDDLE COLUMN : end -->

					<!-- LEFT COLUMN : begin -->
					<div class="left-column col-md-3 col-md-pull-6">
	<?php include('layouts/left_menu.php');?>
						<!-- LEFT SIDEBAR : begin -->
						<aside class="sidebar">
							<div class="widget-list">

<?php include('layouts/document_widget.php');?>


<?php include('layouts/weather_widget.php');?>

<?php include('layouts/gallery_widget.php');?>
								
								
							

							</div>
						</aside>
						<!-- LEFT SIDEBAR : end -->

					</div>
					<!-- LEFT COLUMN : end -->

					<!-- RIGHT COLUMN : begin -->
					<div class="right-column col-md-3">

						<!-- RIGHT SIDEBAR : begin -->
						<aside class="sidebar">
							<div class="widget-list">

		<?php include('layouts/right_notice.php');?>
		
		
		<?php include('layouts/right_image.php');?>
		
		
		<?php include('layouts/right_event.php');?>
								
								

								

							</div>
						</aside>
						<!-- RIGHT SIDEBAR : end -->

					</div>
					<!-- RIGHT COLUMN : end -->

				</div>
			</div>
		</div>
		<!-- CORE : end -->

