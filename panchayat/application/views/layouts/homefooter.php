<?php
defined('BASEPATH') OR exit('No direct script access allowed'); 
?>
<!DOCTYPE html>
<html>
  
<!-- FOOTER : begin -->
        <footer id="footer" class="m-has-bg">
            <div class="footer-bg">
                <div class="footer-inner">

                    <!-- FOOTER TOP : begin -->
                    <div class="footer-top">
                        <div class="c-container">

                            <!-- BOTTOM PANEL : begin -->
                            <div id="bottom-panel">
                                <div class="bottom-panel-inner">
                                    <div class="row">
                                        <div class="col-md-3">

                                            <!-- TEXT WIDGET : begin -->
                                            <div class="widget">
                                                <hr class="c-separator m-transparent hidden-lg hidden-md">
                                                <div class="widget-inner">
                                                    <h3 class="widget-title">About TownPress</h3>
                                                    <div class="widget-content">
                                                        <p>TownPress is a premium Municipality HTML template. It is best suited to be used as a presentation site for small towns or villages.</p>
                                                        <p>You can buy this responsive HTML template on <a href="http://themeforest.net/user/LSVRthemes/portfolio">ThemeForest</a>.</p>
                                                    </div>
                                                </div>
                                            </div>
                                            <!-- TEXT WIDGET : end -->

                                        </div>
                                        <div class="widget-col col-md-3">

                                            <!-- DEFINITION LIST WIDGET : begin -->
                                            <div class="widget definition-list-widget">
                                                <hr class="c-separator m-transparent hidden-lg hidden-md">
                                                <div class="widget-inner">
                                                    <h3 class="widget-title m-has-ico"><i class="widget-ico tp tp-telephone"></i>Phone Numbers</h3>
                                                    <div class="widget-content">
                                                        <dl>
                                                            <dt>Town Clerk</dt>
                                                            <dd>(123) 456-7890</dd>
                                                            <dt>State Police</dt>
                                                            <dd>(123) 456-7891</dd>
                                                            <dt>Fire Department</dt>
                                                            <dd>(123) 456-7892</dd>
                                                        </dl>
                                                        <p class="show-all-btn"><a href="phone-numbers.php">See All Phone Numbers</a></p>
                                                    </div>
                                                </div>
                                            </div>
                                            <!-- DEFINITION LIST WIDGET : end -->

                                        </div>
                                        <div class="widget-col col-md-3">

                                            <!-- MAILCHIMP SUBSCRIBE WIDGET : begin -->
                                            <!-- Please read the documentation to learn how to configure Mailchimp Subscribe widget -->
                                            <div class="widget mailchimp-subscribe-widget">
                                                <hr class="c-separator m-transparent hidden-lg hidden-md">
                                                <div class="widget-inner">
                                                    <h3 class="widget-title m-has-ico"><i class="widget-ico tp tp-at-sign"></i>Join Our Newsletter</h3>
                                                    <div class="widget-content">

                                                        <!-- Add your custom form URL into "action" attribute in following element -->
                                                        <form class="mailchimp-subscribe-form" method="get"
                                                            action="http://lsvr.us14.list-manage.com/subscribe/post-json?u=8291218baaf54ddfd7dbc6016&amp;id=f3e5d696dc&amp;c=?">
                                                            <div class="subscribe-inner">

                                                                <div class="description">
                                                                    <p>Join our newsletter to receive up to date news about our municipality.</p>
                                                                </div>

                                                                <!-- VALIDATION ERROR MESSAGE : begin -->
                                                                <p class="c-alert-message m-warning m-validation-error" style="display: none;"><i class="ico fa fa-exclamation-circle"></i>
                                                                <span>Your email address is required.</span></p>
                                                                <!-- VALIDATION ERROR MESSAGE : end -->

                                                                <!-- SENDING REQUEST ERROR MESSAGE : begin -->
                                                                <p class="c-alert-message m-warning m-request-error" style="display: none;"><i class="ico fa fa-exclamation-circle"></i>
                                                                <span>There was a connection problem. Try again later.</span></p>
                                                                <!-- SENDING REQUEST ERROR MESSAGE : end -->

                                                                <!-- SUCCESS MESSAGE : begin -->
                                                                <p class="c-alert-message m-success" style="display: none;"><i class="ico fa fa-check-circle"></i>
                                                                <span><strong>Form sent successfully!</strong></span></p>
                                                                <!-- SUCCESS MESSAGE : end -->

                                                                <div class="form-fields">
                                                                    <input type="text" placeholder="Your Email Address" name="EMAIL" class="m-required m-email">
                                                                    <button title="Subscribe" type="submit" class="submit-btn">
                                                                        <i class="fa fa-chevron-right"></i>
                                                                        <i class="fa fa-spinner fa-spin"></i>
                                                                    </button>
                                                                </div>

                                                            </div>
                                                        </form>

                                                    </div>
                                                </div>
                                            </div>
                                            <!-- MAILCHIMP SUBSCRIBE WIDGET : end -->

                                        </div>
                                        <div class="widget-col col-md-3">

                                            <!-- TEXT WIDGET : begin -->
                                            <div class="widget">
                                                <hr class="c-separator m-transparent hidden-lg hidden-md">
                                                <div class="widget-inner">
                                                    <h3 class="widget-title m-has-ico"><i class="widget-ico tp tp-envelope"></i>Town Hall Address</h3>
                                                    <div class="widget-content">
                                                        <p>P.O. Box 123 TownPress<br>VT 12345</p>
                                                        <p>Phone: (123) 456-7890<br>Fax: (123) 456-7891<br>
                                                        Email: <a href="#">townhall@townpress.gov</a></p>
                                                    </div>
                                                </div>
                                            </div>
                                            <!-- TEXT WIDGET : end -->

                                        </div>
                                    </div>
                                </div>
                            </div>
                            <!-- BOTTOM PANEL : end -->

                        </div>
                    </div>
                    <!-- FOOTER TOP : end -->

                    <!-- FOOTER BOTTOM : begin -->
                    <div class="footer-bottom">
                        <div class="footer-bottom-inner">
                            <div class="c-container">

                                <!-- FOOTER SOCIAL : begin -->
                                <!-- Here is the list of some popular social networks. There are more icons you can use, just refer to the documentation -->
                                <div class="footer-social">
                                    <ul class="c-social-icons">
                                        <li class="ico-twitter"><a href="#"><i class="fa fa-twitter"></i></a></li>
                                        <li class="ico-facebook"><a href="#"><i class="fa fa-facebook"></i></a></li>
                                        <li class="ico-googleplus"><a href="#"><i class="fa fa-google-plus"></i></a></li>
                                        <li class="ico-instagram"><a href="#"><i class="fa fa-instagram"></i></a></li>
                                        <!--li><a href="#"><i class="fa fa-envelope"></i></a></li-->
                                        <!--li class="ico-angellist"><a href="#"><i class="fa fa-angellist"></i></a></li-->
                                        <!--li class="ico-behance"><a href="#"><i class="fa fa-behance"></i></a></li-->
                                        <!--li class="ico-bitbucket"><a href="#"><i class="fa fa-bitbucket"></i></a></li-->
                                        <!--li class="ico-bitcoin"><a href="#"><i class="fa fa-bitcoin"></i></a></li-->
                                        <!--li class="ico-codepen"><a href="#"><i class="fa fa-codepen"></i></a></li-->
                                        <!--li class="ico-delicious"><a href="#"><i class="fa fa-delicious"></i></a></li-->
                                        <!--li class="ico-deviantart"><a href="#"><i class="fa fa-deviantart"></i></a></li-->
                                        <!--li class="ico-digg"><a href="#"><i class="fa fa-digg"></i></a></li-->
                                        <!--li class="ico-dribbble"><a href="#"><i class="fa fa-dribbble"></i></a></li-->
                                        <!--li class="ico-dropbox"><a href="#"><i class="fa fa-dropbox"></i></a></li-->
                                        <!--li class="ico-flickr"><a href="#"><i class="fa fa-flickr"></i></a></li-->
                                        <!--li class="ico-foursquare"><a href="#"><i class="fa fa-foursquare"></i></a></li-->
                                        <!--li class="ico-git"><a href="#"><i class="fa fa-git"></i></a></li-->
                                        <!--li class="ico-github"><a href="#"><i class="fa fa-github"></i></a></li-->
                                        <!--li class="ico-lastfm"><a href="#"><i class="fa fa-lastfm"></i></a></li-->
                                        <!--li class="ico-linkedin"><a href="#"><i class="fa fa-linkedin"></i></a></li-->
                                        <!--li class="ico-paypal"><a href="#"><i class="fa fa-paypal"></i></a></li-->
                                        <!--li class="ico-pinterest"><a href="#"><i class="fa fa-pinterest"></i></a></li-->
                                        <!--li class="ico-reddit"><a href="#"><i class="fa fa-reddit"></i></a></li-->
                                        <!--li class="ico-skype"><a href="#"><i class="fa fa-skype"></i></a></li-->
                                        <!--li class="ico-soundcloud"><a href="#"><i class="fa fa-soundcloud"></i></a></li-->
                                        <!--li class="ico-spotify"><a href="#"><i class="fa fa-spotify"></i></a></li-->
                                        <!--li class="ico-steam"><a href="#"><i class="fa fa-steam"></i></a></li-->
                                        <!--li class="ico-trello"><a href="#"><i class="fa fa-trello"></i></a></li-->
                                        <!--li class="ico-tumblr"><a href="#"><i class="fa fa-tumblr"></i></a></li-->
                                        <!--li class="ico-twitch"><a href="#"><i class="fa fa-twitch"></i></a></li-->
                                        <!--li class="ico-vimeo"><a href="#"><i class="fa fa-vimeo"></i></a></li-->
                                        <!--li class="ico-vine"><a href="#"><i class="fa fa-vine"></i></a></li-->
                                        <!--li class="ico-vk"><a href="#"><i class="fa fa-vk"></i></a></li-->
                                        <!--li class="ico-wordpress"><a href="#"><i class="fa fa-wordpress"></i></a></li-->
                                        <!--li class="ico-xing"><a href="#"><i class="fa fa-xing"></i></a></li-->
                                        <!--li class="ico-yahoo"><a href="#"><i class="fa fa-yahoo"></i></a></li-->
                                        <!--li class="ico-yelp"><a href="#"><i class="fa fa-yelp"></i></a></li-->
                                        <!--li class="ico-youtube"><a href="#"><i class="fa fa-youtube"></i></a></li-->
                                    </ul>
                                </div>
                                <!-- FOOTER SOCIAL : end -->

                                <!-- FOOTER MENU : begin -->
                                <nav class="footer-menu">
                                    <ul>
                                        <li><a href="index-2.php">Home</a></li>
                                        <li><a href="http://demos.lsvr.sk/townpress.php/documentation/">Documentation</a></li>
                                        <li><a href="http://themeforest.net/user/LSVRthemes/portfolio">Purchase</a></li>
                                    </ul>
                                </nav>
                                <!-- FOOTER MENU : end -->

                                <!-- FOOTER TEXT : begin -->
                                <div class="footer-text">
                                    <p>You can purchase TownPress Municipality HTML template on <a href="http://themeforest.net/user/LSVRthemes/portfolio">ThemeForest.net</a></p>
                                </div>
                                <!-- FOOTER TEXT : end -->

                            </div>
                        </div>
                    </div>
                    <!-- FOOTER BOTTOM : end -->

                </div>
            </div>
        </footer>
        <!-- FOOTER : end -->

        <!-- SCRIPTS : begin -->
        <script src="<?=base_url();?>/assets/library/js/jquery-1.9.1.min.js" type="text/javascript"></script>
        <script src="<?=base_url();?>/assets/library/js/third-party.js" type="text/javascript"></script>
        <script src="<?=base_url();?>/assets/library/js/library.js" type="text/javascript"></script>
        <script src="<?=base_url();?>/assets/library/js/scripts.js" type="text/javascript"></script>
        <!-- SCRIPTS : end -->

    </body>
</html>
 
	




