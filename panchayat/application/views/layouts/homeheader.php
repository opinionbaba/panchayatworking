<?php
defined('BASEPATH') OR exit('No direct script access allowed'); 
?>
<!DOCTYPE html>
<html>
<head>
    <meta http-equiv="content-type" content="text/html; charset=utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <title><?php $title;?></title>
        <link rel="shortcut icon" href="<?=base_url();?>/assets/images/favicon.png">

    <!-- GOOGLE FONTS : begin -->
    <link href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:400,400italic,700,700italic&amp;subset=latin,latin-ext" rel="stylesheet" type="text/css">
    <!-- GOOGLE FONTS : end -->

        <!-- STYLESHEETS : begin -->
    <link rel="stylesheet" type="text/css" href="<?=base_url();?>/assets/library/css/style.css">
    <!-- To change to a different pre-defined color scheme, change "red.css" in the following element to blue.css | bluegray.css | green.css | orange.css
    Please refer to the documentation to learn how to create your own color schemes -->
        <link rel="stylesheet" type="text/css" href="<?=base_url();?>/assets/library/css/skin/red.css">
    <link rel="stylesheet" type="text/css" href="<?=base_url();?>/assets/library/css/custom.css">
    <!--[if lte IE 9]>
    <link rel="stylesheet" type="text/css" href="library/css/oldie.css">
    <![endif]-->
    <!-- STYLESHEETS : end -->

  </head>
  <body>
 <style>
	.submit{
    background: #ec5237;
    height: 39px;
    width: 88px;
	color: #fff;
	}
	</style>
	
	
<!-- HEADER : begin -->
        <!-- If you want to use Standard Header Menu, add "m-has-standard-menu" class to the following element (see home-2.php template for example)
        Remove "m-has-gmap" class if you are not using Google Map in this template -->
        <header id="header" class="m-has-header-tools m-has-gmap">
            <div class="header-inner">

                <!-- HEADER CONTENT : begin -->
                <div class="header-content">
                    <div class="c-container">
                        <div class="header-content-inner">

                            <!-- HEADER BRANDING : begin -->
                            <!-- Logo dimensions can be changed in library/css/custom.css
                            You can remove "m-large-logo" class from following element to use standard (smaller) version of logo -->
                            <div class="header-branding m-large-logo">
                                <a href="<?=base_url();?>"><span>
                                    <img src="<?=base_url();?>/assets/images/header-logo.png"
                                        data-hires="images/header-logo.2x.png"
                                        alt="TownPress - Municipality HTML Template">
                                </span></a>
                            </div>
                            <!-- HEADER BRANDING : end -->

                            <!-- HEADER TOGGLE HOLDER : begin -->
                            <div class="header-toggle-holder">

                                <!-- HEADER TOGGLE : begin -->
                                <button type="button" class="header-toggle">
                                    <i class="ico-open tp tp-menu"></i>
                                    <i class="ico-close tp tp-cross"></i>
                                    <span>Menu</span>
                                </button>
                                <!-- HEADER TOGGLE : end -->

                                <!-- HEADER GMAP SWITCHER : begin -->
                                <button type="button" class="header-gmap-switcher" title="Show on Map">
                                    <i class="ico-open tp tp-map2"></i>
                                    <i class="ico-close tp tp-cross"></i>
                                </button>
                                <!-- HEADER GMAP SWITCHER : end -->

                            </div>
                            <!-- HEADER TOGGLE HOLDER : end -->

                            <!-- HEADER MENU : begin -->
                            <!-- This menu is used as both mobile menu (displayed on devices with screen width < 991px)
                            and standard header menu (only if Header element has "m-has-standard-menu" class) -->
                            <nav class="header-menu">
							
                             <?php include('menu.php');?>
						   </nav>
                            <!-- HEADER MENU : end -->

                            <!-- HEADER TOOLS : begin -->
                            <div class="header-tools">

                                <!-- HEADER SEARCH : begin -->
                                <div class="header-search">
                                    <form method="get" action="http://demos.lsvr.sk/townpress.php/demo/search-results.php" class="c-search-form">
                                        <div class="form-fields">
                                            <input type="text" value="" placeholder="Search this site..." name="s">
                                            <button type="submit" class="submit-btn"><i class="tp tp-magnifier"></i></button>
                                        </div>
                                    </form>
                                </div>
                                <!-- HEADER SEARCH : end -->

                                <!-- HEADER GMAP SWITCHER : begin -->
                                <!-- Remove following block if you are not using Google Map in this template -->
                                <button type="button" class="header-gmap-switcher" title="Show on Map">
                                    <i class="ico-open tp tp-map2"></i>
                                    <i class="ico-close tp tp-cross"></i>
                                    <span>Map</span>
                                </button>
                                <!-- HEADER GMAP SWITCHER : end -->
								<button  id="one" class="button submit" >
									<i class="ico-open tp tp-map2"></i>
									<span>Login</span>
								</button>

                            </div>
                            <!-- HEADER TOOLS : end -->

                        </div>
                    </div>
                </div>
                <!-- HEADER CONTENT : end -->

                <!-- HEADER GMAP : begin -->
                <!-- Add your address into "data-address" attribute
                Change zoom level with "data-zoom" attribute, bigger number = bigger zoom
                Change map type with "data-maptype" attribute, values: roadmap | terrain | hybrid | satellite
                API KEY IS REQUIRED! More info https://developers.google.com/maps/documentation/javascript/get-api-key -->
                <div class="header-gmap">
                    <div class="gmap-canvas"
						data-google-api-key="AIzaSyCHctnM3xR2JUhnaQ5gLrIveomGIIbCS2w"
						data-address="Jakkur, Bengaluru, Karnataka "
						data-zoom="17"
						data-maptype="hybrid"
						data-enable-mousewheel="true">
					</div>
                </div>
                <!-- HEADER GMAP : end -->

            </div>
        </header>
        <!-- HEADER : end --> 
        <!-- HEADER BG : begin -->
        <div class="header-bg">

            <!-- HEADER IMAGE : begin -->
            <!-- To add more images just copy and edit elements with "image-layer" class (see home-2.php template for example)
            Change autoplay speed with "data-autoplay" attribute (in seconds), works only if there are more than one image -->
            <div class="header-image" data-autoplay="8">
                <div class="image-layer" style="background-image: url( '<?=base_url();?>/assets/images/header-01.jpg' );"></div>
                <!-- div class="image-layer" style="background-image: url( 'images/header-02.jpg' );"></div -->
            </div>
            <!-- HEADER IMAGE : begin -->

        </div>
        <!-- HEADER BG : end -->

	




