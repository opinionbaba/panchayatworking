<!-- NOTICES WIDGET : begin -->
								<div class="widget notices-widget">
									<div class="widget-inner">
										<h3 class="widget-title m-has-ico"><i class="widget-ico tp tp-bullhorn"></i>Town Notices</h3>
										<div class="widget-content">
											<ul class="notice-list">

												<!-- NOTICE : begin -->
												<li class="notice">
													<div class="notice-inner">
														<h4 class="notice-title"><a href="notice-detail.php">Annual Marathon Registration</a></h4>
														<span class="notice-date">May 14, 2015</span>
													</div>
												</li>
												<!-- NOTICE : end -->

												<!-- NOTICE : begin -->
												<li class="notice">
													<div class="notice-inner">
														<h4 class="notice-title"><a href="notice-detail.php">Traffic Safety Notice</a></h4>
														<span class="notice-date">July 19, 2015</span>
													</div>
												</li>
												<!-- NOTICE : end -->

												<!-- NOTICE : begin -->
												<li class="notice">
													<div class="notice-inner">
														<h4 class="notice-title"><a href="notice-detail.php">Municipal Waste</a></h4>
														<span class="notice-date">August 24, 2015</span>
													</div>
												</li>
												<!-- NOTICE : end -->

												<!-- NOTICE : begin -->
												<li class="notice">
													<div class="notice-inner">
														<h4 class="notice-title"><a href="notice-detail.php">Upcoming Public Meetings</a></h4>
														<span class="notice-date">October 18, 2015</span>
													</div>
												</li>
												<!-- NOTICE : end -->

											</ul>
											<p class="show-all-btn"><a href="notice-list.php">See All Notices</a></p>
										</div>
									</div>
								</div>
								<!-- NOTICES WIDGET : end -->
