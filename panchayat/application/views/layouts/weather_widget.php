<!-- LOCALE INFO WIDGET : begin -->
								<!-- to remove background image from this widget, simply remove "m-has-bg" class from the following element -->
								<div class="widget locale-info-widget m-has-bg">
									<div class="widget-inner">
										<h3 class="widget-title m-has-ico"><i class="widget-ico tp tp-map-marker"></i>Locale Info</h3>
										<div class="widget-content">
											<ul>
												<li>
													<div class="row-title"><h4>Country</h4></div>
													<div class="row-value">United States</div>
												</li>
												<li>
													<div class="row-title"><h4>State</h4></div>
													<div class="row-value">Vermont</div>
												</li>
												<li>
													<div class="row-title"><h4>County</h4></div>
													<div class="row-value">Lamoille</div>
												</li>
												<li>
													<div class="row-title"><h4>Area</h4></div>
													<div class="row-value">72.7 sq mi (188.4 km<sup>2</sup>)</div>
												</li>
												<li>
													<div class="row-title"><h4>Population</h4></div>
													<div class="row-value">4,314</div>
												</li>
												<li>
													<div class="row-title"><h4>Coordinates</h4></div>
													<div class="row-value">44°28′31″N<br>72°42′8″W</div>
												</li>
												<li>
													<div class="row-title"><h4>Time zone</h4></div>
													<div class="row-value">Eastern (EST) (UTC-5)</div>
												</li>
												<li>
													<div class="row-title"><h4>ZIP code</h4></div>
													<div class="row-value">05672</div>
												</li>
											</ul>
            							</div>
									</div>
								</div>
								<!-- LOCALE INFO WIDGET : end -->
