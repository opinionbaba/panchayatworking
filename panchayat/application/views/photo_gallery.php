<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?>


		<!-- CORE : begin -->
		<div id="core">
			<div class="c-container">
				<div class="row">

					<!-- MIDDLE COLUMN : begin -->
					<div class="middle-column col-md-6 col-md-push-3">

						<!-- PAGE HEADER : begin -->
						<div id="page-header" class="m-has-breadcrumbs">

							<!-- PAGE TITLE : begin -->
							<div class="page-title">
								<h1>Photo Gallery</h1>
							</div>
							<!-- PAGE TITLE : end -->

							<!-- BREADCRUMBS : begin -->
							<div class="breadcrumbs">
								<ul>
									<li class="home"><a href="{{('/')}}">Home</a></li>
									<li>Photo Gallery</li>
								</ul>
							</div>
							<!-- BREADCRUMBS : end -->

						</div>
						<!-- PAGE HEADER : end -->

						<!-- PAGE CONTENT : begin -->
						<div id="page-content">
							<div class="page-content-inner">

								<!-- GALLERY LIST PAGE : begin -->
								<div class="gallery-list-page gallery-page">
									<div class="c-gallery">
										<!-- You can change the number of columns by changing "m-3-columns" class
										in the following element to m-2-columns | m-4-columns | m-5-columns : begin -->
										<ul class="gallery-images m-layout-masonry m-3-columns">

											<!-- GALLERY IMAGE : begin -->
											<li class="gallery-image">
												<div class="gallery-image-inner">
													<a href="gallery-detail.html">
														<img src="<?php echo base_url();?>assets/images/gallery-01.jpg" alt="">
													</a>
													<h2 class="gallery-title"><a href="gallery-detail.html">Summer Camp for Kids</a></h2>
												</div>
											</li>
											<!-- GALLERY IMAGE : end -->

											<!-- GALLERY IMAGE : begin -->
											<li class="gallery-image">
												<div class="gallery-image-inner">
													<a href="gallery-detail.html">
														<img src="<?php echo base_url();?>assets/images/gallery-02.jpg" alt="">
													</a>
													<h2 class="gallery-title"><a href="gallery-detail.html">Annual Marathon</a></h2>
												</div>
											</li>
											<!-- GALLERY IMAGE : end -->

											<!-- GALLERY IMAGE : begin -->
											<li class="gallery-image">
												<div class="gallery-image-inner">
													<a href="gallery-detail.html">
														<img src="<?php echo base_url();?>assets/images/gallery-03.jpg" alt="">
													</a>
													<h2 class="gallery-title"><a href="gallery-detail.html">Summer Rock Festival</a></h2>
												</div>
											</li>
											<!-- GALLERY IMAGE : end -->

											<!-- GALLERY IMAGE : begin -->
											<li class="gallery-image">
												<div class="gallery-image-inner">
													<a href="gallery-detail.html">
														<img src="<?php echo base_url();?>assets/images/gallery-04.jpg" alt="">
													</a>
													<h2 class="gallery-title"><a href="gallery-detail.html">TownPress State Park</a></h2>
												</div>
											</li>
											<!-- GALLERY IMAGE : end -->

											<!-- GALLERY IMAGE : begin -->
											<li class="gallery-image">
												<div class="gallery-image-inner">
													<a href="gallery-detail.html">
														<img src="<?php echo base_url();?>assets/images/gallery-05.jpg" alt="">
													</a>
													<h2 class="gallery-title"><a href="gallery-detail.html">Friendly Soccer Match</a></h2>
												</div>
											</li>
											<!-- GALLERY IMAGE : end -->

											<!-- GALLERY IMAGE : begin -->
											<li class="gallery-image">
												<div class="gallery-image-inner">
													<a href="gallery-detail.html">
														<img src="<?php echo base_url();?>assets/images/gallery-06.jpg" alt="">
													</a>
													<h2 class="gallery-title"><a href="gallery-detail.html">TownPress Sightseeing</a></h2>
												</div>
											</li>
											<!-- GALLERY IMAGE : end -->

											<!-- GALLERY IMAGE : begin -->
											<li class="gallery-image">
												<div class="gallery-image-inner">
													<a href="gallery-detail.html">
														<img src="<?php echo base_url();?>assets/images/gallery-07.jpg" alt="">
													</a>
													<h2 class="gallery-title"><a href="gallery-detail.html">Streets of TownPress</a></h2>
												</div>
											</li>
											<!-- GALLERY IMAGE : end -->

											<!-- GALLERY IMAGE : begin -->
											<li class="gallery-image">
												<div class="gallery-image-inner">
													<a href="gallery-detail.html">
														<img src="<?php echo base_url();?>assets/images/gallery-08.jpg" alt="">
													</a>
													<h2 class="gallery-title"><a href="gallery-detail.html">Campgrounds</a></h2>
												</div>
											</li>
											<!-- GALLERY IMAGE : end -->

											<!-- GALLERY IMAGE : begin -->
											<li class="gallery-image">
												<div class="gallery-image-inner">
													<a href="gallery-detail.html">
														<img src="<?php echo base_url();?>assets/images/gallery-09.jpg" alt="">
													</a>
													<h2 class="gallery-title"><a href="gallery-detail.html">New Housing</a></h2>
												</div>
											</li>
											<!-- GALLERY IMAGE : end -->

										</ul>
									</div>
								</div>
								<!-- GALLERY LIST PAGE : begin -->

								
							</div>
						</div>
						<!-- PAGE CONTENT : end -->



						<hr class="c-separator m-margin-top-small m-margin-bottom-small m-transparent hidden-lg hidden-md">

					</div>
					<!-- MIDDLE COLUMN : end -->

					<!-- LEFT COLUMN : begin -->
					<div class="left-column col-md-3 col-md-pull-6">

						<?php include('layouts/left_menu.php');?>

						<!-- LEFT SIDEBAR : begin -->
						<aside class="sidebar">
							<div class="widget-list">

								<?php include('layouts/gallery_widget.php');?>
								<?php include('layouts/document_widget.php');?>

							</div>
						</aside>
						<!-- LEFT SIDEBAR : end -->

					</div>
					<!-- LEFT COLUMN : end -->

					<!-- RIGHT COLUMN : begin -->
					<div class="right-column col-md-3">

						<!-- RIGHT SIDEBAR : begin -->
						<aside class="sidebar">
							<div class="widget-list">

								<?php include('layouts/right_category.php');?>

							<?php include('layouts/right_image.php');?>

							</div>
						</aside>
						<!-- RIGHT SIDEBAR : end -->

					</div>
					<!-- RIGHT COLUMN : end -->

				</div>
			</div>
		</div>
		<!-- CORE : end -->
