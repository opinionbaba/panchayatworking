<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?>


		<!-- CORE : begin -->
		<div id="core">
			<div class="c-container">
				<div class="row">

					<!-- MIDDLE COLUMN : begin -->
					<div class="middle-column col-md-6 col-md-push-3">

						<!-- PAGE HEADER : begin -->
						<div id="page-header" class="m-has-breadcrumbs">

							<!-- PAGE TITLE : begin -->
							<div class="page-title">
								<h1>Schemes</h1>
							</div>
							<!-- PAGE TITLE : end -->

							<!-- BREADCRUMBS : begin -->
							<div class="breadcrumbs">
								<ul>
									<li class="home"><a href="<?=base_url();?>">Home</a></li>
									<li>Schemes</li>
								</ul>
							</div>
							<!-- BREADCRUMBS : end -->

						</div>
						<!-- PAGE HEADER : end -->

						<!-- PAGE CONTENT : begin -->
						<div id="page-content">
							<div class="page-content-inner">
								<div class="c-content-box">
<script>
$(document).ready(function(){
  $("button").click(function(){
    $(".table").toggle();
  });
});
</script>
</head>
<body>

<button>Toggle between hiding and showing the paragraphs</button>
						 <div class="row">
						 <div class="col-md-6">
								<div class="table table-bordered table-striped">
									<table class="table m-0">
										<tbody>
										<tr>
											<th>Full Name</th>
											<td>David Jhon</td>
										</tr>
										<tr>
											<th>Gender</th>
											<td>Male</td>
										</tr>
										<tr>
											<th>Birth Date</th>
											<td>April 12, 1990</td>
										</tr>
										<tr>
											<th>Marital Status</th>
											<td>Single</td>
										</tr>
										<tr>
											<th>Location</th>
											<td>London, UK</td>
										</tr>
										</tbody>
									</table>
								</div>
							</div>
							<div class="col-md-6">
								<div class="table table-bordered table-striped">
									<table class="table m-0">
										<tbody>
										<tr>
											<th>Full Name</th>
											<td>David Jhon</td>
										</tr>
										<tr>
											<th>Gender</th>
											<td>Male</td>
										</tr>
										<tr>
											<th>Birth Date</th>
											<td>April 12, 1990</td>
										</tr>
										<tr>
											<th>Marital Status</th>
											<td>Single</td>
										</tr>
										<tr>
											<th>Location</th>
											<td>London, UK</td>
										</tr>
										</tbody>
									</table>
								</div>
							</div>
							</div>

								</div>
							</div>
						</div>
						<!-- PAGE CONTENT : end -->


						<hr class="c-separator m-margin-top-small m-margin-bottom-small m-transparent hidden-lg hidden-md">

					</div>
					<!-- MIDDLE COLUMN : end -->

					<!-- LEFT COLUMN : begin -->
					<div class="left-column col-md-3 col-md-pull-6">

						<?php include('layouts/left_menu.php');?>

						<!-- LEFT SIDEBAR : begin -->
						<aside class="sidebar">
							<div class="widget-list">

								<?php include('layouts/gallery_widget.php');?>
								<?php include('layouts/document_widget.php');?>

							</div>
						</aside>
						<!-- LEFT SIDEBAR : end -->

					</div>
					<!-- LEFT COLUMN : end -->

					<!-- RIGHT COLUMN : begin -->
					<div class="right-column col-md-3">

						<!-- RIGHT SIDEBAR : begin -->
						<aside class="sidebar">
							<div class="widget-list">

								<?php include('layouts/right_category.php');?>

							<?php include('layouts/right_image.php');?>

							</div>
						</aside>
						<!-- RIGHT SIDEBAR : end -->

					</div>
					<!-- RIGHT COLUMN : end -->

				</div>
			</div>
		</div>
		<!-- CORE : end -->
