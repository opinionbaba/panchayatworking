$(document).ready(function(){
    $(".ajaxform").submit(function(e){
        e.preventDefault();
        var formval = $(this)[0];
        var data = new FormData(formval);
        var url = data.get('action-url');
        if(data !==null && url!==null)
            ajax(url,data);
        else alert("Filds are required");
    });
    /*updated*/
    $(".ajaxform2").submit(function(e){
        e.preventDefault();
        var formval = $(this)[0];
        var data = new FormData(formval);
        var url = data.get('action-url');
        var id = $(this).attr('id');
        if(data !==null && url!==null)
            ajax(url,data,id);
        else alert("Filds are required");
    });
    $(".delete").click(function(e){
        e.preventDefault();
        if(confirm("Delete This??")){
            var url = $(this).attr('href');
            $.ajax({
                url: url,
                method:"get",
                dataType: 'json',
                processData: false,
                contentType: false,
                cache: false,
                timeout: 6000,
                success: function(result){
                    if(result.status) {
                        $.toast({ text : result.message, bgColor : '#6610f2', position : 'bottom-right' });
                        setInterval(function(){ location.reload();}, 3000); 
                    }
                    else $.toast({ text : result.message, bgColor : '#e83e8c', position : 'bottom-right' });
                }
            })
        }
    });
    $('.toggle_btn').change(function() {
        var status  = 0;
        var id = $(this).attr('id');
        var url = $(this).attr('url');
        if($(this).prop('checked'))
            status = 1;
        else status = 0;
        $.ajax({url: url+'/'+status+'/'+id, dataType: 'json', success: function(result){
            if(result.status) 
                $.toast({ text : result.message, bgColor : '#6610f2', position : 'bottom-right' });
            else $.toast({ text : result.message, bgColor : '#e83e8c', position : 'bottom-right' });
        }});
    });
    /*-----------------------------------------------------------------------*/
    function ajax(url,data,id=""){
        $.ajax({
            url: url,
            method:"post",
            data:data,
            dataType: 'json',
            processData: false,
            contentType: false,
            cache: false,
            timeout: 6000,
            success: function(result){
                if(result.status) {
                    $.toast({ text : result.message, bgColor : '#6610f2', position : 'bottom-right' });
                    $("#"+id)[0].reset();
                    if(id==null)
                        setInterval(function(){ location.reload();}, 3000); 

                }
                else $.toast({ text : result.message, bgColor : '#e83e8c', position : 'bottom-right' }); 
            }
        });
    }

    
});