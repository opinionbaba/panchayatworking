@extends('layouts.app')
{{-- Page Title --}}
@section('page-title')
    Form Validation
@endsection
<body class="skin-blue fixed-layout">
    <!-- ============================================================== -->
    <!-- Preloader - style you can find in spinners.css -->
    <!-- ============================================================== -->
    <div class="preloader">
        <div class="loader">
            <div class="loader__figure"></div>
        </div>
    </div>
	 <div id="main-wrapper">
@section('main-content')

   <div class="page-wrapper">
            <!-- ============================================================== -->
            <!-- Container fluid  -->
            <!-- ============================================================== -->
            <div class="container-fluid">
                <!-- ============================================================== -->
                <!-- Bread crumb and right sidebar toggle -->
                <!-- ============================================================== -->
                <div class="row page-titles">
                    <div class="col-md-5 align-self-center">
                        <h4 class="text-themecolor">Add Document</h4>
                    </div>
                    <div class="col-md-7 align-self-center text-right">
                        <div class="d-flex justify-content-end align-items-center">
                            <ol class="breadcrumb">
                                <li class="breadcrumb-item"><a href="javascript:void(0)">Home</a></li>
                                <li class="breadcrumb-item active">Add Document</li>
                            </ol>
                        </div>
                    </div>
                </div>
				
				
				 <div class="row">
                    <div class="col-lg-12">
					   <div class="card">
                            <div class="card-header bg-info">
                                <h4 class="m-b-0 text-white">Add Document</h4>
                            </div>
                            <div class="card-body">
                                  <form action="{{route('admin.insert-form')}}" enctype="multipart/form-data" method="post" class="needs-validation" novalidate>
										@csrf
									   <div class="form-row">
											<div class="col-md-4 mb-3">
												<label for="validationTooltip01">Name Of Form</label>
												<input type="text" name="name" class="form-control" id="validationTooltip01" placeholder="First name" required>
												<div class="valid-tooltip">
													Looks good!
												</div>
											</div>
										   <div class="col-md-4 mb-3">
												<label  for="inputGroupFile02">Choose file</label>
												<input type="file" name="doc" class="form-control" id="inputGroupFile02">
										   </div>
										</div>
										<button class="btn btn-primary" type="submit">Submit form</button>
									</form>
                            </div>
                        </div>
					</div>
				 </div>
				
			</div>
	</div>
@endsection

@section('js')
    <!--=========================*
        This Page Scripts
*===========================-->
    <!-- Validation Script -->
    <script>
        //  Form Validation
        window.addEventListener('load', function() {
            // Fetch all the forms we want to apply custom Bootstrap validation styles to
            var forms = document.getElementsByClassName('needs-validation');
            // Loop over them and prevent submission
            var validation = Array.prototype.filter.call(forms, function(form) {
                form.addEventListener('submit', function(event) {
                    if (form.checkValidity() === false) {
                        event.preventDefault();
                        event.stopPropagation();
                    }
                    form.classList.add('was-validated');
                }, false);
            });
        }, false);
    </script>
@endsection
