@extends('layouts.app')


{{-- Page Title --}}
@section('page-title')
   Events
@endsection

@section('main-content')
    <div class="row">
    	<a href="{{route('admin.home')}}"><span class="badge badge-info mb-3">Home</span></a>&nbsp;
    	<a href="{{route('admin.allnotice')}}"><span class="badge badge-info mb-3">Notice</span></a>&nbsp;
    	<a href="{{route('admin.allevent')}}"><span class="badge badge-info mb-3">Events</span></a>&nbsp;
    	<a href="{{route('admin.allgallery')}}"><span class="badge badge-info mb-3">Gallery</span></a>&nbsp;
    	<a href="{{route('admin.alldoc')}}"><span class="badge badge-info mb-3">Forms</span></a>&nbsp;
    	<a href="{{route('admin.contact')}}"><span class="badge badge-info mb-3">Contact Us</span></a>&nbsp;
    	<a href="{{route('admin.subs')}}"><span class="badge badge-info mb-3">Subscribers</span></a>&nbsp;
    	<a href=""><span class="badge badge-info mb-3">House tax</span></a>&nbsp;
    	<a href="{{route('admin.allaudit')}}"><span class="badge badge-info mb-3">Audit</span></a>&nbsp;
     </div>
    <div class="row">
       @if(Session::has('success'))
        <div class="alert alert-warning alert-dismissible fade show" role="alert">
			<strong>Success!</strong> {{Session::get('success')}}
			<button type="button" class="close" data-dismiss="alert" aria-label="Close"><span class="ti-close"></span>
			</button>
		</div>
		@endif
        <div class="col-lg-12 mt-4">
            <div class="card">
                <div class="card-body">
                    <h4 class="card_title">Events</h4>
                    <form method="post" action="{{route('admin.save_event')}}" class="needs-validation" enctype ="multipart/form-data" >
					@csrf
                        <div class="form-row">
                            <div class="col-md-4 mb-3">
                                <label for="validationTooltip01">Name Of Event</label>
                                <input type="text" name="event_name" class="form-control" id="validationTooltip01" placeholder=""  required>
                                <div class="valid-tooltip">
                                    Looks good!
                                </div>
                            </div>
                            <div class="col-md-4 mb-3">
                                <label for="validationTooltip02">Event Category</label>
                              <select name="event_cat" class="form-control">
								<option value="">Select Category</option>
								<option value="1">Event</option>
								<option value="2">Culture</option>
								<option value="3">Resource</option>
							  </select>
                            </div>
							<div class="col-md-4 mb-3">
							    <label class="custom-file-label" for="inputGroupFile02">Choose file</label>
                                <input type="file" name="event_file" class="form-control" id="inputGroupFile02">
                           
                            </div>
						</div>
                       <button class="btn btn-primary" type="submit">Submit form</button>
                    </form>
                </div>
            </div>
        </div>
    </div>
@endsection

@section('js')
    <!--=========================*
        This Page Scripts
*===========================-->
    <!-- Validation Script -->
    <script>
        //  Form Validation
        window.addEventListener('load', function() {
            // Fetch all the forms we want to apply custom Bootstrap validation styles to
            var forms = document.getElementsByClassName('needs-validation');
            // Loop over them and prevent submission
            var validation = Array.prototype.filter.call(forms, function(form) {
                form.addEventListener('submit', function(event) {
                    if (form.checkValidity() === false) {
                        event.preventDefault();
                        event.stopPropagation();
                    }
                    form.classList.add('was-validated');
                }, false);
            });
        }, false);
    </script>
@endsection
