@extends('layouts.app')


{{-- Page Title --}}
@section('page-title')
    Upload Gallery
@endsection

{{-- This Page Css --}}
@section('css')
    <!--=========================*
               Dropzone
    *===========================-->
    <link rel="stylesheet" href="{{asset('public/assets/vendors/dropzone/css/dropzone.css')}}">
@endsection

@section('main-content')
<div class="row">
			<a href=""><span class="badge badge-info mb-3">Home</span></a>&nbsp;
			<a href="{{route('admin.allnotice')}}"><span class="badge badge-info mb-3">Notice</span></a>&nbsp;
			<a href="{{route('admin.allevent')}}"><span class="badge badge-info mb-3">Events</span></a>&nbsp;
			<a href="{{route('admin.allgallery')}}"><span class="badge badge-info mb-3">Gallery</span></a>&nbsp;
			<a href="{{route('admin.alldoc')}}"><span class="badge badge-info mb-3">Forms</span></a>&nbsp;
			<a href="{{route('admin.contact')}}"><span class="badge badge-info mb-3">Contact Us</span></a>&nbsp;
			<a href="{{route('admin.subs')}}"><span class="badge badge-info mb-3">Subscribers</span></a>&nbsp;
			<a href=""><span class="badge badge-info mb-3">House tax</span></a>&nbsp;
			<a href="{{route('admin.allaudit')}}"><span class="badge badge-info mb-3">Audit</span></a>&nbsp;
         </div>
    <div class="row">
	
        <div class="col-lg-12 mb-4">
            <div class="alert alert-info">
                This is just a demo dropzone. Selected files are <STRONG>not</STRONG> actually uploaded.
            </div>
        </div>
       
        <div class="col-lg-12 stretched_card mt-4">
            <div class="card">
                <div class="card-body">
                    <h4 class="card_title">File Type Validation Upload</h4>
                    <form action="#" class="dropzone dropzone-success" id="file-validation">
                        <div class="dz-default dz-message">
                            <span><i class="ti-image"></i></span>
                        </div>
						 <button class="btn btn-primary" type="submit">Submit form</button>
                    </form>
                </div>
            </div>
        </div>

    </div>
@endsection

@section('js')
    <!--=========================*
            This Page Scripts
    *===========================-->
    <!-- Dropzone Js -->
    <script src="{{asset('public/assets/vendors/dropzone/js/dropzone.js')}}"></script>

    <!-- Dropzone init Js -->
    <script src="{{asset('public/assets/js/init/dropzone.js')}}"></script>
@endsection
