@extends('layouts.app')


{{-- Page Title --}}
@section('page-title')
    Text Editors
@endsection

{{-- This Page Css --}}
@section('css')
    <!--=========================*
               Summernot
    *===========================-->
    <link rel="stylesheet" href="{{asset('public/assets/vendors/summernote/dist/summernote-bs4.css')}}">
@endsection

@section('main-content')
<div class="row">
			<a href=""><span class="badge badge-info mb-3">Home</span></a>&nbsp;
			<a href="{{route('admin.allnotice')}}"><span class="badge badge-info mb-3">Notice</span></a>&nbsp;
			<a href="{{route('admin.allevent')}}"><span class="badge badge-info mb-3">Events</span></a>&nbsp;
			<a href="{{route('admin.allgallery')}}"><span class="badge badge-info mb-3">Gallery</span></a>&nbsp;
			<a href="{{route('admin.alldoc')}}"><span class="badge badge-info mb-3">Forms</span></a>&nbsp;
			<a href="{{route('admin.contact')}}"><span class="badge badge-info mb-3">Contact Us</span></a>&nbsp;
			<a href="{{route('admin.subs')}}"><span class="badge badge-info mb-3">Subscribers</span></a>&nbsp;
			<a href=""><span class="badge badge-info mb-3">House tax</span></a>&nbsp;
			<a href="{{route('admin.allaudit')}}"><span class="badge badge-info mb-3">Audit</span></a>&nbsp;
         </div>
    <div class="row">
	
	@if(Session::has('success'))
        <div class="alert alert-warning alert-dismissible fade show" role="alert">
			<strong>Success!</strong>  {{Session::get('success')}}
			<button type="button" class="close" data-dismiss="alert" aria-label="Close"><span class="ti-close"></span>
			</button>
		</div>
		@endif
	  <div class="card-body">
			<h4 class="card_title">Notice</h4>
			<form action="{{route('admin.notice')}}" method="post" class="needs-validation" novalidate enctype="multipart/form-data">
			{{ csrf_field() }}
				<div class="form-row">
					<div class="col-md-4 mb-3">
						<input type="text" name="notice" class="form-control" id="validationTooltip01" placeholder="Enter name"  required>
						<div class="valid-tooltip">
							Looks good!
						</div>
					</div>
					<div class="col-md-4 mb-3">
						<label class="custom-file-label" for="inputGroupFile02">Choose file</label>
						<input type="file" name="image" class="form-control" id="inputGroupFile02">
				   </div>
				   <div class="col-md-4 mb-3">
						<select name="cat_id" class="form-control">
							<option value="">Select category</option>
							<option value="1">Notice 1 </option>
							<option value="2">Notice 2 </option>
							<option value="3"> Notice 3</option>
							<option value="4"> Notice 4</option>
							</select>
				   </div>
				</div>
				 <div class="col-lg-12">
					<div class="card">
						<div class="card-body">
							<h4 class="card_title">Notice Description</h4>
							<textarea name="editor1" class="ck-editor" placeholder="Enter Something.."></textarea>
						</div>
					</div>
				</div>
			   
				<button class="btn btn-primary" type="submit">Submit form</button>
			</form>
		</div>
		</div>
       
       
@endsection

@section('js')
    <!--=========================*
            This Page Scripts
    *===========================-->
    <!-- Ck Editor Js -->
    <script src="{{asset('public/assets/vendors/ck-editor/js/ckeditor.js')}}"></script>

    <script src="{{asset('public/assets/js/init/editors.js')}}"></script>
@endsection
