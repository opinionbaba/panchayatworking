@extends('layouts.app')


{{-- Page Title --}}
@section('page-title')
  Set Permissions
@endsection
{{-- This Page Css --}}
@section('css')
    <link rel="stylesheet" type="text/css"
        href="{{asset('public/assets/datatables.net-bs4/css/dataTables.bootstrap4.css')}}">
    <link rel="stylesheet" type="text/css"
        href="{{asset('public/assets/datatables.net-bs4/css/responsive.dataTables.min.css')}}">
		
  <body class="skin-blue fixed-layout">
    <!-- ============================================================== -->
    <!-- Preloader - style you can find in spinners.css -->
    <!-- ============================================================== -->
    <div class="preloader">
        <div class="loader">
            <div class="loader__figure"></div>
        </div>
    </div>
	 <div id="main-wrapper">
	  
@endsection
@section('main-content')
  <div class="page-wrapper">
      <div class="container-fluid">
                <!-- ============================================================== -->
                <!-- Bread crumb and right sidebar toggle -->
                <!-- ============================================================== -->
                <div class="row page-titles">
                    <div class="col-md-5 align-self-center">
                        <h4 class="text-themecolor">My Permissions</h4>
                    </div>
                    <div class="col-md-7 align-self-center text-right">
                        <div class="d-flex justify-content-end align-items-center">
                            <ol class="breadcrumb">
                                <li class="breadcrumb-item"><a href="javascript:void(0)">Home</a></li>
                                <li class="breadcrumb-item active">Permissions</li>
                            </ol>
                        </div>
                    </div>
                </div>
                <!-- ============================================================== -->
                <!-- End Bread crumb and right sidebar toggle -->
                <!-- ============================================================== -->
                <!-- ============================================================== -->
                <!-- Start Page Content -->
                <!-- ============================================================== -->
                <div class="row">
                    <div class="col-12">
					 <div class="card">
							<form action="" method="post">
                            <div class="card-body">
							
                                <h4 class="card-title">My Permissions</h4>
                                <h6 class="card-subtitle">Role Name</h6>
								
								<div class="form-group text-right">
									<input type="submit" class="btn btn-primary" value="Submit Permission">
								</div>
                                <div class="table-responsive">
                                    <table id="myTable" class="table display table-bordered table-striped">
                                        <thead>
                                            <tr>
                                               
												<th>Sr. No.</th>
                                                <th>Module Name</th>
                                                <th>Is Default?</th>
                                                <th>Action</th>
                                            </tr>
                                        </thead>
                                        <tfoot>
                                            <tr>
											   
												<th>Sr. No.</th>
                                                <th>Module Name</th>
                                                <th>Is Default?</th>
                                                <th>Action</th>
                                            </tr>
                                        </tfoot>
                                        <tbody>
                                            <tr>
                                                <td>1</td>
                                                <td>User</td>
                                                <td>Yes</td>
                                                <td>
												 <button type="button" class="btn btn-primary" data-toggle="collapse" data-target="#user">Role Permission Setting</button>
												</td>
                                            </tr>
											
											<tr id="user" class="collapse"> 
											   <td colspan="4">
											   <div class="table-responsive">
													<table class="table">
														<thead>
															<tr>
																<th>Name</th>
																<th colspan="3">Action</th>
															</tr>
														</thead>
														<tbody>
															<tr>
																<td>
																 Basic Details
															   </td>
															   <td colspan="3">
																 <div class="form-check">
																  <label class="form-check-label">
																	<input type="checkbox" class="form-check-input" value="">View
																  </label>
																</div>
																<div class="form-check">
																  <label class="form-check-label">
																	<input type="checkbox" class="form-check-input" value="">Add
																  </label>
																</div>
																<div class="form-check">
																  <label class="form-check-label">
																	<input type="checkbox" class="form-check-input" value="">Edit
																  </label>
																</div>
																<div class="form-check">
																  <label class="form-check-label">
																	<input type="checkbox" class="form-check-input" value="">Delete
																  </label>
																</div>
															   </td>
															</tr>   

                                                            <tr>
																<td>
																Stamp
															   </td>
															   <td colspan="3">
																 <div class="form-check">
																  <label class="form-check-label">
																	<input type="checkbox" class="form-check-input" value="">View
																  </label>
																</div>
																<div class="form-check">
																  <label class="form-check-label">
																	<input type="checkbox" class="form-check-input" value="">Add
																  </label>
																</div>
																<div class="form-check">
																  <label class="form-check-label">
																	<input type="checkbox" class="form-check-input" value="">Edit
																  </label>
																</div>
																<div class="form-check">
																  <label class="form-check-label">
																	<input type="checkbox" class="form-check-input" value="">Delete
																  </label>
																</div>
															   </td>
															</tr>

															<tr>
																<td>
																Bank Details
															   </td>
															   <td colspan="3">
																 <div class="form-check">
																  <label class="form-check-label">
																	<input type="checkbox" class="form-check-input" value="">View
																  </label>
																</div>
																<div class="form-check">
																  <label class="form-check-label">
																	<input type="checkbox" class="form-check-input" value="">Add
																  </label>
																</div>
																<div class="form-check">
																  <label class="form-check-label">
																	<input type="checkbox" class="form-check-input" value="">Edit
																  </label>
																</div>
																<div class="form-check">
																  <label class="form-check-label">
																	<input type="checkbox" class="form-check-input" value="">Delete
																  </label>
																</div>
															   </td>
															</tr>  

                                                            <tr>
																<td>
																Documents
															   </td>
															   <td colspan="3">
																 <div class="form-check">
																  <label class="form-check-label">
																	<input type="checkbox" class="form-check-input" value="">View
																  </label>
																</div>
																<div class="form-check">
																  <label class="form-check-label">
																	<input type="checkbox" class="form-check-input" value="">Add
																  </label>
																</div>
																<div class="form-check">
																  <label class="form-check-label">
																	<input type="checkbox" class="form-check-input" value="">Edit
																  </label>
																</div>
																<div class="form-check">
																  <label class="form-check-label">
																	<input type="checkbox" class="form-check-input" value="">Delete
																  </label>
																</div>
															   </td>
															</tr>  
                  
															 <tr>
																<td>
																Task
															   </td>
															   <td colspan="3">
																 <div class="form-check">
																  <label class="form-check-label">
																	<input type="checkbox" class="form-check-input" value="">View
																  </label>
																</div>
																<div class="form-check">
																  <label class="form-check-label">
																	<input type="checkbox" class="form-check-input" value="">Add
																  </label>
																</div>
																<div class="form-check">
																  <label class="form-check-label">
																	<input type="checkbox" class="form-check-input" value="">Edit
																  </label>
																</div>
																<div class="form-check">
																  <label class="form-check-label">
																	<input type="checkbox" class="form-check-input" value="">Delete
																  </label>
																</div>
															   </td>
															</tr> 

                                                             <tr>
																<td>
																 Time Card
															   </td>
															   <td colspan="3">
																 <div class="form-check">
																  <label class="form-check-label">
																	<input type="checkbox" class="form-check-input" value="">View
																  </label>
																</div>
																<div class="form-check">
																  <label class="form-check-label">
																	<input type="checkbox" class="form-check-input" value="">Add
																  </label>
																</div>
																<div class="form-check">
																  <label class="form-check-label">
																	<input type="checkbox" class="form-check-input" value="">Edit
																  </label>
																</div>
																<div class="form-check">
																  <label class="form-check-label">
																	<input type="checkbox" class="form-check-input" value="">Delete
																  </label>
																</div>
															   </td>
															</tr> 

                                                             <tr>
																<td>
																Leaves
															   </td>
															   <td colspan="3">
																 <div class="form-check">
																  <label class="form-check-label">
																	<input type="checkbox" class="form-check-input" value="">View
																  </label>
																</div>
																<div class="form-check">
																  <label class="form-check-label">
																	<input type="checkbox" class="form-check-input" value="">Add
																  </label>
																</div>
																<div class="form-check">
																  <label class="form-check-label">
																	<input type="checkbox" class="form-check-input" value="">Edit
																  </label>
																</div>
																<div class="form-check">
																  <label class="form-check-label">
																	<input type="checkbox" class="form-check-input" value="">Delete
																  </label>
																</div>
															   </td>
															</tr>  

                                 							 <tr>
																<td>
																Provident Fund
															   </td>
															   <td colspan="3">
																 <div class="form-check">
																  <label class="form-check-label">
																	<input type="checkbox" class="form-check-input" value="">View
																  </label>
																</div>
																<div class="form-check">
																  <label class="form-check-label">
																	<input type="checkbox" class="form-check-input" value="">Add
																  </label>
																</div>
																<div class="form-check">
																  <label class="form-check-label">
																	<input type="checkbox" class="form-check-input" value="">Edit
																  </label>
																</div>
																<div class="form-check">
																  <label class="form-check-label">
																	<input type="checkbox" class="form-check-input" value="">Delete
																  </label>
																</div>
															   </td>
															</tr>  

                                                             <tr>
																<td>
																Overtime
															   </td>
															   <td colspan="3">
																 <div class="form-check">
																  <label class="form-check-label">
																	<input type="checkbox" class="form-check-input" value="">View
																  </label>
																</div>
																<div class="form-check">
																  <label class="form-check-label">
																	<input type="checkbox" class="form-check-input" value="">Add
																  </label>
																</div>
																<div class="form-check">
																  <label class="form-check-label">
																	<input type="checkbox" class="form-check-input" value="">Edit
																  </label>
																</div>
																<div class="form-check">
																  <label class="form-check-label">
																	<input type="checkbox" class="form-check-input" value="">Delete
																  </label>
																</div>
															   </td>
															</tr> 

                                                             <tr>
																<td>
																Salary
															   </td>
															   <td colspan="3">
																 <div class="form-check">
																  <label class="form-check-label">
																	<input type="checkbox" class="form-check-input" value="">View
																  </label>
																</div>
																<div class="form-check">
																  <label class="form-check-label">
																	<input type="checkbox" class="form-check-input" value="">Add
																  </label>
																</div>
																<div class="form-check">
																  <label class="form-check-label">
																	<input type="checkbox" class="form-check-input" value="">Edit
																  </label>
																</div>
																<div class="form-check">
																  <label class="form-check-label">
																	<input type="checkbox" class="form-check-input" value="">Delete
																  </label>
																</div>
															   </td>
															</tr>  

                                                             <tr>
																<td>
																Activity
															   </td>
															   <td colspan="3">
																 <div class="form-check">
																  <label class="form-check-label">
																	<input type="checkbox" class="form-check-input" value="">View
																  </label>
																</div>
																<div class="form-check">
																  <label class="form-check-label">
																	<input type="checkbox" class="form-check-input" value="">Add
																  </label>
																</div>
																<div class="form-check">
																  <label class="form-check-label">
																	<input type="checkbox" class="form-check-input" value="">Edit
																  </label>
																</div>
																<div class="form-check">
																  <label class="form-check-label">
																	<input type="checkbox" class="form-check-input" value="">Delete
																  </label>
																</div>
															   </td>
															</tr>  															
															

															
														</tbody>
													</table>
												</div>
											   </td>											   
											</tr>
                                           
										    <tr>
                                                <td>2</td>
                                                <td>Support Ticket</td>
                                                <td>Yes</td>
                                                <td>
												 <button type="button" class="btn btn-primary" data-toggle="collapse" data-target="#superTickets">Role Permission Setting</button>
												</td>
                                            </tr>
											
											<tr id="superTickets" class="collapse">
											   <td colspan="4">
											   <div class="table-responsive">
													<table class="table">														
														<tbody>
															<tr>
																<td>
																 Support Tickets
															   </td>
															   <td colspan="3">
																 <div class="form-check">
																  <label class="form-check-label">
																	<input type="checkbox" class="form-check-input" value="">View
																  </label>
																</div>
																<div class="form-check">
																  <label class="form-check-label">
																	<input type="checkbox" class="form-check-input" value="">Add
																  </label>
																</div>
																<div class="form-check">
																  <label class="form-check-label">
																	<input type="checkbox" class="form-check-input" value="">Edit
																  </label>
																</div>
																<div class="form-check">
																  <label class="form-check-label">
																	<input type="checkbox" class="form-check-input" value="">Delete
																  </label>
																</div>
															   </td>
															</tr>
														</tbody>
													</table>
                                                </div>
                                                </td>
                                            </tr>												
                                                    
										    <tr>
                                                <td>3</td>
                                                <td>Email</td>
                                                <td>Yes</td>
                                                <td>
												 <button type="button" class="btn btn-primary" data-toggle="collapse" data-target="#email">Role Permission Setting</button>
												</td>
                                            </tr>
											
											<tr id="email" class="collapse">
											   <td colspan="4">
											   <div class="table-responsive">
													<table class="table">														
														<tbody>
															<tr>
																<td>
																 Email
															   </td>
															   <td colspan="3">
																 <div class="form-check">
																  <label class="form-check-label">
																	<input type="checkbox" class="form-check-input" value="">View
																  </label>
																</div>
																<div class="form-check">
																  <label class="form-check-label">
																	<input type="checkbox" class="form-check-input" value="">Add
																  </label>
																</div>
																<div class="form-check">
																  <label class="form-check-label">
																	<input type="checkbox" class="form-check-input" value="">Edit
																  </label>
																</div>
																<div class="form-check">
																  <label class="form-check-label">
																	<input type="checkbox" class="form-check-input" value="">Delete
																  </label>
																</div>
															   </td>
															</tr>
														</tbody>
													</table>
                                                </div>
                                                </td>
                                            </tr>		
											
											 <tr>
                                                <td>4</td>
                                                <td>File Manager</td>
                                                <td>Yes</td>
                                                <td>
												 <button type="button" class="btn btn-primary" data-toggle="collapse" data-target="#fileManager">Role Permission Setting</button>
												</td>
                                            </tr>
											
											<tr id="fileManager" class="collapse">
											   <td colspan="4">
											   <div class="table-responsive">
													<table class="table">														
														<tbody>
															<tr>
																<td>
																 File Manager
															   </td>
															   <td colspan="3">
																 <div class="form-check">
																  <label class="form-check-label">
																	<input type="checkbox" class="form-check-input" value="">View
																  </label>
																</div>
																<div class="form-check">
																  <label class="form-check-label">
																	<input type="checkbox" class="form-check-input" value="">Add
																  </label>
																</div>
																<div class="form-check">
																  <label class="form-check-label">
																	<input type="checkbox" class="form-check-input" value="">Edit
																  </label>
																</div>
																<div class="form-check">
																  <label class="form-check-label">
																	<input type="checkbox" class="form-check-input" value="">Delete
																  </label>
																</div>
															   </td>
															</tr>
														</tbody>
													</table>
                                                </div>
                                                </td>
                                            </tr>		
											
											 <tr>
                                                <td>5</td>
                                                <td>Advertise</td>
                                                <td>Yes</td>
                                                <td>
												 <button type="button" class="btn btn-primary" data-toggle="collapse" data-target="#advertise">Role Permission Setting</button>
												</td>
                                            </tr>
											
											<tr id="advertise" class="collapse">
											   <td colspan="4">
											   <div class="table-responsive">
													<table class="table">														
														<tbody>
															<tr>
																<td>
																Advertise
															   </td>
															   <td colspan="3">
																 <div class="form-check">
																  <label class="form-check-label">
																	<input type="checkbox" class="form-check-input" value="">View
																  </label>
																</div>
																<div class="form-check">
																  <label class="form-check-label">
																	<input type="checkbox" class="form-check-input" value="">Add
																  </label>
																</div>
																<div class="form-check">
																  <label class="form-check-label">
																	<input type="checkbox" class="form-check-input" value="">Edit
																  </label>
																</div>
																<div class="form-check">
																  <label class="form-check-label">
																	<input type="checkbox" class="form-check-input" value="">Delete
																  </label>
																</div>
															   </td>
															</tr>
														</tbody>
													</table>
                                                </div>
                                                </td>
                                            </tr>		
											
											 <tr>
                                                <td>6</td>
                                                <td>Certificates</td>
                                                <td>Yes</td>
                                                <td>
												 <button type="button" class="btn btn-primary" data-toggle="collapse" data-target="#certificate">Role Permission Setting</button>
												</td>
                                            </tr>
											
											<tr id="certificate" class="collapse">
											   <td colspan="4">
											   <div class="table-responsive">
													<table class="table">														
														<tbody>
															<tr>
																<td>
																Certificate Name
															   </td>
															   <td colspan="3">
																 <div class="form-check">
																  <label class="form-check-label">
																	<input type="checkbox" class="form-check-input" value="">View
																  </label>
																</div>
																<div class="form-check">
																  <label class="form-check-label">
																	<input type="checkbox" class="form-check-input" value="">Add
																  </label>
																</div>
																<div class="form-check">
																  <label class="form-check-label">
																	<input type="checkbox" class="form-check-input" value="">Edit
																  </label>
																</div>
																<div class="form-check">
																  <label class="form-check-label">
																	<input type="checkbox" class="form-check-input" value="">Delete
																  </label>
																</div>
															   </td>
															</tr>
															
															
															<tr>
																<td>
																Passport
															   </td>
															   <td colspan="3">
																 <div class="form-check">
																  <label class="form-check-label">
																	<input type="checkbox" class="form-check-input" value="">View
																  </label>
																</div>
																<div class="form-check">
																  <label class="form-check-label">
																	<input type="checkbox" class="form-check-input" value="">Add
																  </label>
																</div>
																<div class="form-check">
																  <label class="form-check-label">
																	<input type="checkbox" class="form-check-input" value="">Edit
																  </label>
																</div>
																<div class="form-check">
																  <label class="form-check-label">
																	<input type="checkbox" class="form-check-input" value="">Delete
																  </label>
																</div>
															   </td>
															</tr>
															
															
															<tr>
																<td>
																Aadhar Card
															   </td>
															   <td colspan="3">
																 <div class="form-check">
																  <label class="form-check-label">
																	<input type="checkbox" class="form-check-input" value="">View
																  </label>
																</div>
																<div class="form-check">
																  <label class="form-check-label">
																	<input type="checkbox" class="form-check-input" value="">Add
																  </label>
																</div>
																<div class="form-check">
																  <label class="form-check-label">
																	<input type="checkbox" class="form-check-input" value="">Edit
																  </label>
																</div>
																<div class="form-check">
																  <label class="form-check-label">
																	<input type="checkbox" class="form-check-input" value="">Delete
																  </label>
																</div>
															   </td>
															</tr>
															
															
															<tr>
																<td>
																Pan Card
															   </td>
															   <td colspan="3">
																 <div class="form-check">
																  <label class="form-check-label">
																	<input type="checkbox" class="form-check-input" value="">View
																  </label>
																</div>
																<div class="form-check">
																  <label class="form-check-label">
																	<input type="checkbox" class="form-check-input" value="">Add
																  </label>
																</div>
																<div class="form-check">
																  <label class="form-check-label">
																	<input type="checkbox" class="form-check-input" value="">Edit
																  </label>
																</div>
																<div class="form-check">
																  <label class="form-check-label">
																	<input type="checkbox" class="form-check-input" value="">Delete
																  </label>
																</div>
															   </td>
															</tr>
															
															
															<tr>
																<td>
																Ration Card
															   </td>
															   <td colspan="3">
																 <div class="form-check">
																  <label class="form-check-label">
																	<input type="checkbox" class="form-check-input" value="">View
																  </label>
																</div>
																<div class="form-check">
																  <label class="form-check-label">
																	<input type="checkbox" class="form-check-input" value="">Add
																  </label>
																</div>
																<div class="form-check">
																  <label class="form-check-label">
																	<input type="checkbox" class="form-check-input" value="">Edit
																  </label>
																</div>
																<div class="form-check">
																  <label class="form-check-label">
																	<input type="checkbox" class="form-check-input" value="">Delete
																  </label>
																</div>
															   </td>
															</tr>
															
															<tr>
																<td>
																Certificate Description
															   </td>
															   <td colspan="3">
																 <div class="form-check">
																  <label class="form-check-label">
																	<input type="checkbox" class="form-check-input" value="">View
																  </label>
																</div>
																<div class="form-check">
																  <label class="form-check-label">
																	<input type="checkbox" class="form-check-input" value="">Add
																  </label>
																</div>
																<div class="form-check">
																  <label class="form-check-label">
																	<input type="checkbox" class="form-check-input" value="">Edit
																  </label>
																</div>
																<div class="form-check">
																  <label class="form-check-label">
																	<input type="checkbox" class="form-check-input" value="">Delete
																  </label>
																</div>
															   </td>
															</tr>
															
															<tr>
																<td>
																Voter Card
															   </td>
															   <td colspan="3">
																 <div class="form-check">
																  <label class="form-check-label">
																	<input type="checkbox" class="form-check-input" value="">View
																  </label>
																</div>
																<div class="form-check">
																  <label class="form-check-label">
																	<input type="checkbox" class="form-check-input" value="">Add
																  </label>
																</div>
																<div class="form-check">
																  <label class="form-check-label">
																	<input type="checkbox" class="form-check-input" value="">Edit
																  </label>
																</div>
																<div class="form-check">
																  <label class="form-check-label">
																	<input type="checkbox" class="form-check-input" value="">Delete
																  </label>
																</div>
															   </td>
															</tr>
															
															
															<tr>
																<td>
																Health Card
															   </td>
															   <td colspan="3">
																 <div class="form-check">
																  <label class="form-check-label">
																	<input type="checkbox" class="form-check-input" value="">View
																  </label>
																</div>
																<div class="form-check">
																  <label class="form-check-label">
																	<input type="checkbox" class="form-check-input" value="">Add
																  </label>
																</div>
																<div class="form-check">
																  <label class="form-check-label">
																	<input type="checkbox" class="form-check-input" value="">Edit
																  </label>
																</div>
																<div class="form-check">
																  <label class="form-check-label">
																	<input type="checkbox" class="form-check-input" value="">Delete
																  </label>
																</div>
															   </td>
															</tr>
															
															
															<tr>
																<td>
																Birth Certificate
															   </td>
															   <td colspan="3">
																 <div class="form-check">
																  <label class="form-check-label">
																	<input type="checkbox" class="form-check-input" value="">View
																  </label>
																</div>
																<div class="form-check">
																  <label class="form-check-label">
																	<input type="checkbox" class="form-check-input" value="">Add
																  </label>
																</div>
																<div class="form-check">
																  <label class="form-check-label">
																	<input type="checkbox" class="form-check-input" value="">Edit
																  </label>
																</div>
																<div class="form-check">
																  <label class="form-check-label">
																	<input type="checkbox" class="form-check-input" value="">Delete
																  </label>
																</div>
															   </td>
															</tr>
															
															<tr>
																<td>
																Driving Licence
															   </td>
															   <td colspan="3">
																 <div class="form-check">
																  <label class="form-check-label">
																	<input type="checkbox" class="form-check-input" value="">View
																  </label>
																</div>
																<div class="form-check">
																  <label class="form-check-label">
																	<input type="checkbox" class="form-check-input" value="">Add
																  </label>
																</div>
																<div class="form-check">
																  <label class="form-check-label">
																	<input type="checkbox" class="form-check-input" value="">Edit
																  </label>
																</div>
																<div class="form-check">
																  <label class="form-check-label">
																	<input type="checkbox" class="form-check-input" value="">Delete
																  </label>
																</div>
															   </td>
															</tr>
															
															
														</tbody>
													</table>
                                                </div>
                                                </td>
                                            </tr>		
											
											 <tr>
                                                <td>7</td>
                                                <td>Attendance</td>
                                                <td>Yes</td>
                                                <td>
												 <button type="button" class="btn btn-primary" data-toggle="collapse" data-target="#attendance">Role Permission Setting</button>
												</td>
                                            </tr>
											
											<tr id="attendance" class="collapse">
											   <td colspan="4">
											   <div class="table-responsive">
													<table class="table">														
														<tbody>
															<tr>
																<td>
																Secretary
															   </td>
															   <td colspan="3">
																 <div class="form-check">
																  <label class="form-check-label">
																	<input type="checkbox" class="form-check-input" value="">View
																  </label>
																</div>
																<div class="form-check">
																  <label class="form-check-label">
																	<input type="checkbox" class="form-check-input" value="">Add
																  </label>
																</div>
																<div class="form-check">
																  <label class="form-check-label">
																	<input type="checkbox" class="form-check-input" value="">Edit
																  </label>
																</div>
																<div class="form-check">
																  <label class="form-check-label">
																	<input type="checkbox" class="form-check-input" value="">Delete
																  </label>
																</div>
															   </td>
															</tr>
															
															
															<tr>
																<td>
																LDC
															   </td>
															   <td colspan="3">
																 <div class="form-check">
																  <label class="form-check-label">
																	<input type="checkbox" class="form-check-input" value="">View
																  </label>
																</div>
																<div class="form-check">
																  <label class="form-check-label">
																	<input type="checkbox" class="form-check-input" value="">Add
																  </label>
																</div>
																<div class="form-check">
																  <label class="form-check-label">
																	<input type="checkbox" class="form-check-input" value="">Edit
																  </label>
																</div>
																<div class="form-check">
																  <label class="form-check-label">
																	<input type="checkbox" class="form-check-input" value="">Delete
																  </label>
																</div>
															   </td>
															</tr>
															
															
															<tr>
																<td>
																Contractual
															   </td>
															   <td colspan="3">
																 <div class="form-check">
																  <label class="form-check-label">
																	<input type="checkbox" class="form-check-input" value="">View
																  </label>
																</div>
																<div class="form-check">
																  <label class="form-check-label">
																	<input type="checkbox" class="form-check-input" value="">Add
																  </label>
																</div>
																<div class="form-check">
																  <label class="form-check-label">
																	<input type="checkbox" class="form-check-input" value="">Edit
																  </label>
																</div>
																<div class="form-check">
																  <label class="form-check-label">
																	<input type="checkbox" class="form-check-input" value="">Delete
																  </label>
																</div>
															   </td>
															</tr>
															
															
															<tr>
																<td>
																Ward Number
															   </td>
															   <td colspan="3">
																 <div class="form-check">
																  <label class="form-check-label">
																	<input type="checkbox" class="form-check-input" value="">View
																  </label>
																</div>
																<div class="form-check">
																  <label class="form-check-label">
																	<input type="checkbox" class="form-check-input" value="">Add
																  </label>
																</div>
																<div class="form-check">
																  <label class="form-check-label">
																	<input type="checkbox" class="form-check-input" value="">Edit
																  </label>
																</div>
																<div class="form-check">
																  <label class="form-check-label">
																	<input type="checkbox" class="form-check-input" value="">Delete
																  </label>
																</div>
															   </td>
															</tr>
															
															
															<tr>
																<td>
																Ward Member
															   </td>
															   <td colspan="3">
																 <div class="form-check">
																  <label class="form-check-label">
																	<input type="checkbox" class="form-check-input" value="">View
																  </label>
																</div>
																<div class="form-check">
																  <label class="form-check-label">
																	<input type="checkbox" class="form-check-input" value="">Add
																  </label>
																</div>
																<div class="form-check">
																  <label class="form-check-label">
																	<input type="checkbox" class="form-check-input" value="">Edit
																  </label>
																</div>
																<div class="form-check">
																  <label class="form-check-label">
																	<input type="checkbox" class="form-check-input" value="">Delete
																  </label>
																</div>
															   </td>
															</tr>
															
															<tr>
																<td>
																Managers
															   </td>
															   <td colspan="3">
																 <div class="form-check">
																  <label class="form-check-label">
																	<input type="checkbox" class="form-check-input" value="">View
																  </label>
																</div>
																<div class="form-check">
																  <label class="form-check-label">
																	<input type="checkbox" class="form-check-input" value="">Add
																  </label>
																</div>
																<div class="form-check">
																  <label class="form-check-label">
																	<input type="checkbox" class="form-check-input" value="">Edit
																  </label>
																</div>
																<div class="form-check">
																  <label class="form-check-label">
																	<input type="checkbox" class="form-check-input" value="">Delete
																  </label>
																</div>
															   </td>
															</tr>
															
															<tr>
																<td>
																Clerk
															   </td>
															   <td colspan="3">
																 <div class="form-check">
																  <label class="form-check-label">
																	<input type="checkbox" class="form-check-input" value="">View
																  </label>
																</div>
																<div class="form-check">
																  <label class="form-check-label">
																	<input type="checkbox" class="form-check-input" value="">Add
																  </label>
																</div>
																<div class="form-check">
																  <label class="form-check-label">
																	<input type="checkbox" class="form-check-input" value="">Edit
																  </label>
																</div>
																<div class="form-check">
																  <label class="form-check-label">
																	<input type="checkbox" class="form-check-input" value="">Delete
																  </label>
																</div>
															   </td>
															</tr>
															
															
															<tr>
																<td>
																Peon
															   </td>
															   <td colspan="3">
																 <div class="form-check">
																  <label class="form-check-label">
																	<input type="checkbox" class="form-check-input" value="">View
																  </label>
																</div>
																<div class="form-check">
																  <label class="form-check-label">
																	<input type="checkbox" class="form-check-input" value="">Add
																  </label>
																</div>
																<div class="form-check">
																  <label class="form-check-label">
																	<input type="checkbox" class="form-check-input" value="">Edit
																  </label>
																</div>
																<div class="form-check">
																  <label class="form-check-label">
																	<input type="checkbox" class="form-check-input" value="">Delete
																  </label>
																</div>
															   </td>
															</tr>
															
															
															<tr>
																<td>
																Sarpanch
															   </td>
															   <td colspan="3">
																 <div class="form-check">
																  <label class="form-check-label">
																	<input type="checkbox" class="form-check-input" value="">View
																  </label>
																</div>
																<div class="form-check">
																  <label class="form-check-label">
																	<input type="checkbox" class="form-check-input" value="">Add
																  </label>
																</div>
																<div class="form-check">
																  <label class="form-check-label">
																	<input type="checkbox" class="form-check-input" value="">Edit
																  </label>
																</div>
																<div class="form-check">
																  <label class="form-check-label">
																	<input type="checkbox" class="form-check-input" value="">Delete
																  </label>
																</div>
															   </td>
															</tr>
															
															<tr>
																<td>
                                                                  Dy Sarpanch
															   </td>
															   <td colspan="3">
																 <div class="form-check">
																  <label class="form-check-label">
																	<input type="checkbox" class="form-check-input" value="">View
																  </label>
																</div>
																<div class="form-check">
																  <label class="form-check-label">
																	<input type="checkbox" class="form-check-input" value="">Add
																  </label>
																</div>
																<div class="form-check">
																  <label class="form-check-label">
																	<input type="checkbox" class="form-check-input" value="">Edit
																  </label>
																</div>
																<div class="form-check">
																  <label class="form-check-label">
																	<input type="checkbox" class="form-check-input" value="">Delete
																  </label>
																</div>
															   </td>
															</tr>
															
															
														</tbody>
													</table>
                                                </div>
                                                </td>
                                            </tr>		
											
											  <tr>
                                                <td>8</td>
                                                <td>Request</td>
                                                <td>Yes</td>
                                                <td>
												 <button type="button" class="btn btn-primary" data-toggle="collapse" data-target="#request">Role Permission Setting</button>
												</td>
                                            </tr>
											
											<tr id="request" class="collapse">
											   <td colspan="4">
											   <div class="table-responsive">
													<table class="table">														
														<tbody>
															<tr>
																<td>
                                                                 Request
															   </td>
															   <td colspan="3">
																 <div class="form-check">
																  <label class="form-check-label">
																	<input type="checkbox" class="form-check-input" value="">View
																  </label>
																</div>
																<div class="form-check">
																  <label class="form-check-label">
																	<input type="checkbox" class="form-check-input" value="">Add
																  </label>
																</div>
																<div class="form-check">
																  <label class="form-check-label">
																	<input type="checkbox" class="form-check-input" value="">Edit
																  </label>
																</div>
																<div class="form-check">
																  <label class="form-check-label">
																	<input type="checkbox" class="form-check-input" value="">Delete
																  </label>
																</div>
															   </td>
															</tr>
														</tbody>
													</table>
                                                </div>
                                                </td>
                                            </tr>
											
											  <tr>
                                                <td>9</td>
                                                <td>Website</td>
                                                <td>Yes</td>
                                                <td>
												 <button type="button" class="btn btn-primary" data-toggle="collapse" data-target="#website">Role Permission Setting</button>
												</td>
                                            </tr>
											
											<tr id="website" class="collapse">
											   <td colspan="4">
											   <div class="table-responsive">
													<table class="table">														
														<tbody>
															<tr>
																<td>
																 Website
															   </td>
															   <td colspan="3">
																 <div class="form-check">
																  <label class="form-check-label">
																	<input type="checkbox" class="form-check-input" value="">View
																  </label>
																</div>
																<div class="form-check">
																  <label class="form-check-label">
																	<input type="checkbox" class="form-check-input" value="">Add
																  </label>
																</div>
																<div class="form-check">
																  <label class="form-check-label">
																	<input type="checkbox" class="form-check-input" value="">Edit
																  </label>
																</div>
																<div class="form-check">
																  <label class="form-check-label">
																	<input type="checkbox" class="form-check-input" value="">Delete
																  </label>
																</div>
															   </td>
															</tr>
														</tbody>
													</table>
                                                </div>
                                                </td>
                                            </tr>
											
											
											 <tr>
                                                <td>10</td>
                                                <td>CitiZen</td>
                                                <td>Yes</td>
                                                <td>
												 <button type="button" class="btn btn-primary" data-toggle="collapse" data-target="#citizen">Role Permission Setting</button>
												</td>
                                            </tr>
											
											<tr id="citizen" class="collapse">
											   <td colspan="4">
											   <div class="table-responsive">
													<table class="table">														
														<tbody>
															<tr>
																<td>
																 Citizens
															   </td>
															   <td colspan="3">
																 <div class="form-check">
																  <label class="form-check-label">
																	<input type="checkbox" class="form-check-input" value="">View
																  </label>
																</div>
																<div class="form-check">
																  <label class="form-check-label">
																	<input type="checkbox" class="form-check-input" value="">Add
																  </label>
																</div>
																<div class="form-check">
																  <label class="form-check-label">
																	<input type="checkbox" class="form-check-input" value="">Edit
																  </label>
																</div>
																<div class="form-check">
																  <label class="form-check-label">
																	<input type="checkbox" class="form-check-input" value="">Delete
																  </label>
																</div>
															   </td>
															</tr>
														</tbody>
													</table>
                                                </div>
                                                </td>
                                            </tr>
											
											
											 <tr>
                                                <td>11</td>
                                                <td>Audit</td>
                                                <td>Yes</td>
                                                <td>
												 <button type="button" class="btn btn-primary" data-toggle="collapse" data-target="#audit">Role Permission Setting</button>
												</td>
                                            </tr>
											
											<tr id="audit" class="collapse">
											   <td colspan="4">
											   <div class="table-responsive">
													<table class="table">														
														<tbody>
															<tr>
																<td>
																 Audit
															   </td>
															   <td colspan="3">
																 <div class="form-check">
																  <label class="form-check-label">
																	<input type="checkbox" class="form-check-input" value="">View
																  </label>
																</div>
																<div class="form-check">
																  <label class="form-check-label">
																	<input type="checkbox" class="form-check-input" value="">Add
																  </label>
																</div>
																<div class="form-check">
																  <label class="form-check-label">
																	<input type="checkbox" class="form-check-input" value="">Edit
																  </label>
																</div>
																<div class="form-check">
																  <label class="form-check-label">
																	<input type="checkbox" class="form-check-input" value="">Delete
																  </label>
																</div>
															   </td>
															</tr>
														</tbody>
													</table>
                                                </div>
                                                </td>
                                            </tr>
											
											 <tr>
                                                <td>12</td>
                                                <td>Forms</td>
                                                <td>Yes</td>
                                                <td>
												 <button type="button" class="btn btn-primary" data-toggle="collapse" data-target="#forms">Role Permission Setting</button>
												</td>
                                            </tr>
											
											<tr id="forms" class="collapse">
											   <td colspan="4">
											   <div class="table-responsive">
													<table class="table">														
														<tbody>
															<tr>
																<td>
																 Forms
															   </td>
															   <td colspan="3">
																 <div class="form-check">
																  <label class="form-check-label">
																	<input type="checkbox" class="form-check-input" value="">View
																  </label>
																</div>
																<div class="form-check">
																  <label class="form-check-label">
																	<input type="checkbox" class="form-check-input" value="">Add
																  </label>
																</div>
																<div class="form-check">
																  <label class="form-check-label">
																	<input type="checkbox" class="form-check-input" value="">Edit
																  </label>
																</div>
																<div class="form-check">
																  <label class="form-check-label">
																	<input type="checkbox" class="form-check-input" value="">Delete
																  </label>
																</div>
															   </td>
															</tr>
														</tbody>
													</table>
                                                </div>
                                                </td>
                                            </tr>
											
											 <tr>
                                                <td>13</td>
                                                <td>Gallery</td>
                                                <td>Yes</td>
                                                <td>
												 <button type="button" class="btn btn-primary" data-toggle="collapse" data-target="#gallery">Role Permission Setting</button>
												</td>
                                            </tr>
											
											<tr id="gallery" class="collapse">
											   <td colspan="4">
											   <div class="table-responsive">
													<table class="table">														
														<tbody>
															<tr>
																<td>
																 Gallery
															   </td>
															   <td colspan="3">
																 <div class="form-check">
																  <label class="form-check-label">
																	<input type="checkbox" class="form-check-input" value="">View
																  </label>
																</div>
																<div class="form-check">
																  <label class="form-check-label">
																	<input type="checkbox" class="form-check-input" value="">Add
																  </label>
																</div>
																<div class="form-check">
																  <label class="form-check-label">
																	<input type="checkbox" class="form-check-input" value="">Edit
																  </label>
																</div>
																<div class="form-check">
																  <label class="form-check-label">
																	<input type="checkbox" class="form-check-input" value="">Delete
																  </label>
																</div>
															   </td>
															</tr>
														</tbody>
													</table>
                                                </div>
                                                </td>
                                            </tr>
											
											 <tr>
                                                <td>14</td>
                                                <td>Events</td>
                                                <td>Yes</td>
                                                <td>
												 <button type="button" class="btn btn-primary" data-toggle="collapse" data-target="#event">Role Permission Setting</button>
												</td>
                                            </tr>
											
											<tr id="event" class="collapse">
											   <td colspan="4">
											   <div class="table-responsive">
													<table class="table">														
														<tbody>
															<tr>
																<td>
																 Events
															   </td>
															   <td colspan="3">
																 <div class="form-check">
																  <label class="form-check-label">
																	<input type="checkbox" class="form-check-input" value="">View
																  </label>
																</div>
																<div class="form-check">
																  <label class="form-check-label">
																	<input type="checkbox" class="form-check-input" value="">Add
																  </label>
																</div>
																<div class="form-check">
																  <label class="form-check-label">
																	<input type="checkbox" class="form-check-input" value="">Edit
																  </label>
																</div>
																<div class="form-check">
																  <label class="form-check-label">
																	<input type="checkbox" class="form-check-input" value="">Delete
																  </label>
																</div>
															   </td>
															</tr>
														</tbody>
													</table>
                                                </div>
                                                </td>
                                            </tr>
                                        
										
										     <tr>
                                                <td>15</td>
                                                <td>Notice</td>
                                                <td>Yes</td>
                                                <td>
												 <button type="button" class="btn btn-primary" data-toggle="collapse" data-target="#notice">Role Permission Setting</button>
												</td>
                                            </tr>
											
											<tr id="notice" class="collapse">
											   <td colspan="4">
											   <div class="table-responsive">
													<table class="table">														
														<tbody>
															<tr>
																<td>
																 Notice
															   </td>
															   <td colspan="3">
																 <div class="form-check">
																  <label class="form-check-label">
																	<input type="checkbox" class="form-check-input" value="">View
																  </label>
																</div>
																<div class="form-check">
																  <label class="form-check-label">
																	<input type="checkbox" class="form-check-input" value="">Add
																  </label>
																</div>
																<div class="form-check">
																  <label class="form-check-label">
																	<input type="checkbox" class="form-check-input" value="">Edit
																  </label>
																</div>
																<div class="form-check">
																  <label class="form-check-label">
																	<input type="checkbox" class="form-check-input" value="">Delete
																  </label>
																</div>
															   </td>
															</tr>
														</tbody>
													</table>
                                                </div>
                                                </td>
                                            </tr>
											
											
											 <tr>
                                                <td>16</td>
                                                <td>Contact Us</td>
                                                <td>Yes</td>
                                                <td>
												 <button type="button" class="btn btn-primary" data-toggle="collapse" data-target="#contact">Role Permission Setting</button>
												</td>
                                            </tr>
											
											<tr id="contact" class="collapse">
											   <td colspan="4">
											   <div class="table-responsive">
													<table class="table">														
														<tbody>
															<tr>
																<td>
																 Website
															   </td>
															   <td colspan="3">
																 <div class="form-check">
																  <label class="form-check-label">
																	<input type="checkbox" class="form-check-input" value="">View
																  </label>
																</div>
																<div class="form-check">
																  <label class="form-check-label">
																	<input type="checkbox" class="form-check-input" value="">Add
																  </label>
																</div>
																<div class="form-check">
																  <label class="form-check-label">
																	<input type="checkbox" class="form-check-input" value="">Edit
																  </label>
																</div>
																<div class="form-check">
																  <label class="form-check-label">
																	<input type="checkbox" class="form-check-input" value="">Delete
																  </label>
																</div>
															   </td>
															</tr>
														</tbody>
													</table>
                                                </div>
                                                </td>
                                            </tr>
											
											
											 <tr>
                                                <td>16</td>
                                                <td>Subscribers</td>
                                                <td>Yes</td>
                                                <td>
												 <button type="button" class="btn btn-primary" data-toggle="collapse" data-target="#subscribe">Role Permission Setting</button>
												</td>
                                            </tr>
											
											<tr id="subscribe" class="collapse">
											   <td colspan="4">
											   <div class="table-responsive">
													<table class="table">														
														<tbody>
															<tr>
																<td>
																 Subscribers
															   </td>
															   <td colspan="3">
																 <div class="form-check">
																  <label class="form-check-label">
																	<input type="checkbox" class="form-check-input" value="">View
																  </label>
																</div>
																<div class="form-check">
																  <label class="form-check-label">
																	<input type="checkbox" class="form-check-input" value="">Add
																  </label>
																</div>
																<div class="form-check">
																  <label class="form-check-label">
																	<input type="checkbox" class="form-check-input" value="">Edit
																  </label>
																</div>
																<div class="form-check">
																  <label class="form-check-label">
																	<input type="checkbox" class="form-check-input" value="">Delete
																  </label>
																</div>
															   </td>
															</tr>
														</tbody>
													</table>
                                                </div>
                                                </td>
                                            </tr>
											
											
											 <tr>
                                                <td>17</td>
                                                <td>Category</td>
                                                <td>Yes</td>
                                                <td>
												 <button type="button" class="btn btn-primary" data-toggle="collapse" data-target="#category">Role Permission Setting</button>
												</td>
                                            </tr>
											
											<tr id="category" class="collapse">
											   <td colspan="4">
											   <div class="table-responsive">
													<table class="table">														
														<tbody>
															<tr>
																<td>
																 Website
															   </td>
															   <td colspan="3">
																 <div class="form-check">
																  <label class="form-check-label">
																	<input type="checkbox" class="form-check-input" value="">View
																  </label>
																</div>
																<div class="form-check">
																  <label class="form-check-label">
																	<input type="checkbox" class="form-check-input" value="">Add
																  </label>
																</div>
																<div class="form-check">
																  <label class="form-check-label">
																	<input type="checkbox" class="form-check-input" value="">Edit
																  </label>
																</div>
																<div class="form-check">
																  <label class="form-check-label">
																	<input type="checkbox" class="form-check-input" value="">Delete
																  </label>
																</div>
															   </td>
															</tr>
														</tbody>
													</table>
                                                </div>
                                                </td>
                                            </tr>
										
										</tbody>
                                    </table>
                                </div>
								<div class="form-group text-right">
									<input type="submit" class="btn btn-primary" value="Submit Permission">
								</div>
                            </div>
							</form>
                        </div>
                    </div>
                </div>
            </div>
     </div>
       
	   
@endsection
@section('js')


    <script>
        $(function () {
            $('#myTable').DataTable();
            var table = $('#example').DataTable({
                "columnDefs": [{
                    "visible": false,
                    "targets": 2
                }],
                "order": [
                    [2, 'asc']
                ],
                "displayLength": 25,
                "drawCallback": function (settings) {
                    var api = this.api();
                    var rows = api.rows({
                        page: 'current'
                    }).nodes();
                    var last = null;
                    api.column(2, {
                        page: 'current'
                    }).data().each(function (group, i) {
                        if (last !== group) {
                            $(rows).eq(i).before('<tr class="group"><td colspan="5">' + group + '</td></tr>');
                            last = group;
                        }
                    });
                }
            });
            // Order by the grouping
            $('#example tbody').on('click', 'tr.group', function () {
                var currentOrder = table.order()[0];
                if (currentOrder[0] === 2 && currentOrder[1] === 'asc') {
                    table.order([2, 'desc']).draw();
                } else {
                    table.order([2, 'asc']).draw();
                }
            });
            // responsive table
            $('#config-table').DataTable({
                responsive: true
            });
            $('#example23').DataTable({
                dom: 'Bfrtip',
                buttons: [
                    'copy', 'csv', 'excel', 'pdf', 'print'
                ]
            });
            $('.buttons-copy, .buttons-csv, .buttons-print, .buttons-pdf, .buttons-excel').addClass('btn btn-primary mr-1');
        });

    </script>
@endsection
