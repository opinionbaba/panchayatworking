@extends('layouts.app')


{{-- Page Title --}}
@section('page-title')
  Advertisement
@endsection
{{-- This Page Css --}}
@section('css')

    <!--=========================*
               Datatable
  
  *===========================-->
  <body class="skin-blue fixed-layout">
    <!-- ============================================================== -->
    <!-- Preloader - style you can find in spinners.css -->
    <!-- ============================================================== -->
    <div class="preloader">
        <div class="loader">
            <div class="loader__figure"></div>
        </div>
    </div>
     <div id="main-wrapper">
@endsection
@section('main-content')
<div class="page-wrapper">
    <div class="container-fluid">
        <!-- ============================================================== -->
        <!-- Bread crumb and right sidebar toggle -->
        <!-- ============================================================== -->
        <div class="row page-titles">
            <div class="col-md-5 align-self-center">
                <h4 class="text-themecolor"></h4>
            </div>
            <div class="col-md-7 align-self-center text-right">
                <div class="d-flex justify-content-end align-items-center">
                    <ol class="breadcrumb">
                        <li class="breadcrumb-item"><a href="javascript:void(0)">Home</a></li>
                        <li class="breadcrumb-item active"></li>
                    </ol>
                    <button type="button" class="btn btn-info d-none d-lg-block m-l-15" data-toggle="modal" data-target="#update_model" ><i
                            class="fa fa-plus-circle"></i> Add New Ads</button>
                            
                </div>
            </div>
        </div>
        <!-- ============================================================== -->
        <!-- End Bread crumb and right sidebar toggle -->
        <!-- ============================================================== -->
        <!-- ============================================================== -->
        <!-- Start Page Content -->
        <!-- ============================================================== -->
        <div class="row">
            <div class="col-12">             
                <div class="card">
                    <div class="card-body">
                        <h4 class="card-title">Advertisement Category</h4>
                        <div class="table-responsive m-t-40">
                            <table id="example23"
                                class="display nowrap table table-hover table-striped table-bordered"
                                cellspacing="0" width="100%">
                                <thead>
                                    <tr>
                                        <th>Sr No</th>
                                        <th>Slot</th>
                                        <th>Size</th>
                                        <th>Status</th>
                                        <th>Created By</th>
                                        <th>Date</th>
                                        <th>Action</th>
                                    </tr>
                                </thead>
                                <tfoot>
                                    <tr>
                                        <th>Sr No</th>
                                        <th>Slot</th>
                                        <th>Size</th>
                                        <th>Status</th>
                                        <th>Created By</th>
                                        <th>Date</th>
                                        <th>Action</th>
                                    </tr>
                                </tfoot>
                                <tbody>
                                  @php
                                    $num=1;
                                    @endphp
                                    @foreach($ads as $value)
                                    <tr>
                                        <td>{{$num}}</td>
                                        <td>{{$value->name}}</td>
                                        <td>{{$value->height.' X '.$value->width}} </td>
                                        <td><input type="checkbox" id ="{{$value->id}}" url="adsstatus" data-toggle="toggle" data-on="Enabled" data-off="Disabled" class="toggle_btn" data-onstyle="success" data-offstyle="danger" {{ $value->status == 1 ? "checked" : "" }}></td>
                                        <td>{{$value->uname}}</td>
                                        <td>{{$value->date}}</td>
                                        <td> 
                                            <a href = "" class='forum-title update_ads' value="{{json_encode($value)}}" data-toggle="modal" data-target="#update_model"><i class="ti-pencil mr-1 btn btn-success"></i></a>
                                              
                                            <a href = "{{route('ads-delete',$value->id)}}" class='forum-title delete'><i class="ti-trash btn btn-danger"></i></a>
                                            </td>
                                    </tr>
                                @php
                                $num++;
                                @endphp
                                @endforeach
                                   
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- Advertisemet price ------------------------------------------------------------------------ -->
        <div class="row">
            <div class="col-12">             
                <div class="card">
                    <div class="card-body">
                        <h4 class="card-title">Advertisement Price  <a href = "" title="new" class='forum-title' data-toggle="modal" data-target="#price_model" id="newpricebtn"><span class="fa btn-success fa-plus-circle"></span></a></h4>
                        <div class="table-responsive m-t-40">
                            <table id="example24"
                                class="display nowrap table table-hover table-striped table-bordered"
                                cellspacing="0" width="100%">
                                <thead>
                                    <tr>
                                        <th>Sr No</th>
                                        <th>Slot</th>
                                        <th>Size</th>
                                        <th>Price</th>
                                        <th>Price Type</th>
                                        <th>Panchayat</th>
                                        <th>Status</th>
                                        <!-- <th>Created By</th>
                                        <th>Date</th> -->
                                        <th>Action</th>
                                    </tr>
                                </thead>
                                <tfoot>
                                    <tr>
                                        <th>Sr No</th>
                                        <th>Slot</th>
                                        <th>Size</th>
                                        <th>Price</th>
                                        <th>Price Type</th>
                                        <th>Panchayat</th>
                                        <th>Status</th>
                                        <!-- <th>Created By</th>
                                        <th>Date</th> -->
                                        <th>Action</th>
                                    </tr>
                                </tfoot>
                                <tbody>
                                  @php
                                    $num=1;
                                    @endphp
                                    @foreach($price as $value)
                                    <tr>
                                        <td>{{$num}}</td>
                                        <td>{{$value->name}}</td>
                                        <td>{{$value->height.' X '.$value->width}} </td>
                                        <td>{{$value->price}} </td>
                                        <td>{{$value->price_type}} </td>
                                        <td>{{$value->panc_name ?? ''}} </td>
                                        <td><input type="checkbox" id ="{{$value->id}}" url="pricestatus" data-toggle="toggle" data-on="Enabled" data-off="Disabled" class="toggle_btn" data-onstyle="success" data-offstyle="danger" {{ $value->status == 1 ? "checked" : "" }}></td>
                                        <!-- <td>{{$value->created_by}}</td>
                                        <td>{{$value->created_att}}</td> -->
                                        <td> 
                                            <a href = "" class='forum-title update_ads_price' value="{{json_encode($value)}}" data-toggle="modal" data-target="#price_model"><i class="ti-pencil mr-1 btn btn-success"></i></a>
                                              
                                            <a href = "{{route('adspricedelete',$value->id)}}" class='forum-title delete'><i class="ti-trash btn btn-danger"></i></a>
                                            </td>
                                    </tr>
                                @php
                                $num++;
                                @endphp
                                @endforeach
                                   
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="modal fade" id="update_model">
            <div class="modal-dialog modal-dialog-centered" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <h5 class="modal-title">Update Category</h5>
                        <button type="button" class="close" data-dismiss="modal"><span>&times;</span></button>
                    </div>
                    <div class="modal-body">
                        <form  action="{{route('ads-update')}}"  method ="post" class="ajaxform needs-validation" novalidate="" enctype="multipart/form-data">
                          @csrf
                            <div class="form-row">
                                <div class="col-md-12">
                                    <label for="validationCustom01">Ads Name</label>
                                    <input type="text" name="name"  value="" class="form-control" id="ads-name" placeholder="Ads Name"  required="">
                                </div>
                            </div>
                            <div class="form-row">
                                <div class="col-md-12">
                                    <label for="validationCustom02">Height</label>
                                    <input type="text"  value="" class="form-control" id="ads-height" name="height" placeholder="Enter height.." required="">
                                    
                                </div>
                            </div>
                            <div class="form-row">
                                <div class="col-md-12">
                                    <label for="validationCustom01">Width</label>
                                    <input type="text" class="form-control" id="ads-width"  value="" name="width" placeholder="Enter Width" required="">
                                </div>
                            </div>
                            <div class="form-row mt-1">
                                <input type="hidden" name="ads_id" id="ads_id">
                                <input type="hidden" name="action-url" value="{{route('ads-update')}}">
                                <button class="btn btn-primary" type="submit">Submit</button>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
        <div class="modal fade" id="price_model">
            <div class="modal-dialog modal-dialog-centered" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <h5 class="modal-title">New Price</h5>
                        <button type="button" class="close" data-dismiss="modal"><span>&times;</span></button>
                    </div>
                    <div class="modal-body">
                        <form method ="post" class="ajaxform needs-validation" novalidate="" enctype="multipart/form-data">
                          @csrf
                            <div class="form-row">
                                <div class="col-md-12">
                                    <label for="validationCustom01">Select Ads</label>
                                    <select name="ads_category" id="ads_category" class="form-control">
                                        @foreach($ads as $item)
                                           <option value="{{$item->id}}" > {{$item->name .' '. $item->height . ' X ' . $item->width}}</option>
                                        @endforeach
                                    </select>
                                </div>
                            </div>
                            <div class="form-row">
                                <div class="col-md-12">
                                    <label for="validationCustom02">Enter Pice</label>
                                    <input type="number"  value="" class="form-control" id="ads-price" name="price" placeholder="Enter Price..." required="">
                                    
                                </div>
                            </div>
                            <div class="form-row">
                                <div class="col-md-12">
                                    <label for="validationCustom01">Price Type</label>
                                    <select name = "price_type" id="price_type" class="form-control">
                                        <option value="Daily">Daily</option>
                                        <option value="Monthly">Monthly</option>
                                        <option value="Yearly">Yearly</option>
                                    </select>
                                </div>
                            </div>
                            <div class="form-row mt-2">
                                <input type="hidden" name="pid" id="pid">
                                <input type="hidden" name="action-url" value="{{route('ads-price')}}">
                                <button class="btn btn-primary" type="submit">Save</button>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
        <!-- end -->
    </div>
</div>
@endsection
@section('js')
    <!--=========================*
            This Page Scripts
    *===========================-->
    <!-- Data Table js -->
   <script>
        $(function () {
            $('#myTable').DataTable();
            var table = $('#example').DataTable({
                "columnDefs": [{
                    "visible": false,
                    "targets": 2
                }],
                "order": [
                    [2, 'asc']
                ],
                "displayLength": 25,
                "drawCallback": function (settings) {
                    var api = this.api();
                    var rows = api.rows({
                        page: 'current'
                    }).nodes();
                    var last = null;
                    api.column(2, {
                        page: 'current'
                    }).data().each(function (group, i) {
                        if (last !== group) {
                            $(rows).eq(i).before('<tr class="group"><td colspan="5">' + group + '</td></tr>');
                            last = group;
                        }
                    });
                }
            });
            // Order by the grouping
            $('#example tbody').on('click', 'tr.group', function () {
                var currentOrder = table.order()[0];
                if (currentOrder[0] === 2 && currentOrder[1] === 'asc') {
                    table.order([2, 'desc']).draw();
                } else {
                    table.order([2, 'asc']).draw();
                }
            });
            // responsive table
            $('#config-table').DataTable({
                responsive: true
            });
            $('#example24').DataTable({
                dom: 'Bfrtip',
                buttons: [
                    'csv', 'excel', 'pdf', 'print'
                ]
            });
            $('#example23').DataTable({
                dom: 'Bfrtip',
                buttons: [
                    'copy', 'csv', 'excel', 'pdf', 'print'
                ]
            });
            $('.buttons-copy, .buttons-csv, .buttons-print, .buttons-pdf, .buttons-excel').addClass('btn btn-primary mr-1');
        });

    </script>
    <script type="text/javascript">
        $(document).ready(function(){
            $(".update_ads").click(function(e){
                var data = $(this).attr('value');
                var d = jQuery.parseJSON(data);
                $("#ads-name").val(d.name);
                $("#ads-height").val(d.height);
                $("#ads-width").val(d.width);
                $("#ads_id").val(d.id);
            });
            $(".update_ads_price").click(function(e){
                var data = $(this).attr('value');
                var d = jQuery.parseJSON(data);
                $("#ads-price").val(d.price);
                $('#price_type').val(d.price_type);
                $('#ads_category').val(d.ads_category);
                $("#pid").val(d.id);
            });
            $("#newpricebtn").click(function(){
                $("#pid").val("");
            });
        });
    </script>
@endsection
