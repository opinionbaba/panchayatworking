@extends('layouts.app')


{{-- Page Title --}}
@section('page-title')
  Accounts
@endsection
{{-- This Page Css --}}
@section('css')
 <link rel="stylesheet" type="text/css"
        href="{{asset('public/assets/datatables.net-bs4/css/dataTables.bootstrap4.css')}}">
    <link rel="stylesheet" type="text/css"
        href="{{asset('public/assets/datatables.net-bs4/css/responsive.dataTables.min.css')}}">
    <!--=========================*
               Datatable
  
  *===========================-->
  <body class="skin-blue fixed-layout">
    <!-- ============================================================== -->
    <!-- Preloader - style you can find in spinners.css -->
    <!-- ============================================================== -->
    <div class="preloader">
        <div class="loader">
            <div class="loader__figure"></div>
        </div>
    </div>
	 <div id="main-wrapper">
	  
@endsection
@section('main-content')
 <div class="page-wrapper">
      <div class="container-fluid">
          <div class="row page-titles">
                    <div class="col-md-5 align-self-center">
                        <h4 class="text-themecolor">Datatable</h4>
                    </div>
                    <div class="col-md-7 align-self-center text-right">
                        <div class="d-flex justify-content-end align-items-center">
                            <ol class="breadcrumb">
                                <li class="breadcrumb-item"><a href="javascript:void(0)">Home</a></li>
                                <li class="breadcrumb-item active">Datatable</li>
                            </ol>
                            <a href="{{route('get_step_first')}}" class="btn btn-info d-none d-lg-block m-l-15" ><i
                                    class="fa fa-plus-circle"></i> Add New Panchayat</a>
									
                        </div>
                    </div>
                </div>
                 <div class="row">
                    <div class="col-12">
					  @if(Session::has('add'))
				<div class="alert alert-info alert-dismissible fade show" role="alert">
					<strong></strong>  {{Session::get('add')}}
					<button type="button" class="close" data-dismiss="alert" aria-label="Close"><span class="ti-close"></span>
					</button>
				</div>
				@endif
				@if(Session::has('delete'))
				<div class="alert alert-danger alert-dismissible fade show" role="alert">
					<strong></strong>  {{Session::get('delete')}}
					<button type="button" class="close" data-dismiss="alert" aria-label="Close"><span class="ti-close"></span>
					</button>
				</div>
				@endif
                        <div class="card">
                            <div class="card-body">
                                <h4 class="card-title">Data Export</h4>
                                <h6 class="card-subtitle">Export data to Copy, CSV, Excel, PDF & Print</h6>
                                <div class="table-responsive m-t-40">
                                    <table id="example23"
                                        class="display nowrap table table-hover table-striped table-bordered"
                                        cellspacing="0" width="100%">
                                        <thead>
                                            <tr>
                                                <th>Sr No</th>
												<th>Panchayat Name</th>
												<th>Number</th>
												<th>Address</th>
												<th>Email</th>
												<th>Domain</th>
												<th>Logo</th>
												<th>App Logo</th>
												<th>Action</th>
                                            </tr>
                                        </thead>
                                        <tfoot>
                                            <tr>
                                                <th>Sr No</th>
												<th>Panchayat Name</th>
												<th>Number</th>
												<th>Address</th>
												<th>Email</th>
												<th>Domain</th>
												<th>Logo</th>
												<th>App Logo</th>
												<th>Action</th>
                                            </tr>
                                        </tfoot>
                                        <tbody>
                                           @php
														$num =1;
														@endphp
														@foreach($all as $value)
														<tr>
															<td>{{$num}}</td>
															<td><span class="badge badge-info">{{$value->panc_name}}</span></td>
															<td>{{$value->panc_number}}</td>
															<td>{{$value->panc_address}}</td>
															<td>{{$value->panc_email}}</td>
															<td>{{$value->panc_domain}}</td>
															<td><img src="{{asset('public/upload/panchayat/panchayat1/logo')}}/{{$value->panc_logo}}" class="img-circle" width="45px" alt="{{$value->panc_logo}}"></img></td>
															<td><img src="{{asset('public/upload/panchayat/panchayat1/logo')}}/{{$value->panc_app_logo}}" class="img-circle" width="45px" alt="{{$value->panc_app_logo}}"></img></td>
								<td>
								  <a href = "" class='forum-title' name = "abc" data-toggle="modal" data-target="#usermodal{{$value->id}}"><i class="ti-pencil mr-1 btn btn-success"></i></a>
								  <a href = "{{route('show_panchayat',$value->id)}}" class='forum-title' name = "abc" ><i class="ti-eye mr-1 btn btn-success"></i></a>
									 <a href = "{{route('delete-panchayat',$value->id)}}" class='forum-title'><i class="ti-trash btn btn-danger"></i></a>
								</td>
														</tr>
								 <div class="modal fade" id="usermodal{{$value->id}}">
									<div class="modal-dialog modal-dialog-centered" role="document">
										<div class="modal-content">
											<div class="modal-header">
												<h5 class="modal-title">Update Form</h5>
												<button type="button" class="close" data-dismiss="modal"><span>&times;</span></button>
											</div>
											<div class="modal-body">
												<form method="post" action="{{route('edit_panchyat')}}" class="needs-validation" novalidate="" enctype="multipart/form-data">
													@csrf
													<div class="form-row">
														<div class="col-md-4 mb-3">
															<label for="validationCustom01">Panchyat name</label>
															<input type="text" name="panc_name" class="form-control" value="{{$value->panc_name}}"  required="">
															
														</div>
														<div class="col-md-4 mb-3">
															<label for="validationCustom02">Panchyat Number</label>
															<input type="text" name="panc_number" class="form-control" value="{{$value->panc_number}} " required="">
															
														</div>
														<div class="col-md-4 mb-3">
															<label for="validationCustomUsername">Panchyat Address</label>
															<div class="input-group">
																<textarea name="panc_address" class="form-control" placeholder="Enter Panchyat Address.." rows="1">{{$value->panc_address}}</textarea>
																
															</div>
														</div>
													</div>
													<div class="form-row">
														<div class="col-md-6 mb-3">
															<label for="validationCustom01">Panchyat Email</label>
															<input type="email" name="panc_email" class="form-control" value="{{$value->panc_email}} " required="">
															
														</div>
														<div class="col-md-6 mb-3">
															<label for="validationCustom02">Panchyat Domain</label>
															<input type="text" name="panc_domain" class="form-control"value="{{$value->panc_domain}} " required="">
														
														</div>
													</div>
													<div class="form-row">
														<div class="col-md-6 mb-3">
															<label for="validationCustom01">Panchyat Latitude</label>
															<input type="text" name="panc_lat"  class="form-control" value="{{$value->panc_lat}} " required="">
															<div class="valid-feedback">
																Looks good!
															</div>
														</div>
														<div class="col-md-6 mb-3">
															<label for="validationCustom02">Panchyat Longitude</label>
															<input type="text" name="panc_long" class="form-control" value="{{$value->panc_long}} "  required="">
															
														</div>
													</div>
													<div class="form-row">
														<div class="col-md-6 mb-3">
															<label for="validationCustom01">Panchyat Logo</label>
															<input type="file" name="panc_logo" class="form-control"  required="">
														</div>
														<div class="col-md-6 mb-3">
															<label for="validationCustom02">Panchyat App Logo</label>
															<input type="file" name="panc_app_logo" class="form-control">
														</div>
													</div>
													<button class="btn btn-primary" type="submit">Submit form</button>
												</form>
											</div>
										</div>
									</div>
								</div>
														@php
														$num++;
														@endphp
														@endforeach
                                           
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                       
                       
                    </div>
                </div>
               </div>
			 <div class="modal bs-example-modal-lg" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true" style="display: none;">
				<div class="modal-dialog modal-xl">
					<div class="modal-content">
						<div class="modal-header">
							<h4 class="modal-title" id="myLargeModalLabel">Add New Panchayat Accounts</h4>
							<button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
						</div>
						<div class="modal-body">
							<form method="post" action="#" class="needs-validation" novalidate="" enctype="multipart/form-data">
													@csrf
													<div class="form-row">
														<div class="col-md-4 mb-3">
															<label for="validationCustom01">Panchyat name</label>
															<input type="text" name="panc_name" class="form-control" id="validationCustom01" placeholder="Panchyat name.."  required="">
															
														</div>
														<div class="col-md-4 mb-3">
															<label for="validationCustom02">Panchyat Number</label>
															<input type="number" name="panc_number" class="form-control" id="validationCustom02" placeholder="Enter number.." required="">
															
														</div>
														<div class="col-md-4 mb-3">
															<label for="validationCustomUsername">Panchyat Address</label>
															<div class="input-group">
																<textarea name="panc_address" class="form-control" placeholder="Enter Panchyat Address.." rows="1"></textarea>
																
															</div>
														</div>
													</div>
													<div class="form-row">
														<div class="col-md-6 mb-3">
															<label for="validationCustom01">Panchyat Email</label>
															<input type="email" name="panc_email" class="form-control" id="validationCustom01" placeholder="Enter Email" required="">
															
														</div>
														<div class="col-md-6 mb-3">
															<label for="validationCustom02">Panchyat Domain</label>
															<input type="text" name="panc_domain" class="form-control" id="validationCustom02" placeholder="Enter Domain.." required="">
														
														</div>
													</div>
													<div class="form-row">
														<div class="col-md-6 mb-3">
															<label for="validationCustom01">Panchyat Latitude</label>
															<input type="text" name="panc_lat"  class="form-control" id="validationCustom01" placeholder="Ex:91.222" required="">
															<div class="valid-feedback">
																Looks good!
															</div>
														</div>
														<div class="col-md-6 mb-3">
															<label for="validationCustom02">Panchyat Longitude</label>
															<input type="text" name="panc_long" class="form-control" id="validationCustom02" placeholder="Ex:90.222"  required="">
															
														</div>
													</div>
													<div class="form-row">
														<div class="col-md-6 mb-3">
															<label for="validationCustom01">Panchyat Logo</label>
															<input type="file" name="panc_logo" class="form-control"  required="">
															
														</div>
														<div class="col-md-6 mb-3">
															<label for="validationCustom02">Panchyat App Logo</label>
															<input type="file" name="panc_app_logo" class="form-control">
															
														</div>
													</div>
													<button class="btn btn-primary" type="submit">Submit form</button>
												</form>
						
						</div>
						<div class="modal-footer">
							<button type="button" class="btn btn-danger waves-effect text-left" data-dismiss="modal">Close</button>
						</div>
					</div>
					<!-- /.modal-content -->
				</div>
				<!-- /.modal-dialog -->
			</div>
            <!-- ============================================================== -->
            <!-- End Container fluid  -->
            <!-- ============================================================== -->
 
 </div>
@endsection
@section('js')
  <script>
        $(function () {
            $('#myTable').DataTable();
            var table = $('#example').DataTable({
                "columnDefs": [{
                    "visible": false,
                    "targets": 2
                }],
                "order": [
                    [2, 'asc']
                ],
                "displayLength": 25,
                "drawCallback": function (settings) {
                    var api = this.api();
                    var rows = api.rows({
                        page: 'current'
                    }).nodes();
                    var last = null;
                    api.column(2, {
                        page: 'current'
                    }).data().each(function (group, i) {
                        if (last !== group) {
                            $(rows).eq(i).before('<tr class="group"><td colspan="5">' + group + '</td></tr>');
                            last = group;
                        }
                    });
                }
            });
            // Order by the grouping
            $('#example tbody').on('click', 'tr.group', function () {
                var currentOrder = table.order()[0];
                if (currentOrder[0] === 2 && currentOrder[1] === 'asc') {
                    table.order([2, 'desc']).draw();
                } else {
                    table.order([2, 'asc']).draw();
                }
            });
            // responsive table
            $('#config-table').DataTable({
                responsive: true
            });
            $('#example23').DataTable({
                dom: 'Bfrtip',
                buttons: [
                    'copy', 'csv', 'excel', 'pdf', 'print'
                ]
            });
            $('.buttons-copy, .buttons-csv, .buttons-print, .buttons-pdf, .buttons-excel').addClass('btn btn-primary mr-1');
        });

    </script>
@endsection
