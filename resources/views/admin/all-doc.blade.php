@extends('layouts.app')


{{-- Page Title --}}
@section('page-title')
    Data Table
@endsection

{{-- This Page Css --}}
@section('css')
   <link rel="stylesheet" type="text/css"
        href="{{asset('public/assets/datatables.net-bs4/css/dataTables.bootstrap4.css')}}">
    <link rel="stylesheet" type="text/css"
        href="{{asset('public/assets/datatables.net-bs4/css/responsive.dataTables.min.css')}}">
    <!--=========================*
               Datatable
  
  *===========================-->
  <body class="skin-blue fixed-layout">
    <!-- ============================================================== -->
    <!-- Preloader - style you can find in spinners.css -->
    <!-- ============================================================== -->
    <div class="preloader">
        <div class="loader">
            <div class="loader__figure"></div>
        </div>
    </div>
	 <div id="main-wrapper">
@endsection

@section('main-content')
<div class="page-wrapper">
      <div class="container-fluid">
                <!-- ============================================================== -->
                <!-- Bread crumb and right sidebar toggle -->
                <!-- ============================================================== -->
                <div class="row page-titles">
                    <div class="col-md-5 align-self-center">
                        <h4 class="text-themecolor">All Documents</h4>
                    </div>
                    <div class="col-md-7 align-self-center text-right">
                        <div class="d-flex justify-content-end align-items-center">
                            <ol class="breadcrumb">
                                <li class="breadcrumb-item"><a href="javascript:void(0)">Home</a></li>
                                <li class="breadcrumb-item active">Datatable</li>
                            </ol>
                            <a href="{{route('admin.adddoc')}}" class="btn btn-info d-none d-lg-block m-l-15" ><i
                                    class="fa fa-plus-circle"></i> Add New document</a>
									
                        </div>
                    </div>
                </div>
                <!-- ============================================================== -->
                <!-- End Bread crumb and right sidebar toggle -->
                <!-- ============================================================== -->
                <!-- ============================================================== -->
                <!-- Start Page Content -->
                <!-- ============================================================== -->
                <div class="row">
                    <div class="col-12">
					  @if(Session::has('success'))
						<div class="alert alert-warning alert-dismissible fade show" role="alert">
							<strong>Success!</strong>  {{Session::get('success')}}
							<button type="button" class="close" data-dismiss="alert" aria-label="Close"><span class="ti-close"></span>
							</button>
						</div>
						@endif
						@if(Session::has('delete'))
						<div class="alert alert-success alert-dismissible fade show" role="alert">
							<strong>Success!</strong> Session::get('delete')
							<button type="button" class="close" data-dismiss="alert" aria-label="Close"><span class="ti-close"></span>
							</button>
						</div>
						@endif
						
                        <div class="card">
                            <div class="card-body">
                                <h4 class="card-title">Document Data</h4>
                                <h6 class="card-subtitle">Export data to Copy, CSV, Excel, PDF & Print</h6>
                                <div class="table-responsive m-t-40">
                                    <table id="example23"
                                        class="display nowrap table table-hover table-striped table-bordered"
                                        cellspacing="0" width="100%">
                                        <thead>
                                            <tr>
                                                <th>Sr No</th>
												<th>Form Name</th>
												<th>Form</th>
												<th>date</th>
												<th>Action</th>
                               
                                            </tr>
                                        </thead>
                                        <tfoot>
                                            <tr>
                                                <th>Sr No</th>
												<th>Form Name</th>
												<th>Form</th>
												<th>date</th>
												<th>Action</th>
                                            </tr>
                                        </tfoot>
                                        <tbody>
                                           							@php
							$num = 1;
							@endphp
							@foreach($all as $value)
                            <tr>
							<td>{{$num}}</td>
                                <td>{{$value->doc_name}}</td>
                                <td><img src="{{asset('public/upload/form_temp')}}/{{$value->file}}" style=" height: 50px;" alt="{{$value->file}}"></img></td>
                                <td>{{$value->created_at}}</td>
                                <td> 
								  <a href = "" class='forum-title' name = "abc" data-toggle="modal" data-target="#usermodal{{$value->id}}"><i class="ti-pencil mr-1 btn btn-success"></i></a>
									 <a href = "form-delete/{{$value->id}}" class='forum-title'><i class="ti-trash btn btn-danger"></i></a>
								</td>
								 <!-- Vertically centered modal -->
								<div class="modal fade" id="usermodal{{$value->id}}">
									<div class="modal-dialog modal-dialog-centered" role="document">
										<div class="modal-content">
											<div class="modal-header">
												<h5 class="modal-title">Update Form</h5>
												<button type="button" class="close" data-dismiss="modal"><span>&times;</span></button>
											</div>
											<div class="modal-body">
											 <form action="{{route('admin.update-form')}}" method="post" class="needs-validation" novalidate enctype="multipart/form-data">
													@csrf
												   <div class="row">
														<div class="col-md-6">
															<label for="validationTooltip01">Name Of Document</label>
															<input type="text" name="name" class="form-control" id="validationTooltip01" value="{{$value->doc_name}}" required>
															<div class="valid-tooltip">
																Looks good!
															</div>
														</div>
													
													
													   <div class="col-md-6">
															<label class="custom-file-label" for="inputGroupFile02">Choose file</label>
															<input type="file" name="doc" class="form-control" id="inputGroupFile02">
													   </div>
													</div>
													</div>
												<div class="modal-footer">
													<button class="btn btn-primary" type="submit">Submit form</button>
												<button type="button" class="btn btn-light" data-dismiss="modal">Close</button>
												</div>
												</form>
											</div>
										</div>
									</div>
								</div>
								@php
								$num++;
								@endphp
							</tr>
							@endforeach
                                           
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                       
                       
                    </div>
                </div>
            </div>
			
 </div>
   
@endsection

@section('js')
    <script>
        $(function () {
            $('#myTable').DataTable();
            var table = $('#example').DataTable({
                "columnDefs": [{
                    "visible": false,
                    "targets": 2
                }],
                "order": [
                    [2, 'asc']
                ],
                "displayLength": 25,
                "drawCallback": function (settings) {
                    var api = this.api();
                    var rows = api.rows({
                        page: 'current'
                    }).nodes();
                    var last = null;
                    api.column(2, {
                        page: 'current'
                    }).data().each(function (group, i) {
                        if (last !== group) {
                            $(rows).eq(i).before('<tr class="group"><td colspan="5">' + group + '</td></tr>');
                            last = group;
                        }
                    });
                }
            });
            // Order by the grouping
            $('#example tbody').on('click', 'tr.group', function () {
                var currentOrder = table.order()[0];
                if (currentOrder[0] === 2 && currentOrder[1] === 'asc') {
                    table.order([2, 'desc']).draw();
                } else {
                    table.order([2, 'asc']).draw();
                }
            });
            // responsive table
            $('#config-table').DataTable({
                responsive: true
            });
            $('#example23').DataTable({
                dom: 'Bfrtip',
                buttons: [
                    'copy', 'csv', 'excel', 'pdf', 'print'
                ]
            });
            $('.buttons-copy, .buttons-csv, .buttons-print, .buttons-pdf, .buttons-excel').addClass('btn btn-primary mr-1');
        });

    </script>
  
@endsection
