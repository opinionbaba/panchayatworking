@extends('layouts.app')


{{-- Page Title --}}
@section('page-title')
    Gallery
@endsection

{{-- This Page Css --}}
@section('css')
 <link rel="stylesheet" href="{{asset('public/assets/Magnific-Popup-master/dist/magnific-popup.css')}}">
 <link rel="stylesheet" href="{{asset('public/assets/dist/css/pages/user-card.css')}}">
   <body class="skin-blue fixed-layout">
    <!-- ============================================================== -->
    <!-- Preloader - style you can find in spinners.css -->
    <!-- ============================================================== -->
    <div class="preloader">
        <div class="loader">
            <div class="loader__figure"></div>
        </div>
    </div>
	 <div id="main-wrapper">
	 
@endsection
@section('main-content')
        <div class="page-wrapper">
            
            <div class="container-fluid">
                <div class="row page-titles">
                    <div class="col-md-5 align-self-center">
                        <h4 class="text-themecolor">User Card</h4>
                    </div>
                    <div class="col-md-7 align-self-center text-right">
                        <div class="d-flex justify-content-end align-items-center">
                            <ol class="breadcrumb">
                                <li class="breadcrumb-item"><a href="javascript:void(0)">Home</a></li>
                                <li class="breadcrumb-item active">User Card</li>
                            </ol>
                            <button type="button" class="btn btn-info d-none d-lg-block m-l-15"><i class="fa fa-plus-circle"></i> Create New</button>
                        </div>
                    </div>
                </div>
				
				 <div class="row el-element-overlay">
                  
                    <div class="col-lg-3 col-md-6">
                        <div class="card">
                            <div class="el-card-item">
                                <div class="el-card-avatar el-overlay-1">
                                    <img src="{{asset('public/assets_old/images/portfolio/bw1.jpg')}}" alt="user" />
                                    <div class="el-overlay scrl-dwn">
                                        <ul class="el-info">
                                            <li>
                                                <a class="btn default btn-outline image-popup-vertical-fit" href="{{asset('public/assets_old/images/portfolio/bw1.jpg')}}">
                                                    <i class="icon-magnifier"></i>
                                                </a>
                                            </li>
                                            <li>
                                                <a class="btn default btn-outline" href="javascript:void(0);">
                                                    <i class="icon-link"></i>
                                                </a>
                                            </li>
                                        </ul>
                                    </div>
                                </div>
                                <div class="el-card-content">
                                    <h4 class="box-title">Genelia Deshmukh</h4>
                                    <small>Managing Director</small>
                                    <br/> </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-3 col-md-6">
                        <div class="card">
                            <div class="el-card-item">
                                <div class="el-card-avatar el-overlay-1">
                                    <img src="{{asset('public/assets_old/images/portfolio/bw1.jpg')}}" alt="user" />
                                    <div class="el-overlay scrl-dwn">
                                        <ul class="el-info">
                                            <li>
                                                <a class="btn default btn-outline image-popup-vertical-fit" href="{{asset('public/assets_old/images/portfolio/bw1.jpg')}}">
                                                    <i class="icon-magnifier"></i>
                                                </a>
                                            </li>
                                            <li>
                                                <a class="btn default btn-outline" href="javascript:void(0);">
                                                    <i class="icon-link"></i>
                                                </a>
                                            </li>
                                        </ul>
                                    </div>
                                </div>
                                <div class="el-card-content">
                                    <h4 class="box-title">Genelia Deshmukh</h4>
                                    <small>Managing Director</small>
                                    <br/> </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-3 col-md-6">
                        <div class="card">
                            <div class="el-card-item">
                                <div class="el-card-avatar el-overlay-1">
                                    <img src="{{asset('public/assets_old/images/portfolio/bw3.jpg')}}" alt="user" />
                                    <div class="el-overlay scrl-dwn">
                                        <ul class="el-info">
                                            <li>
                                                <a class="btn default btn-outline image-popup-vertical-fit" href="{{asset('public/assets_old/images/portfolio/bw3.jpg')}}">
                                                    <i class="icon-magnifier"></i>
                                                </a>
                                            </li>
                                            <li>
                                                <a class="btn default btn-outline" href="javascript:void(0);">
                                                    <i class="icon-link"></i>
                                                </a>
                                            </li>
                                        </ul>
                                    </div>
                                </div>
                                <div class="el-card-content">
                                    <h4 class="box-title">Genelia Deshmukh</h4>
                                    <small>Managing Director</small>
                                    <br/> </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-3 col-md-6">
                        <div class="card">
                            <div class="el-card-item">
                                <div class="el-card-avatar el-overlay-1">
                                    <img src="{{asset('public/assets_old/images/portfolio/bw4.jpg')}}" alt="user" />
                                    <div class="el-overlay scrl-dwn">
                                        <ul class="el-info">
                                            <li>
                                                <a class="btn default btn-outline image-popup-vertical-fit" href="{{asset('public/assets_old/images/portfolio/bw4.jpg')}}">
                                                    <i class="icon-magnifier"></i>
                                                </a>
                                            </li>
                                            <li>
                                                <a class="btn default btn-outline" href="javascript:void(0);">
                                                    <i class="icon-link"></i>
                                                </a>
                                            </li>
                                        </ul>
                                    </div>
                                </div>
                                <div class="el-card-content">
                                    <h4 class="box-title">Genelia Deshmukh</h4>
                                    <small>Managing Director</small>
                                    <br/> </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-3 col-md-6">
                        <div class="card">
                            <div class="el-card-item">
                                <div class="el-card-avatar el-overlay-1">
                                    <img src="{{asset('public/assets_old/images/portfolio/bw5.jpg')}}" alt="user" />
                                    <div class="el-overlay scrl-dwn">
                                        <ul class="el-info">
                                            <li>
                                                <a class="btn default btn-outline image-popup-vertical-fit" href="{{asset('public/assets_old/images/portfolio/bw5.jpg')}}">
                                                    <i class="icon-magnifier"></i>
                                                </a>
                                            </li>
                                            <li>
                                                <a class="btn default btn-outline" href="javascript:void(0);">
                                                    <i class="icon-link"></i>
                                                </a>
                                            </li>
                                        </ul>
                                    </div>
                                </div>
                                <div class="el-card-content">
                                    <h4 class="box-title">Genelia Deshmukh</h4>
                                    <small>Managing Director</small>
                                    <br/> </div>
                            </div>
                        </div>
                    </div>  


                     <div class="col-lg-3 col-md-6">
                        <div class="card">
                            <div class="el-card-item">
                                <div class="el-card-avatar el-overlay-1">
                                    <img src="{{asset('public/assets_old/images/portfolio/bw1.jpg')}}" alt="user" />
                                    <div class="el-overlay scrl-dwn">
                                        <ul class="el-info">
                                            <li>
                                                <a class="btn default btn-outline image-popup-vertical-fit" href="{{asset('public/assets_old/images/portfolio/bw1.jpg')}}">
                                                    <i class="icon-magnifier"></i>
                                                </a>
                                            </li>
                                            <li>
                                                <a class="btn default btn-outline" href="javascript:void(0);">
                                                    <i class="icon-link"></i>
                                                </a>
                                            </li>
                                        </ul>
                                    </div>
                                </div>
                                <div class="el-card-content">
                                    <h4 class="box-title">Genelia Deshmukh</h4>
                                    <small>Managing Director</small>
                                    <br/> </div>
                            </div>
                        </div>
                    </div>


                     <div class="col-lg-3 col-md-6">
                        <div class="card">
                            <div class="el-card-item">
                                <div class="el-card-avatar el-overlay-1">
                                    <img src="{{asset('public/assets_old/images/portfolio/bw7.jpg')}}" alt="user" />
                                    <div class="el-overlay scrl-dwn">
                                        <ul class="el-info">
                                            <li>
                                                <a class="btn default btn-outline image-popup-vertical-fit" href="{{asset('public/assets_old/images/portfolio/bw7.jpg')}}">
                                                    <i class="icon-magnifier"></i>
                                                </a>
                                            </li>
                                            <li>
                                                <a class="btn default btn-outline" href="javascript:void(0);">
                                                    <i class="icon-link"></i>
                                                </a>
                                            </li>
                                        </ul>
                                    </div>
                                </div>
                                <div class="el-card-content">
                                    <h4 class="box-title">Genelia Deshmukh</h4>
                                    <small>Managing Director</small>
                                    <br/> </div>
                            </div>
                        </div>
                    </div>

                     <div class="col-lg-3 col-md-6">
                        <div class="card">
                            <div class="el-card-item">
                                <div class="el-card-avatar el-overlay-1">
                                    <img src="{{asset('public/assets_old/images/portfolio/bw8.jpg')}}" alt="user" />
                                    <div class="el-overlay scrl-dwn">
                                        <ul class="el-info">
                                            <li>
                                                <a class="btn default btn-outline image-popup-vertical-fit" href="{{asset('public/assets_old/images/portfolio/bw8.jpg')}}">
                                                    <i class="icon-magnifier"></i>
                                                </a>
                                            </li>
                                            <li>
                                                <a class="btn default btn-outline" href="javascript:void(0);">
                                                    <i class="icon-link"></i>
                                                </a>
                                            </li>
                                        </ul>
                                    </div>
                                </div>
                                <div class="el-card-content">
                                    <h4 class="box-title">Genelia Deshmukh</h4>
                                    <small>Managing Director</small>
                                    <br/> </div>
                            </div>
                        </div>
                    </div>					


			   </div>
				
				
				
				
			</div>
		</div>




@endsection

@section('js')
   
@endsection
