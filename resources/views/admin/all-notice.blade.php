@extends('layouts.app')


{{-- Page Title --}}
@section('page-title')
   Notice
@endsection

{{-- This Page Css --}}
@section('css')
  <body class="skin-blue fixed-layout">
    <div class="preloader">
        <div class="loader">
            <div class="loader__figure"></div>
        </div>
    </div>
	 <div id="main-wrapper">
@endsection

@section('main-content')
<div class="page-wrapper">
     <div class="container-fluid">
        <!-- ============================================================== -->
        <!-- Bread crumb and right sidebar toggle -->
        <!-- ============================================================== -->
        <div class="row page-titles">
            <div class="col-md-5 align-self-center">
                <h4 class="text-themecolor">Datatable</h4>
            </div>
            <div class="col-md-7 align-self-center text-right">
                <div class="d-flex justify-content-end align-items-center">
                    <ol class="breadcrumb">
                        <li class="breadcrumb-item"><a href="javascript:void(0)">Home</a></li>
                        <li class="breadcrumb-item active">Datatable</li>
                    </ol>
                    <a href="{{route('admin.addnotice')}}" class="btn btn-info d-none d-lg-block m-l-15"><i
                            class="fa fa-plus-circle"></i>Notice</a>
							
                </div>
            </div>
        </div>
		
		 <div class="row">
		 @if(Session::has('success'))
<div class="alert alert-warning alert-dismissible fade show" role="alert">
	<strong>Success!</strong>  {{Session::get('success')}}
	<button type="button" class="close" data-dismiss="alert" aria-label="Close"><span class="ti-close"></span>
	</button>
</div>
@endif

@if(Session::has('delete'))
<div class="alert alert-danger alert-dismissible fade show" role="alert">
	<strong>Success!</strong>  {{Session::get('delete')}}
	<button type="button" class="close" data-dismiss="alert" aria-label="Close"><span class="ti-close"></span>
	</button>
</div>
@endif
            <div class="col-12">
			     <div class="card">
                    <div class="card-body">
                        <h4 class="card-title">Basic Table</h4>
                        <h6 class="card-subtitle">Add class <code>.table</code></h6>
						
						<a href=""><span class="badge badge-info mb-3">Home</span></a>&nbsp;
						<a href="{{route('admin.allnotice')}}"><span class="badge badge-info mb-3">Notice</span></a>&nbsp;
						<a href="{{route('admin.allevent')}}"><span class="badge badge-info mb-3">Events</span></a>&nbsp;
						<a href="{{route('admin.allgallery')}}"><span class="badge badge-info mb-3">Gallery</span></a>&nbsp;
						<a href="{{route('admin.alldoc')}}"><span class="badge badge-info mb-3">Forms</span></a>&nbsp;
						<a href="{{route('admin.contact')}}"><span class="badge badge-info mb-3">Contact Us</span></a>&nbsp;
						<a href="{{route('admin.subs')}}"><span class="badge badge-info mb-3">Subscribers</span></a>&nbsp;
						<a href=""><span class="badge badge-info mb-3">House tax</span></a>&nbsp;
						<a href="{{route('admin.allaudit')}}"><span class="badge badge-info mb-3">Audit</span></a>&nbsp;
	
	
	
                        <div class="table-responsive">
                            <table class="table">
                                <thead>
                                    <tr>
                                         <th>Sr No</th>
										<th>Notice Name</th>
										<th>Description</th>
										<th>File</th>
										<th>Date</th>
										<th>Start Date</th>
                       
                                    </tr>
                                </thead>
                                <tbody>
                                   @php
					$num = 1;
					@endphp
					@foreach($single as $value)
                    <tr>
                        <td>{{$num}}</td>
                        <td>{{$value->notice}}</td>
                        <td>{!!$value->description!!}</td>
                        <td><img src="{{asset('public/upload/notice')}}/{{$value->file}}" style=" height: 50px;" alt="{{$value->file}}"></img></td>
                        <td>{{$value->created_at}}</td>
                         <td> 
						  <a href = "" class='forum-title' name = "abc" data-toggle="modal" data-target="#usermodal{{$value->id}}"><i class="ti-pencil mr-1 btn btn-success"></i></a>
							 <a href = "{{ route('admin.notice_delete' , $value->id)}}" class='forum-title'><i class="ti-trash btn btn-danger"></i></a>
						</td>
						<div class="modal fade" id="usermodal{{$value->id}}">
							<div class="modal-dialog modal-dialog-centered" role="document">
								<div class="modal-content">
									<div class="modal-header">
										<h5 class="modal-title">Update Form</h5>
										<button type="button" class="close" data-dismiss="modal"><span>&times;</span></button>
									</div>
									<div class="modal-body">
									 <form action="{{route('admin.update-notice')}}" method="post" class="needs-validation" novalidate enctype="multipart/form-data">
											@csrf
											<input type="hidden" value="{{$value->id}}" name="id">
										   <div class="row">
												<div class="col-md-6">
													<label for="validationTooltip01">Name Of Notice</label>
													<input type="text" name="notice_name" class="form-control" id="validationTooltip01" value="{{$value->notice}}" required>
													<div class="valid-tooltip">
														Looks good!
													</div>
												</div>
												<!--div class="col-md-6">
													<label for="validationTooltip01">Category</label>
													<select name="update_cat" class="form-control">
													<option selected  value="{{$value->event_cat}}">{{$value->event_cat}}</option>
													</select>
													<div class="valid-tooltip">
														Looks good!
													</div>
												</div-->
										<div class="col-md-6">
													<label class="custom-file-label" for="inputGroupFile02">Choose file</label>
													<input type="file" name="doc" class="form-control" id="inputGroupFile02">
											   </div>
											</div>
											<div class="row">
												<div class="card">
													<div class="card-body">
														<h4 class="card_title">Notice Description</h4>
														<textarea name="editor1" class="ck-editor" placeholder="Enter Something..">{!!$value->description!!}</textarea>
													</div>
												</div>
											</div>
											</div>
										<div class="modal-footer">
											<button class="btn btn-primary" type="submit">Submit form</button>
										<button type="button" class="btn btn-light" data-dismiss="modal">Close</button>
										</div>
										</form>
									</div>
								</div>
							</div>
                    </tr>
					@php
						$num++;
						@endphp
					@endforeach
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
			</div>
		 </div>
		
		
	</div>
			
</div>
				
				
@endsection

@section('js')
    <!--=========================*
            This Page Scripts
    *===========================-->
   
@endsection
