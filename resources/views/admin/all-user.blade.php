@extends('layouts.app')


{{-- Page Title --}}
@section('page-title')
  Accounts
@endsection
{{-- This Page Css --}}
@section('css')
    <link rel="stylesheet" type="text/css"
        href="{{asset('public/assets/datatables.net-bs4/css/dataTables.bootstrap4.css')}}">
    <link rel="stylesheet" type="text/css"
        href="{{asset('public/assets/datatables.net-bs4/css/responsive.dataTables.min.css')}}">
    <!--=========================*
               Datatable
  
  *===========================-->
  <body class="skin-blue fixed-layout">
    <!-- ============================================================== -->
    <!-- Preloader - style you can find in spinners.css -->
    <!-- ============================================================== -->
    <div class="preloader">
        <div class="loader">
            <div class="loader__figure"></div>
        </div>
    </div>
	 <div id="main-wrapper">
@endsection
@section('main-content')

    <div class="page-wrapper">
      <div class="container-fluid">
                <!-- ============================================================== -->
                <!-- Bread crumb and right sidebar toggle -->
                <!-- ============================================================== -->
                <div class="row page-titles">
                    <div class="col-md-5 align-self-center">
                        <h4 class="text-themecolor">Datatable</h4>
                    </div>
                    <div class="col-md-7 align-self-center text-right">
                        <div class="d-flex justify-content-end align-items-center">
                            <ol class="breadcrumb">
                                <li class="breadcrumb-item"><a href="javascript:void(0)">Home</a></li>
                                <li class="breadcrumb-item active">Datatable</li>
                            </ol>
                            <button type="button" class="btn btn-info d-none d-lg-block m-l-15" data-toggle="modal" data-target="#newuser" ><i
                                    class="fa fa-plus-circle"></i> Add New User</button>
									
                        </div>
                    </div>
                </div>
                <!-- ============================================================== -->
                <!-- End Bread crumb and right sidebar toggle -->
                <!-- ============================================================== -->
                <!-- ============================================================== -->
                <!-- Start Page Content -->
                <!-- ============================================================== -->
                <div class="row">
                    <div class="col-12">
					  @if(Session::has('add'))
						<div class="alert alert-info alert-dismissible fade show" role="alert">
							<strong></strong>  {{Session::get('add')}}
							<button type="button" class="close" data-dismiss="alert" aria-label="Close"><span class="ti-close"></span>
							</button>
						</div>
						@endif
						@if(Session::has('delete'))
						<div class="alert alert-danger alert-dismissible fade show" role="alert">
							<strong></strong>  {{Session::get('delete')}}
							<button type="button" class="close" data-dismiss="alert" aria-label="Close"><span class="ti-close"></span>
							</button>
						</div>
						@endif
				
				
                        <div class="card">
                            <div class="card-body">
                                <h4 class="card-title">Data Export</h4>
                                <h6 class="card-subtitle">Export data to Copy, CSV, Excel, PDF & Print</h6>
                                <div class="table-responsive m-t-40">
                                    <table id="example23"
                                        class="display nowrap table table-hover table-striped table-bordered"
                                        cellspacing="0" width="100%">
                                        <thead>
                                            <tr>
                                                <th>Sr No</th>
												<th>Panchayat</th>
												<th>Role</th>
												<th>Name</th>
												<th>Number</th>
												<th>Email</th>
												<th>Ward No</th>
												<th>Profile</th>
												<th>Action</th>
                                            </tr>
                                        </thead>
                                        <tfoot>
                                            <tr>
                                                 <th>Sr No</th>
												<th>Panchayat</th>
												<th>Role</th>
												<th>Name</th>
												<th>Number</th>
												<th>Email</th>
												<th>Ward No</th>
												<th>Profile</th>
												<th>Action</th>
                                            </tr>
                                        </tfoot>
                                        <tbody>
                                        @php
										$num=1;
										@endphp
										@foreach($Panchayat_user as $value)
										<tr>
											<td>{{$num}}</td>
											<td><span class="badge badge-info">{{$value->panc_name}}</span></td>
											<td>{{$value->role_id}}</td>
											<td>{{$value->name}}</td>
											<td>{{$value->mobile}}</td>
											<td>{{$value->email}}</td>
											<td>{{$value->ward_number}}</td>
											<td><img src="{{asset('public//upload/panchayat/panchayat1/role/LDC')}}/{{$value->profile}}" width="40" class="img-cicle"></img></td>
														
															
											<td> 
											  <a href = "" class='forum-title' name = "abc" data-toggle="modal" data-target="#usermodal{{$value->id}}"><i class="ti-pencil mr-1 btn btn-success"></i></a>
											  <a href = "{{route('user-view',$value->id)}}" class='forum-title' name = "abc"><i class="ti-eye mr-1 btn btn-success"></i></a>
												 <a href = "{{route('user-delete',$value->id)}}" class='forum-title'><i class="ti-trash btn btn-danger"></i></a>
											</td>
									</tr>
									<div class="modal fade" id="usermodal{{$value->id}}">
										<div class="modal-dialog modal-dialog-centered" role="document">
										<div class="modal-content">
											<div class="modal-header">
												<h5 class="modal-title">Update Form</h5>
												<button type="button" class="close" data-dismiss="modal"><span>&times;</span></button>
											</div>
											<div class="modal-body">
												<form action="{{route('edit-user',$value->id)}}" method ="post" class="needs-validation" novalidate="" enctype="multipart/form-data">
												  @csrf
													<div class="form-row">
														<div class="col-md-4 mb-3">
															<label for="validationCustom01">User name</label>
															<input type="text" name="name"  value="{{$value->name}}" class="form-control" id="validationCustom01" placeholder="User name.."  required="">
															
														</div>
														<div class="col-md-4 mb-3">
															<label for="validationCustom02">Mobile Number</label>
															<input type="number"  value="{{$value->mobile}}" class="form-control" id="validationCustom02" name="mobile" placeholder="Enter number.." required="">
															
														</div>
														<div class="col-md-4 mb-3">
															<label for="validationCustom01">Email</label>
															<input type="email" class="form-control" id="validationCustom01"  value="{{$value->email}}" name="email" placeholder="Enter Email" required="">
															<div class="valid-feedback">
																Looks good!
															</div>
														</div>
														
													</div>
													<div class="form-row">
														<div class="col-md-6 mb-3">
															<label for="validationCustom01">Select Role</label>
															<select name="role_id" class="form-control">
																<option value="">--Select Role--</option>
																@foreach($role as $roles)
																 <option value="{{ $roles->id }}" {{ ($value->role_id == $roles->id) ? 'selected' : '' }}>
																		{{$roles->role_name}}
																	</option>
															    
																@endforeach
															</select>
															
														</div>
														<div class="col-md-6 mb-3">
															<label for="validationCustom01">Select Panchyat</label>
															<select name="pn_id" class="form-control">
																<option value="">--Select Panchayat--</option>
																@foreach($panchyat as $val)
															     <option value="{{$val->id}}" <?php if($value->pn_id == $val->id){ echo "selected"; }?>>{{$val->panc_name}}</option>
																@endforeach
															</select>
															<div class="valid-feedback">
																Looks good!
															</div>
														</div>
													</div>
													<div class="form-row">
														<div class="col-md-6 mb-3">
															<label for="validationCustom01">Profile</label>
															<input type="file" name="profile" class="form-control" id="validationCustom01" placeholder="Enter Email" required="">
															<div class="valid-feedback">
																Looks good!
															</div>
														</div>
														<div class="col-md-6 mb-3">
															<label for="validationCustom01">Ward No</label>
															<input type="text" name="ward_number" class="form-control"   value="{{$value->ward_number}}" required="">
															<div class="valid-feedback">
																Looks good!
															</div>
														</div>
														
													</div>
													<button class="btn btn-primary" type="submit">Submit form</button>
												</form>
											</div>
										</div>
									</div>
								</div>
								@php
								$num++;
								@endphp
								@endforeach
                                           
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                       
                       
                    </div>
                </div>
                <!-- ============================================================== -->
                <!-- End PAge Content -->
                <!-- ============================================================== -->
                <!-- ============================================================== -->
                <!-- Right sidebar -->
                <!-- ============================================================== -->
                <!-- .right-sidebar -->
             
            </div>
			
			
            <!-- new user  -->

            <div class="modal fade" id="newuser">
				<div class="modal-dialog modal-dialog-centered" role="document">
					<div class="modal-content">
						<div class="modal-header">
							<h5 class="modal-title">New User</h5>
							<button type="button" class="close" data-dismiss="modal"><span>&times;</span></button>
						</div>
						<div class="modal-body">
							<form action="{{route('new-user')}}" method ="post" class="needs-validation" novalidate="" enctype="multipart/form-data">
							  @csrf
								<div class="form-row">
									<div class="col-md-4 mb-3">
										<label for="validationCustom01">User name</label>
										<input type="text" name="name"  value="" class="form-control" id="validationCustom01" placeholder="User name.." required="required">
										
									</div>
									<div class="col-md-4 mb-3">
										<label for="validationCustom02">Mobile Number</label>
										<input type="number"  value="" class="form-control" id="validationCustom02" name="mobile" placeholder="Enter number.." required>
										
									</div>
									<div class="col-md-4 mb-3">
										<label for="validationCustom01">Email</label>
										<input type="email" class="form-control" id="validationCustom01"  value="" name="email" placeholder="Enter Email" required>
										<div class="valid-feedback">
											Looks good!
										</div>
									</div>
									
								</div>
								<div class="form-row">
									<div class="col-md-6 mb-3">
										<label for="validationCustom01">Select Role</label>
										<select name="role_id" class="form-control">
											<option value="">--Select Role--</option>
											@foreach($role as $roles)
											 <option value="{{ $roles->id }}">
													{{$roles->role_name}}
											</option>
											@endforeach
										</select>
										
									</div>
									<div class="col-md-6 mb-3">
										<label for="validationCustom01">Select Panchyat</label>
										<select name="pn_id" class="form-control" required>
											<option value="">--Select Panchayat--</option>
											@foreach($panchyat as $val)
										     <option value="{{$val->id}}">{{$val->panc_name}}</option>
											@endforeach
										</select>
										<div class="valid-feedback">
											Looks good!
										</div>
									</div>
								</div>
								<div class="form-row">
									<div class="col-md-6 mb-3">
										<label for="validationCustom01">Profile</label>
										<input type="file" name="profile" class="form-control" id="validationCustom01" placeholder="Enter Email" required="required">
										<div class="valid-feedback">
											Looks good!
										</div>
									</div>
									<div class="col-md-6 mb-3">
										<label for="validationCustom01">Ward No</label>
										<input type="text" name="ward_number" class="form-control"   value="" required>
										<div class="valid-feedback">
											Looks good!
										</div>
									</div>
									
								</div>
								<button class="btn btn-primary" type="submit">Submit form</button>
							</form>
						</div>
					</div>
				</div>
			</div>

            <!-- new user  -->
            <!-- ============================================================== -->
            <!-- End Container fluid  -->
            <!-- ============================================================== -->
 
 </div>
@endsection
@section('js')
    <!--=========================*
            This Page Scripts
    *===========================-->
    <!-- Data Table js -->
   <script>
        $(function () {
            $('#myTable').DataTable();
            var table = $('#example').DataTable({
                "columnDefs": [{
                    "visible": false,
                    "targets": 2
                }],
                "order": [
                    [2, 'asc']
                ],
                "displayLength": 25,
                "drawCallback": function (settings) {
                    var api = this.api();
                    var rows = api.rows({
                        page: 'current'
                    }).nodes();
                    var last = null;
                    api.column(2, {
                        page: 'current'
                    }).data().each(function (group, i) {
                        if (last !== group) {
                            $(rows).eq(i).before('<tr class="group"><td colspan="5">' + group + '</td></tr>');
                            last = group;
                        }
                    });
                }
            });
            // Order by the grouping
            $('#example tbody').on('click', 'tr.group', function () {
                var currentOrder = table.order()[0];
                if (currentOrder[0] === 2 && currentOrder[1] === 'asc') {
                    table.order([2, 'desc']).draw();
                } else {
                    table.order([2, 'asc']).draw();
                }
            });
            // responsive table
            $('#config-table').DataTable({
                responsive: true
            });
            $('#example23').DataTable({
                dom: 'Bfrtip',
                buttons: [
                    'copy', 'csv', 'excel', 'pdf', 'print'
                ]
            });
            $('.buttons-copy, .buttons-csv, .buttons-print, .buttons-pdf, .buttons-excel').addClass('btn btn-primary mr-1');
        });

    </script>
@endsection
