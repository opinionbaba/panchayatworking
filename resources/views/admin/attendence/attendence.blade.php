@extends('layouts.app')
{{-- Page Title --}}
@section('page-title')
    Cards
@endsection
@section('css')
<link rel="stylesheet" type="text/css"
        href="{{asset('public/assets/datatables.net-bs4/css/dataTables.bootstrap4.css')}}">
    <link rel="stylesheet" type="text/css"
        href="{{asset('public/assets/datatables.net-bs4/css/responsive.dataTables.min.css')}}">
		
    <body class="skin-blue fixed-layout">
    <div class="preloader">
        <div class="loader">
            <div class="loader__figure"></div>
            <p class="loader__label">Elite admin</p>
        </div>
    </div>
    <div id="main-wrapper">
	
@endsection

@section('main-content')

 <div class="page-wrapper">
            <div class="container-fluid">
                <div class="row page-titles">
                    <div class="col-md-5 align-self-center">
                        <h4 class="text-themecolor">Tabs</h4>
                    </div>
                    <div class="col-md-7 align-self-center text-right">
                        <div class="d-flex justify-content-end align-items-center">
                            <ol class="breadcrumb">
                                <li class="breadcrumb-item"><a href="javascript:void(0)">Home</a></li>
                                <li class="breadcrumb-item active">Tabs</li>
                            </ol>
                            <button type="button" class="btn btn-info d-none d-lg-block m-l-15"><i class="fa fa-plus-circle"></i> Create New</button>
                        </div>
                    </div>
                </div>
				
				<div class="card">
					<div class="card-body">
						<h4 class="card-title">Default Tab</h4>
						<h6 class="card-subtitle">Use default tab with class <code>nav-tabs & tabcontent-border </code></h6>
						<!-- Nav tabs -->
						<div class="button-box">
						<a class="tst1 btn btn-info" href="#">Homee</a>
						<a class="tst1 btn btn-info" href="{{route('admin.allnotice')}}">Notice</a>
						<a class="tst1 btn btn-info" href="{{route('admin.allevent')}}">Events</a>
						<a class="tst1 btn btn-info" href="{{route('admin.allgallery')}}">Gallery</a>
						<a class="tst1 btn btn-info" href="{{route('admin.alldoc')}}">Form</a>
						<a class="tst1 btn btn-info" href="{{route('admin.contact')}}">Contact Us</a>
						<a class="tst1 btn btn-info" href="{{route('admin.subs')}}">Subscribers</a>
						<a class="tst1 btn btn-info" href="#">House Tax</a>
						<a class="tst1 btn btn-info" href="{{route('admin.allaudit')}}">Audit</a>
					   </div>
					   
					    <div class="row" style="margin-top:40px;">
							 <div class="col-md-3">
							  <div class="team_member">
								<img src="{{asset('public/upload/notice/1584789291.jpg')}}" alt="Team Member" style="width:100%">
									<div class="member_name">
										<h3>JHON DOE</h3>
										<span>CEO/Founder</span>
									</div>
								</div>
							  </div>
							   <div class="col-md-9 ">
								
					  <div class="page-TimeShifts page-BgGray js-TimeShifts">
                        <div class="TShifts">
                            <table class="TShifts__table table-responsive">
							<thead>
							<th class="Month"></th>
							<th>1<small></small></th>
							<th>2<small></small></th>
							<th>3<small></small></th>
							<th>4<small></small></th>
							<th>5<small></small></th>
							<th>6<small></small></th>
							<th>7<small></small></th>
							<th>8<small></small></th>
							<th>9<small></small></th>
							<th>10<small></small></th>
							<th>11<small></small></th>
							<th>12<small></small></th>
							<th>13<small></small></th>
							<th>14<small></small></th>
							<th>15<small></small></th>
							<th>16<small></small></th>
							<th>17<small></small></th>
							<th>18<small></small></th>
							<th>19<small></small></th>
							<th>20<small></small></th>
							<th>21<small></small></th>
							<th>22<small></small></th>
							<th>23<small></small></th>
							<th>24<small></small></th>
							<th>25<small></small></th>
							<th>26<small></small></th>
							<th>27<small></small></th>
							<th>28<small></small></th>
							<th>29<small></small></th>
							<th>30<small></small></th>
							<th>31<small></small>
							</thead>
										
										     
									                                                                          
                              <tbody id="drawWorkTimeTableBody">   
								<tr class="row0">
								<td>Jan</td>
								<td></td>
								<td></td>
								<td></td>
								<td></td>
								<td></td>
								<td></td>
								<td></td>
								<td></td>
								<td></td>
								<td></td>
								<td></td>
								<td></td>
								<td></td>
								<td></td>
								<td></td>
								<td></td>
								<td></td>
								<td></td>
								<td></td>
								<td></td>
								<td></td>
								<td></td>
								<td></td>
								<td></td>
								<td></td>
								<td></td>
								<td></td>
								<td></td>
								<td></td>
								<td></td>
								<td></td>
							
								</tr>
                                    <tr class="row1">
                                        <tr class="row0">
								<td>Feb</td>
								<td></td>
								<td></td>
								<td></td>
								<td></td>
								<td></td>
								<td></td>
								<td></td>
								<td></td>
								<td></td>
								<td></td>
								<td></td>
								<td></td>
								<td></td>
								<td></td>
								<td></td>
								<td></td>
								<td></td>
								<td></td>
								<td></td>
								<td></td>
								<td></td>
								<td></td>
								<td></td>
								<td></td>
								<td></td>
								<td></td>
								<td></td>
								<td></td>
								<td></td>
								<td></td>
								<td></td>
								<td></td>
								</tr>
                                    <tr class="row2">
                                       <tr class="row0">
								<td>Mar</td>
								<td></td>
								<td></td>
								<td></td>
								<td></td>
								<td></td>
								<td></td>
								<td></td>
								<td></td>
								<td></td>
								<td></td>
								<td></td>
								<td></td>
								<td></td>
								<td></td>
								<td></td>
								<td></td>
								<td></td>
								<td></td>
								<td></td>
								<td></td>
								<td></td>
								<td></td>
								<td></td>
								<td></td>
								<td></td>
								
								<td></td>
								<td></td>
								<td></td>
								<td></td>
								<td></td>
								<td></td>
								</tr>
                                    <tr class="row3">
                                       <tr class="row0">
								<td>Apr</td>
								<td></td>
								<td></td>
								<td></td>
								<td></td>
								<td></td>
								<td></td>
								<td></td>
								<td></td>
								<td></td>
								<td></td>
								<td></td>
								<td></td>
								<td></td>
								<td></td>
								<td></td>
								<td></td>
								<td></td>
								<td></td>
								<td></td>
								<td></td>
								<td></td>
								<td></td>
								<td></td>
								<td></td>
								<td></td>
								<td></td>
								<td></td>
								<td></td>
								<td></td>
								<td></td>
								<td></td>
								
								</tr>
                                    <tr class="row4">
                                      <tr class="row0">
								<td>May</td>
								<td></td>
								<td></td>
								<td></td>
								<td></td>
								<td></td>
								<td></td>
								<td></td>
								<td></td>
								<td></td>
								<td></td>
								<td></td>
								<td></td>
								<td></td>
								<td></td>
								<td></td>
								<td></td>
								<td></td>
								<td></td>
								<td></td>
								<td></td>
								<td></td>
								<td></td>
								<td></td>
								<td></td>
								<td></td>
								<td></td>
								<td></td>
								<td></td>
								<td></td>
								<td></td>
								<td></td>
								
								</tr>
                                    <tr class="row5">
                                        <tr class="row0">
								<td>Jun</td>
								<td></td>
								<td></td>
								<td></td>
								<td></td>
								<td></td>
								<td></td>
								<td></td>
								<td></td>
								<td></td>
								<td></td>
								<td></td>
								<td></td>
								<td></td>
								<td></td>
								<td></td>
								<td></td>
								<td></td>
								<td></td>
								<td></td>
								<td></td>
								<td></td>
								<td></td>
								<td></td>
								<td></td>
								<td></td>
								<td></td>
								<td></td>
								<td></td>
								<td></td>
								<td></td>
								<td></td>
							
								</tr>
                                    <tr class="row6">
                                       <tr class="row0">
								<td>Jul</td>
								<td></td>
								<td></td>
								<td></td>
								<td></td>
								<td></td>
								<td></td>
								<td></td>
								<td></td>
								<td></td>
								<td></td>
								<td></td>
								<td></td>
								<td></td>
								<td></td>
								<td></td>
								<td></td>
								<td></td>
								<td></td>
								<td></td>
								<td></td>
								<td></td>
								<td></td>
								<td></td>
								<td></td>
								<td></td>
								<td></td>
								<td></td>
								<td></td>
								<td></td>
								<td></td>
								<td></td>
								
								</tr>
                                    <tr class="row7">
                                       <tr class="row0">
								<td>Aug</td>
								<td></td>
								<td></td>
								<td></td>
								<td></td>
								<td></td>
								<td></td>
								<td></td>
								<td></td>
								<td></td>
								<td></td>
								<td></td>
								<td></td>
								<td></td>
								<td></td>
								<td></td>
								<td></td>
								<td></td>
								<td></td>
								<td></td>
								<td></td>
								<td></td>
								<td></td>
								<td></td>
								<td></td>
								<td></td>
								<td></td>
								<td></td>
								<td></td>
								<td></td>
								<td></td>
								<td></td>
								
								</tr>
                                    <tr class="row8">
                                       <tr class="row0">
								<td>Sep</td>
								<td></td>
								<td></td>
								<td></td>
								<td></td>
								<td></td>
								<td></td>
								<td></td>
								<td></td>
								<td></td>
								<td></td>
								<td></td>
								<td></td>
								<td></td>
								<td></td>
								<td></td>
								<td></td>
								<td></td>
								<td></td>
								<td></td>
								<td></td>
								<td></td>
								<td></td>
								<td></td>
								<td></td>
								<td></td>
								<td></td>
								<td></td>
								<td></td>
								<td></td>
								<td></td>
								<td></td>
								
								</tr>
                                    <tr class="row9">
                                       <tr class="row0">
								<td>Oct</td>
								<td></td>
								<td></td>
								<td></td>
								<td></td>
								<td></td>
								<td></td>
								<td></td>
								<td></td>
								<td></td>
								<td></td>
								<td></td>
								<td></td>
								<td></td>
								<td></td>
								<td></td>
								<td></td>
								<td></td>
								<td></td>
								<td></td>
								<td></td>
								<td></td>
								<td></td>
								<td></td>
								<td></td>
								<td></td>
								<td></td>
								<td></td>
								<td></td>
								<td></td>
								<td></td>
								<td></td>
								
								</tr>
                                    <tr class="row10">
                                       <tr class="row0">
								<td>Nov</td>
								<td></td>
								<td></td>
								<td></td>
								<td></td>
								<td></td>
								<td></td>
								<td></td>
								<td></td>
								<td></td>
								<td></td>
								<td></td>
								<td></td>
								<td></td>
								<td></td>
								<td></td>
								<td></td>
								<td></td>
								<td></td>
								<td></td>
								<td></td>
								<td></td>
								<td></td>
								<td></td>
								<td></td>
								<td></td>
								<td></td>
								<td></td>
								<td></td>
								<td></td>
								<td></td>
								<td></td>
							
								</tr>
								<tr class="row11">
								   <tr class="row0">
								<td>Dec</td>
								<td></td>
								<td></td>
								<td></td>
								<td></td>
								<td></td>
								<td></td>
								<td></td>
								<td></td>
								<td></td>
								<td></td>
								<td></td>
								<td></td>
								<td></td>
								<td></td>
								<td></td>
								<td></td>
								<td></td>
								<td></td>
								<td></td>
								<td></td>
								<td></td>
								<td></td>
								<td></td>
								<td></td>
								<td></td>
								<td></td>
								<td></td>
								<td></td>
								<td></td>
								<td></td>
								<td></td>
								
								</tr>
								</tbody>
								</table>
							</div>
						</div>
						</div>
                
                    </div>
					   
					   
					</div>
				</div>
						
 
@endsection
