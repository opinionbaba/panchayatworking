@extends('layouts.app')


{{-- Page Title --}}
@section('page-title')
  Accounts
@endsection
{{-- This Page Css --}}
@section('css')
<link rel="stylesheet" type="text/css"
        href="{{asset('public/assets/datatables.net-bs4/css/dataTables.bootstrap4.css')}}">
    <link rel="stylesheet" type="text/css"
        href="{{asset('public/assets/datatables.net-bs4/css/responsive.dataTables.min.css')}}">
		
    <body class="skin-blue fixed-layout">
    <div class="preloader">
        <div class="loader">
            <div class="loader__figure"></div>
            <p class="loader__label">Elite admin</p>
        </div>
    </div>
    <div id="main-wrapper">
	
@endsection
@section('main-content')
<div class="page-wrapper">
    <div class="container-fluid">
		<div class="row page-titles">
			<div class="col-md-5 align-self-center">
				<h4 class="text-themecolor">Attendance</h4>
			</div>
			<div class="col-md-7 align-self-center text-right">
				<div class="d-flex justify-content-end align-items-center">
					<ol class="breadcrumb">
						<li class="breadcrumb-item"><a href="javascript:void(0)">Home</a></li>
						<li class="breadcrumb-item active">Attendance</li>
					</ol>
				</div>
			</div>
		</div>
		
		 <div class="card">
			<div class="card-body">
				<h4 class="card-title">Attendance</h4>
				<h6 class="card-subtitle">Use default tab with class <code>nav-tabs & tabcontent-border </code></h6>
				<!-- Nav tabs -->
				<ul class="nav nav-tabs" role="tablist">
					 <li class="nav-item">
						<a class="nav-link active" id="home-tab" data-toggle="tab" href="#panchyat" role="tab" aria-controls="home" aria-selected="true"><i class="ti-list"></i>Secretary</a>
					</li>
					<li class="nav-item">
						<a class="nav-link" id="profile-tab" data-toggle="tab" href="#ward" role="tab" aria-controls="profile" aria-selected="false"><i class="ti-user"></i>LDC</a>
					</li>
					 <li class="nav-item">
						<a class="nav-link" id="profile-tab" data-toggle="tab" href="#employee" role="tab" aria-controls="profile" aria-selected="false"><i class="ti-user"></i>Contractual</a>
					</li>
					 <li class="nav-item">
						<a class="nav-link" id="profile-tab" data-toggle="tab" href="#citizen" role="tab" aria-controls="profile" aria-selected="false"><i class="ti-user"></i>Ward Member</a>
					</li>
					 <li class="nav-item">
						<a class="nav-link" id="profile-tab" data-toggle="tab" href="#subscriber" role="tab" aria-controls="profile" aria-selected="false"><i class="ti-user"></i>Managers</a>
					</li>
					 <li class="nav-item">
						<a class="nav-link" id="profile-tab" data-toggle="tab" href="#income" role="tab" aria-controls="profile" aria-selected="false"><i class="ti-user"></i>Clerk</a>
					</li>
					<li class="nav-item">
						<a class="nav-link" id="profile-tab" data-toggle="tab" href="#expenditure" role="tab" aria-controls="profile" aria-selected="false"><i class="ti-user"></i>Peon</a>
					</li>
					<li class="nav-item">
						<a class="nav-link" id="profile-tab" data-toggle="tab" href="#statics" role="tab" aria-controls="profile" aria-selected="false"><i class="ti-user"></i>Sarpanch</a>
					</li>
					<li class="nav-item">
						<a class="nav-link" id="profile-tab" data-toggle="tab" href="#dy_sarpanch" role="tab" aria-controls="profile" aria-selected="false"><i class="ti-user"></i>Dy Sarpanch</a>
					</li>
				</ul>
				<!-- Tab panes -->
				<div class="tab-content tabcontent-border">
					<div class="tab-pane fade show active" id="panchyat" role="tabpanel" aria-labelledby="home-tab">
						<div class="col-lg-12 mt-4 stretched_card">
							<div class="card">
								<div class="card-body">
									<div class="table-responsive m-t-40">
                                    <table id="example23"
                                        class="display nowrap table table-hover table-striped table-bordered"
                                        cellspacing="0" width="100%">
                                        <thead>
                                             <tr>
												<th>Sr No</th>
												<th>Name</th>
												<th align="center">Attendance</th>
												<th>Report</th>
											  </tr>
                                        </thead>
                                        <tfoot>
                                             <tr>
												<th>Sr No</th>
												<th>Name</th>
												<th align="center">Attendance</th>
												<th>Report</th>
											  </tr>
                                        </tfoot>
                                        <tbody>
                                            @php 
											$num=1;
											@endphp
											@foreach ($panchyat as $value)
											  <tr>
												<td>{{$num}}</td>
												<td>{{$value->name}}</td>
												
												   <td class="studentID" data-studentid="1" data-title="Attendance">
													<a href="" class="badge badge-success badge-pill">Present</a>
													<a href="" class="badge badge-warning badge-pill">Late</a>
													<a href=""class="badge badge-info badge-pill">Half Day</a>
													<a href="" class="badge badge-danger badge-pill">Absent</a>
												   </td>
													 <td><a href="{{route('attdence.secretary')}}" class="badge badge-success badge-pill"><i class="fa fa-file"></i> Report</a></td>
												</tr>
												@php 
												$num++;
												@endphp
												@endforeach
                                           
                                        </tbody>
                                    </table>
                                </div>
								 
								</div>
							</div>
						</div>
						<!-- With Badges -->
					</div>
					
					
					<div class="tab-pane fade" id="ward" role="tabpanel" aria-labelledby="profile-tab">
					    <div class="col-lg-12 mt-4 stretched_card">
							<div class="card">
								<div class="card-body">
									<div class="table-responsive m-t-40">
                                    <table id="ward1"
                                        class="display nowrap table table-hover table-striped table-bordered"
                                        cellspacing="0" width="100%">
                                        <thead>
                                              <tr>
												<th>Sr No</th>
												<th>Name</th>
												<th align="center">Attendance</th>
												<th>Report</th>
											 </tr>
                                        </thead>
                                        <tfoot>
                                             <tr>
												<th>Sr No</th>
												<th>Name</th>
												<th align="center">Attendance</th>
												<th>Report</th>
											  </tr>
                                        </tfoot>
                                        <tbody>
                                           <tr>
											<td>{{$num}}</td>
											<td></td>
											   <td class="studentID" data-studentid="1" data-title="Attendance">
												<a href="" class="badge badge-success badge-pill">Present</a>
												<a href="" class="badge badge-warning badge-pill">Late</a>
												<a href=""class="badge badge-info badge-pill">Half Day</a>
												<a href="" class="badge badge-danger badge-pill">Absent</a>
											   </td>
												 <td><a href="{{route('attdence.secretary')}}" class="badge badge-success badge-pill"><i class="fa fa-file"></i> Report</a></td>
											</tr>
                                           
                                        </tbody>
                                    </table>
                                </div>
								 
								</div>
							</div>
						</div>
					
					</div>
					
					 <div class="tab-pane fade" id="employee" role="tabpanel" aria-labelledby="profile-tab">
                        <div class="col-lg-12 mt-4 stretched_card">
							<div class="card">
								<div class="card-body">
									<div class="table-responsive m-t-40">
                                    <table id="employee1"
                                        class="display nowrap table table-hover table-striped table-bordered"
                                        cellspacing="0" width="100%">
                                        <thead>
                                              <tr>
												<th>Sr No</th>
												<th>Name</th>
												<th align="center">Attendance</th>
												<th>Report</th>
											 </tr>
                                        </thead>
                                        <tfoot>
                                             <tr>
												<th>Sr No</th>
												<th>Name</th>
												<th align="center">Attendance</th>
												<th>Report</th>
											  </tr>
                                        </tfoot>
                                        <tbody>
                                           <tr>
											<td>1</td>
											<td>John</td>
											   <td class="studentID" data-studentid="1" data-title="Attendance">
												<a href="" class="badge badge-success badge-pill">Present</a>
												<a href="" class="badge badge-warning badge-pill">Late</a>
												<a href=""class="badge badge-info badge-pill">Half Day</a>
												<a href="" class="badge badge-danger badge-pill">Absent</a>
											   </td>
												 <td><a href="{{route('attdence.secretary')}}" class="badge badge-success badge-pill"><i class="fa fa-file"></i> Report</a></td>
											</tr>
                                           
                                        </tbody>
                                    </table>
                                </div>
								 
								</div>
							</div>
						</div>
		             </div>
					
					 <div class="tab-pane fade" id="citizen" role="tabpanel" aria-labelledby="profile-tab">
                        <div class="col-lg-12 mt-4 stretched_card">
							<div class="card">
								<div class="card-body">
									<div class="table-responsive m-t-40">
                                    <table id="citizen1"
                                        class="display nowrap table table-hover table-striped table-bordered"
                                        cellspacing="0" width="100%">
                                        <thead>
                                              <tr>
												<th>Sr No</th>
												<th>Name</th>
												<th align="center">Attendance</th>
												<th>Report</th>
											 </tr>
                                        </thead>
                                        <tfoot>
                                             <tr>
												<th>Sr No</th>
												<th>Name</th>
												<th align="center">Attendance</th>
												<th>Report</th>
											  </tr>
                                        </tfoot>
                                        <tbody>
                                           <tr>
											<td>1</td>
											<td>John</td>
											   <td class="studentID" data-studentid="1" data-title="Attendance">
												<a href="" class="badge badge-success badge-pill">Present</a>
												<a href="" class="badge badge-warning badge-pill">Late</a>
												<a href=""class="badge badge-info badge-pill">Half Day</a>
												<a href="" class="badge badge-danger badge-pill">Absent</a>
											   </td>
												 <td><a href="{{route('attdence.secretary')}}" class="badge badge-success badge-pill"><i class="fa fa-file"></i> Report</a></td>
											</tr>
                                        </tbody>
                                    </table>
                                </div>
								 
								</div>
							</div>
						</div>
		             </div>
					 
					 
					 <div class="tab-pane fade" id="subscriber" role="tabpanel" aria-labelledby="profile-tab">
						<div class="col-lg-12 mt-4 stretched_card">
							<div class="card">
								<div class="card-body">
									<div class="table-responsive m-t-40">
                                    <table id="subscriber1"
                                        class="display nowrap table table-hover table-striped table-bordered"
                                        cellspacing="0" width="100%">
                                        <thead>
                                              <tr>
												<th>Sr No</th>
												<th>Name</th>
												<th align="center">Attendance</th>
												<th>Report</th>
											 </tr>
                                        </thead>
                                        <tfoot>
                                             <tr>
												<th>Sr No</th>
												<th>Name</th>
												<th align="center">Attendance</th>
												<th>Report</th>
											  </tr>
                                        </tfoot>
                                        <tbody>
                                          <tr>
											<td>1</td>
											<td>John</td>
											   <td class="studentID" data-studentid="1" data-title="Attendance">
												<a href="" class="badge badge-success badge-pill">Present</a>
												<a href="" class="badge badge-warning badge-pill">Late</a>
												<a href=""class="badge badge-info badge-pill">Half Day</a>
												<a href="" class="badge badge-danger badge-pill">Absent</a>
											   </td>
												 <td><a href="{{route('attdence.secretary')}}" class="badge badge-success badge-pill"><i class="fa fa-file"></i> Report</a></td>
											</tr>
                                        </tbody>
                                    </table>
                                </div>
								 
								</div>
							</div>
						</div>
					</div>
					
					
					 <div class="tab-pane fade" id="income" role="tabpanel" aria-labelledby="profile-tab">
						<div class="col-lg-12 mt-4 stretched_card">
							<div class="card">
								<div class="card-body">
									<div class="table-responsive m-t-40">
                                    <table id="income1"
                                        class="display nowrap table table-hover table-striped table-bordered"
                                        cellspacing="0" width="100%">
                                        <thead>
                                              <tr>
												<th>Sr No</th>
												<th>Name</th>
												<th align="center">Attendance</th>
												<th>Report</th>
											 </tr>
                                        </thead>
                                        <tfoot>
                                             <tr>
												<th>Sr No</th>
												<th>Name</th>
												<th align="center">Attendance</th>
												<th>Report</th>
											  </tr>
                                        </tfoot>
                                        <tbody>
                                          <tr>
											<td>1</td>
											<td>John</td>
											   <td class="studentID" data-studentid="1" data-title="Attendance">
												<a href="" class="badge badge-success badge-pill">Present</a>
												<a href="" class="badge badge-warning badge-pill">Late</a>
												<a href=""class="badge badge-info badge-pill">Half Day</a>
												<a href="" class="badge badge-danger badge-pill">Absent</a>
											   </td>
												 <td><a href="{{route('attdence.secretary')}}" class="badge badge-success badge-pill"><i class="fa fa-file"></i> Report</a></td>
											</tr>
                                        </tbody>
                                    </table>
                                </div>
								 
								</div>
							</div>
						</div>  
					 </div>
					
					
					 <div class="tab-pane fade" id="expenditure" role="tabpanel" aria-labelledby="profile-tab">
						<div class="col-lg-12 mt-4 stretched_card">
							<div class="card">
								<div class="card-body">
									<div class="table-responsive m-t-40">
                                    <table id="expenditure1"
                                        class="display nowrap table table-hover table-striped table-bordered"
                                        cellspacing="0" width="100%">
                                        <thead>
                                              <tr>
												<th>Sr No</th>
												<th>Name</th>
												<th align="center">Attendance</th>
												<th>Report</th>
											 </tr>
                                        </thead>
                                        <tfoot>
                                             <tr>
												<th>Sr No</th>
												<th>Name</th>
												<th align="center">Attendance</th>
												<th>Report</th>
											  </tr>
                                        </tfoot>
                                        <tbody>
                                           <tr>
											<td>1</td>
											<td>John</td>
											   <td class="studentID" data-studentid="1" data-title="Attendance">
												<a href="" class="badge badge-success badge-pill">Present</a>
												<a href="" class="badge badge-warning badge-pill">Late</a>
												<a href=""class="badge badge-info badge-pill">Half Day</a>
												<a href="" class="badge badge-danger badge-pill">Absent</a>
											   </td>
												 <td><a href="{{route('attdence.secretary')}}" class="badge badge-success badge-pill"><i class="fa fa-file"></i> Report</a></td>
											</tr>
                                           
                                        </tbody>
                                    </table>
                                </div>
								 
								</div>
							</div>
						</div>	
					 </div>
					
					 <div class="tab-pane fade" id="statics" role="tabpanel" aria-labelledby="profile-tab">
					    <div class="col-lg-12 mt-4 stretched_card">
							<div class="card">
								<div class="card-body">
									<div class="table-responsive m-t-40">
                                    <table id="statics1"
                                        class="display nowrap table table-hover table-striped table-bordered"
                                        cellspacing="0" width="100%">
                                        <thead>
                                              <tr>
												<th>Sr No</th>
												<th>Name</th>
												<th align="center">Attendance</th>
												<th>Report</th>
											 </tr>
                                        </thead>
                                        <tfoot>
                                             <tr>
												<th>Sr No</th>
												<th>Name</th>
												<th align="center">Attendance</th>
												<th>Report</th>
											  </tr>
                                        </tfoot>
                                        <tbody>
                                           <tr>
											<td>1</td>
											<td>John</td>
											   <td class="studentID" data-studentid="1" data-title="Attendance">
												<a href="" class="badge badge-success badge-pill">Present</a>
												<a href="" class="badge badge-warning badge-pill">Late</a>
												<a href=""class="badge badge-info badge-pill">Half Day</a>
												<a href="" class="badge badge-danger badge-pill">Absent</a>
											   </td>
												 <td><a href="{{route('attdence.secretary')}}" class="badge badge-success badge-pill"><i class="fa fa-file"></i> Report</a></td>
											</tr>
                                           
                                        </tbody>
                                    </table>
                                </div>
								 
								</div>
							</div>
						</div>
					 </div>
								
					 <div class="tab-pane fade" id="dy_sarpanch" role="tabpanel" aria-labelledby="profile-tab">
                         <div class="col-lg-12 mt-4 stretched_card">
							<div class="card">
								<div class="card-body">
									<div class="table-responsive m-t-40">
                                    <table id="dy_sarpanch1"
                                        class="display nowrap table table-hover table-striped table-bordered"
                                        cellspacing="0" width="100%">
                                        <thead>
                                              <tr>
												<th>Sr No</th>
												<th>Name</th>
												<th align="center">Attendance</th>
												<th>Report</th>
											 </tr>
                                        </thead>
                                        <tfoot>
                                             <tr>
												<th>Sr No</th>
												<th>Name</th>
												<th align="center">Attendance</th>
												<th>Report</th>
											  </tr>
                                        </tfoot>
                                        <tbody>
                                           <tr>
											<td>1</td>
											<td>John</td>
											   <td class="studentID" data-studentid="1" data-title="Attendance">
												<a href="" class="badge badge-success badge-pill">Present</a>
												<a href="" class="badge badge-warning badge-pill">Late</a>
												<a href=""class="badge badge-info badge-pill">Half Day</a>
												<a href="" class="badge badge-danger badge-pill">Absent</a>
											   </td>
												 <td><a href="{{route('attdence.secretary')}}" class="badge badge-success badge-pill"><i class="fa fa-file"></i> Report</a></td>
											</tr>
                                           
                                        </tbody>
                                    </table>
                                </div>
								 
								</div>
							</div>
						</div>
		             </div>
					
					
					
				</div>
			</div>
		</div>
						
						
						
	</div>
</div>


@endsection

@section('js')
    <!--=========================*
            This Page Scripts
    *===========================-->
    <!-- Data Table js -->
   <script>
        $(function () {
            $('#myTable').DataTable();
         
            $('#example23').DataTable({
                dom: 'Bfrtip',
                buttons: [
                    'copy', 'csv', 'excel', 'pdf', 'print'
                ]
            });
			$('#ward1').DataTable({
                dom: 'Bfrtip',
                buttons: [
                    'copy', 'csv', 'excel', 'pdf', 'print'
                ]
            });
			$('#employee1').DataTable({
                dom: 'Bfrtip',
                buttons: [
                    'copy', 'csv', 'excel', 'pdf', 'print'
                ]
            });
			
			$('#citizen1').DataTable({
                dom: 'Bfrtip',
                buttons: [
                    'copy', 'csv', 'excel', 'pdf', 'print'
                ]
            });
			$('#subscriber1').DataTable({
                dom: 'Bfrtip',
                buttons: [
                    'copy', 'csv', 'excel', 'pdf', 'print'
                ]
            });
			$('#income1').DataTable({
                dom: 'Bfrtip',
                buttons: [
                    'copy', 'csv', 'excel', 'pdf', 'print'
                ]
            });
			$('#expenditure1').DataTable({
                dom: 'Bfrtip',
                buttons: [
                    'copy', 'csv', 'excel', 'pdf', 'print'
                ]
            });
			$('#statics1').DataTable({
                dom: 'Bfrtip',
                buttons: [
                    'copy', 'csv', 'excel', 'pdf', 'print'
                ]
            });
			$('#dy_sarpanch1').DataTable({
                dom: 'Bfrtip',
                buttons: [
                    'copy', 'csv', 'excel', 'pdf', 'print'
                ]
            });
            $('.buttons-copy, .buttons-csv, .buttons-print, .buttons-pdf, .buttons-excel').addClass('btn btn-primary mr-1');
        });

    </script>
@endsection

