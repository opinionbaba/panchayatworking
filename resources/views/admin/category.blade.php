@extends('layouts.app')


{{-- Page Title --}}
@section('page-title')
    List Group
@endsection

{{-- This Page Css --}}
@section('css')
 <link rel="stylesheet" type="text/css"
        href="{{asset('public/assets/datatables.net-bs4/css/dataTables.bootstrap4.css')}}">
    <link rel="stylesheet" type="text/css"
        href="{{asset('public/assets/datatables.net-bs4/css/responsive.dataTables.min.css')}}">
  <body class="skin-blue fixed-layout">
    <!-- ============================================================== -->
    <!-- Preloader - style you can find in spinners.css -->
    <!-- ============================================================== -->
    <div class="preloader">
        <div class="loader">
            <div class="loader__figure"></div>
            <p class="loader__label">Elite admin</p>
        </div>
    </div>
	
	 <div id="main-wrapper">
@endsection

@section('main-content')
  <div class="page-wrapper">
            <!-- ============================================================== -->
            <!-- Container fluid  -->
            <!-- ============================================================== -->
            <div class="container-fluid">
                <!-- ============================================================== -->
                <!-- Bread crumb and right sidebar toggle -->
                <!-- ============================================================== -->
                <div class="row page-titles">
                    <div class="col-md-5 align-self-center">
                        <h4 class="text-themecolor">Category</h4>
                    </div>
                    <div class="col-md-7 align-self-center text-right">
                        <div class="d-flex justify-content-end align-items-center">
                            <ol class="breadcrumb">
                                <li class="breadcrumb-item"><a href="javascript:void(0)">Home</a></li>
                                <li class="breadcrumb-item active">Category</li>
                            </ol>
                            <button type="button" class="btn btn-info d-none d-lg-block m-l-15" data-toggle="modal" data-target="#main-category"><i
                                    class="fa fa-plus-circle"></i> Add New Category</button>
                        </div>
                    </div>
                </div>
				
				<div class="card">
                    <div class="card-body">
                        <div class="row">
                            <div class="col-md-12">
								   <h4 class="card_title">All Notice</h4>
                                <div class="button-box">
                                    <a class="tst1 btn btn-info" href="#">Homee</a>
                                    <a class="tst1 btn btn-info" href="{{route('admin.allnotice')}}">Notice</a>
                                    <a class="tst1 btn btn-info" href="{{route('admin.allevent')}}">Events</a>
                                    <a class="tst1 btn btn-info" href="{{route('admin.allgallery')}}">Gallery</a>
                                    <a class="tst1 btn btn-info" href="{{route('admin.alldoc')}}">Form</a>
                                    <a class="tst1 btn btn-info" href="{{route('admin.contact')}}">Contact Us</a>
                                    <a class="tst1 btn btn-info" href="{{route('admin.subs')}}">Subscribers</a>
                                    <a class="tst1 btn btn-info" href="#">House Tax</a>
                                    <a class="tst1 btn btn-info" href="{{route('admin.allaudit')}}">Audit</a>
                                </div>
									@if(Session::has('delete'))
						<div class="alert alert-danger alert-dismissible fade show" role="alert">
							<strong></strong>  {{Session::get('delete')}}
							<button type="button" class="close" data-dismiss="alert" aria-label="Close"><span class="ti-close"></span>
							</button>
						</div>
						@endif
						<div class="modal fade" id="main-category">
								<div class="modal-dialog modal-dialog-centered" role="document">
									<div class="modal-content">
										<div class="modal-header">
											<h5 class="modal-title">New Main-Category</h5>
											<button type="button" class="close" data-dismiss="modal"><span>&times;</span></button>
										</div>
										<div class="modal-body">
										 <form action="{{route('admin.insert-main-cat')}}" method="post" class="needs-validation" novalidate enctype="multipart/form-data">
												@csrf
												 <div class="row">
													<div class="col-md-12">
														<label for="validationTooltip01">Name Of Main-Category</label>
														<input type="text" name="name" class="form-control" id="validationTooltip01" value="" required>
														<div class="valid-tooltip">
															Looks good!
														</div>
													</div>
											   </div>
										</div>
											<div class="modal-footer">
												<button class="btn btn-primary" type="submit">Submit form</button>
											<button type="button" class="btn btn-light" data-dismiss="modal">Close</button>
											</div>
											</form>
										</div>
									</div>
								</div>
								
							
							
							    <div class="table-responsive m-t-40">
                                    <table id="myTable" class="table table-bordered table-striped text-center">
                                        <thead>
                                            <tr>
                                                <th>Category</th>
                                                <th>Add Category</th>
                                                <th>Action</th>
                                            </tr>
                                        </thead>
                                        <tbody>
										  @foreach($all as $value)
                                            <tr>
                                                <td>{{$value->name}}</td>
                                                <td>
												  <a href="{{route('sub-cat',$value->id)}}"><span class="badge badge-success ">Add Sub Category!</span></a>
												</td>
                                                <td> 
												 <a href="{{route('delete-main-cat',$value->id)}}"><span class="badge badge-success "><i class="fa fa-trash"></i></span></a>
												</td>
                                            </tr>
							              @endforeach  
                                        </tbody>
                                    </table>
                                </div>
							
							
							
							
								
                            </div>
                        </div>
					</div>
				</div>
	
    </div>
    </div>
    </div>
@endsection

@section('js')
    <script>
        $(function () {
            $('#myTable').DataTable();
            var table = $('#example').DataTable({
                "columnDefs": [{
                    "visible": false,
                    "targets": 2
                }],
                "order": [
                    [2, 'asc']
                ],
                "displayLength": 25,
                "drawCallback": function (settings) {
                    var api = this.api();
                    var rows = api.rows({
                        page: 'current'
                    }).nodes();
                    var last = null;
                    api.column(2, {
                        page: 'current'
                    }).data().each(function (group, i) {
                        if (last !== group) {
                            $(rows).eq(i).before('<tr class="group"><td colspan="5">' + group + '</td></tr>');
                            last = group;
                        }
                    });
                }
            });
            // Order by the grouping
            $('#example tbody').on('click', 'tr.group', function () {
                var currentOrder = table.order()[0];
                if (currentOrder[0] === 2 && currentOrder[1] === 'asc') {
                    table.order([2, 'desc']).draw();
                } else {
                    table.order([2, 'asc']).draw();
                }
            });
            // responsive table
            $('#config-table').DataTable({
                responsive: true
            });
            $('#example23').DataTable({
                dom: 'Bfrtip',
                buttons: [
                    'copy', 'csv', 'excel', 'pdf', 'print'
                ]
            });
            $('.buttons-copy, .buttons-csv, .buttons-print, .buttons-pdf, .buttons-excel').addClass('btn btn-primary mr-1');
        });

    </script>
@endsection
