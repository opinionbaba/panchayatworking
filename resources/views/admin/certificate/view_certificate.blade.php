@extends('layouts.app')


{{-- Page Title --}}
@section('page-title')
    Profile Page
@endsection

{{-- This Page Css --}}
@section('css')
<style>
.img {
	height: 38px;
    width: 35px;
    border-radius: 36px;
}
</style>
  <body class="skin-blue fixed-layout">
    <div class="preloader">
        <div class="loader">
            <div class="loader__figure"></div>
            <p class="loader__label">Elite admin</p>
        </div>
    </div>
    <div id="main-wrapper">  
@endsection

@section('main-content')
        <div class="page-wrapper">
            <div class="container-fluid">
                <div class="row page-titles">
                    <div class="col-md-5 align-self-center">
                        <h4 class="text-themecolor">View Certificate</h4>
                    </div>
                    <div class="col-md-7 align-self-center text-right">
                        <div class="d-flex justify-content-end align-items-center">
                            <ol class="breadcrumb">
                                <li class="breadcrumb-item"><a href="javascript:void(0)">Home</a></li>
                                <li class="breadcrumb-item active">Certificate</li>
                            </ol>                           
                        </div>
                    </div>
                </div>
				
				<div class="row">
				      <div class="col-12">
					   @foreach ($certificate as $value)
                        <div class="card">
                            <div class="card-body">
                                <h4 class="card-title">view Certificate</h4>
                                <h6 class="card-subtitle">Add<code>.table-bordered</code>for borders on all sides of the table and cells.</h6>
                                <div class="table-responsive">
                                    <table class="table table-bordered">
                                        <thead>
										 
                                            <tr>
                                                <th scope="row">Head Name</th>
                                                <td>Value</td>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            
                                                <tr>
													<th scope="row">Certificate Name</th>
													<td>{{$value->cer_name}}</td>
												</tr>
												<tr>
													<th scope="row">Passport</th>
													 <td>@if($value->passport==1)
													 <span class="badge badge-info">Required</span>
													  @else
														 <span class="badge badge-danger">Not Required</span>
													@endif
													</td>
												</tr>
                                                
												<tr>
													<th scope="row">Aadhar Card</th>
													<td>@if($value->aadhar_card==1)
													 <span class="badge badge-info">Required</span>
													  @else
														 <span class="badge badge-danger">Not Required</span>
													@endif
													</td>
												</tr>
												<tr>
													<th scope="row">Pan Card</th>
													<td>@if($value->pan_card==1)
													 <span class="badge badge-info">Required</span>
													  @else
														 <span class="badge badge-danger">Not Required</span>
													@endif
													</td>
												</tr>
												<tr>
													<th scope="row">Ration Card</th>
												   <td>@if($value->ration_card==1)
													 <span class="badge badge-info">Required</span>
													  @else
														 <span class="badge badge-danger">Not Required</span>
													@endif
													</td>
												</tr>
                                          
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
				</div>
				
				
				<div class="row">
				   <div class="col-12">
                        <div class="card">
                            <div class="card-body">
                                <h4 class="card-title">Second Table Certificate</h4>
                                
                                <div class="table-responsive">
                                    <table class="table table-bordered">
                                        <thead>
                                            <tr>
                                                <th>Name</th>
                                                <th>Value</th>                                                
                                            </tr>
                                        </thead>
                                        <tbody>
                                            <tr>
												<th scope="row">Certificate Description</th>
												<td>{{$value->cer_desc}}</td>
											</tr>
										  
											<tr>
												<th scope="row">Voter Card</th>
												<td>@if($value->voter_card==1)
												 <span class="badge badge-info">Required</span>
												  @else
													 <span class="badge badge-danger">Not Required</span>
												@endif
												</td>
											</tr>
											
											<tr>
												<th scope="row">Health Card</th>
												<td>@if($value->health_card==1)
												 <span class="badge badge-info">Required</span>
												  @else
													 <span class="badge badge-danger">Not Required</span>
												@endif
												</td>
											</tr>
											  <tr>
												<th scope="row">Birth Certificate</th>
											   <td>@if($value->birth_certi==1)
												 <span class="badge badge-info">Required</span>
												  @else
													 <span class="badge badge-danger">Not Required</span>
												@endif
												</td>
											</tr>
											<tr>
												<th scope="row">Driving Licence</th>
												  <td>@if($value->driving_lic==1)
												 <span class="badge badge-info">Required</span>
												  @else
													 <span class="badge badge-danger">Not Required</span>
												@endif
												</td>
											</tr>
														
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
					@endforeach
                    </div>
				
				</div>
				
				
				
				
				
			
				
				
				
            </div>
        </div>
				
	
	
@endsection

@section('js')
    
@endsection
