@extends('layouts.app')


{{-- Page Title --}}
@section('page-title')
  Accounts
@endsection
{{-- This Page Css --}}
@section('css')
   <link rel="stylesheet" type="text/css"
        href="{{asset('public/assets/datatables.net-bs4/css/dataTables.bootstrap4.css')}}">
    <link rel="stylesheet" type="text/css"
        href="{{asset('public/assets/datatables.net-bs4/css/responsive.dataTables.min.css')}}">
    <!--=========================*
               Datatable
  
  *===========================-->
  <body class="skin-blue fixed-layout">
    <!-- ============================================================== -->
    <!-- Preloader - style you can find in spinners.css -->
    <!-- ============================================================== -->
    <div class="preloader">
        <div class="loader">
            <div class="loader__figure"></div>
        </div>
    </div>
	 <div id="main-wrapper">
	  
@endsection
@section('main-content')
 <div class="page-wrapper">
      <div class="container-fluid">
                <!-- ============================================================== -->
                <!-- Bread crumb and right sidebar toggle -->
                <!-- ============================================================== -->
                <div class="row page-titles">
                    <div class="col-md-5 align-self-center">
                        <h4 class="text-themecolor">Datatable</h4>
                    </div>
                    <div class="col-md-7 align-self-center text-right">
                        <div class="d-flex justify-content-end align-items-center">
                            <ol class="breadcrumb">
                                <li class="breadcrumb-item"><a href="javascript:void(0)">Home</a></li>
                                <li class="breadcrumb-item active">Datatable</li>
                            </ol>
                            <button type="button" class="btn btn-info d-none d-lg-block m-l-15" data-toggle="modal" data-target="#exampleModalCenter" ><i
                                    class="fa fa-plus-circle"></i> Add New Member</button>
									
                        </div>
                    </div>
                </div>
                <!-- ============================================================== -->
                <!-- End Bread crumb and right sidebar toggle -->
                <!-- ============================================================== -->
                <!-- ============================================================== -->
                <!-- Start Page Content -->
                <!-- ============================================================== -->
                <div class="row">
                    <div class="col-12">
					    <div class="card">
                            <div class="card-body">
                                <h4 class="card-title">Default Tab</h4>
                                <h6 class="card-subtitle">Use default tab with class <code>nav-tabs & tabcontent-border </code></h6>
                                <!-- Nav tabs -->
                                <ul class="nav nav-tabs" role="tablist">
                                    <li class="nav-item"> <a class="nav-link active" data-toggle="tab" href="#home" role="tab"><span class="hidden-sm-up"><i class="ti-home"></i></span> <span class="hidden-xs-down">Citizens List</span></a> </li>
                                    <li class="nav-item"> <a class="nav-link" data-toggle="tab" href="#profile" role="tab"><span class="hidden-sm-up"><i class="ti-user"></i></span> <span class="hidden-xs-down">Add Citizen</span></a> </li>
                                   
                                </ul>
                                <!-- Tab panes -->
                                <div class="tab-content tabcontent-border">
                                    <div class="tab-pane active" id="home" role="tabpanel">
                                        <div class="p-20">
                                            <div class="table-responsive m-t-40">
                                    <table id="example23"
                                        class="display nowrap table table-hover table-striped table-bordered"
                                        cellspacing="0" width="100%">
                                        <thead>
                                            <tr>
                                               <th>Sr No</th>
												<th>Citizen type</th>
												<th>Name</th>
												<th>Number</th>
												<th>House Number</th>
												<th>Email</th>
												<th>Action</th>
                                            </tr>
                                        </thead>
                                        <tfoot>
                                            <tr>
                                                <th>Sr No</th>
												<th>Citizen type</th>
												<th>Name</th>
												<th>Number</th>
												<th>House Number</th>
												<th>Email</th>
												<th>Action</th>
                                            </tr>
                                        </tfoot>
                                        <tbody>
                                          @php
										$num=1;
										@endphp
										@foreach($all as $value)
										<tr>
											<td>{{$num}}</td>
											<td>@if($value->citizen_type == 1)
												<span class="badge badge-success">Individual</span>
											@else
												<span class="badge badge-warning">Business</span>
											@endif
											</td>
											<td>{{$value->f_name}}</td>
											<td>{{$value->mobile}}</td>
											<td>{{$value->house_number}}</td>
											<td>{{$value->email}}</td>
											<td> 
											  <a href = "" class='forum-title' name = "abc" data-toggle="modal" data-target="#usermodal"><i class="ti-pencil mr-1 btn btn-success"></i></a>
											  <a href = "{{route('citizen.view',$value->id)}}" class='forum-title' ><i class="ti-eye mr-1 btn btn-success"></i></a>
												 <a href = "" class='forum-title'><i class="ti-trash btn btn-danger"></i></a>
										</td>
										</tr>
										@php
										$num++;
										@endphp
										@endforeach
                                           
                                        </tbody>
                                    </table>
                                </div>
                                        </div>
                                    </div>
									
									
                                    <div class="tab-pane  p-20" id="profile" role="tabpanel">
									  
										 <ul class="nav nav-tabs customtab" role="tablist">
											<li class="nav-item">
												<a class="nav-link active" id="home-tab" data-toggle="tab" href="#test" role="tab" aria-controls="home" aria-selected="true"><i class="ti-user"></i>Personal Details</a>
											</li>
											 <li class="nav-item">
												<a class="nav-link " id="asset-tab" data-toggle="tab" href="#asset" role="tab" aria-controls="home" aria-selected="true"><i class="ti-home"></i> Asset</a>
											</li> 
											<li class="nav-item">
												<a class="nav-link " id="asset-tab" data-toggle="tab" href="#documents" role="tab" aria-controls="home" aria-selected="true"><i class="ti-file"></i> Documents</a>
											</li>
											<li class="nav-item">
												<a class="nav-link " id="asset-tab" data-toggle="tab" href="#other" role="tab" aria-controls="home" aria-selected="true"><i class="fa fa-cc-diners-club"></i> Members</a>
											</li>
											<li class="nav-item">
												<a class="nav-link " id="asset-tab" data-toggle="tab" href="#bank" role="tab" aria-controls="home" aria-selected="true"><i class="fa fa-black-tie"></i> Bank Details</a>
											</li>
										</ul>
										<!-- Tab panes -->
										<div class="tab-content">
											 <div class="tab-pane fade show active" id="test" role="tabpanel" aria-labelledby="home-tab">
								<div class="col-lg-12">
                                   <div class="card">
											<div class="card-body">
												<h4 class="card_title">Add New User</h4>
												<form class="needs-validation" novalidate="">
													<div class="form-row">
														<div class="col-md-4 mb-3">
															<label for="validationCustom01">Select Type</label>
															<select name="" class="form-control">
																<option value="">--Select Type--</option>
																<option value="">Individual</option>
																<option value="">Business</option>
															</select>
															<div class="valid-feedback">
																Looks good!
															</div>
														</div>
														<div class="col-md-4 mb-3">
															<label for="validationCustom01">First name</label>
															<input type="text" name="f_name" class="form-control" id="validationCustom01" placeholder="First name.."  required="">
															<div class="valid-feedback">
																Looks good!
															</div>
														</div>
														<div class="col-md-4 mb-3">
															<label for="validationCustom01">Middle name</label>
															<input type="text" name="m_name" class="form-control" id="validationCustom01" placeholder="Middle name.."  required="">
															<div class="valid-feedback">
																Looks good!
															</div>
														</div>
													</div>
													<div class="form-row">
														<div class="col-md-4 mb-3">
															<label for="validationCustom01">last name</label>
															<input type="text" name="l_name" class="form-control" id="validationCustom01" placeholder="last name.."  required="">
															<div class="valid-feedback">
																Looks good!
															</div>
														</div>
														<div class="col-md-4 mb-3">
															<label for="validationCustom02">Ward Number</label>
															<input type="number" name="w_no" class="form-control" id="validationCustom02" placeholder="Enter ward number.." required="">
															<div class="valid-feedback">
																Looks good!
															</div>
														</div>
														<div class="col-md-4 mb-3">
															<label for="validationCustom01">Mobile Number</label>
															<input type="number" name="number" class="form-control" id="validationCustom01" placeholder="Enter mobile number..." required="">
															<div class="valid-feedback">
																Looks good!
															</div>
														</div>
														
													</div>
													<div class="form-row">
														<div class="col-md-4 mb-3">
															<label for="validationCustom01">Landlilne name</label>
															<input type="text" name="land_number" class="form-control" id="validationCustom01" placeholder="Landlilne number..."  required="">
															<div class="valid-feedback">
																Looks good!
															</div>
														</div>
														<div class="col-md-4 mb-3">
															<label for="validationCustom02">Family Number</label>
															<input type="number" name="f_number" class="form-control" id="validationCustom02" placeholder="Enter Family. .." required="">
															<div class="valid-feedback">
																Looks good!
															</div>
														</div>
														<div class="col-md-4 mb-3">
															<label for="validationCustom01">Email</label>
															<input type="email" name="email" class="form-control" id="validationCustom01" placeholder="Enter Email..." required="">
															<div class="valid-feedback">
																Looks good!
															</div>
														</div>
													</div>
													<div class="form-row">
														<div class="col-md-4 mb-3">
															<label for="validationCustom01">House No</label>
															<input type="text" name="h_number" class="form-control" id="validationCustom01" placeholder="EX:124HDB"  required="">
															<div class="valid-feedback">
																Looks good!
															</div>
														</div>
														<div class="col-md-4 mb-3">
															<label for="validationCustom02">Area</label>
															<input type="text" name="area" class="form-control" id="validationCustom02" placeholder="Enter area..." required="">
															<div class="valid-feedback">
																Looks good!
															</div>
														</div>
														<div class="col-md-4 mb-3">
															<label for="validationCustom01">Locality</label>
															<input type="text" name="locality"class="form-control" id="validationCustom01" placeholder="Enter locality..." required="">
															<div class="valid-feedback">
																Looks good!
															</div>
														</div>
													</div>
													<div class="form-row">
														<div class="col-md-3 mb-3">
															<label for="validationCustom01">Village</label>
															<input type="text" name="village" class="form-control" id="validationCustom01" placeholder="User village name.."  required="">
															<div class="valid-feedback">
																Looks good!
															</div>
														</div>
														<div class="col-md-3 mb-3">
															<label for="validationCustom02">City</label>
															<input type="text" name="city"  class="form-control" id="validationCustom02" placeholder="Enter City.." required="">
															<div class="valid-feedback">
																Looks good!
															</div>
														</div>
														<div class="col-md-3 mb-3">
															<label for="validationCustom01">State</label>
															<input type="text" name="state" class="form-control" id="validationCustom01" placeholder="Enter state Name.." required="">
															<div class="valid-feedback">
																Looks good!
															</div>
														</div>
														<div class="col-md-3 mb-3">
															<label for="validationCustom01">Pin Code</label>
															<input type="text" name="pin_code" class="form-control" id="validationCustom01" placeholder="Enter Pin Code.." required="">
															<div class="valid-feedback">
																Looks good!
															</div>
														</div>
													</div>
													
													<button class="btn btn-primary" type="submit">Submit form</button>
												</form>
											</div>
										</div>
										</div>
                                </div>
                                <div class="tab-pane fade" id="asset" role="tabpanel" aria-labelledby="asset-tab">
                                    <div class="col-lg-12">
										<div class="card">
											<div class="card-body">
												<h4 class="card_title">Add New User</h4>
												<form class="needs-validation" novalidate="">
													<div class="form-row">
														<div class="col-md-6 mb-3">
															<label for="validationCustom01">Select Type</label>
															<select name="" class="form-control">
																<option value="">--Select--</option>
																<option value="">House</option>
																<option value="">Factory</option>
																<option value="">Shop</option>
																<option value="">Business </option>
															</select>
															<div class="valid-feedback">
																Looks good!
															</div>
														</div>
													 </div>
													<button class="btn btn-primary" type="submit">Submit form</button>
												</form>
											</div>
										</div>
									</div>
                                </div>
                                <div class="tab-pane fade" id="documents" role="tabpanel" aria-labelledby="contact-tab">
                                  <div class="col-lg-12">
                                   <div class="card">
											<div class="card-body">
												<h4 class="card_title">Add New User</h4>
												<form class="needs-validation" novalidate="">
													<div class="form-row">
														<div class="col-md-4 mb-3">
															<label for="validationCustom01">Aadhar Number</label>
															<input type="text" name="f_name" class="form-control" id="validationCustom01" placeholder="Aadhar Number.."  required="">
															<div class="valid-feedback">
																Looks good!
															</div>
														</div>
														<div class="col-md-4 mb-3">
															<label for="validationCustom01">Aadhar Front Image</label>
															<input type="file" name="m_name" class="form-control"   required="">
															<div class="valid-feedback">
																Looks good!
															</div>
														</div>
														<div class="col-md-4 mb-3">
															<label for="validationCustom01">Aadhar Back Image</label>
															<input type="file" name="m_name" class="form-control"   required="">
															<div class="valid-feedback">
																Looks good!
															</div>
														</div>
													</div>
													<div class="form-row">
														<div class="col-md-4 mb-3">
															<label for="validationCustom01">Ration Card Number</label>
															<input type="text" name="village" class="form-control" id="validationCustom01" placeholder="Ration Card Number.."  required="">
															<div class="valid-feedback">
																Looks good!
															</div>
														</div>
														<div class="col-md-4 mb-3">
															<label for="validationCustom02">Ration Card Front</label>
															<input type="file" name="city"  class="form-control"  required="">
															<div class="valid-feedback">
																Looks good!
															</div>
														</div>
														<div class="col-md-4 mb-3">
															<label for="validationCustom02">Ration Card Back</label>
															<input type="file" name="city"  class="form-control"  required="">
															<div class="valid-feedback">
																Looks good!
															</div>
														</div>
													</div>
													<div class="form-row">
														<div class="col-md-4 mb-3">
															<label for="validationCustom01">Passport Number</label>
															<input type="text" name="land_number" class="form-control" id="validationCustom01" placeholder="Landlilne number..."  required="">
															<div class="valid-feedback">
																Looks good!
															</div>
														</div>
														<div class="col-md-4 mb-3">
															<label for="validationCustom02">Passport Front Image</label>
															<input type="file" name="f_number" class="form-control"  required="">
															<div class="valid-feedback">
																Looks good!
															</div>
														</div>
														<div class="col-md-4 mb-3">
															<label for="validationCustom02">Passport Back Image</label>
															<input type="file" name="f_number" class="form-control"  required="">
															<div class="valid-feedback">
																Looks good!
															</div>
														</div>
													</div>
													<div class="form-row">
														<div class="col-md-4 mb-3">
															<label for="validationCustom01">Voter Id</label>
															<input type="text" name="h_number" class="form-control" id="validationCustom01" placeholder="Enter Voter Id.."  required="">
															<div class="valid-feedback">
																Looks good!
															</div>
														</div>
														<div class="col-md-4 mb-3">
															<label for="validationCustom02">Voter Card Front</label>
															<input type="file" name="area" class="form-control"  required="">
															<div class="valid-feedback">
																Looks good!
															</div>
														</div>
														<div class="col-md-4 mb-3">
															<label for="validationCustom02">Voter Card Back</label>
															<input type="file" name="area" class="form-control"  required="">
															<div class="valid-feedback">
																Looks good!
															</div>
														</div>
													</div>
													<div class="form-row">
														<div class="col-md-6 mb-3">
															<label for="validationCustom01">Pan Number</label>
															<input type="text" name="l_name" class="form-control" id="validationCustom01" placeholder="Pan Number.."  required="">
															<div class="valid-feedback">
																Looks good!
															</div>
														</div>
														<div class="col-md-6 mb-3">
															<label for="validationCustom02">Pan Card Image</label>
															<input type="file" name="w_no" class="form-control"  required="">
															<div class="valid-feedback">
																Looks good!
															</div>
														</div>
													</div>
													<div class="form-row">
														<div class="col-md-6 mb-3">
															<label for="validationCustom01">Health Card Number</label>
															<input type="text" name="state" class="form-control" id="validationCustom01" placeholder="Enter Health Card Number.." required="">
															<div class="valid-feedback">
																Looks good!
															</div>
														</div>
														<div class="col-md-6 mb-3">
															<label for="validationCustom01">Health Card Image</label>
															<input type="file" name="pin_code" class="form-control" required="">
															<div class="valid-feedback">
																Looks good!
															</div>
														</div>
													</div>
													<div class="form-row">
														<div class="col-md-6 mb-3">
															<label for="validationCustom01">Birth Certificate Number</label>
															<input type="text" name="state" class="form-control" id="validationCustom01" placeholder="Enter Birth Certificate Number.." required="">
															<div class="valid-feedback">
																Looks good!
															</div>
														</div>
														<div class="col-md-6 mb-3">
															<label for="validationCustom01">Birth Certificate Image</label>
															<input type="file" name="pin_code" class="form-control" required="">
															<div class="valid-feedback">
																Looks good!
															</div>
														</div>
													</div>
													<div class="form-row">
														<div class="col-md-6 mb-3">
															<label for="validationCustom01">Driving Licence Number</label>
															<input type="text" name="state" class="form-control" id="validationCustom01" placeholder="Driving Licence Number.." required="">
															<div class="valid-feedback">
																Looks good!
															</div>
														</div>
														<div class="col-md-6 mb-3">
															<label for="validationCustom01">Driving Licence Image</label>
															<input type="file" name="pin_code" class="form-control"   required="">
															<div class="valid-feedback">
																Looks good!
															</div>
														</div>
													</div>
													
													
													<button class="btn btn-primary" type="submit">Submit form</button>
												</form>
											</div>
										</div>
										</div>
                                </div>
								  <div class="tab-pane fade" id="other" role="tabpanel" aria-labelledby="contact-tab">
                                   <div class="col-lg-12">
										<div class="card">
											<div class="card-body">
												<h4 class="card_title">Family Member Details</h4>
												<form class="needs-validation" novalidate="">
													<div class="form-row">
														<div class="col-md-6 mb-3">
															<label for="validationCustom01">Total Family Member</label>
															<input type="number" name="f_name" class="form-control" id="validationCustom01" placeholder="Total Family Member.."  required="">
															<div class="valid-feedback">
																Looks good!
															</div>
														</div>
														<div class="col-md-6 mb-3">
															<label for="validationCustom01">Total Voter member</label>
															<input type="number" name="m_name" class="form-control" id="validationCustom01" placeholder="Total Voter Member.."  required="">
															<div class="valid-feedback">
																Looks good!
															</div>
														</div>
													 </div>
													<button class="btn btn-primary" type="submit">Submit form</button>
												</form>
												
											</div>
										</div>
									</div>
                                 </div>
								  <div class="tab-pane fade" id="bank" role="tabpanel" aria-labelledby="contact-tab">
								        <div class="col-lg-12">
										<div class="card">
											<div class="card-body">
												<h4 class="card_title">Bank Details</h4>
												<form class="needs-validation" novalidate="">
													<div class="form-row">
														<div class="col-md-6 mb-3">
															<label for="validationCustom01">Account Number</label>
															<input type="text" name="f_name" class="form-control" id="validationCustom01" placeholder="Account Number.."  required="">
															<div class="valid-feedback">
																Looks good!
															</div>
														</div>
														<div class="col-md-6 mb-3">
															<label for="validationCustom01">IFSC Code</label>
															<input type="text" name="m_name" class="form-control" id="validationCustom01" placeholder="IFSC Code.."  required="">
															<div class="valid-feedback">
																Looks good!
															</div>
														</div>
													 </div>
													 <div class="form-row">
														<div class="col-md-6 mb-3">
															<label for="validationCustom01">Bank Name</label>
															<input type="text" name="f_name" class="form-control" id="validationCustom01" placeholder="Bank Name.."  required="">
															<div class="valid-feedback">
																Looks good!
															</div>
														</div>
														<div class="col-md-6 mb-3">
															<label for="validationCustom01">Branch</label>
															<input type="text" name="m_name" class="form-control" id="validationCustom01" placeholder="Branch Name.."  required="">
															<div class="valid-feedback">
																Looks good!
															</div>
														</div>
													 </div>
													  <div class="form-row">
														<div class="col-md-6 mb-3">
															<label for="validationCustom01">UPI Id</label>
															<input type="text" name="f_name" class="form-control" id="validationCustom01" placeholder="UPI Id.."  required="">
															<div class="valid-feedback">
																Looks good!
															</div>
														</div>
														<div class="col-md-6 mb-3">
															<label for="validationCustom01">Phone Pay Number</label>
															<input type="text" name="m_name" class="form-control" id="validationCustom01" placeholder="Phone Pay Number.."  required="">
															<div class="valid-feedback">
																Looks good!
															</div>
														</div>
													 </div>
													  <div class="form-row">
														<div class="col-md-6 mb-3">
															<label for="validationCustom01">Google Pay Number</label>
															<input type="text" name="f_name" class="form-control" id="validationCustom01" placeholder="Google Pay Number.."  required="">
															<div class="valid-feedback">
																Looks good!
															</div>
														</div>
														<div class="col-md-6 mb-3">
															<label for="validationCustom01">Paytm Number</label>
															<input type="text" name="m_name" class="form-control" id="validationCustom01" placeholder="Paytm Number.."  required="">
															<div class="valid-feedback">
																Looks good!
															</div>
														</div>
													 </div>
													<button class="btn btn-primary" type="submit">Submit form</button>
												</form>
											</div>
										</div>
									</div>
								  
								  </div>
											
									  
									</div>
                                </div>
                            </div>
                        </div>
						
						
                       
                       
                    </div>
                </div>
                <!-- ============================================================== -->
                <!-- End PAge Content -->
                <!-- ============================================================== -->
                <!-- ============================================================== -->
                <!-- Right sidebar -->
                <!-- ============================================================== -->
                <!-- .right-sidebar -->
             
            </div>
			
			
		      <div class="modal fade" id="exampleModalCenter">
                        <div class="modal-dialog modal-dialog-centered" role="document">
                            <div class="modal-content">
                                <div class="modal-header">
                                    <h5 class="modal-title">Add New Family Memeber</h5>
                                    <button type="button" class="close" data-dismiss="modal"><span>&times;</span></button>
                                </div>
                                <div class="modal-body">
                                                    <form class="needs-validation" novalidate="">
													<div class="form-row">
														<div class="col-md-4 mb-3">
															<label for="validationCustom01"> First Name</label>
															<input type="text" name="f_name" class="form-control" id="validationCustom01" placeholder="First name.."  required="">
															<div class="valid-feedback">
																Looks good!
															</div>
														</div>
														<div class="col-md-4 mb-3">
															<label for="validationCustom01">Middle Name</label>
															<input type="text" name="m_name" class="form-control" id="validationCustom01" placeholder="Middle Name.."  required="">
															<div class="valid-feedback">
																Looks good!
															</div>
														</div>
														<div class="col-md-4 mb-3">
															<label for="validationCustom01">Last Name</label>
															<input type="text" name="m_name" class="form-control" id="validationCustom01" placeholder="Last Name.."  required="">
															<div class="valid-feedback">
																Looks good!
															</div>
														</div>
													 </div>
													 <div class="form-row">
														<div class="col-md-6 mb-3">
															<label for="validationCustom01">Mobile Number</label>
															<input type="number" name="f_name" class="form-control" id="validationCustom01" placeholder="Mobile Number.."  required="">
															<div class="valid-feedback">
																Looks good!
															</div>
														</div>
														<div class="col-md-6 mb-3">
															<label for="validationCustom01">Age</label>
															<input type="number" name="m_name" class="form-control" id="validationCustom01" placeholder="Age.."  required="">
															<div class="valid-feedback">
																Looks good!
															</div>
														</div>
													</div>
													 
													<button class="btn btn-primary" type="submit">Submit form</button>
													
												</form>
											</div>
                               
                            </div>
                        </div>
                    </div>
            <!-- ============================================================== -->
            <!-- End Container fluid  -->
            <!-- ============================================================== -->
 
 </div>
       
@endsection
@section('js')
    <script>
        $(function () {
            $('#myTable').DataTable();
            var table = $('#example').DataTable({
                "columnDefs": [{
                    "visible": false,
                    "targets": 2
                }],
                "order": [
                    [2, 'asc']
                ],
                "displayLength": 25,
                "drawCallback": function (settings) {
                    var api = this.api();
                    var rows = api.rows({
                        page: 'current'
                    }).nodes();
                    var last = null;
                    api.column(2, {
                        page: 'current'
                    }).data().each(function (group, i) {
                        if (last !== group) {
                            $(rows).eq(i).before('<tr class="group"><td colspan="5">' + group + '</td></tr>');
                            last = group;
                        }
                    });
                }
            });
            // Order by the grouping
            $('#example tbody').on('click', 'tr.group', function () {
                var currentOrder = table.order()[0];
                if (currentOrder[0] === 2 && currentOrder[1] === 'asc') {
                    table.order([2, 'desc']).draw();
                } else {
                    table.order([2, 'asc']).draw();
                }
            });
            // responsive table
            $('#config-table').DataTable({
                responsive: true
            });
            $('#example23').DataTable({
                dom: 'Bfrtip',
                buttons: [
                    'copy', 'csv', 'excel', 'pdf', 'print'
                ]
            });
            $('.buttons-copy, .buttons-csv, .buttons-print, .buttons-pdf, .buttons-excel').addClass('btn btn-primary mr-1');
        });

    </script>
@endsection
