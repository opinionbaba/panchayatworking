@extends('layouts.app')


{{-- Page Title --}}
@section('page-title')
   Contact
@endsection

{{-- This Page Css --}}
@section('css')
  <link rel="stylesheet" type="text/css"
        href="{{asset('public/assets/datatables.net-bs4/css/dataTables.bootstrap4.css')}}">
    <link rel="stylesheet" type="text/css"
        href="{{asset('public/assets/datatables.net-bs4/css/responsive.dataTables.min.css')}}">
    <!--=========================*
               Datatable
  
  *===========================-->
  <body class="skin-blue fixed-layout">
    <!-- ============================================================== -->
    <!-- Preloader - style you can find in spinners.css -->
    <!-- ============================================================== -->
    <div class="preloader">
        <div class="loader">
            <div class="loader__figure"></div>
        </div>
    </div>
	 <div id="main-wrapper">
	  
@endsection

@section('main-content')
<div class="page-wrapper">
            <div class="container-fluid">
                <!-- ============================================================== -->
                <!-- Bread crumb and right sidebar toggle -->
                <!-- ============================================================== -->
                <div class="row page-titles">
                    <div class="col-md-5 align-self-center">
                        <h4 class="text-themecolor">Datatable</h4>
                    </div>
                    <div class="col-md-7 align-self-center text-right">
                        <div class="d-flex justify-content-end align-items-center">
                            <ol class="breadcrumb">
                                <li class="breadcrumb-item"><a href="javascript:void(0)">Home</a></li>
                                <li class="breadcrumb-item active">Datatable</li>
                            </ol>
                           		
                        </div>
                    </div>
                </div>
                <!-- ============================================================== -->
                <!-- End Bread crumb and right sidebar toggle -->
                <!-- ============================================================== -->
                <!-- ============================================================== -->
                <!-- Start Page Content -->
                <!-- ============================================================== -->
                <div class="row">
                    <div class="col-12">
					@if(Session::has('delete'))
        <div class="alert alert-danger alert-dismissible fade show" role="alert">
			<strong></strong>  {{Session::get('delete')}}
			<button type="button" class="close" data-dismiss="alert" aria-label="Close"><span class="ti-close"></span>
			</button>
		</div>
		@endif
		
					    <div class="card">
                            <div class="card-body">
                                <h4 class="card-title">Contact</h4>
                                <div class="button-box">
                                    <a class="tst1 btn btn-info" href="#">Homee</a>
                                    <a class="tst1 btn btn-info" href="{{route('admin.allnotice')}}">Notice</a>
                                    <a class="tst1 btn btn-info" href="{{route('admin.allevent')}}">Events</a>
                                    <a class="tst1 btn btn-info" href="{{route('admin.allgallery')}}">Gallery</a>
                                    <a class="tst1 btn btn-info" href="{{route('admin.alldoc')}}">Form</a>
                                    <a class="tst1 btn btn-info" href="{{route('admin.contact')}}">Contact Us</a>
                                    <a class="tst1 btn btn-info" href="{{route('admin.subs')}}">Subscribers</a>
                                    <a class="tst1 btn btn-info" href="#">House Tax</a>
                                    <a class="tst1 btn btn-info" href="{{route('admin.allaudit')}}">Audit</a>
                                   </div>
								
                                <div class="table-responsive m-t-40">
                                    <table id="example23"
                                        class="display nowrap table table-hover table-striped table-bordered"
                                        cellspacing="0" width="100%">
                                        <thead>
                                            <tr>
												<th>Sr No</th>
												<th>Name</th>
												<th>Mobile</th>
												<th>Email</th>
												<th>Subject</th>
												<th>Message</th>
												<th>Date</th>
												<th>Action</th>
											</tr>
											</thead>
											<tbody>
											@php 
											$num =1;
											@endphp
											@foreach($contact as $value)
											<tr>
												<td>{{$num}}</td>
												<td>{{$value->name}}</td>
												<td>{{$value->mobile_no}}</td>
												<td>{{$value->email}}</td>
												<td>{{$value->subject}}</td>
												<td>{{$value->created_at}}</td>
												<td>{{$value->message}}</td>
												<td> 
												  <a href = "{{route('contact-delete',$value->id)}}" class='forum-title'><i class="ti-trash btn btn-danger"></i></a>
												</td>
											</tr>
											@php
											$num++;
											@endphp
											@endforeach
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
					</div>
				</div>
				
				
		    </div>
		</div>
@endsection

@section('js')
       <script>
        $(function () {
           
            $('#example23').DataTable({
                dom: 'Bfrtip',
                buttons: [
                    'copy', 'csv', 'excel', 'pdf', 'print'
                ]
            });
            $('.buttons-copy, .buttons-csv, .buttons-print, .buttons-pdf, .buttons-excel').addClass('btn btn-primary mr-1');
        });

    </script>
@endsection
