<html>

<style>
@import "compass/css3";

@import url(https://fonts.googleapis.com/css?family=Raleway:300,400);
@import url(https://maxcdn.bootstrapcdn.com/font-awesome/4.1.0/css/font-awesome.min.css);
*, *:after,*:before {
  box-sizing: border-box;
  margin: 0;
  padding: 0;
 }
html,body {
  width: 100%;
  height: 100%;
  background: #333;
  overflow: hidden;
  -ms-text-size-adjust: 100%;
  -webkit-text-size-adjust: 100%;
  -moz-text-size-adjust: auto;
  -moz-text-size-adjust: 100%;
  text-rendering: optimizelegibility;
  color: whitesmoke;
  font: 400 normal 16px/20px 'Raleway', 'Quicksand', 'Open Sans', 'HelveticaNeue-Light', 'Helvetica Neue Light', 'Helvetica Neue', 'Helvetica', 'Arial', 'Lucida Grande', sans-serif;
}
// Z-INDEXs:
// 100 - main
// 200 - windows
// 300 - header
// 400 - screensaver

footer, header{
  position: fixed;
  top: 0;
  left: 0;
  z-index: 300;
  width: 100%;
  height: 30px;
  line-height: 2em;
  box-shadow: 0 0 15px 2px rgba(0, 0, 0, 0.3);
  color: #fff;
  border-bottom: 1px solid #22c9e8;
  white-space: nowrap;
  background:rgba(47, 53, 56,0.95);
  border-bottom: 1px solid #709AD1;
  *{margin:0 0.3rem;}
}
.bar{
  padding:0 0.5rem;
  margin:0;
  cursor: default;
  white-space:nowrap;  
}.fa-home{
	margin-left:1rem;
}
main{
  position: fixed;
  top: 0;
  left: 0;
  z-index: 100;
  width: 100%;
  height: 100%;
  padding-top: 30px;
  background: linear-gradient(to bottom,rgba(0, 0, 90,0.4), #10d787);
}
.window {
  position: absolute;
  z-index: 200;
  max-width: calc(80px*4 +2rem);
  min-height: 150px;
  background: rgba(25, 25, 25, 0.95);
  color: whitesmoke;
  font-size: 14px;
  border-radius:5px;
  resize: both;
  overflow: auto;
  .header {
    padding: 0.5rem;
    cursor: default;
    background: rgba(47, 53, 56,0.95);
    border-bottom:1px solid #22c9e8;
    h1 {
      text-align: center;
      font-size: 1.2rem;
      font-weight: 400;
    }
    > .close{
      right: 1rem;
      color: rgba(255, 0, 0,0.6);
    }
    > .maximise{
      right: 2.5rem;
      color: rgba(0, 255,50,0.6);
    }
    >.minimise{
      right: 4.0rem;
      color: rgba(255, 80, 0, 0.6);
    }
    >.forward{
      left: 2.5rem;
    }
    >.back{
      left: 1rem;
    }
    >.forward,>.back,>.close,
    >.maximise,>.minimise{
      position:absolute;
      font-size: 18px;
      top: 0.5rem;
      cursor:pointer;
    }
  }
  .sub-header {
    background: rgba(255, 255, 255, 0.1);
    padding: 0.5rem;
  }
  .content{
    max-width: 100%;
    max-height: 100%;
    overflow: auto;
    word-wrap: break-word;
    white-space: wrap;
    resize: none;   
    .file-item {
      display: inline-block;
      max-width: 80px;
      min-width: 80px;
      max-height: 100px;
      min-height: 100px;
      margin: 1srem;
      cursor: pointer;
      resize: none;
      
      &:hover {
        background: rgba(255, 255, 255, 0.1);
        border-radius:5px;
      }
      > .icon {
        width: 80px;
        height: 80px;
        text-align: center;
        
        > * {
          transform: translateY(20%);
        }
      }
      > .text {
        width: 80px;
        overflow: hidden;
        text-align: center;
      }
      /*
      &.folder {
        > .icon {
          background: linear-gradient(transparentize(#22c9e8, 0.3), transparentize(#1455db, 0.3));
          border: 1px solid whitesmoke;
          border-radius: 3px;
        }
      }
      */
    }
  }
}
.apple{
  display:none;
}
.project-pilow{
  display: block;
  position: absolute;
  top:30px;
  left:1rem;
  margin: 0;
  background:rgba(25, 25, 25, 0.9);
  cursor: default;
  li{
    padding: 2px 20px;
    &:hover{
      background: lighten(rgba(25, 25, 25, 0.8), 10%);
      color: white;
      }}
    }
    .pillow{
      display: none;
    }
    .big{
      width: 100%;
      height: 95%;
      top: 20px;
      left: 0;
    }
footer{
  top: auto;
  bottom: 0;
  left: 0;
  height:50px;
  line-height:50px;
  z-index:9999;
  line-height: 50px;
  span:first-child{margin-left:1rem;}
  span{
    padding: 2px 15px;
    font-size: 30px;
    margin:0;
    text-outline: 1px 1px #000;
    &:hover{
    cursor: default;
    color: #22c9e8;
    }
  }
}
.screensaver {
  position: fixed;
  top: 0;
  left: 0;
  z-index: 800;
  width: 100%;
  min-height:600px;  
  height: 100%;
  background:
    linear-gradient(to right bottom, #543A69, transparent),
    linear-gradient(to left bottom, #7A8E97, transparent),
    linear-gradient(to left top, #6093A8, transparent),
    linear-gradient(to right top, #BC8394, transparent) whitesmoke;
  text-align: center;
  color: whitesmoke;
  font-weight: 300;
  z-index:99999;
  > .content {
    position: absolute;
    top: 20%;
    left: 50%;
    transform: translate(-50%, -50%);
    time {
      display: block;
      font-size: 5rem;
      margin-bottom:10px;
      line-height: 1.25 * 1.5rem;
      &:last-of-type {
        font-size: 1.5rem;
        line-height: 1.25 * 5rem;
      }
    }
  }
}


/*-----------------------------------------------------------------------------------*/
/*	01.	Dock
/*-----------------------------------------------------------------------------------*/

.dock {
	position: absolute;
	bottom: 0;
	z-index: 9999;
	width: 100%;
	text-align: center;
}
.dock ul {
	position: relative;
	display: inline-block;
	padding: 0 5px;
	margin: 0;
	background:url(../img/dock-bg.png) repeat-x bottom;
}
.dock ul:before, .dock ul:after {
	content: "";
	position: absolute;
	top: 0;
	bottom: 0;
	width: 38px;
}
.dock ul:before {
	left: -38px;
	background: url(../img/dock-bg-left.png) no-repeat left bottom;
}
.dock ul:after {
	right: -38px;
	background: url(../img/dock-bg-right.png) no-repeat right bottom;
}
.dock li {
	display: inline-block;
	position: relative;
	margin: 0 0 15px 0;
	-webkit-box-reflect: below -16px -webkit-gradient(linear, left top, left bottom, from(transparent), color-stop(91%, rgba(255, 255, 255, .1)), color-stop(91.01%, transparent), to(transparent));
}
.dock a {
	display: block;
	cursor: default;
	outline: none;
}
.dock em {
	position: absolute;
	top: -34px;
	left: 50%;
	display: none;
	width: 150px;
	margin-left: -75px;
	text-align: center;
}
.dock em:after {
	content: " ";
	position: absolute;
	bottom: -6px;
	left: 50%;
	margin-left: -6px;
	width: 0;
	height: 0;
	border-left: 6px solid transparent;
	border-right: 6px solid transparent;
	border-top: 6px solid rgba(0, 0, 0, 0.6);
	border-bottom: none;
}
.dock em span {
	display: inline-block;
	padding: 5px 12px;
	background: rgba(0, 0, 0, 0.6);
	font-style:normal;
	font-size:12px;
	color:#fff;
	text-shadow:0 1px 1px #000;
	border-radius: 10px;
}
.dock li:hover em {
	display: block;
}
.dock img {
	width: 48px;
	height: auto;
	border: none;
	-webkit-transition:all 0.1s linear;
	-moz-transition:all 0.1s linear;
}
.dock li.ok:hover img {
	width: 128px;
	-webkit-transition:all 0.15s linear;
	-moz-transition:all 0.15s linear;
}
.dock li.prev img, .dock li.next img {
	width:96px;
	-webkit-transition:all 0.15s linear;
	-moz-transition:all 0.15s linear;
}
.dock li.prev-ancor img, .dock li.next-ancor img {
	width:72px;
	-webkit-transition:all 0.15s linear;
	-moz-transition:all 0.15s linear;
}
.dock li a .fresh {
	content: " ";
	position: absolute;
	bottom: -8px;
	left: 50%;
	width: 8px;
	height: 6px;
	opacity:1;
	background-color: rgba(255, 255, 255, .8);
	margin-left: -2px;
	border-radius:100%;
	box-shadow: inset 0 1px 3px rgba(75, 255, 255, .4), 0 0 4px rgba(75, 255, 255, .5), 0 -1px 7px rgb(75, 255, 255);
	-webkit-box-reflect: below 5px -webkit-gradient(linear, left top, left bottom, from(transparent), color-stop(91%, rgba(255, 255, 255, 0.5)), color-stop(91.01%, transparent), to(transparent));
}
.dock ul li{
  transition: all .3s ease-in-out; }
.dock ul li:hover{
  transform: scale(2);
  z-index:999;
}
.bounce {
	-webkit-animation: bounce 0.4s 4 alternate ease-out;
	-moz-animation: bounce 0.4s 4 alternate ease-out;
}
@-webkit-keyframes bounce {
	  0% { -webkit-transform: translateY(0); }
	100% { -webkit-transform: translateY(-20px); }
}
@-moz-keyframes bounce {
	  0% { -moz-transform: translateY(0); }
	100% { -moz-transform: translateY(-20px); }
}
#hint{
  display:none;
  position:absolute;
  top:0%;
  left:0%;
  z-index:99999;
  width:310px;
  border-radius:5px;
  padding:50px;
  height:310px;
  color:#EEE;
  background: linear-gradient(to right bottom, rgba(84,58,105,0.9), transparent),
    linear-gradient(to left bottom, rgba(122,142,151,0.9), transparent),
    linear-gradient(to right top, rgba(188,131,148,0.9) transparent),
    linear-gradient(to left top, rgba(96,147,168,0.9), transparent);/*rgba(84,58,105,0.95);*/
  text-shadow: 0 0 3px #FF0000;
  >b{
    font-weight:bold;
    color:black;
  }
}
#close{
  display:none;
  color:red;
  z-index: 999991;
  font-size:22px;
  position:absolute;
  top:0;right:0;
  margin:10px;
  cursor:pointer;
  &:hover{
    text-shadow: 0 0 3px #FFF;
    transform: scale(1.01);
  }
}
</style>
<body>
<header>
  <ul class="apple">
    <li>Home</li>
    <li class="pro-pillow">Project Pillow</li>
    <li>Files</li>
    <li>Preference</li>
    <li class="logoff">Log Off</li>
  </ul>
  <span id="apple" class="bar fa fa-home"></span>
  <span class="bar" style="font-weight:800;">Finder</span>
  <span class="bar">File</span>
  <span class="bar">Edit</span>
  <span class="bar">View</span>
  <span class="bar">Go</span>
  <span class="bar">Windows</span>
  <span class="bar">Help</span>

  <span style="float: right;line-height: 1.9em;" class="bar fa fa-th-list"></span>
  <time style="float: right" class="bar time">time</time>
  <date style="float: right" class="bar date">date</date>
  <volume style="float: right;line-height: 1.9em;" class="bar fa fa-volume-up"></volume>
  <undo style="float: right;line-height: 1.9em;" class="bar fa fa-undo"></undo>
</header>
<main>
  <div class="window">
    <div class="header">
      <h1>Home</h1>
      <div class="back">
        <i class="fa fa-arrow-circle-left"></i>
      </div>
      <div class="forward">
        <i class="fa fa-arrow-circle-right"></i>
      </div>
      <div class="minimise">
        <i class="fa fa-minus-circle"></i>
      </div>
      <div class="maximise">
        <i class="fa fa-plus-circle"></i>
      </div>
      <div class="close">
        <i class="fa fa-times-circle"></i>
      </div>
    </div>
    <div class="content">
      <div class="file-item">
        <div class="icon"><i class="fa fa-4x fa-file-pdf-o"></i></div>
        <div class="text">specs.pdf</div>
      </div>
      <div class="file-item">
        <div class="icon"><i class="fa fa-4x fa-file-excel-o"></i></div>
        <div class="text">plan.xls</div>
      </div>
      <div class="file-item">
        <div class="icon"><i class="fa fa-4x fa-file-archive-o"></i></div>
        <div class="text">stuff.zip</div>
      </div>
      <div class="file-item">
        <div class="icon"><i class="fa fa-4x fa-file-code-o"></i></div>
        <div class="text">project.js</div>
      </div>
      <div class="file-item folder">
        <div class="icon"><i class="fa fa-4x fa-file"></i></div>
        <div class="text">Documents</div>
      </div>
      <div class="file-item folder">
        <div class="icon"><i class="fa fa-4x fa-music"></i>
        </div>
        <div class="text">Music</div>
      </div>
      <div class="file-item folder">
        <div class="icon"><i class="fa fa-4x fa-camera"></i>
        </div>
        <div class="text">Pictures</div>
      </div>
      <div class="file-item folder">
        <div class="icon"><i class="fa fa-4x fa-video-camera"></i>
        </div>
        <div class="text">Videos</div>
      </div>
    </div>
  </div>
  <div class="pillow window">
    <div class="header">
      <h1>Project Pillow</h1>
      <div class="minimise">
        <i class="fa fa-minus-circle"></i>
      </div>
      <div class="maximise">
        <i class="fa fa-plus-circle"></i>
      </div>
      <div class="close"><i class="fa fa-times-circle"></i></div>
    </div>
    <div class="content">
      <div class="file-item">
        <div class="icon"><i class="fa fa-4x fa-file-pdf-o"></i></div>
        <div class="text">specs.pdf</div>
      </div>
      <div class="file-item">
        <div class="icon"><i class="fa fa-4x fa-file-excel-o"></i></div>
        <div class="text">plan.xls</div>
      </div>
      <div class="file-item">
        <div class="icon"><i class="fa fa-4x fa-file-archive-o"></i></div>
        <div class="text">stuff.zip</div>
      </div>
      <div class="file-item">
        <div class="icon"><i class="fa fa-4x fa-file-code-o"></i></div>
        <div class="text">project.js</div>
      </div>
    </div>
    <!--Content-->
  </div>
</main><!--
<div class="screensaver">
  <div class="content">
    <time class="time">time</time>
    <time class="date">date</time>
    <div>
    </div>
  </div>
  <!--Content--><!--
  <form class="login">
    <img id="img" src="http://www.startinparis.com/wp-content/uploads/2012/05/03-Mention-Icon-OSX-512-300x300.png" />
    <h1 style="font-weight:bold; font-size:28px;margin-bottom:10px;">Login</h1>
    <input id="user" type="text" name="u" placeholder="Username" required="required" />
    <input id="pass" type="password" name="p" placeholder="Password" required="required" />
    <button type="submit" class="btn btn-primary btn-block btn-large">Let me in</button>
    <p id="hint">
      
      Please close this dialogue and use <b>islamike</b> as your username and password. Thanks
      <p id="close">x</p>
    </p>
  </form>
</div>-->
<div class="dock">
  <ul>
    <li id="finderr">
      <a href="#warning" data-rel="showOp">
        <em><span>Finder</span></em>
        <img src="http://www.alessioatzeni.com/mac-osx-lion-css3/res/img/FinderIcon.png" alt="Finder" />
      </a>
    </li>
    <li id="launchPad">
      <a href="#warning" data-rel="showOp">
        <em><span>Launchpad</span></em>
        <img src="http://www.alessioatzeni.com/mac-osx-lion-css3/res/img/launchPad.png" alt="Launchpad" />
      </a>
    </li>
    <li id="expose">
      <a href="#warning" data-rel="showOp">
        <em><span>Mission Control</span></em>
        <img src="http://www.alessioatzeni.com/mac-osx-lion-css3/res/img/expose.png" alt="Mission Control" />
      </a>
    </li>
    <li id="appStore">
      <a href="#warning" data-rel="showOp">
        <em><span>App Store</span></em>
        <img src="http://www.alessioatzeni.com/mac-osx-lion-css3/res/img/appstore.png" alt="App Store" />
      </a>
    </li>
    <li id="safari">
      <a href="#warning" data-rel="showOp">
        <em><span>Safari</span></em>
        <img src="http://www.alessioatzeni.com/mac-osx-lion-css3/res/img/Safari.png" alt="Safari" />
      </a>
    </li>
    <li id="iChat">
      <a href="#warning" data-rel="showOp">
        <em><span>iChat</span></em>
        <img src="http://www.alessioatzeni.com/mac-osx-lion-css3/res/img/ichat.png" alt="iChat" />
      </a>
    </li>
    <li id="facetime">
      <a href="#warning" data-rel="showOp">
        <em><span>FaceTime</span></em>
        <img src="http://www.alessioatzeni.com/mac-osx-lion-css3/res/img/facetime.png" alt="Facetime" />
      </a>
    </li>
    <li id="addressBook">
      <a href="#warning" data-rel="showOp">
        <em><span>Address Book</span></em>
        <img src="http://www.alessioatzeni.com/mac-osx-lion-css3/res/img/address.png" alt="Address Book" />
      </a>
    </li>
    <li id="preview">
      <a href="#warning" data-rel="showOp">
        <em><span>Preview</span></em>
        <img src="http://www.alessioatzeni.com/mac-osx-lion-css3/res/img/preview.png" alt="Preview" />
      </a>
    </li>
    <li id="iTunes">
      <a href="#warning" data-rel="showOp">
        <em><span>iTunes</span></em>
        <img src="http://www.alessioatzeni.com/mac-osx-lion-css3/res/img/iTunes.png" alt="iTunes" />
      </a>
    </li>
    <li id="preferences">
      <a href="#warning" data-rel="showOp">
        <em><span>System Preferences</span></em>
        <img src="http://www.alessioatzeni.com/mac-osx-lion-css3/res/img/preferences.png" alt="System Preferences" />
      </a>
    </li>
    <li id="trash">
      <a href="#trash" data-rel="showOpTrash">
        <em><span>Trash</span></em>
        <img src="http://www.alessioatzeni.com/mac-osx-lion-css3/res/img/trash.png" alt="Trash" />
      </a>
    </li>
  </ul>
</div>
<script>
//Get the time and date
function setTime(ts) {
  var date = new Date(ts);
  $('.time').text(date.toLocaleString(navigator.language, {
    hour: '2-digit',
    minute: '2-digit'
  })).attr('datetime', date.getHours() + ':' + date.getMinutes());
  $('.date').text(date.toLocaleString(navigator.language, {
    year: 'numeric',
    month: 'long',
    day: 'numeric'
  })).attr('datetime', date.getFullYear() + '-' + (date.getMonth() + 1) + '-' + date.getDate());
  setTimeout(function() {
    setTime(Date.now());
  }, 1000);
}
setTime(Date.now());

//Set the date and time on Screensaver
$('.btn-primary').on('click', function(e) {
  e.preventDefault();
  if (($('#user').val() == 'islamike') && ($('#pass').val() == 'islamike')) {
    $('.screensaver').fadeOut(500);
  } else {
    $('#hint').show();
    $('#close').show();
  }
  $('#close').on('click', function() {
    $('#hint').hide();
    $('#close').hide();
  });
});

$(".logoff").on('click', function(e) {
  e.preventDefault();
  $('#user').val('');
  $('#pass').val('');
  $('.screensaver').fadeIn(500);
});
//Make the Window inside the app moveable
$('.window').draggable({
  handle: '.header'
});

//When user clicks on the Red Cross, close the Window and reset it
$('.close').on('click', function() {
  $(this).parent().parent().hide();
  $(this).parent().parent().removeClass('big');
});

//When on Window clicked, bring it to the Front
$('.window').mousedown(function() {
  $(".window").css("z-index", "200");
  $(this).css("z-index", "300");
});

//When the green plus button clicked, make the window fullscreen
$('.maximise').click(function() {
  $(this).parent().parent().toggleClass('big');
});

//When the orange minus button click, minimise the window
$('.minimise').click(function() {
  $(this).parent().parent().css("visibility", "hidden");
});

//When the home icon is clicked, show dropdown menu
$('#apple').click(function() {
  $('.apple').toggleClass('project-pilow');
});
//From the dropdown menu if Project Pillow is click, show it.
$('.pro-pillow').click(function() {
  $('.apple').toggleClass('project-pilow');
  $('.pillow').show();
  $('.pillow').css('visibility', 'visible');
  $(".window").css("z-index", "200");
  $(this).css("z-index", "300");
});
$('#finderr').on('click', function() {
  $('.pillow').show();
  $('.pillow').css('visibility', 'visible');
  $(".window").css("z-index", "200");
  $(this).css("z-index", "300");
});

if ($('.window').css('visibility') == 'hidden') {
  $('.file-manager').removeClass('fa-folder').addClass('fa-folder-open');
} else {
  $('.file-manager').click(function() {
    if ($('.window').css('visibility') == 'hidden') {
      $('.window').css('visibility', 'visible');
      $(this).removeClass('fa-folder-open').addClass('fa-folder');
    } else {
      $('.window').css('visibility', 'hidden');
      $(this).removeClass('fa-folder').addClass('fa-folder-open');
    }
  });
}


$('.dock ul li').on('mouseover',function(){
  $(this).prev().prev().css('transform: scale(1.3)');
  $(this).prev('li').css('transform','scale(1.5)');
  
  $(this).css('transform','scale(2)');
  
  $(this).next('li').css('transform', 'scale(1.5)');
  $(this).next('li').next('li').css('transform', 'scale(1.3)');
});
$('dock ul li').mouseout(function(){
  $(this).css('transform', 'scale(1.0)');
  $(this).prev('li').css('transform', 'scale(1.0)');
  $(this).prev('li').prev('li').css('transform', 'scale(1.0)');
  
  $(this).css('transform', 'scale(1.0)');
  
  $(this).next('li').next('li').css('transform', 'scale(1.0)');
  $(this).next('li').css('transform', 'scale(1.0)');
});
</script>
</body>
</html>