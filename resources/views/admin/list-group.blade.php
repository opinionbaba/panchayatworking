@extends('layouts.app')


{{-- Page Title --}}
@section('page-title')
    List Group
@endsection

{{-- This Page Css --}}
@section('css')
    
    <link rel="stylesheet" href="{{asset('assets/vendors/ladda-button/css/ladda-themeless.min.css')}}">
	<body class="skin-blue fixed-layout">
    <div class="preloader">
        <div class="loader">
            <div class="loader__figure"></div>
            <p class="loader__label">Elite admin</p>
        </div>
    </div>
    <div id="main-wrapper">  
@endsection

@section('main-content')
<div class="page-wrapper">
            <!-- ============================================================== -->
            <!-- Container fluid  -->
            <!-- ============================================================== -->
            <div class="container-fluid">
                <!-- ============================================================== -->
                <!-- Bread crumb and right sidebar toggle -->
                <!-- ============================================================== -->
                <div class="row page-titles">
                    <div class="col-md-5 align-self-center">
                        <h4 class="text-themecolor">Notice List</h4>
                    </div>
                    <div class="col-md-7 align-self-center text-right">
                        <div class="d-flex justify-content-end align-items-center">
                            <ol class="breadcrumb">
                                <li class="breadcrumb-item"><a href="javascript:void(0)">Home</a></li>
                                <li class="breadcrumb-item active">Notice List</li>
                            </ol>
                            <button type="button" class="btn btn-info d-none d-lg-block m-l-15"><i class="fa fa-plus-circle"></i> Create New</button>
                        </div>
                    </div>
                </div>
				
				<div class="card-group">
                    <div class="card">
                        <div class="card-body">
                            <div class="row">
                                <div class="col-md-12">
								   <h4 class="card_title">All Notice</h4>
                   
                                    <div class="button-box" style="margin-bottom:40px">
                                        <a class="tst1 btn btn-info" href="home">Home</a>
                                        <a class="tst1 btn btn-info" href="{{route('admin.allnotice')}}">Notice</a>
                                        <a class="tst1 btn btn-info" href="{{route('admin.allevent')}}">Events</a>
                                        <a class="tst1 btn btn-info" href="{{route('admin.allgallery')}}">Gallery</a>
                                        <a class="tst1 btn btn-info" href="{{route('admin.alldoc')}}">Form</a>
                                        <a class="tst1 btn btn-info" href="{{route('admin.contact')}}">Contact Us</a>
                                        <a class="tst1 btn btn-info" href="{{route('admin.subs')}}">Subscribers</a>
                                        <a class="tst1 btn btn-info" href="#">House Tax</a>
                                        <a class="tst1 btn btn-info" href="{{route('admin.allaudit')}}">Audit</a>
                                    
                                   </div>
								     <ul class="list-group">
									@foreach($event as $value)
										<li class="list-group-item d-flex justify-content-between align-items-center">
												@if($value->event_cat == '1')
													Event
												@endif
												@if($value->event_cat == '2')
													Culture
												@else
													Resource
												@endif
											<a href="full_event/{{$value->event_cat}}"><span class="badge badge-primary ">Click Here!</span></a>
										</li>
									  @endforeach
									</ul>
                                </div>	
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
@endsection
