<!doctype html>
<html lang="zxx">
<head>
    
    <meta charset="UTF-8">
    <meta http-equiv="x-ua-compatible" content="ie=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="Gelr Bootstrap 4 Admin Template">
    <title>Login</title>
	<link href="{{asset('public/assets/morrisjs/morris.css')}}" rel="stylesheet">
    <link href="{{asset('public/assets/toast-master/css/jquery.toast.css')}}" rel="stylesheet">
    <link href="{{asset('public/assets/dist/css/style.min.css')}}" rel="stylesheet">
	<link href="{{asset('public/assets/dist/css/pages/login-register-lock.css')}}" rel="stylesheet">
    <link href="{{asset('public/assets/dist/css/style.min.css')}}" rel="stylesheet">
    <link rel="shortcut icon" type="image/x-icon" href="{{asset('public/assets/images/favicon.png')}}">
</head>
<body class="skin-default card-no-border">
    <div class="preloader">
        <div class="loader">
            <div class="loader__figure"></div>
            <p class="loader__label">Elite admin</p>
        </div>
    </div>
    <section id="wrapper">
        <div class="login-register" style="background-image:url({{asset('public/assets/images/background/login-register.jpg')}};">
            <div class="login-box card">
                <div class="card-body">
                    <form class="form-horizontal form-material"  method="post" action="{{route('superadmin-login')}}">
					  @csrf
                        <h3 class="text-center m-b-20">Sign In</h3>
                        <div class="form-group ">
                            <div class="col-xs-12">
                                <input class="form-control" type="text" required="" name="email" placeholder="Enter the email"  id="exampleInputEmail1">
							</div>
                        </div>
                        <div class="form-group">
                            <div class="col-xs-12">
                                <input class="form-control" type="password" required="" placeholder="Password"  name="password" id="exampleInputPassword1"> </div>
                        </div>
                        <div class="form-group row">
                            <div class="col-md-12">
                                <div class="d-flex no-block align-items-center">
                                    <div class="custom-control custom-checkbox">
                                        <input type="checkbox" class="custom-control-input" id="customControlAutosizing">
                                        <label class="custom-control-label" for="customControlAutosizing">Remember me</label>
                                    </div> 
                                    <div class="ml-auto">
                                        <a href="#" id="to-recover" class="text-muted"><i class="fas fa-lock m-r-5"></i> Forgot pwd?</a> 
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="form-group text-center">
                            <div class="col-xs-12 p-b-20">
                                <button class="btn btn-block btn-lg btn-info btn-rounded" id="form_submit" type="submit">Log In</button>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-xs-12 col-sm-12 col-md-12 m-t-10 text-center">
                                <div class="social">
                                    <button class="btn  btn-facebook" data-toggle="tooltip" title="Login with Facebook"> <i aria-hidden="true" class="fab fa-facebook-f"></i> </button>
                                    <button class="btn btn-googleplus" data-toggle="tooltip" title="Login with Google"> <i aria-hidden="true" class="fab fa-google-plus-g"></i> </button>
                                </div>
                            </div>
                        </div>
                        <div class="form-group m-b-0">
                            <div class="col-sm-12 text-center">
                                Don't have an account? <a href="pages-register.html" class="text-info m-l-5"><b>Sign Up</b></a>
                            </div>
                        </div>
                    </form>
                  
                </div>
            </div>
        </div>
    </section>
    
	
	



<!--=========================*
            Scripts
*===========================-->

<!-- Jquery Js -->
<script src="{{asset('public/assets/jquery/jquery-3.2.1.min.js')}}"></script>
    <script src="{{asset('public/assets/popper/popper.min.js')}}../assets/node_modules/"></script>
    <script src="{{asset('public/assets/bootstrap/dist/js/bootstrap.min.js')}}"></script>
    <script src="{{asset('public/assets/dist/js/custom.min.js')}}"></script>
    <script src="{{asset('public/assets/dist/js/dashboard1.js')}}"></script>

<script>
    jQuery(document).ready(function ($) {
        $('.form-gp input').on('focus', function() {
            $(this).parent('.form-gp').addClass('focused');
        });
        $('.form-gp input').on('focusout', function() {
            if ($(this).val().length === 0) {
                $(this).parent('.form-gp').removeClass('focused');
            }
        });
    });
</script>

</body>
</html>
