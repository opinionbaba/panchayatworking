@extends('layouts.app')


{{-- Page Title --}}
@section('page-title')
    Form Validation
@endsection
@section('css')
  <body class="skin-blue fixed-layout">
    <div class="preloader">
        <div class="loader">
            <div class="loader__figure"></div>
            <p class="loader__label">Elite admin</p>
        </div>
    </div>
    <div id="main-wrapper">   
@endsection

@section('main-content')
<div class="page-wrapper">
            <div class="container-fluid">
                <div class="row page-titles">
                    <div class="col-md-5 align-self-center">
                        <h4 class="text-themecolor">Notice List</h4>
                    </div>
                    <div class="col-md-7 align-self-center text-right">
                        <div class="d-flex justify-content-end align-items-center">
                            <ol class="breadcrumb">
                                <li class="breadcrumb-item"><a href="javascript:void(0)">Home</a></li>
                                <li class="breadcrumb-item active">Notice List</li>
                            </ol>
							
							<a href="{{route('admin.all-cat')}}" class="badge badge-primary">Show Category</a>
                        </div>
                    </div>
                </div>
				@if(Session::has('success'))
				<div class="alert alert-warning alert-dismissible fade show" role="alert">
					<strong>Success!</strong> {{Session::get('success')}}
					<button type="button" class="close" data-dismiss="alert" aria-label="Close"><span class="ti-close"></span>
					</button>
				</div>
				@endif
				<div class="card-group">
                    <div class="card">
                        <div class="card-body">
                            <div class="row">
                                <div class="col-md-12">
								   <h4 class="card_title">All Notice</h4>
                   
                                   <form action="{{route('admin.insert-main-cat')}}" enctype="multipart/form-data" method="post" class="needs-validation" novalidate>
										@csrf
									   <div class="form-row">
											<div class="col-md-4 mb-3">
												<label for="validationTooltip01">Name Of Category</label>
												<input type="text" name="name" class="form-control" id="validationTooltip01" placeholder="First name" required>
												<div class="valid-tooltip">
													Looks good!
												</div>
											</div>
										</div>
										<button class="btn btn-primary" type="submit">Submit form</button>
									</form>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
								   
				
   
@endsection

@section('js')
    <!--=========================*
        This Page Scripts
*===========================-->
    <!-- Validation Script -->
    <script>
        //  Form Validation
        window.addEventListener('load', function() {
            // Fetch all the forms we want to apply custom Bootstrap validation styles to
            var forms = document.getElementsByClassName('needs-validation');
            // Loop over them and prevent submission
            var validation = Array.prototype.filter.call(forms, function(form) {
                form.addEventListener('submit', function(event) {
                    if (form.checkValidity() === false) {
                        event.preventDefault();
                        event.stopPropagation();
                    }
                    form.classList.add('was-validated');
                }, false);
            });
        }, false);
    </script>
@endsection
