@extends('layouts.app')


{{-- Page Title --}}
@section('page-title')
  Accounts
@endsection
{{-- This Page Css --}}
@section('css')
   <link rel="stylesheet" type="text/css"
        href="{{asset('public/assets/datatables.net-bs4/css/dataTables.bootstrap4.css')}}">
    <link rel="stylesheet" type="text/css"
        href="{{asset('public/assets/datatables.net-bs4/css/responsive.dataTables.min.css')}}">
    <!--=========================*
               Datatable
  
  *===========================-->
  <body class="skin-blue fixed-layout">
    <!-- ============================================================== -->
    <!-- Preloader - style you can find in spinners.css -->
    <!-- ============================================================== -->
    <div class="preloader">
        <div class="loader">
            <div class="loader__figure"></div>
        </div>
    </div>
	 <div id="main-wrapper">
	  
@endsection
@section('main-content')

       <div class="page-wrapper">
      <div class="container-fluid">
                <!-- ============================================================== -->
                <!-- Bread crumb and right sidebar toggle -->
                <!-- ============================================================== -->
                <div class="row page-titles">
                    <div class="col-md-5 align-self-center">
                        <h4 class="text-themecolor">User Role</h4>
                    </div>
                    <div class="col-md-7 align-self-center text-right">
                        <div class="d-flex justify-content-end align-items-center">
                            <ol class="breadcrumb">
                                <li class="breadcrumb-item"><a href="javascript:void(0)">Home</a></li>
                                <li class="breadcrumb-item active">User Role</li>
                            </ol>
                            <button type="button" class="btn btn-info d-none d-lg-block m-l-15" data-toggle="modal" data-target=".add-role" ><i
                                    class="fa fa-plus-circle"></i> Add New Role</button>
									
                        </div>
                    </div>
                </div>
                <!-- ============================================================== -->
                <!-- End Bread crumb and right sidebar toggle -->
                <!-- ============================================================== -->
                <!-- ============================================================== -->
                <!-- Start Page Content -->
                <!-- ============================================================== -->
                <div class="row">
                    <div class="col-12">
					 @if(Session::has('insrt'))
			<div class="alert alert-info alert-dismissible fade show" role="alert">
				<strong>{{Session::get('insrt')}}</strong>
				<button type="button" class="close" data-dismiss="alert" aria-label="Close"><span class="ti-close"></span>
				</button>
			</div>
			@endif
			@if(Session::has('edit'))
			<div class="alert alert-warning alert-dismissible fade show" role="alert">
				<strong> {{Session::get('edit')}}</strong>
				<button type="button" class="close" data-dismiss="alert" aria-label="Close"><span class="ti-close"></span>
				</button>
			</div>
			@endif
			@if(Session::has('delete'))
			<div class="alert alert-danger alert-dismissible fade show" role="alert">
				<strong> {{Session::get('delete')}}</strong>
				<button type="button" class="close" data-dismiss="alert" aria-label="Close"><span class="ti-close"></span>
				</button>
			</div>
			@endif
                        <div class="card">
                            <div class="card-body">
                                <h4 class="card-title">Data Export</h4>
                                <h6 class="card-subtitle">Export data to Copy, CSV, Excel, PDF & Print</h6>
                                <div class="table-responsive m-t-40">
                                    <table id="example23"
                                        class="display nowrap table table-hover table-striped table-bordered"
                                        cellspacing="0" width="100%">
                                        <thead>
                                            <tr>
                                               <th>Sr No</th>
												<th>Role Name</th>
												<th>Title</th>
												<th>Action</th>
                                            </tr>
                                        </thead>
                                        <tfoot>
                                            <tr>
                                               <th>Sr No</th>
												<th>Role Name</th>
												<th>Title</th>
												<th>Action</th>
                                            </tr>
                                        </tfoot>
                                        <tbody>
                                           @php
														$num=1;
														@endphp
														@foreach($all as $value)
														<tr>
															<td>{{$num}}</td>
															<td><span class="badge badge-warning">{{$value->role_name}}</span></td>
															<td>{{$value->description}}</td>
															 <td> 
								  <a href = "" class='forum-title' name = "abc" data-toggle="modal" data-target="#usermodal{{$value->id}}"><i class="ti-pencil mr-1 btn btn-success"></i></a>
								<a href = "{{route('role-delete',$value->id)}}" class='forum-title'><i class="ti-trash btn btn-danger"></i></a>
								</td>
														</tr>
								 <div class="modal fade" id="usermodal{{$value->id}}">
									<div class="modal-dialog modal-dialog-centered" role="document">
										<div class="modal-content">
											<div class="modal-header">
												<h5 class="modal-title">Update Form</h5>
												<button type="button" class="close" data-dismiss="modal"><span>&times;</span></button>
											</div>
											<div class="modal-body">
											<form method="post"  action ="{{route('edit-role',$value->id)}}"class="needs-validation" novalidate="">
												@csrf
													<div class="form-row">
														<div class="col-md-12 mb-3">
															<label for="validationCustom01">Role name</label>
															<input type="text" class="form-control" id="validationCustom01" value="{{$value->role_name}}" name="name" placeholder="Role name.."  required="">
														</div>
														</div>
														<div class="form-row">
														  <div class="col-md-12 mb-3">
															<label for="validationCustomUsername">Title </label>
															<div class="input-group">
																<textarea name="desc" class="form-control" placeholder="Enter Description..." rows="5">{{$value->description}}</textarea>
															</div>
														  </div>
														</div>
													</div>
												<button class="btn btn-primary" type="submit">Submit form</button>
												</form>
											</div>
										</div>
									</div>
														@php
														$num++;
														@endphp
														@endforeach
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                       
                       
                    </div>
                </div>
                <!-- ============================================================== -->
                <!-- End PAge Content -->
                <!-- ============================================================== -->
                <!-- ============================================================== -->
                <!-- Right sidebar -->
                <!-- ============================================================== -->
                <!-- .right-sidebar -->
             
            </div>
			
			
			 <div class="modal add-role" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true" style="display: none;">
				<div class="modal-dialog modal-xl">
					<div class="modal-content">
						<div class="modal-header">
							<h4 class="modal-title" id="myLargeModalLabel">Add New Role</h4>
							<button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
						</div>
						<div class="modal-body">
							<form method="post"  action ="{{route('add-role')}}"class="needs-validation" novalidate="">
							@csrf
								    <div class="form-row">
									   <div class="col-md-12 mb-3">
										<label for="validationCustom01">Role name</label>
										<input type="text" class="form-control" id="validationCustom01" name="name" placeholder="Role name.."  required="">
										<div class="valid-feedback">
											Looks good!
										</div>
									  </div>
									</div>
									<div class="form-row">
									  <div class="col-md-12 mb-3">
										<label for="validationCustomUsername">Title </label>
										<div class="input-group">
											<textarea name="desc" class="form-control" placeholder="Enter Description..." rows="5"></textarea>
											<div class="invalid-feedback">
												Please choose a username.
											</div>
										</div>
									  </div>
									</div>
									
									<div class="form-row">
									  <div class="col-md-12 mb-3">
									    <button class="btn btn-primary" type="submit">Submit form</button>
									  </div>
									</div>
							</form>
						</div>
						<div class="modal-footer">
							<button type="button" class="btn btn-danger waves-effect text-left" data-dismiss="modal">Close</button>
						</div>
					</div>
					<!-- /.modal-content -->
				</div>
				<!-- /.modal-dialog -->
			</div>
            <!-- ============================================================== -->
            <!-- End Container fluid  -->
            <!-- ============================================================== -->
 
 </div>
@endsection
@section('js')
  <script>
        $(function () {
            $('#myTable').DataTable();
            var table = $('#example').DataTable({
                "columnDefs": [{
                    "visible": false,
                    "targets": 2
                }],
                "order": [
                    [2, 'asc']
                ],
                "displayLength": 25,
                "drawCallback": function (settings) {
                    var api = this.api();
                    var rows = api.rows({
                        page: 'current'
                    }).nodes();
                    var last = null;
                    api.column(2, {
                        page: 'current'
                    }).data().each(function (group, i) {
                        if (last !== group) {
                            $(rows).eq(i).before('<tr class="group"><td colspan="5">' + group + '</td></tr>');
                            last = group;
                        }
                    });
                }
            });
            // Order by the grouping
            $('#example tbody').on('click', 'tr.group', function () {
                var currentOrder = table.order()[0];
                if (currentOrder[0] === 2 && currentOrder[1] === 'asc') {
                    table.order([2, 'desc']).draw();
                } else {
                    table.order([2, 'asc']).draw();
                }
            });
            // responsive table
            $('#config-table').DataTable({
                responsive: true
            });
            $('#example23').DataTable({
                dom: 'Bfrtip',
                buttons: [
                    'copy', 'csv', 'excel', 'pdf', 'print'
                ]
            });
            $('.buttons-copy, .buttons-csv, .buttons-print, .buttons-pdf, .buttons-excel').addClass('btn btn-primary mr-1');
        });

    </script>
@endsection
