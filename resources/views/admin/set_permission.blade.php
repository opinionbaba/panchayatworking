@extends('layouts.app')


{{-- Page Title --}}
@section('page-title')
  Set Permissions
@endsection
{{-- This Page Css --}}
@section('css')
   <link rel="stylesheet" type="text/css"
        href="{{asset('public/assets/datatables.net-bs4/css/dataTables.bootstrap4.css')}}">
    <link rel="stylesheet" type="text/css"
        href="{{asset('public/assets/datatables.net-bs4/css/responsive.dataTables.min.css')}}">
		
  <body class="skin-blue fixed-layout">
    <!-- ============================================================== -->
    <!-- Preloader - style you can find in spinners.css -->
    <!-- ============================================================== -->
    <div class="preloader">
        <div class="loader">
            <div class="loader__figure"></div>
        </div>
    </div>
	 <div id="main-wrapper">
	  
@endsection
@section('main-content')
 <div class="page-wrapper">
      <div class="container-fluid">
                <!-- ============================================================== -->
                <!-- Bread crumb and right sidebar toggle -->
                <!-- ============================================================== -->
                <div class="row page-titles">
                    <div class="col-md-5 align-self-center">
                        <h4 class="text-themecolor">Set Role Permissions</h4>
                    </div>
                    <div class="col-md-7 align-self-center text-right">
                        <div class="d-flex justify-content-end align-items-center">
                            <ol class="breadcrumb">
                                <li class="breadcrumb-item"><a href="javascript:void(0)">Home</a></li>
                                <li class="breadcrumb-item active">Permissions</li>
                            </ol>
                            
									
                        </div>
                    </div>
                </div>
                <!-- ============================================================== -->
                <!-- End Bread crumb and right sidebar toggle -->
                <!-- ============================================================== -->
                <!-- ============================================================== -->
                <!-- Start Page Content -->
                <!-- ============================================================== -->
                <div class="row">
                    <div class="col-12">
					    <div class="card">
                            <div class="card-body">
                                <h4 class="card-title">Set Role Permissions</h4>
                                 <div class="table-responsive m-t-40">
                                    <table id="example23"
                                        class="display nowrap table table-hover table-striped table-bordered"
                                        cellspacing="0" width="100%">
                                        <thead>
                                            <tr>
                                               <th>Sr No</th>
												<th>Role Type</th>
												<th>Title</th>
												<th>Status</th>
												
												<th>Action</th>
                                            </tr>
                                        </thead>
                                        
                                        <tbody>
											@php
											$num =1;
											@endphp
											@foreach($role as $value)
											<tr>
                                                <td>{{$num}}</td>
												<td>{{$value->role_name}}</td>
												<td>{{$value->description}}</td>
												<td>Active</td>
												<td>
													<a href = "{{route('add_permission')}}" class='forum-title btn btn-info' name = "abc" >Set Permission</a>
													
													
								
												</td>
                                            </tr>
											@php
											$num++;
											@endphp
											@endforeach
                                        </tbody>
										<tfoot>
                                            <tr>
                                                <th>#</th>
												<th>Role Type</th>
												<th>Title</th>
												<th>Status</th>
												
												<th>Action</th>
                                            </tr>
                                        </tfoot>
                                    </table>
                                </div>
                                       
                            </div>
                        </div>
                    </div>
                </div>
            </div>
     </div>
       
@endsection
@section('js')
 <script>
        $(function () {
            $('#myTable').DataTable();
            var table = $('#example').DataTable({
                "columnDefs": [{
                    "visible": false,
                    "targets": 2
                }],
                "order": [
                    [2, 'asc']
                ],
                "displayLength": 25,
                "drawCallback": function (settings) {
                    var api = this.api();
                    var rows = api.rows({
                        page: 'current'
                    }).nodes();
                    var last = null;
                    api.column(2, {
                        page: 'current'
                    }).data().each(function (group, i) {
                        if (last !== group) {
                            $(rows).eq(i).before('<tr class="group"><td colspan="5">' + group + '</td></tr>');
                            last = group;
                        }
                    });
                }
            });
            // Order by the grouping
            $('#example tbody').on('click', 'tr.group', function () {
                var currentOrder = table.order()[0];
                if (currentOrder[0] === 2 && currentOrder[1] === 'asc') {
                    table.order([2, 'desc']).draw();
                } else {
                    table.order([2, 'asc']).draw();
                }
            });
            // responsive table
            $('#config-table').DataTable({
                responsive: true
            });
            $('#example23').DataTable({
                dom: 'Bfrtip',
                buttons: [
                    'copy', 'csv', 'excel', 'pdf', 'print'
                ]
            });
            $('.buttons-copy, .buttons-csv, .buttons-print, .buttons-pdf, .buttons-excel').addClass('btn btn-primary mr-1');
        });

    </script>
@endsection
