@extends('layouts.app')


{{-- Page Title --}}
@section('page-title')
    Profile Page
@endsection

{{-- This Page Css --}}
@section('css')
<body class="skin-blue fixed-layout">
 
   <div id="main-wrapper">
<style>
.card
{
	width: 100%;
}
.pro_img {
  position: relative;
  text-align: center;
  color: white;
}

.bottom-left {
  position: absolute;
  bottom: 8px;
  font-size:20px;
  font-weight:bold;
  left: 16px;
}

.top-left {
  position: absolute;
  top: 8px;
  left: 16px;
}

.top-right {
  position: absolute;
  top: 8px;
  right: 16px;
}

.bottom-right {
  position: absolute;
  bottom: 8px;
  right: 16px;
}

.centered {
  position: absolute;
  top: 50%;
  left: 50%;
  transform: translate(-50%, -50%);
}
.switch {
  position: relative;
  display: inline-block;
 
}

.switch input { 
  opacity: 0;
  width: 0;
  height: 0;
}

.slider {
  position: absolute;
  cursor: pointer;
  top: 0;
  left: 0;
  right: 0;
  bottom: 0;
  background-color: #ccc;
  -webkit-transition: .4s;
  transition: .4s;
}

.slider:before {
  position: absolute;
  content: "";
  height: 26px;
  width: 26px;
  left: 4px;
  bottom: 4px;
  background-color: white;
  -webkit-transition: .4s;
  transition: .4s;
}

input:checked + .slider {
  background-color: #2196F3;
}

input:focus + .slider {
  box-shadow: 0 0 1px #2196F3;
}

input:checked + .slider:before {
  -webkit-transform: translateX(26px);
  -ms-transform: translateX(26px);
  transform: translateX(26px);
}

/* Rounded sliders */
.slider.round {
  border-radius: 34px;
}

.slider.round:before {
  border-radius: 50%;
}

</style>
@endsection

@section('main-content')
 <div class="page-wrapper">
	<div class="container-fluid">
		<div class="row page-titles">
			<div class="col-md-5 align-self-center">
				<h4 class="text-themecolor">Panchyat User</h4>
			</div>
			<div class="col-md-7 align-self-center text-right">
				<div class="d-flex justify-content-end align-items-center">
					<ol class="breadcrumb">
						<li class="breadcrumb-item"><a href="javascript:void(0)">Home</a></li>
						<li class="breadcrumb-item active">Panchyat User</li>
					</ol>
				</div>
			</div>
		</div>
		
		
		 <div class="row">
			<div class="pro_img">
			  <img src="https://s3-us-west-2.amazonaws.com/s.cdpn.io/3/cheese-curds.jpg" alt="Snow" style="width:100%;height:300px;">
			  <div class="bottom-left">Panchayat Name</div>
			 </div>
		   <div class="card">
                            <div class="card-body">
                                <h4 class="card-title pb-3">My Information: </h4>
                                
                                <!-- Nav tabs -->
                                <div class="vtabs">
                                    <ul class="nav nav-tabs tabs-vertical" role="tablist">
                                        <li class="nav-item"> <a class="nav-link active" data-toggle="tab" href="#BasicDetails" role="tab"><span class="hidden-sm-up"><i class="ti-hand-point-right"></i></span> <span class="hidden-xs-down">Basic Details</span> </a> </li>
                                        <li class="nav-item"> <a class="nav-link" data-toggle="tab" href="#ads" role="tab"><span class="hidden-sm-up"><i class="ti-hand-point-right"></i></span> <span class="hidden-xs-down">Ads</span></a> </li>
                                        <li class="nav-item"> <a class="nav-link" data-toggle="tab" href="#timing" role="tab"><span class="hidden-sm-up"><i class="ti-hand-point-right"></i></span> <span class="hidden-xs-down">Timing</span></a> </li>
									 </ul>
                                    <!-- Tab panes -->
                                    <div class="tab-content">
                                        <div class="tab-pane active" id="BasicDetails" role="tabpanel">
                                            <div class="">
                                                <h3>Basic Details </h3>
												<button type="button" class="btn btn-info d-none d-lg-block m-l-15" data-toggle="modal" data-target="#update" style="float:right">Update</button>
												<div class="table-responsive">
												<table class="table">
													<tbody>
														<tr>
															<td>Name</td>
															<td>Test</td>
														</tr>
														<tr>
															<td>Email</td>
															<td>Test@gmail.com</td>
														</tr>
														<tr>
															<td>Photo</td>
															<td><img src="https://cdn.shopify.com/shopifycloud/hatchful-web/assets/6fcc76cfd1c59f44d43a485167fb3139.png" height="100px"></td>
														</tr>
														<tr>
															<td>Ward No</td>
															<td>#54324</td>
														</tr>
														<tr>
															<td>Select role</td>
															<td>Peon</td>
														</tr>
														<tr>
															<td>RFID Number</td>
															<td>7488</td>
														</tr>
														<tr>
															<td>Date of joining</td>
															<td>04-08-2020</td>
														</tr>
													</tbody>
											  </table>
                                            </div>
                                            </div>
                                       </div>
									   
				<!--------(Shubham)Code Start From Here--------->			   
                 <div class="tab-pane" id="timing" role="tabpanel">
	              <h3 class="pb-2">Staff Timing </h3>
				   <button type="button" class="btn btn-info d-none d-lg-block m-l-15" data-toggle="modal" data-target="#edittiming" style="float:right">Edit Timing</button>
						<div class="table-responsive">
						  <table class="table">
							<thead>
								<tr>
									<th>Check In</th>
									<th>Check Out</th>
									<th>Late </th>
									<th>Absent</th>
								</tr>
                            </thead>
                              <tbody>
                                <tr>
								 <td>09:00</td>
								 <td>07:00</td>
								 <td>After 09:30</td>
                                 <td>After Luntch (01:30)</td>
								</tr>
							   </tbody>
                             </table>
                            </div>
						  </div>
<div class="modal bs-example-modal-lg" id="edittiming" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true" style="display: none;">
 <div class="modal-dialog ">
	<div class="modal-content">
		<div class="modal-header">
			<h4 class="modal-title" id="myLargeModalLabel">Edit Timing</h4>
			<button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
		</div>
		<div class="modal-body">
			<form action="" method ="post" class="needs-validation" novalidate="" enctype="multipart/form-data">
			  @csrf
				<div class="form-row">
				  <div class="col-md-6">
					<label for="validationCustom01">Enter Check In Time</label>
					<input type="text" name="name"  class="form-control" id="validationCustom01" placeholder="check in time..."  required="">
				  </div>
					<div class="col-md-6 mb-3">
						<label for="validationCustom01">Enter Check Out Time</label>
					<input type="text" name="name"  class="form-control" id="validationCustom01" placeholder="check out time..."  required="">
					</div>
				</div>
				<div class="form-row">
				  <div class="col-md-6">
					<label for="validationCustom01">Late Time</label>
					<input type="text" name="name"  class="form-control" id="validationCustom01" placeholder="late time..."  required="">
				  </div>
					<div class="col-md-6 mb-3">
						<label for="validationCustom01">Absent Time</label>
					<input type="text" name="name"  class="form-control" id="validationCustom01" placeholder="Absent time..."  required="">
					</div>
				</div>
			<button class="btn btn-primary" type="submit">Submit form</button>
		  </form>
		</div>
	</div>
</div>
</div>
                    			
								
 <div class="tab-pane" id="ads" role="tabpanel">
  <h3 class="pb-2">Staff Timing </h3>
    <div class="table-responsive">
		  <table class="table">
			<tbody>
			<tr>
				<td>Ads</td>
				<td><div class="container">
					<div class="btn-group btn-toggle"> 
						<button class="btn btn-info" data-toggle="collapse" data-target="#collapsible">Active</button>
						<button class="btn btn-danger off" >Deactive</button>
					 </div>
					  <div class="well collapse" id="off"> 
						You are Disable this Add
					  </div>
					  <div class="well collapse" id="collapsible"> 
						<form class="form" class="needs-validation">
						 <div class="form-row">
						  <div class="col-md-4">
							<label for="validationCustom01">300*250</label>
							<input type="text" name="name"  class="form-control" id="validationCustom01" placeholder="Enter per day price..."  required="">
						  </div>
						   <div class="col-md-4">
							<label for="validationCustom01">250*250</label>
							<input type="text" name="name"  class="form-control" id="validationCustom01" placeholder="Enter per day price..."  required="">
						  </div>
						   <div class="col-md-4">
							<label for="validationCustom01">728*90</label>
							<input type="text" name="name"  class="form-control" id="validationCustom01" placeholder="Enter per day price..."  required="">
						  </div>
							
						</div>
						  <button type="submit" class="btn btn-primary">Submit</button>
						</form> 
					  </div>
					 </div>
				    </td> 
			       </tr>
			     </tbody>
		       </table>
			</div>
		  </div>	
		</div>
	</div>
  </div>
</div>


    </div>
 </div>



   
@endsection

@section('js')
<script>
$('.off').click(function() {
	alert('You want to off this ad');
	});

</script>
 <script>
    // MAterial Date picker    
    $('#sdate').bootstrapMaterialDatePicker({ weekStart: 0, time: false });
    $('#edate').bootstrapMaterialDatePicker({ weekStart: 0, time: false });
    $('#Task_edate').bootstrapMaterialDatePicker({ weekStart: 0, time: false });
    $('#Task_sdate').bootstrapMaterialDatePicker({ weekStart: 0, time: false });
    $('#Leave_sdate').bootstrapMaterialDatePicker({ weekStart: 0, time: false });
    $('#Leave_edate').bootstrapMaterialDatePicker({ weekStart: 0, time: false });
    $('#Provident_edate').bootstrapMaterialDatePicker({ weekStart: 0, time: false });
    $('#Provident_sdate').bootstrapMaterialDatePicker({ weekStart: 0, time: false });
    $('#Overtime_sdate').bootstrapMaterialDatePicker({ weekStart: 0, time: false });
    $('#Overtime_edate').bootstrapMaterialDatePicker({ weekStart: 0, time: false });
    $('#Salary_edate').bootstrapMaterialDatePicker({ weekStart: 0, time: false });
    $('#Salary_sdate').bootstrapMaterialDatePicker({ weekStart: 0, time: false });
    $('#Activity_sdate').bootstrapMaterialDatePicker({ weekStart: 0, time: false });
    $('#Activity_edate').bootstrapMaterialDatePicker({ weekStart: 0, time: false });
	
	
	
    $('#timepicker').bootstrapMaterialDatePicker({ format: 'HH:mm', time: true, date: false });
	
	
    $('#date-format').bootstrapMaterialDatePicker({ format: 'dddd DD MMMM YYYY - HH:mm' });

    $('#min-date').bootstrapMaterialDatePicker({ format: 'DD/MM/YYYY HH:mm', minDate: new Date() });
    // Clock pickers
    $('#single-input').clockpicker({
        placement: 'bottom',
        align: 'left',
        autoclose: true,
        'default': 'now'
    });
    $('.clockpicker').clockpicker({
        donetext: 'Done',
    }).find('input').change(function() {
        console.log(this.value);
    });
    $('#check-minutes').click(function(e) {
        // Have to stop propagation here
        e.stopPropagation();
        input.clockpicker('show').clockpicker('toggleView', 'minutes');
    });
    if (/mobile/i.test(navigator.userAgent)) {
        $('input').prop('readOnly', true);
    }
    // Colorpicker
    $(".colorpicker").asColorPicker();
    $(".complex-colorpicker").asColorPicker({
        mode: 'complex'
    });
    $(".gradient-colorpicker").asColorPicker({
        mode: 'gradient'
    });
    // Date Picker
    jQuery('.mydatepicker, #datepicker').datepicker();
    jQuery('#datepicker-autoclose').datepicker({
        autoclose: true,
        todayHighlight: true
    });
    jQuery('#date-range').datepicker({
        toggleActive: true
    });
    jQuery('#datepicker-inline').datepicker({
        todayHighlight: true
    });

	// -------------------------------
	// Start Date Range Picker
	// -------------------------------

    // Basic Date Range Picker
    $('.daterange').daterangepicker();

    // Date & Time
    $('.datetime').daterangepicker({
        timePicker: true,
        timePickerIncrement: 30,
        locale: {
            format: 'MM/DD/YYYY h:mm A'
        }
    });

    //Calendars are not linked
    $('.timeseconds').daterangepicker({
        timePicker: true,
        timePickerIncrement: 30,
        timePicker24Hour: true,
        timePickerSeconds: true,
        locale: {
            format: 'MM-DD-YYYY h:mm:ss'
        }
    });

    // Single Date Range Picker
    $('.singledate').daterangepicker({
        singleDatePicker: true,
        showDropdowns: true
    });

    // Auto Apply Date Range
    $('.autoapply').daterangepicker({
        autoApply: true,
    });

    // Calendars are not linked
    $('.linkedCalendars').daterangepicker({
        linkedCalendars: false,
    });

    // Date Limit
    $('.dateLimit').daterangepicker({
        dateLimit: {
            days: 7
        },
    });

    // Show Dropdowns
    $('.showdropdowns').daterangepicker({
        showDropdowns: true,
    });

    // Show Week Numbers
    $('.showweeknumbers').daterangepicker({
        showWeekNumbers: true,
    });

     $('.dateranges').daterangepicker({
        ranges: {
            'Today': [moment(), moment()],
            'Yesterday': [moment().subtract(1, 'days'), moment().subtract(1, 'days')],
            'Last 7 Days': [moment().subtract(6, 'days'), moment()],
            'Last 30 Days': [moment().subtract(29, 'days'), moment()],
            'This Month': [moment().startOf('month'), moment().endOf('month')],
            'Last Month': [moment().subtract(1, 'month').startOf('month'), moment().subtract(1, 'month').endOf('month')]
        }
    });

    // Always Show Calendar on Ranges
    $('.shawCalRanges').daterangepicker({
        ranges: {
            'Today': [moment(), moment()],
            'Yesterday': [moment().subtract(1, 'days'), moment().subtract(1, 'days')],
            'Last 7 Days': [moment().subtract(6, 'days'), moment()],
            'Last 30 Days': [moment().subtract(29, 'days'), moment()],
            'This Month': [moment().startOf('month'), moment().endOf('month')],
            'Last Month': [moment().subtract(1, 'month').startOf('month'), moment().subtract(1, 'month').endOf('month')]
        },
        alwaysShowCalendars: true,
    });

    // Top of the form-control open alignment
    $('.drops').daterangepicker({
        drops: "up" // up/down
    });

    // Custom button options
    $('.buttonClass').daterangepicker({
        drops: "up",
        buttonClasses: "btn",
        applyClass: "btn-info",
        cancelClass: "btn-danger"
    });

	jQuery('#date-range').datepicker({
        toggleActive: true
    });
    jQuery('#datepicker-inline').datepicker({
        todayHighlight: true
    });

    // Daterange picker
    $('.input-daterange-datepicker').daterangepicker({
        buttonClasses: ['btn', 'btn-sm'],
        applyClass: 'btn-danger',
        cancelClass: 'btn-inverse'
    });
    $('.input-daterange-timepicker').daterangepicker({
        timePicker: true,
        format: 'MM/DD/YYYY h:mm A',
        timePickerIncrement: 30,
        timePicker12Hour: true,
        timePickerSeconds: false,
        buttonClasses: ['btn', 'btn-sm'],
        applyClass: 'btn-danger',
        cancelClass: 'btn-inverse'
    });
    $('.input-limit-datepicker').daterangepicker({
        format: 'MM/DD/YYYY',
        minDate: '06/01/2015',
        maxDate: '06/30/2015',
        buttonClasses: ['btn', 'btn-sm'],
        applyClass: 'btn-danger',
        cancelClass: 'btn-inverse',
        dateLimit: {
            days: 6
        }
    });




    </script>

    <!--=========================*
            This Page Scripts
    *===========================-->
    <!-- Fancy Box Js -->
    <script src="{{asset('public/assets/js/jquery.fancybox.pack.js')}}"></script>
    <script src="{{asset('public/assets/js/init/fancy.js')}}"></script>

    <!-- Data Table js -->
    <script src="{{asset('public/assets/vendors/data-table/js/jquery.dataTables.js')}}"></script>
    <script src="{{asset('public/assets/vendors/data-table/js/jquery.dataTables.min.js')}}"></script>
    <script src="{{asset('public/assets/vendors/data-table/js/dataTables.bootstrap4.min.js')}}"></script>
    <script src="{{asset('public/assets/vendors/data-table/js/dataTables.responsive.min.js')}}"></script>
    <script src="{{asset('public/assets/vendors/data-table/js/responsive.bootstrap.min.js')}}"></script>

    <!-- Data table Init -->
    <script src="{{asset('public/assets/js/init/data-table.js')}}"></script>
@endsection
