@extends('layouts.app')


{{-- Page Title --}}
@section('page-title')
 Subscriber
@endsection

{{-- This Page Css --}}
@section('css')
<link rel="stylesheet" type="text/css"
        href="{{asset('public/assets/datatables.net-bs4/css/dataTables.bootstrap4.css')}}">
    <link rel="stylesheet" type="text/css"
        href="{{asset('public/assets/datatables.net-bs4/css/responsive.dataTables.min.css')}}">
   
    <body class="skin-blue fixed-layout">
    <div class="preloader">
        <div class="loader">
            <div class="loader__figure"></div>
            <p class="loader__label">Elite admin</p>
        </div>
    </div>
    <div id="main-wrapper">   
    
@endsection

@section('main-content')
<div class="page-wrapper">
            <!-- ============================================================== -->
            <!-- Container fluid  -->
            <!-- ============================================================== -->
            <div class="container-fluid">
                <!-- ============================================================== -->
                <!-- Bread crumb and right sidebar toggle -->
                <!-- ============================================================== -->
                <div class="row page-titles">
                    <div class="col-md-5 align-self-center">
                        <h4 class="text-themecolor">Notice List</h4>
                    </div>
                    <div class="col-md-7 align-self-center text-right">
                        <div class="d-flex justify-content-end align-items-center">
                            <ol class="breadcrumb">
                                <li class="breadcrumb-item"><a href="javascript:void(0)">Home</a></li>
                                <li class="breadcrumb-item active">Notice List</li>
                            </ol>
                            <button type="button" class="btn btn-info d-none d-lg-block m-l-15"><i class="fa fa-plus-circle"></i> Create New</button>
                        </div>
                    </div>
                </div>
				@if(Session::has('delete'))
				<div class="alert alert-danger alert-dismissible fade show" role="alert">
					<strong></strong>  {{Session::get('delete')}}
					<button type="button" class="close" data-dismiss="alert" aria-label="Close"><span class="ti-close"></span>
					</button>
				</div>
				@endif
		
				<div class="card-group">
                    <div class="card">
                        <div class="card-body">
                            <div class="row">
                                <div class="col-md-12">
								   <h4 class="card_title">All Notice</h4>
                   
                                   <div class="button-box">
                                    <a class="tst1 btn btn-info" href="#">Homee</a>
                                    <a class="tst1 btn btn-info" href="{{route('admin.allnotice')}}">Notice</a>
                                    <a class="tst1 btn btn-info" href="{{route('admin.allevent')}}">Events</a>
                                    <a class="tst1 btn btn-info" href="{{route('admin.allgallery')}}">Gallery</a>
                                    <a class="tst1 btn btn-info" href="{{route('admin.alldoc')}}">Form</a>
                                    <a class="tst1 btn btn-info" href="{{route('admin.contact')}}">Contact Us</a>
                                    <a class="tst1 btn btn-info" href="{{route('admin.subs')}}">Subscribers</a>
                                    <a class="tst1 btn btn-info" href="#">House Tax</a>
                                    <a class="tst1 btn btn-info" href="{{route('admin.allaudit')}}">Audit</a>
                                   </div>
								   
								   <div class="table-responsive m-t-40">
                                    <table id="example23"
                                        class="display nowrap table table-hover table-striped table-bordered"
                                        cellspacing="0" width="100%">
                                        <thead>
                                            <tr>
                                                <th>Sr No</th>
												<th>Email</th>
												<th>Date</th>
												<th>Action</th>
                                            </tr>
                                        </thead>
                                        <tfoot>
                                            <tr>
                                                <th>Sr No</th>
												<th>Email</th>
												<th>Date</th>
												<th>Action</th>
                                            </tr>
                                        </tfoot>
                                        <tbody>
										   @php 
											$num =1;
											@endphp
											@foreach($all as $value)
											<tr>
												<td>{{$num}}</td>
												<td>{{$value->email}}</td>
												<td>{{$value->created_at}}</td>
												<td> 
												  <a href = "{{route('subs-delete',$value->id)}}" class='forum-title'><i class="ti-trash btn btn-danger"></i></a>
												</td>
											 </tr>
											@php
											$num++;
											@endphp
										   @endforeach
										</tbody>
									</table>
                                  </div>									
									
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
@endsection

@section('js')
    <script>
        $(function () {
            $('#myTable').DataTable();
            var table = $('#example').DataTable({
                "columnDefs": [{
                    "visible": false,
                    "targets": 2
                }],
                "order": [
                    [2, 'asc']
                ],
                "displayLength": 25,
                "drawCallback": function (settings) {
                    var api = this.api();
                    var rows = api.rows({
                        page: 'current'
                    }).nodes();
                    var last = null;
                    api.column(2, {
                        page: 'current'
                    }).data().each(function (group, i) {
                        if (last !== group) {
                            $(rows).eq(i).before('<tr class="group"><td colspan="5">' + group + '</td></tr>');
                            last = group;
                        }
                    });
                }
            });
            // Order by the grouping
            $('#example tbody').on('click', 'tr.group', function () {
                var currentOrder = table.order()[0];
                if (currentOrder[0] === 2 && currentOrder[1] === 'asc') {
                    table.order([2, 'desc']).draw();
                } else {
                    table.order([2, 'asc']).draw();
                }
            });
            // responsive table
            $('#config-table').DataTable({
                responsive: true
            });
            $('#example23').DataTable({
                dom: 'Bfrtip',
                buttons: [
                    'copy', 'csv', 'excel', 'pdf', 'print'
                ]
            });
            $('.buttons-copy, .buttons-csv, .buttons-print, .buttons-pdf, .buttons-excel').addClass('btn btn-primary mr-1');
        });

    </script>
@endsection
