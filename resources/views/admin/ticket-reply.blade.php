@extends('layouts.app')


{{-- Page Title --}}
@section('page-title')
  Accounts
@endsection
{{-- This Page Css --}}
@section('css')
   <link rel="stylesheet" type="text/css"
        href="{{asset('public/assets/datatables.net-bs4/css/dataTables.bootstrap4.css')}}">
    <link rel="stylesheet" type="text/css"
        href="{{asset('public/assets/datatables.net-bs4/css/responsive.dataTables.min.css')}}">
   
  <body class="skin-blue fixed-layout">
   
    <div class="preloader">
        <div class="loader">
            <div class="loader__figure"></div>
        </div>
    </div>
	 <div id="main-wrapper">
	  
@endsection
@section('main-content')

       <div class="page-wrapper">
      <div class="container-fluid">
               
                <div class="row page-titles">
                    <div class="col-md-5 align-self-center">
                        <h4 class="text-themecolor">Datatable</h4>
                    </div>
                    <div class="col-md-7 align-self-center text-right">
                        <div class="d-flex justify-content-end align-items-center">
                            <ol class="breadcrumb">
                                <li class="breadcrumb-item"><a href="javascript:void(0)">Home</a></li>
                                <li class="breadcrumb-item active">Datatable</li>
                            </ol>
                          
                        </div>
                    </div>
                </div>
			  <div class="card">
				<div class="card-body">
					<h4 class="card-title">Subject : </h4>{{$data->subject}}
					 <h4 class="card-title">Description : </h4>{{$data->desc}}
					
					</div>
				 </div>
                        
              
              <div class="row">
                    <div class="col-lg-12">
                        <div class="card">
                            <div class="row">
                                <div class="col-xlg-2 col-lg-3 col-md-4 ">
                                   <div class="card">
                            <div class="card-body">
                                <center class="m-t-30"> <img src="{{asset('public/assets/images/users/5.jpg')}}" class="img-circle" width="150" />
								@if(!empty($citizen))
                                   <h4 class="card-title m-t-10">{{$citizen->username}}</h4>
                                   <div class="row text-center justify-content-md-center">
                                       <p>Ticket ID : {{$data->ticket_id}} </p>
                                       <p>Contact No : {{$citizen->mobile}} </p>
                                      
                                    </div>
								@else
								<h4 class="card-title m-t-10">Default</h4>
                                   <div class="row text-center justify-content-md-center">
                                       <p>Ticket ID : *** </p>
                                       <p>Contact No : ******* </p>
                                      
                                    </div>
								@endif
                                </center>
                            </div>
                            <div>
                                <hr> </div>
                            
                        </div>
                                </div>
                                <div class="col-xlg-10 col-lg-9 col-md-8 bg-light border-left">
                                    <div class="card-body">
                                        <h3 class="card-title">Reply</h3>
										  <form method="post" action="{{route('reply-to-citizen')}}" enctype="multipart/form-data">
										   @csrf
										   <input type="hidden" value="{{$data->ticket_id}}" name="ticket_id">
										   <input type="hidden" value="{{Auth::user()->id}}" name="panc_user_id">
										   <div class="row">
										  <div class="col-md-6">
										    <label>Select Status</label>
											<div class="form-group">
												<select name="reply_status" class="form-control">
												  <option value="">--Select--</option>
												  <option value="1">Open</option>
												  <option value="2">Close</option>
												</select>
											</div>
										  </div>
										   
										  <div class="col-md-6">
										    <label>Select Priority</label>
											<div class="form-group">
											  <input type="text" name="status_priority" value="{{$data->priority}}" class="form-control" disabled>
											  </div>
										   </div>
									    </div>
                                        <div class="form-group">
										 <label>Subject</label>
                                            <input class="form-control" name="subject" value="{{$data->subject}}" placeholder="Subject:">
                                        </div>
                                        <div class="form-group">
                                            <textarea class="textarea_editor form-control" name="panc_desc" rows="4" placeholder="Enter text ..."></textarea>
                                        </div>
                                        <h4><i class="ti-link"></i> Attachment</h4>
                                         <div class="fallback">
                                              <input name="reply_file" type="file" multiple />
                                           </div>
                                        
                                        <button type="submit" class="btn btn-success m-t-20"><i class="fa fa-envelope-o"></i> Reply</button>
                                        <button type="reset" class="btn btn-dark m-t-20"><i class="fa fa-times"></i> Discard</button>
										</form>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
 
 </div>
 </div>
@endsection

