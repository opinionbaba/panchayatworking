@extends('layouts.app')


{{-- Page Title --}}
@section('page-title')
    Profile Page
@endsection

{{-- This Page Css --}}
@section('css')
<body class="skin-blue fixed-layout">
  <div class="preloader">
	<div class="loader">
		<div class="loader__figure"></div>
		<p class="loader__label">Panchyat</p>
	</div>
   </div>
   <div id="main-wrapper">
<style>
.card
{
	width: 100%;
}
.pro_img {
  position: relative;
  text-align: center;
  color: white;
}

.bottom-left {
  position: absolute;
  bottom: 8px;
  font-size:20px;
  font-weight:bold;
  left: 16px;
}

.top-left {
  position: absolute;
  top: 8px;
  left: 16px;
}

.top-right {
  position: absolute;
  top: 8px;
  right: 16px;
}

.bottom-right {
  position: absolute;
  bottom: 8px;
  right: 16px;
}

.centered {
  position: absolute;
  top: 50%;
  left: 50%;
  transform: translate(-50%, -50%);
}

</style>
@endsection

@section('main-content')

  <!-- ============================================================== 
						Basic Detail Update
   ============================================================== -->
<div class="modal bs-example-modal-lg" id="update"tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true" style="display: none;">
 <div class="modal-dialog modal-xl">
	<div class="modal-content">
		<div class="modal-header">
			<h4 class="modal-title" id="myLargeModalLabel">Update Details</h4>
			<button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
		</div>
		<div class="modal-body">
			<form action="{{route('add-panachyat-user')}}" method ="post" class="needs-validation" novalidate="" enctype="multipart/form-data">
			  @csrf
				<div class="form-row">
				  <center><img src="https://cdn.shopify.com/shopifycloud/hatchful-web/assets/6fcc76cfd1c59f44d43a485167fb3139.png" height="161px" style="margin-left: 311px;"></center>
				</div>
				<div class="form-row">
				  <div class="col-md-4">
					<label for="validationCustom01">Name</label>
					<input type="text" name="name" class="form-control" id="validationCustom01" placeholder="User name.."  required="">
				  </div>
					<div class="col-md-4 mb-3">
						<label for="validationCustom02">Ward Number</label>
						<input type="number" class="form-control" id="validationCustom02" name="mobile" placeholder="Enter Ward number.." required="">
						
					</div>
					<div class="col-md-4 mb-3">
						<label for="validationCustom02">Enter Email</label>
						<input type="email" class="form-control" id="validationCustom02" name="email" placeholder="Enter email.." required="">
					</div>
				</div>
				<div class="form-row">
					<div class="col-md-4 mb-3">
						<label for="validationCustom01">Select Role</label>
						<select name="role_id" class="form-control">
							<option value="">--Select Role--</option>
							<option value="">Peon<option>
							<option value="">Sarapnch</option>
							
						</select>
					</div>
					<div class="col-md-4 mb-3">
						<label for="validationCustom02">RFID Number</label>
						<input type="email" class="form-control" id="validationCustom02" name="email" placeholder="RFID Number.." required="">
					</div>
					<div class="col-md-4 mb-3">
						<label for="validationCustom02">Joining Date</label>
						<input type="date" class="form-control" id="validationCustom02" name="email" placeholder="" required="">
					</div>
				</div>
				
			 <button class="btn btn-primary" type="submit">Submit form</button>
		  </form>
		</div>
		<div class="modal-footer">
			<button type="button" class="btn btn-danger waves-effect text-left" data-dismiss="modal">Close</button>
		</div>
	</div>
	<!-- /.modal-content -->
</div>
</div>
   <!-- ============================================================== 
						Stamp Update
   ============================================================== -->
<div class="modal bs-example-modal-lg" id="update-stamp" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true" style="display: none;">
 <div class="modal-dialog ">
	<div class="modal-content">
		<div class="modal-header">
			<h4 class="modal-title" id="myLargeModalLabel">Stamp Update</h4>
			<button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
		</div>
		<div class="modal-body">
			<form action="{{route('add-panachyat-user')}}" method ="post" class="needs-validation" novalidate="" enctype="multipart/form-data">
			  @csrf
				<div class="form-row">
				  <center><img src="https://cdn.shopify.com/shopifycloud/hatchful-web/assets/6fcc76cfd1c59f44d43a485167fb3139.png" height="161px" style="margin-left: 162px;"></center>
				</div>
				<div class="form-row">
				  <div class="col-md-6">
					<label for="validationCustom01">Stamp Name</label>
					<input type="text" name="name" class="form-control" id="validationCustom01" placeholder="Stamp name.."  required="">
				  </div>
					<div class="col-md-6 mb-3">
						<label for="validationCustom02">Status</label>
						<select name="role_id" class="form-control">
							<option value="">--Select Status--</option>
							<option value="">Success<option>
							<option value="">Delete</option>
							
						</select>
					</div>
				</div>
			<button class="btn btn-primary" type="submit">Submit form</button>
		  </form>
		</div>
		<div class="modal-footer">
			<button type="button" class="btn btn-danger waves-effect text-left" data-dismiss="modal">Close</button>
		</div>
	</div>
	<!-- /.modal-content -->
</div>
</div>			
			 <!-- ============================================================== 
						Bank Details Update
             ============================================================== -->
<div class="modal bs-example-modal-lg" id="update-bank" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true" style="display: none;">
 <div class="modal-dialog modal-xl">
	<div class="modal-content ">
		<div class="modal-header">
			<h4 class="modal-title" id="myLargeModalLabel">Stamp Update</h4>
			<button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
		</div>
		<div class="modal-body">
			<form action="{{route('add-panachyat-user')}}" method ="post" class="needs-validation" novalidate="" enctype="multipart/form-data">
			  @csrf
				
				<div class="form-row">
				  <div class="col-md-4">
					<label for="validationCustom01">Account Holder Name</label>
					<input type="text" name="name" class="form-control" id="validationCustom01" placeholder="Account Holder name.."  required="">
				  </div>
				  <div class="col-md-4">
					<label for="validationCustom01">Account Number</label>
					<input type="text" name="name" class="form-control" id="validationCustom01" placeholder="Account Number.."  required="">
				  </div>
				  <div class="col-md-4">
					<label for="validationCustom01">IFSC</label>
					<input type="text" name="name" class="form-control" id="validationCustom01" placeholder="Enter IFSC.."  required="">
				  </div>
				 </div>
					<div class="form-row">
					 <div class="col-md-6">
						<label for="validationCustom01">Bank Name</label>
						<input type="text" name="name" class="form-control" id="validationCustom01" placeholder="Bank name.."  required="">
					  </div>
					  <div class="col-md-6">
						<label for="validationCustom01">Branch Name</label>
						<input type="text" name="name" class="form-control" id="validationCustom01" placeholder="Branch Name.."  required="">
					  </div>
					 
				   </div>
				
			<button class="btn btn-primary" type="submit">Submit form</button>
		  </form>
		</div>
		<div class="modal-footer">
			<button type="button" class="btn btn-danger waves-effect text-left" data-dismiss="modal">Close</button>
		</div>
	</div>
	<!-- /.modal-content -->
</div>
</div>
	   <!-- ============================================================== 
								Task Update
		============================================================== -->
<div class="modal bs-example-modal-lg" id="update-task" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true" style="display: none;">
 <div class="modal-dialog ">
	<div class="modal-content">
		<div class="modal-header">
			<h4 class="modal-title" id="myLargeModalLabel">Task Update</h4>
			<button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
		</div>
		<div class="modal-body">
			<form action="{{route('add-panachyat-user')}}" method ="post" class="needs-validation" novalidate="" enctype="multipart/form-data">
			  @csrf
				<div class="form-row">
				  <div class="col-md-4">
					<label for="validationCustom01">Task Name</label>
					<input type="text" name="name" class="form-control" id="validationCustom01" placeholder="Task name.."  required="">
				  </div>
				  <div class="col-md-4">
					<label for="validationCustom01">Assign Task</label>
					<input type="text" name="name" class="form-control" id="validationCustom01" placeholder="Assign Task.."  required="">
				  </div>
				  <div class="col-md-4">
					<label for="validationCustom01">Status</label>
					<input type="text" name="name" class="form-control" id="validationCustom01" placeholder="Status."  required="">
				  </div>
					
				</div>
			<button class="btn btn-primary" type="submit">Submit form</button>
		  </form>
		</div>
		<div class="modal-footer">
			<button type="button" class="btn btn-danger waves-effect text-left" data-dismiss="modal">Close</button>
		</div>
	</div>
	<!-- /.modal-content -->
</div>
</div>
            <!-- ============================================================== -->
 <div class="page-wrapper">
	<div class="container-fluid">
		<div class="row page-titles">
			<div class="col-md-5 align-self-center">
				<h4 class="text-themecolor">Panchyat User</h4>
			</div>
			<div class="col-md-7 align-self-center text-right">
				<div class="d-flex justify-content-end align-items-center">
					<ol class="breadcrumb">
						<li class="breadcrumb-item"><a href="javascript:void(0)">Home</a></li>
						<li class="breadcrumb-item active">Panchyat User</li>
					</ol>
				</div>
			</div>
		</div>
		
		
		 <div class="row">
			<div class="pro_img">
			  <img src="https://s3-us-west-2.amazonaws.com/s.cdpn.io/3/cheese-curds.jpg" alt="Snow" style="width:100%;height:300px;">
			  <div class="bottom-left">Shubham Kumar</div>
			 </div>
		   <div class="card">
                            <div class="card-body">
                                <h4 class="card-title pb-3">My Information: </h4>
                                
                                <!-- Nav tabs -->
                                <div class="vtabs">
                                    <ul class="nav nav-tabs tabs-vertical" role="tablist">
                                        <li class="nav-item"> <a class="nav-link active" data-toggle="tab" href="#BasicDetails" role="tab"><span class="hidden-sm-up"><i class="ti-hand-point-right"></i></span> <span class="hidden-xs-down">Basic Details</span> </a> </li>
                                        <li class="nav-item"> <a class="nav-link" data-toggle="tab" href="#Stamp" role="tab"><span class="hidden-sm-up"><i class="ti-hand-point-right"></i></span> <span class="hidden-xs-down">Stamp</span></a> </li>
                                        <li class="nav-item"> <a class="nav-link" data-toggle="tab" href="#Bank" role="tab"><span class="hidden-sm-up"><i class="ti-hand-point-right"></i></span> <span class="hidden-xs-down">Bank</span></a> </li>
										<li class="nav-item"> <a class="nav-link" data-toggle="tab" href="#Documents" role="tab"><span class="hidden-sm-up"><i class="ti-hand-point-right"></i></span> <span class="hidden-xs-down">Documents</span></a> </li>
										<li class="nav-item"> <a class="nav-link" data-toggle="tab" href="#Task" role="tab"><span class="hidden-sm-up"><i class="ti-hand-point-right"></i></span> <span class="hidden-xs-down">Task</span></a> </li>
										<li class="nav-item"> <a class="nav-link" data-toggle="tab" href="#TimeCard" role="tab"><span class="hidden-sm-up"><i class="ti-hand-point-right"></i></span> <span class="hidden-xs-down">Time Card</span></a> </li>
										<li class="nav-item"> <a class="nav-link" data-toggle="tab" href="#Leaves" role="tab"><span class="hidden-sm-up"><i class="ti-hand-point-right"></i></span> <span class="hidden-xs-down">Leaves</span></a> </li>
										<li class="nav-item"> <a class="nav-link" data-toggle="tab" href="#ProvidentFund" role="tab"><span class="hidden-sm-up"><i class="ti-hand-point-right"></i></span> <span class="hidden-xs-down">Provident Fund</span></a> </li>
										<li class="nav-item"> <a class="nav-link" data-toggle="tab" href="#Overtime" role="tab"><span class="hidden-sm-up"><i class="ti-hand-point-right"></i></span> <span class="hidden-xs-down">Overtime</span></a> </li>
										<li class="nav-item"> <a class="nav-link" data-toggle="tab" href="#Salary" role="tab"><span class="hidden-sm-up"><i class="ti-hand-point-right"></i></span> <span class="hidden-xs-down">Salary</span></a> </li>
										<li class="nav-item"> <a class="nav-link" data-toggle="tab" href="#Activity" role="tab"><span class="hidden-sm-up"><i class="ti-hand-point-right"></i></span> <span class="hidden-xs-down">Activity</span></a> </li>
                                    </ul>
                                    <!-- Tab panes -->
                                    <div class="tab-content">
                                        <div class="tab-pane active" id="BasicDetails" role="tabpanel">
                                            <div class="">
                                                <h3>Basic Details </h3>
												<button type="button" class="btn btn-info d-none d-lg-block m-l-15" data-toggle="modal" data-target="#update" style="float:right">Update</button>
												<div class="table-responsive">
												<table class="table">
													<tbody>
														<tr>
															<td>Name</td>
															<td>Test</td>
														</tr>
														<tr>
															<td>Email</td>
															<td>Test@gmail.com</td>
														</tr>
														<tr>
															<td>Photo</td>
															<td><img src="https://cdn.shopify.com/shopifycloud/hatchful-web/assets/6fcc76cfd1c59f44d43a485167fb3139.png" height="100px"></td>
														</tr>
														<tr>
															<td>Ward No</td>
															<td>#54324</td>
														</tr>
														<tr>
															<td>Select role</td>
															<td>Peon</td>
														</tr>
														<tr>
															<td>RFID Number</td>
															<td>7488</td>
														</tr>
														<tr>
															<td>Date of joining</td>
															<td>04-08-2020</td>
														</tr>
													</tbody>
											  </table>
                                            </div>
                                            </div>
                                      
									  </div>
									  
                                        <div class="tab-pane" id="Stamp" role="tabpanel">
										  
                                                <h3>Stamp </h3>
												<button type="button" class="btn btn-info d-none d-lg-block m-l-15" data-toggle="modal" data-target=".bs-example-modal-lg" style="float:right">Add New Stamp</button>
												
											<div class="table-responsive">
												<table class="table">
													<thead>
														<tr>
															<th>#</th>
															<th>Name of Stamp</th>
															<th>Upload Stamp</th>
															<th>Status</th>
															<th>Date</th>
															<th>Action</th>
														</tr>
													</thead>
													<tbody>
														<tr>
															<td>1</td>
															<td>Test</td>
															<td><img src="https://cdn.shopify.com/shopifycloud/hatchful-web/assets/6fcc76cfd1c59f44d43a485167fb3139.png" height="100px"></td>
															<td>Success</td>
															<td>04-08-2020</td>
															<td>
																<a href = "" class='forum-title' name = "abc" data-toggle="modal" data-target="#update-stamp"><i class="ti-pencil mr-1 btn btn-success"></i></a>
								  <a href = "" class='forum-title' name = "abc"><i class="ti-eye mr-1 btn btn-success"></i></a>
									 <a href = "" class='forum-title'><i class="ti-trash btn btn-danger"></i></a>
															</td>
														</tr>
														
														
													</tbody>
											  </table>
                                            </div>
                                            
                                      
										</div>
                                        
										<div class="tab-pane" id="Bank" role="tabpanel">
											 
                                                <h3>Bank </h3>
												<button type="button" class="btn btn-info d-none d-lg-block m-l-15" data-toggle="modal" data-target=".bs-example-modal-lg" style="float:right">Add New Account</button>
												<div class="table-responsive">
												<table class="table">
                                        <thead>
                                            <tr>
                                                <th>#</th>
                                                <th>Account Name</th>
                                                <th>Account Number</th>
                                                <th>IFSC Code</th>
                                                <th>Bank name</th>
                                                <th>Branch </th>
                                                <th>Action</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            <tr>
                                                <td>1</td>
                                                <td>Deshmukh</td>
                                                <td>Prohaska</td>
                                                <td>Prohaska</td>
                                                <td>Prohaska</td>
                                                <td>@Genelia</td>
                                                <td>
													<a href = "" class='forum-title' name = "abc" data-toggle="modal" data-target="#update-bank"><i class="ti-pencil mr-1 btn btn-success"></i></a>
								  <a href = "" class='forum-title' name = "abc"><i class="ti-eye mr-1 btn btn-success"></i></a>
									 <a href = "" class='forum-title'><i class="ti-trash btn btn-danger"></i></a>
												</td>
                                            </tr>
                                            <tr>
                                                <td>2</td>
                                                <td>Deshmukh</td>
                                                <td>Gaylord</td>
                                                <td>Gaylord</td>
                                                <td>Gaylord</td>
                                                <td>@Ritesh</td>
                                                <td><a href = "" class='forum-title' name = "abc" data-toggle="modal" data-target=".bs-example-modal-lg"><i class="ti-pencil mr-1 btn btn-success"></i></a>
								  <a href = "" class='forum-title' name = "abc"><i class="ti-eye mr-1 btn btn-success"></i></a>
									 <a href = "" class='forum-title'><i class="ti-trash btn btn-danger"></i></a> </td>
                                            </tr>
                                            <tr>
                                                <td>3</td>
                                                <td>Sanghani</td>
                                                <td>Gusikowski</td>
                                                <td>@Govinda</td>
                                                <td>@Govinda</td>
                                                <td>@Govinda</td>
                                                <td><a href = "" class='forum-title' name = "abc" data-toggle="modal" data-target=".bs-example-modal-lg"><i class="ti-pencil mr-1 btn btn-success"></i></a>
								  <a href = "" class='forum-title' name = "abc"><i class="ti-eye mr-1 btn btn-success"></i></a>
									 <a href = "" class='forum-title'><i class="ti-trash btn btn-danger"></i></a></td>
                                            </tr>
                                            
                                        </tbody>
                                    </table>
                                            
                                            </div>
										</div>
                                       
									   <div class="tab-pane" id="Documents" role="tabpanel">
									   <h3>Documents</h3>
										<button type="button" class="btn btn-info d-none d-lg-block m-l-15" data-toggle="modal" data-target=".bs-example-modal-lg" style="float:right">Add New Document</button>
												<div class="table-responsive">
												<table class="table">
                                        <thead>
                                            <tr>
                                                <th>#</th>
                                                <th>Aadhar card</th>
                                                <th>Pan card</th>
                                                <th>Passport</th>
                                                <th>Voter Id</th>
                                                <th>Ration Card </th>
                                                <th>Driving licence </th>
                                                <th>Action</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            <tr>
                                                <td>1</td>
                                                <td><img src="https://cdn.shopify.com/shopifycloud/hatchful-web/assets/6fcc76cfd1c59f44d43a485167fb3139.png" height="50px"></td>
                                                <td><img src="https://cdn.shopify.com/shopifycloud/hatchful-web/assets/6fcc76cfd1c59f44d43a485167fb3139.png" height="50px"></td>
                                                <td><img src="https://cdn.shopify.com/shopifycloud/hatchful-web/assets/6fcc76cfd1c59f44d43a485167fb3139.png" height="50px"></td>
                                                <td><img src="https://cdn.shopify.com/shopifycloud/hatchful-web/assets/6fcc76cfd1c59f44d43a485167fb3139.png" height="50px"></td>
                                                <td><img src="https://cdn.shopify.com/shopifycloud/hatchful-web/assets/6fcc76cfd1c59f44d43a485167fb3139.png" height="50px"></td>
                                                <td><img src="https://cdn.shopify.com/shopifycloud/hatchful-web/assets/6fcc76cfd1c59f44d43a485167fb3139.png" height="50px"></td>
                                                <td>
													<a href = "" class='forum-title' name = "abc" data-toggle="modal" data-target=".bs-example-modal-lg"><i class="ti-pencil mr-1 btn btn-success"></i></a>
								  <a href = "" class='forum-title' name = "abc"><i class="ti-eye mr-1 btn btn-success"></i></a>
									 <a href = "" class='forum-title'><i class="ti-trash btn btn-danger"></i></a>
												</td>
                                            </tr>
             
                                        </tbody>
                                    </table>
									   </div>
									 </div>
						<!--------(Shubham)Code Start From Here--------->			   
                 <div class="tab-pane" id="Task" role="tabpanel">
	               <button type="button" class="btn btn-info d-none d-lg-block m-l-15" data-toggle="modal" data-target=".bs-example-modal-lg" style="float:right">Add New Task</button>
					  <h3 class="pb-2">Task </h3>
						<div class="input-group">
							<input type="text" class="form-control" placeholder="Start date" id="Task_sdate">
							<span class="form-control text-center" width="30%">TO</span>
							<input type="text" class="form-control" placeholder="End date" id="Task_edate">
							<input type="submit" class="form-control btn-primary" value="Search">
					    </div>
						 <div class="table-responsive">
						  <table class="table">
							<thead>
								<tr>
									<th>#</th>
									<th>Task Name</th>
									<th>Assign Task</th>
									<th>Status</th>
									<th>Action</th>
								</tr>
                            </thead>
                              <tbody>
                                <tr>
								 <td>1</td>
								 <td>Deshmukh</td>
								 <td>Prohaska</td>
								 <td>@Genelia</td>
                                 <td>
								   <a href = "" class='forum-title' name = "abc" data-toggle="modal" data-target="#update-task"><i class="ti-pencil mr-1 btn btn-success"></i></a>
								  <a href = "" class='forum-title' name = "abc"><i class="ti-eye mr-1 btn btn-success"></i></a>
									 <a href = "" class='forum-title'><i class="ti-trash btn btn-danger"></i></a>
							     </td>
                                </tr>
							   </tbody>
                             </table>
                            </div>
						  </div>
										
                       <div class="tab-pane" id="TimeCard" role="tabpanel">
						  <h3>Time Card </h3>
							<div class="input-group">
								<input type="text" class="form-control" placeholder="Start date" id="sdate">
								<span class="form-control text-center" width="30%">TO</span>
								<input type="text" class="form-control" placeholder="End date" id="edate">
								<input type="submit" class="form-control btn-primary" value="Search">
							</div>
							<div class="panel-heading">
							  <h4 class="panel-title">
								<strong>Works Hours Details of January-2020</strong>
							  </h4>
							</div>
                           <div class="box-header" style="border-bottom: 1px solid red">
                              <h4 class="box-title" style="font-size: 15px">
                                 <strong>Week : 01 </strong>
                              </h4>
                            </div>
                               <div class="table-responsive">
                                <table class="table table-bordered table-hover">
                                    <thead>
                                    <tr>
                                      <th> 01.01.2020</th>
                                      <th> 01.01.2020</th>
                                      <th> 01.01.2020</th>
                                      <th> 01.01.2020</th>
                                      <th> 01.01.2020</th>
									 </tr>
                                    </thead>
                                    <tbody>
									  <tr>
                                         <td> 0 : 0 m </td>
                                         <td> 0 : 0 m </td>
										 <td>
										   <span style="font-size: 12px;" class="label label-info std_p">Holiday</span>
										  </td>
										  <td> 0 : 0 m </td>
										  <td> 0 : 0 m </td>
                                        </tr>
                                    </tbody>
								 </table>
									<table>
                                        <tbody>
										 <tr>
                                            <td colspan="2" class="text-right">
                                                <strong style="margin-right: 10px; ">Total Working Hour                                                    : </strong>
                                            </td>
                                              <td>0 : 0 m</td>
                                           </tr>
                                         </tbody>
								     </table>
									</div>
									<div class="panel-heading">
							  <h4 class="panel-title">
								<strong>Works Hours Details of January-2020</strong>
							  </h4>
							</div>
                           <div class="box-header" style="border-bottom: 1px solid red">
                              <h4 class="box-title" style="font-size: 15px">
                                 <strong>Week : 01 </strong>
                              </h4>
                            </div>
                               <div class="table-responsive">
                                <table class="table table-bordered table-hover">
                                    <thead>
                                    <tr>
                                      <th> 01.01.2020</th>
                                      <th> 01.01.2020</th>
                                      <th> 01.01.2020</th>
                                      <th> 01.01.2020</th>
                                      <th> 01.01.2020</th>
									 </tr>
                                    </thead>
                                    <tbody>
									  <tr>
                                         <td> 0 : 0 m </td>
                                         <td> 0 : 0 m </td>
										 <td>
										   <span style="font-size: 12px;" class="label label-info std_p">Holiday</span>
										  </td>
										  <td> 0 : 0 m </td>
										  <td> 0 : 0 m </td>
                                        </tr>
                                    </tbody>
								 </table>
									<table>
                                        <tbody>
										 <tr>
                                            <td colspan="2" class="text-right">
                                                <strong style="margin-right: 10px; ">Total Working Hour                                                    : </strong>
                                            </td>
                                              <td>0 : 0 m</td>
                                           </tr>
                                         </tbody>
								     </table>
									</div>
									<div class="panel-heading">
							  <h4 class="panel-title">
								<strong>Works Hours Details of January-2020</strong>
							  </h4>
							</div>
                           <div class="box-header" style="border-bottom: 1px solid red">
                              <h4 class="box-title" style="font-size: 15px">
                                 <strong>Week : 01 </strong>
                              </h4>
                            </div>
                               <div class="table-responsive">
                                <table class="table table-bordered table-hover">
                                    <thead>
                                    <tr>
                                      <th> 01.01.2020</th>
                                      <th> 01.01.2020</th>
                                      <th> 01.01.2020</th>
                                      <th> 01.01.2020</th>
                                      <th> 01.01.2020</th>
									 </tr>
                                    </thead>
                                    <tbody>
									  <tr>
                                         <td> 0 : 0 m </td>
                                         <td> 0 : 0 m </td>
										 <td>
										   <span style="font-size: 12px;" class="label label-info std_p">Holiday</span>
										  </td>
										  <td> 0 : 0 m </td>
										  <td> 0 : 0 m </td>
                                        </tr>
                                    </tbody>
								 </table>
									<table>
                                        <tbody>
										 <tr>
                                            <td colspan="2" class="text-right">
                                                <strong style="margin-right: 10px; ">Total Working Hour                                                    : </strong>
                                            </td>
                                              <td>0 : 0 m</td>
                                           </tr>
                                         </tbody>
								     </table>
									</div>
								</div>
										
               <div class="tab-pane" id="Leaves" role="tabpanel">
				<button type="button" class="btn btn-info d-none d-lg-block m-l-15" data-toggle="modal" data-target=".bs-example-modal-lg" style="float:right">Add New Leave</button>
				 <h3 class="pb-2">Leaves </h3>
				  <div class="input-group">
					<input type="text" class="form-control" placeholder="Start date" id="Leave_sdate">
					<span class="form-control text-center" width="30%">TO</span>
					<input type="text" class="form-control" placeholder="End date" id="Leave_edate">
					<input type="submit" class="form-control btn-primary" value="Search">
				  </div>
                    <div class="panel panel-custom">
					  <!-- Default panel contents -->
						<div class="panel-heading">
							<div class="panel-title">
								<strong>Leave Details Of MD Nayeem</strong>
							</div>
						</div>
						<table class="table">
						 <tbody>
                          <tr>
							   <td><strong> Casual</strong>:</td>
							   <td>16.13/20 </td>
						  </tr>
						   <tr>
							   <td><strong> Sick</strong>:</td>
							   <td>16.13/20 </td>
						  </tr>
						   <tr>
							   <td><strong> Paid</strong>:</td>
							   <td>16.13/20 </td>
						  </tr>
						   <tr>
							   <td><strong> Unpaid</strong>:</td>
							   <td>16.13/20 </td>
						  </tr>
						   <tr>
							<td class="l_report">
								<strong> Total</strong>:
							</td>
							<td class="l_report"> 16.13 /67 </td>
					      </tr>
                        </tbody>
					  </table>
					</div>
					<div class="panel panel-custom">
						<div class="panel-heading">Leave Report</div>
						<div class="panel-body">
							<div id="panelChart5">
								<div class="chart-pie-my flot-chart" style="padding: 0px; position: relative;"><canvas class="flot-base" width="787" height="250" style="direction: ltr; position: absolute; left: 0px; top: 0px; width: 787px; height: 250px;"></canvas><canvas class="flot-overlay" width="787" height="250" style="direction: ltr; position: absolute; left: 0px; top: 0px; width: 787px; height: 250px;"></canvas><div class="legend"><div style="position: absolute; width: 208px; height: 23px; top: 5px; right: 5px; background-color: rgb(255, 255, 255); opacity: 0.85;"> </div><table style="position:absolute;top:5px;right:5px;;font-size:smaller;color:#545454"><tbody><tr><td class="legendColorBox"><div style="border:1px solid #ccc;padding:1px"><div style="width:4px;height:0;border:5px solid #37bc9b;overflow:hidden"></div></div></td><td class="legendLabel">Casual ( <small>Leave Quota: 20 Taken: 16.13</small>)</td></tr></tbody></table></div><div class="pieLabelBackground" style="position: absolute; width: 37px; height: 21px; top: 214.5px; left: 271px; background-color: rgb(34, 34, 34); opacity: 0.8;"></div><span class="pieLabel" id="pieLabel0" style="position: absolute; top: 214.5px; left: 271px;"><div class="flot-pie-label">100%</div></span></div>
							</div>
						</div>
					</div>
				</div>
										
                                        <div class="tab-pane" id="ProvidentFund" role="tabpanel">
											
											<h3 class="pb-2">Provident Fund </h3>
											
                                            <div class="input-group">
                                                <input type="text" class="form-control" placeholder="Start date" id="Provident_sdate">
												<span class="form-control text-center" width="30%">TO</span>
                                                <input type="text" class="form-control" placeholder="End date" id="Provident_edate">
                                                <input type="submit" class="form-control btn-primary" value="Search">
                                            </div>
                                       
                           
												<div class="table-responsive">
												
												<table class="table">
                                        <thead>
                                            <tr>
                                                <th>#</th>
                                                <th>Title</th>
                                                <th>Amount</th>
                                                <th>Start Date</th>
                                                <th>End Date</th>
                                                <th>Status</th>
                                                <th>Action</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            <tr>
                                                <td>1</td>
                                                <td>Deshmukh</td>
                                                <td>&#8377;2000</td>
                                                <td>01-03-2020</td>
                                                <td>03-03-2020</td>
												<td>Success</td>
                                                <td>
													<a href = "" class='forum-title' name = "abc" data-toggle="modal" data-target=".bs-example-modal-lg"><i class="ti-pencil mr-1 btn btn-success"></i></a>
								  <a href = "" class='forum-title' name = "abc"><i class="ti-eye mr-1 btn btn-success"></i></a>
									 <a href = "" class='forum-title'><i class="ti-trash btn btn-danger"></i></a>
												</td>
                                            </tr>

                                        </tbody>
                                    </table>
                                            
                                            </div>
										</div>
                                        
										<div class="tab-pane" id="Overtime" role="tabpanel">
											<h3 class="pb-2">Overtime </h3>
											
                                            <div class="input-group">
                                                <input type="text" class="form-control" placeholder="Start date" id="Overtime_sdate">
												<span class="form-control text-center" width="30%">TO</span>
                                                <input type="text" class="form-control" placeholder="End date" id="Overtime_edate">
                                                <input type="submit" class="form-control btn-primary" value="Search">
                                            </div>
                                       
                           
												<div class="table-responsive">
												
												<table class="table">
                                        <thead>
                                            <tr>
                                                <th>#</th>
                                                <th>Title</th>
                                                <th>Amount</th>
                                                <th>Start Date</th>
                                                <th>End Date</th>
                                                <th>Overtime Hour</th>
                                                <th>Action</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            <tr>
                                                <td>1</td>
                                                <td>Deshmukh</td>
                                                <td>&#8377;2000</td>
                                                <td>01-03-2020</td>
                                                <td>03-03-2020</td>
												<td>3hr-25m-11s</td>
                                                <td>
													<a href = "" class='forum-title' name = "abc" data-toggle="modal" data-target=".bs-example-modal-lg"><i class="ti-pencil mr-1 btn btn-success"></i></a>
								  <a href = "" class='forum-title' name = "abc"><i class="ti-eye mr-1 btn btn-success"></i></a>
									 <a href = "" class='forum-title'><i class="ti-trash btn btn-danger"></i></a>
												</td>
                                            </tr>

                                        </tbody>
                                    </table>
                                            
                                            </div>
										</div>
										
                 <div class="tab-pane" id="Salary" role="tabpanel">
				   <h3 class="pb-2">Salary </h3>
					<div class="input-group">
						<input type="text" class="form-control" placeholder="Start date" id="Salary_sdate">
						<span class="form-control text-center" width="30%">TO</span>
						<input type="text" class="form-control" placeholder="End date" id="Salary_edate">
						<input type="submit" class="form-control btn-primary" value="Search">
					</div>
                       <div class="table-responsive">
						<table class="table">
							<thead>
								<tr>
									<th>#</th>
									<th>Name</th>
									<th>Salary Month</th>
									<th>Date</th>
									<th>Overtime Hour</th>
									<th>Action</th>
								</tr>
							</thead>
                                <tbody>
									<tr>
									<td>1</td>
									<td>Shubham</td>
									<td>January</td>
									<td>03-03-2020</td>
									<td>3hr-25m-11s</td>
									<td>
											<a href = "" class='forum-title' name = "abc" data-toggle="modal" data-target=".bs-example-modal-lg"><i class="ti-pencil mr-1 btn btn-success"></i></a>
						  <a href = "{{route('sallery-pdf')}}" class='forum-title' name = "abc"><i class="ti-eye mr-1 btn btn-success"></i></a>
							 <a href = "" class='forum-title'><i class="ti-trash btn btn-danger"></i></a>
										</td>
									</tr>

                            </tbody>
                          </table>
                        </div>
					</div>
										
                                        <div class="tab-pane" id="Activity" role="tabpanel">
											<h3 class="pb-2">Activity </h3>
											
                                            <div class="input-group">
                                                <input type="text" class="form-control" placeholder="Start date" id="Activity_sdate">
												<span class="form-control text-center" width="30%">TO</span>
                                                <input type="text" class="form-control" placeholder="End date" id="Activity_edate">
                                                <input type="submit" class="form-control btn-primary" value="Search">
                                            </div>
                                       
                           
												<div class="table-responsive">
												
												<table class="table">
                                        <thead>
                                            <tr>
                                                <th>#</th>
                                                <th>Title</th>
                                                <th>Activity</th>
                                                
                                                <th>Date</th>
                                                <th>Status</th>
                                                <th>Action</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            <tr>
                                                <td>1</td>
                                                <td>Deshmukh</td>
                                                <td>Request Document</td>
                                                
                                                <td>03-03-2020</td>
												<td>Success</td>
                                                <td>
													<a href = "" class='forum-title' name = "abc" data-toggle="modal" data-target=".bs-example-modal-lg"><i class="ti-pencil mr-1 btn btn-success"></i></a>
								  <a href = "" class='forum-title' name = "abc"><i class="ti-eye mr-1 btn btn-success"></i></a>
									 <a href = "" class='forum-title'><i class="ti-trash btn btn-danger"></i></a>
												</td>
                                            </tr>

                                        </tbody>
                                    </table>
                                            
                                            </div>
										</div>
										
                                    </div>
                                </div>
                            </div>
                        </div>


    </div>
 </div>



   
@endsection

@section('js')

 <script>
    // MAterial Date picker    
    $('#sdate').bootstrapMaterialDatePicker({ weekStart: 0, time: false });
    $('#edate').bootstrapMaterialDatePicker({ weekStart: 0, time: false });
    $('#Task_edate').bootstrapMaterialDatePicker({ weekStart: 0, time: false });
    $('#Task_sdate').bootstrapMaterialDatePicker({ weekStart: 0, time: false });
    $('#Leave_sdate').bootstrapMaterialDatePicker({ weekStart: 0, time: false });
    $('#Leave_edate').bootstrapMaterialDatePicker({ weekStart: 0, time: false });
    $('#Provident_edate').bootstrapMaterialDatePicker({ weekStart: 0, time: false });
    $('#Provident_sdate').bootstrapMaterialDatePicker({ weekStart: 0, time: false });
    $('#Overtime_sdate').bootstrapMaterialDatePicker({ weekStart: 0, time: false });
    $('#Overtime_edate').bootstrapMaterialDatePicker({ weekStart: 0, time: false });
    $('#Salary_edate').bootstrapMaterialDatePicker({ weekStart: 0, time: false });
    $('#Salary_sdate').bootstrapMaterialDatePicker({ weekStart: 0, time: false });
    $('#Activity_sdate').bootstrapMaterialDatePicker({ weekStart: 0, time: false });
    $('#Activity_edate').bootstrapMaterialDatePicker({ weekStart: 0, time: false });
	
	
	
    $('#timepicker').bootstrapMaterialDatePicker({ format: 'HH:mm', time: true, date: false });
	
	
    $('#date-format').bootstrapMaterialDatePicker({ format: 'dddd DD MMMM YYYY - HH:mm' });

    $('#min-date').bootstrapMaterialDatePicker({ format: 'DD/MM/YYYY HH:mm', minDate: new Date() });
    // Clock pickers
    $('#single-input').clockpicker({
        placement: 'bottom',
        align: 'left',
        autoclose: true,
        'default': 'now'
    });
    $('.clockpicker').clockpicker({
        donetext: 'Done',
    }).find('input').change(function() {
        console.log(this.value);
    });
    $('#check-minutes').click(function(e) {
        // Have to stop propagation here
        e.stopPropagation();
        input.clockpicker('show').clockpicker('toggleView', 'minutes');
    });
    if (/mobile/i.test(navigator.userAgent)) {
        $('input').prop('readOnly', true);
    }
    // Colorpicker
    $(".colorpicker").asColorPicker();
    $(".complex-colorpicker").asColorPicker({
        mode: 'complex'
    });
    $(".gradient-colorpicker").asColorPicker({
        mode: 'gradient'
    });
    // Date Picker
    jQuery('.mydatepicker, #datepicker').datepicker();
    jQuery('#datepicker-autoclose').datepicker({
        autoclose: true,
        todayHighlight: true
    });
    jQuery('#date-range').datepicker({
        toggleActive: true
    });
    jQuery('#datepicker-inline').datepicker({
        todayHighlight: true
    });

	// -------------------------------
	// Start Date Range Picker
	// -------------------------------

    // Basic Date Range Picker
    $('.daterange').daterangepicker();

    // Date & Time
    $('.datetime').daterangepicker({
        timePicker: true,
        timePickerIncrement: 30,
        locale: {
            format: 'MM/DD/YYYY h:mm A'
        }
    });

    //Calendars are not linked
    $('.timeseconds').daterangepicker({
        timePicker: true,
        timePickerIncrement: 30,
        timePicker24Hour: true,
        timePickerSeconds: true,
        locale: {
            format: 'MM-DD-YYYY h:mm:ss'
        }
    });

    // Single Date Range Picker
    $('.singledate').daterangepicker({
        singleDatePicker: true,
        showDropdowns: true
    });

    // Auto Apply Date Range
    $('.autoapply').daterangepicker({
        autoApply: true,
    });

    // Calendars are not linked
    $('.linkedCalendars').daterangepicker({
        linkedCalendars: false,
    });

    // Date Limit
    $('.dateLimit').daterangepicker({
        dateLimit: {
            days: 7
        },
    });

    // Show Dropdowns
    $('.showdropdowns').daterangepicker({
        showDropdowns: true,
    });

    // Show Week Numbers
    $('.showweeknumbers').daterangepicker({
        showWeekNumbers: true,
    });

     $('.dateranges').daterangepicker({
        ranges: {
            'Today': [moment(), moment()],
            'Yesterday': [moment().subtract(1, 'days'), moment().subtract(1, 'days')],
            'Last 7 Days': [moment().subtract(6, 'days'), moment()],
            'Last 30 Days': [moment().subtract(29, 'days'), moment()],
            'This Month': [moment().startOf('month'), moment().endOf('month')],
            'Last Month': [moment().subtract(1, 'month').startOf('month'), moment().subtract(1, 'month').endOf('month')]
        }
    });

    // Always Show Calendar on Ranges
    $('.shawCalRanges').daterangepicker({
        ranges: {
            'Today': [moment(), moment()],
            'Yesterday': [moment().subtract(1, 'days'), moment().subtract(1, 'days')],
            'Last 7 Days': [moment().subtract(6, 'days'), moment()],
            'Last 30 Days': [moment().subtract(29, 'days'), moment()],
            'This Month': [moment().startOf('month'), moment().endOf('month')],
            'Last Month': [moment().subtract(1, 'month').startOf('month'), moment().subtract(1, 'month').endOf('month')]
        },
        alwaysShowCalendars: true,
    });

    // Top of the form-control open alignment
    $('.drops').daterangepicker({
        drops: "up" // up/down
    });

    // Custom button options
    $('.buttonClass').daterangepicker({
        drops: "up",
        buttonClasses: "btn",
        applyClass: "btn-info",
        cancelClass: "btn-danger"
    });

	jQuery('#date-range').datepicker({
        toggleActive: true
    });
    jQuery('#datepicker-inline').datepicker({
        todayHighlight: true
    });

    // Daterange picker
    $('.input-daterange-datepicker').daterangepicker({
        buttonClasses: ['btn', 'btn-sm'],
        applyClass: 'btn-danger',
        cancelClass: 'btn-inverse'
    });
    $('.input-daterange-timepicker').daterangepicker({
        timePicker: true,
        format: 'MM/DD/YYYY h:mm A',
        timePickerIncrement: 30,
        timePicker12Hour: true,
        timePickerSeconds: false,
        buttonClasses: ['btn', 'btn-sm'],
        applyClass: 'btn-danger',
        cancelClass: 'btn-inverse'
    });
    $('.input-limit-datepicker').daterangepicker({
        format: 'MM/DD/YYYY',
        minDate: '06/01/2015',
        maxDate: '06/30/2015',
        buttonClasses: ['btn', 'btn-sm'],
        applyClass: 'btn-danger',
        cancelClass: 'btn-inverse',
        dateLimit: {
            days: 6
        }
    });




    </script>

    <!--=========================*
            This Page Scripts
    *===========================-->
    <!-- Fancy Box Js -->
    <script src="{{asset('public/assets/js/jquery.fancybox.pack.js')}}"></script>
    <script src="{{asset('public/assets/js/init/fancy.js')}}"></script>

    <!-- Data Table js -->
    <script src="{{asset('public/assets/vendors/data-table/js/jquery.dataTables.js')}}"></script>
    <script src="{{asset('public/assets/vendors/data-table/js/jquery.dataTables.min.js')}}"></script>
    <script src="{{asset('public/assets/vendors/data-table/js/dataTables.bootstrap4.min.js')}}"></script>
    <script src="{{asset('public/assets/vendors/data-table/js/dataTables.responsive.min.js')}}"></script>
    <script src="{{asset('public/assets/vendors/data-table/js/responsive.bootstrap.min.js')}}"></script>

    <!-- Data table Init -->
    <script src="{{asset('public/assets/js/init/data-table.js')}}"></script>
@endsection
