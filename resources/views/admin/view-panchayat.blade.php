@extends('layouts.app')


{{-- Page Title --}}
@section('page-title')
    Profile Page
@endsection

{{-- This Page Css --}}
@section('css')
<body class="skin-blue fixed-layout">
  <div class="preloader">
	<div class="loader">
		<div class="loader__figure"></div>
		<p class="loader__label">Panchyat</p>
	</div>
   </div>
   <div id="main-wrapper">
<style>
.img {
	height: 38px;
    width: 35px;
    border-radius: 36px;
}
.card
{
	width: 100%;
}
.pro_img {
  position: relative;
  text-align: center;
  color: white;
}

.bottom-left {
  position: absolute;
  bottom: 8px;
  font-size:20px;
  font-weight:bold;
  left: 16px;
}

.top-left {
  position: absolute;
  top: 8px;
  left: 16px;
}

.top-right {
  position: absolute;
  top: 8px;
  right: 16px;
}

.bottom-right {
  position: absolute;
  bottom: 8px;
  right: 16px;
}

.centered {
  position: absolute;
  top: 50%;
  left: 50%;
  transform: translate(-50%, -50%);
}

</style>
@endsection

@section('main-content')

 <div class="page-wrapper">
	<div class="container-fluid">
		<div class="row page-titles">
			<div class="col-md-5 align-self-center">
				<h4 class="text-themecolor">Panchyat User</h4>
			</div>
			<div class="col-md-7 align-self-center text-right">
				<div class="d-flex justify-content-end align-items-center">
					<ol class="breadcrumb">
						<li class="breadcrumb-item"><a href="javascript:void(0)">Home</a></li>
						<li class="breadcrumb-item active">Panchyat User</li>
					</ol>
				</div>
			</div>
		</div>
		
		
		 <div class="row">
			<div class="pro_img">
			  <img src="https://s3-us-west-2.amazonaws.com/s.cdpn.io/3/cheese-curds.jpg" alt="Snow" style="width:100%;height:300px;">
			  <div class="bottom-left">Panchayat Name</div>
			 </div>
		   <div class="card">
                            <div class="card-body">
                                <h4 class="card-title pb-3">My Information: </h4>
                                
                                <!-- Nav tabs -->
                                <div class="vtabs">
                                    <ul class="nav nav-tabs tabs-vertical" role="tablist">
                                        <li class="nav-item"> <a class="nav-link active" data-toggle="tab" href="#BasicDetails" role="tab"><span class="hidden-sm-up"><i class="ti-hand-point-right"></i></span> <span class="hidden-xs-down">Basic Details</span> </a> </li>
                                        <li class="nav-item"> <a class="nav-link" data-toggle="tab" href="#member" role="tab"><span class="hidden-sm-up"><i class="ti-hand-point-right"></i></span> <span class="hidden-xs-down">Member</span></a> </li>
                                        <li class="nav-item"> <a class="nav-link" data-toggle="tab" href="#Bank" role="tab"><span class="hidden-sm-up"><i class="ti-hand-point-right"></i></span> <span class="hidden-xs-down">Staff</span></a> </li>
										<li class="nav-item"> <a class="nav-link" data-toggle="tab" href="#Documents" role="tab"><span class="hidden-sm-up"><i class="ti-hand-point-right"></i></span> <span class="hidden-xs-down">Setting</span></a> </li>
										<li class="nav-item"> <a class="nav-link" data-toggle="tab" href="#Task" role="tab"><span class="hidden-sm-up"><i class="ti-hand-point-right"></i></span> <span class="hidden-xs-down">Timing</span></a> </li>
										<li class="nav-item"> <a class="nav-link" data-toggle="tab" href="#TimeCard" role="tab"><span class="hidden-sm-up"><i class="ti-hand-point-right"></i></span> <span class="hidden-xs-down">RFID Device</span></a> </li>
									 </ul>
                                    <!-- Tab panes -->
                                    <div class="tab-content">
                                        <div class="tab-pane active" id="BasicDetails" role="tabpanel">
                                            <div class="">
                                                <h3>Basic Details </h3>
												<div class="table-responsive">
												<table class="table">
													<tbody>
														@foreach ($all as $value)
                                                        <tr>
                                                            <th scope="row">Panchayat Name</th>
                                                            <td>{{$value->panc_name}}</td>
                                                        </tr>
                                                        <tr>
                                                            <th scope="row"> Panchayat Address</th>
                                                            <td>{{$value->panc_address}}</td>
                                                        </tr>
                                                        <tr>
                                                            <th scope="row">Panchayat Number</th>
                                                            <td>{{$value->panc_number}}</td>
                                                        </tr>
                                                        <tr>
                                                            <th scope="row">Panchayat Email</th>
                                                            <td>{{$value->panc_email}}</td>
                                                        </tr>
                                                        <tr>
                                                            <th scope="row">Panchayat App Logo</th>
                                                            <td><img src="{{asset('public/upload/panchayat/panchayat1/logo')}}/{{$value->panc_app_logo}}" class="img" alt="{{$value->panc_app_logo}}"></img></td>
                                                        </tr>
														 <tr>
                                                            <th scope="row">Panchayat Latitude</th>
                                                            <td>{{$value->panc_lat}}</td>
                                                        </tr>
                                                        <tr>
                                                            <th scope="row">Panchayat Longitude</th>
                                                            <td>{{$value->panc_long}}</td>
                                                        </tr>
                                                         <tr>
                                                            <th scope="row">Date</th>
                                                            <td>{{$value->created_at}}</td>
                                                        </tr>
														  <tr>
                                                            <th scope="row">Panchayat Domain</th>
                                                            <td><a href="#">{{$value->panc_domain}}</a></td>
                                                        </tr>
														<tr>
                                                            <th scope="row">Panchayat Logo</th>
                                                            <td><img src="{{asset('public/upload/panchayat/panchayat1/logo')}}/{{$value->panc_logo}}" class="img" alt="{{$value->panc_logo}}"></img></td>
                                                        </tr>
															@endforeach
                                                        </tbody>
												 </tbody>
											  </table>
                                            </div>
                                            </div>
                                       </div>
									  
                                        <div class="tab-pane" id="member" role="tabpanel">
										  <h3>All Member</h3>
											<!--button type="button" class="btn btn-info d-none d-lg-block m-l-15" data-toggle="modal" data-target=".bs-example-modal-lg" style="float:right">Add New Stamp</button-->
											<div class="table-responsive">
												<table class="table">
													<thead>
														<tr>
															<th>Sr No</th>
															<th>Profile</th>
															<th>Name</th>
															<th>Email</th>
															<th>Mobile</th>
															<th>Ward No</th>
															<th>View</th>
														</tr>
													</thead>
														@php
										                $num =1;
														@endphp
														@foreach($Panchayat_user as $value)
													<tr>
													   <td>{{$num}}</td>
														 <td><img src="{{asset('public/upload/panchayat/panchayat1/role/LDC')}}/{{$value->profile}}" class="img" alt="{{$value->profile}}"></img></td>
														<td>{{$value->name}}</td>
														<td>{{$value->email}}</td>
														<td>{{$value->mobile}}</td>
														<td>{{$value->ward_number}}</td>
														<td> <a href = "{{route('show_panchayat',$value->id)}}" class='forum-title' name = "abc" ><i class="ti-eye mr-1 btn btn-success"></i></a></td>
													</tr>
													@php
													$num++;
													@endphp
													@endforeach
													</tbody>
												</table>
											</div>
										</div>
                                        
										<div class="tab-pane" id="staff" role="tabpanel">
										  <h3>All Staff</h3>
											<div class="table-responsive">
												<table class="table">
													<thead>
														<tr>
															<th>Sr No</th>
															<th>Profile</th>
															<th>Name</th>
															<th>Email</th>
															<th>Mobile</th>
															<th>Ward No</th>
															<th>View</th>
														</tr>
													</thead>
														@php
										                $num =1;
														@endphp
														@foreach($staff as $value)
													<tr>
													   <td>{{$num}}</td>
														 <td><img src="{{asset('public/upload/panchayat/panchayat1/role/LDC')}}/{{$value->profile}}" class="img" alt="{{$value->profile}}"></img></td>
														<td>{{$value->name}}</td>
														<td>{{$value->email}}</td>
														<td>{{$value->mobile}}</td>
														<td>{{$value->ward_number}}</td>
														<td> <a href = "{{route('show_panchayat',$value->id)}}" class='forum-title' name = "abc" ><i class="ti-eye mr-1 btn btn-success"></i></a></td>
													</tr>
													@php
													$num++;
													@endphp
													@endforeach
													</tbody>
												</table>
											</div>
										</div>
                                        
                                       
									   <div class="tab-pane" id="Documents" role="tabpanel">
									   <h3>Documents</h3>
										<button type="button" class="btn btn-info d-none d-lg-block m-l-15" data-toggle="modal" data-target=".bs-example-modal-lg" style="float:right">Add New Document</button>
												<div class="table-responsive">
												<table class="table">
                                        <thead>
                                            <tr>
                                                <th>#</th>
                                                <th>Aadhar card</th>
                                                <th>Pan card</th>
                                                <th>Passport</th>
                                                <th>Voter Id</th>
                                                <th>Ration Card </th>
                                                <th>Driving licence </th>
                                                <th>Action</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            <tr>
                                                <td>1</td>
                                                <td><img src="https://cdn.shopify.com/shopifycloud/hatchful-web/assets/6fcc76cfd1c59f44d43a485167fb3139.png" height="50px"></td>
                                                <td><img src="https://cdn.shopify.com/shopifycloud/hatchful-web/assets/6fcc76cfd1c59f44d43a485167fb3139.png" height="50px"></td>
                                                <td><img src="https://cdn.shopify.com/shopifycloud/hatchful-web/assets/6fcc76cfd1c59f44d43a485167fb3139.png" height="50px"></td>
                                                <td><img src="https://cdn.shopify.com/shopifycloud/hatchful-web/assets/6fcc76cfd1c59f44d43a485167fb3139.png" height="50px"></td>
                                                <td><img src="https://cdn.shopify.com/shopifycloud/hatchful-web/assets/6fcc76cfd1c59f44d43a485167fb3139.png" height="50px"></td>
                                                <td><img src="https://cdn.shopify.com/shopifycloud/hatchful-web/assets/6fcc76cfd1c59f44d43a485167fb3139.png" height="50px"></td>
                                                <td>
													<a href = "" class='forum-title' name = "abc" data-toggle="modal" data-target=".bs-example-modal-lg"><i class="ti-pencil mr-1 btn btn-success"></i></a>
								  <a href = "" class='forum-title' name = "abc"><i class="ti-eye mr-1 btn btn-success"></i></a>
									 <a href = "" class='forum-title'><i class="ti-trash btn btn-danger"></i></a>
												</td>
                                            </tr>
             
                                        </tbody>
                                    </table>
									   </div>
									 </div>
						<!--------(Shubham)Code Start From Here--------->			   
                 <div class="tab-pane" id="Task" role="tabpanel">
	              <h3 class="pb-2">Staff Timing </h3>
				   <button type="button" class="btn btn-info d-none d-lg-block m-l-15" data-toggle="modal" data-target="#timing" style="float:right">Edit Timing</button>
						<!--div class="input-group">
							<input type="text" class="form-control" placeholder="Start date" id="Task_sdate">
							<span class="form-control text-center" width="30%">TO</span>
							<input type="text" class="form-control" placeholder="End date" id="Task_edate">
							<input type="submit" class="form-control btn-primary" value="Search">
					    </div-->
						 <div class="table-responsive">
						  <table class="table">
							<thead>
								<tr>
									<th>Check In</th>
									<th>Check Out</th>
									<th>Late </th>
									<th>Absent</th>
								</tr>
                            </thead>
                              <tbody>
                                <tr>
								 <td>09:00</td>
								 <td>07:00</td>
								 <td>After 09:30</td>
                                 <td>After Luntch (01:30)</td>
								</tr>
							   </tbody>
                             </table>
                            </div>
						  </div>
<!--============================================================== 
						Stamp Update
   ============================================================== -->
<div class="modal bs-example-modal-lg" id="timing" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true" style="display: none;">
 <div class="modal-dialog ">
	<div class="modal-content">
		<div class="modal-header">
			<h4 class="modal-title" id="myLargeModalLabel">Edit Timing</h4>
			<button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
		</div>
		<div class="modal-body">
			<form action="" method ="post" class="needs-validation" novalidate="" enctype="multipart/form-data">
			  @csrf
				<div class="form-row">
				  <div class="col-md-6">
					<label for="validationCustom01">Enter Check In Time</label>
					<input type="text" name="name"  class="form-control" id="validationCustom01" placeholder="check in time..."  required="">
				  </div>
					<div class="col-md-6 mb-3">
						<label for="validationCustom01">Enter Check Out Time</label>
					<input type="text" name="name"  class="form-control" id="validationCustom01" placeholder="check out time..."  required="">
					</div>
				</div>
				<div class="form-row">
				  <div class="col-md-6">
					<label for="validationCustom01">Late Time</label>
					<input type="text" name="name"  class="form-control" id="validationCustom01" placeholder="late time..."  required="">
				  </div>
					<div class="col-md-6 mb-3">
						<label for="validationCustom01">Absent Time</label>
					<input type="text" name="name"  class="form-control" id="validationCustom01" placeholder="Absent time..."  required="">
					</div>
				</div>
			<button class="btn btn-primary" type="submit">Submit form</button>
		  </form>
		</div>
		<div class="modal-footer">
			<button type="button" class="btn btn-danger waves-effect text-left" data-dismiss="modal">Close</button>
		</div>
	</div>
	<!-- /.modal-content -->
</div>
</div>
						  
				<div class="tab-pane" id="TimeCard" role="tabpanel">
	              <h3 class="pb-2">RFID Device</h3>
				   <button type="button" class="btn btn-info d-none d-lg-block m-l-15" data-toggle="modal" data-target="#rfid" style="float:right">Edit RFID</button>
					<div class="table-responsive">
						  <table class="table">
							<thead>
								<tr>
									<th>SSID Num</th>
									<th>MID Num</th>
								</tr>
                            </thead>
                              <tbody>
                                <tr>
								 <td>RD12345</td>
								 <td>5345</td>
								 </tr>
							   </tbody>
                             </table>
                            </div>
						  </div>
   <!-- ============================================================== 
						Rfid
   ============================================================== -->
<div class="modal bs-example-modal-lg" id="rfid" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true" style="display: none;">
 <div class="modal-dialog ">
	<div class="modal-content">
		<div class="modal-header">
			<h4 class="modal-title" id="myLargeModalLabel">Edit RFID</h4>
			<button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
		</div>
		<div class="modal-body">
			<form action="" method ="post" class="needs-validation" novalidate="" enctype="multipart/form-data">
			  @csrf
				<div class="form-row">
				  <div class="col-md-6">
					<label for="validationCustom01">Enter SSID</label>
					<input type="text" name="name" value="RF12345" class="form-control" id="validationCustom01" placeholder="SSID..."  required="">
				  </div>
					<div class="col-md-6 mb-3">
						<label for="validationCustom01">Enter MID</label>
					<input type="text" name="name" value="5345" class="form-control" id="validationCustom01" placeholder="MID..."  required="">
					</div>
				</div>
			<button class="btn btn-primary" type="submit">Submit form</button>
		  </form>
		</div>
		<div class="modal-footer">
			<button type="button" class="btn btn-danger waves-effect text-left" data-dismiss="modal">Close</button>
		</div>
	</div>
	<!-- /.modal-content -->
</div>
</div>
										
               <div class="tab-pane" id="Leaves" role="tabpanel">
				<button type="button" class="btn btn-info d-none d-lg-block m-l-15" data-toggle="modal" data-target=".bs-example-modal-lg" style="float:right">Add New Leave</button>
				 <h3 class="pb-2">Leaves </h3>
				  <div class="input-group">
					<input type="text" class="form-control" placeholder="Start date" id="Leave_sdate">
					<span class="form-control text-center" width="30%">TO</span>
					<input type="text" class="form-control" placeholder="End date" id="Leave_edate">
					<input type="submit" class="form-control btn-primary" value="Search">
				  </div>
                    <div class="panel panel-custom">
					  <!-- Default panel contents -->
						<div class="panel-heading">
							<div class="panel-title">
								<strong>Leave Details Of MD Nayeem</strong>
							</div>
						</div>
						<table class="table">
						 <tbody>
                          <tr>
							   <td><strong> Casual</strong>:</td>
							   <td>16.13/20 </td>
						  </tr>
						   <tr>
							   <td><strong> Sick</strong>:</td>
							   <td>16.13/20 </td>
						  </tr>
						   <tr>
							   <td><strong> Paid</strong>:</td>
							   <td>16.13/20 </td>
						  </tr>
						   <tr>
							   <td><strong> Unpaid</strong>:</td>
							   <td>16.13/20 </td>
						  </tr>
						   <tr>
							<td class="l_report">
								<strong> Total</strong>:
							</td>
							<td class="l_report"> 16.13 /67 </td>
					      </tr>
                        </tbody>
					  </table>
					</div>
					<div class="panel panel-custom">
						<div class="panel-heading">Leave Report</div>
						<div class="panel-body">
							<div id="panelChart5">
								<div class="chart-pie-my flot-chart" style="padding: 0px; position: relative;"><canvas class="flot-base" width="787" height="250" style="direction: ltr; position: absolute; left: 0px; top: 0px; width: 787px; height: 250px;"></canvas><canvas class="flot-overlay" width="787" height="250" style="direction: ltr; position: absolute; left: 0px; top: 0px; width: 787px; height: 250px;"></canvas><div class="legend"><div style="position: absolute; width: 208px; height: 23px; top: 5px; right: 5px; background-color: rgb(255, 255, 255); opacity: 0.85;"> </div><table style="position:absolute;top:5px;right:5px;;font-size:smaller;color:#545454"><tbody><tr><td class="legendColorBox"><div style="border:1px solid #ccc;padding:1px"><div style="width:4px;height:0;border:5px solid #37bc9b;overflow:hidden"></div></div></td><td class="legendLabel">Casual ( <small>Leave Quota: 20 Taken: 16.13</small>)</td></tr></tbody></table></div><div class="pieLabelBackground" style="position: absolute; width: 37px; height: 21px; top: 214.5px; left: 271px; background-color: rgb(34, 34, 34); opacity: 0.8;"></div><span class="pieLabel" id="pieLabel0" style="position: absolute; top: 214.5px; left: 271px;"><div class="flot-pie-label">100%</div></span></div>
							</div>
						</div>
					</div>
				</div>
			  </div>
	        </div>
	    </div>
     </div>
   </div>
 </div>



   
@endsection

@section('js')

 <script>
    // MAterial Date picker    
    $('#sdate').bootstrapMaterialDatePicker({ weekStart: 0, time: false });
    $('#edate').bootstrapMaterialDatePicker({ weekStart: 0, time: false });
    $('#Task_edate').bootstrapMaterialDatePicker({ weekStart: 0, time: false });
    $('#Task_sdate').bootstrapMaterialDatePicker({ weekStart: 0, time: false });
    $('#Leave_sdate').bootstrapMaterialDatePicker({ weekStart: 0, time: false });
    $('#Leave_edate').bootstrapMaterialDatePicker({ weekStart: 0, time: false });
    $('#Provident_edate').bootstrapMaterialDatePicker({ weekStart: 0, time: false });
    $('#Provident_sdate').bootstrapMaterialDatePicker({ weekStart: 0, time: false });
    $('#Overtime_sdate').bootstrapMaterialDatePicker({ weekStart: 0, time: false });
    $('#Overtime_edate').bootstrapMaterialDatePicker({ weekStart: 0, time: false });
    $('#Salary_edate').bootstrapMaterialDatePicker({ weekStart: 0, time: false });
    $('#Salary_sdate').bootstrapMaterialDatePicker({ weekStart: 0, time: false });
    $('#Activity_sdate').bootstrapMaterialDatePicker({ weekStart: 0, time: false });
    $('#Activity_edate').bootstrapMaterialDatePicker({ weekStart: 0, time: false });
	
	
	
    $('#timepicker').bootstrapMaterialDatePicker({ format: 'HH:mm', time: true, date: false });
	
	
    $('#date-format').bootstrapMaterialDatePicker({ format: 'dddd DD MMMM YYYY - HH:mm' });

    $('#min-date').bootstrapMaterialDatePicker({ format: 'DD/MM/YYYY HH:mm', minDate: new Date() });
    // Clock pickers
    $('#single-input').clockpicker({
        placement: 'bottom',
        align: 'left',
        autoclose: true,
        'default': 'now'
    });
    $('.clockpicker').clockpicker({
        donetext: 'Done',
    }).find('input').change(function() {
        console.log(this.value);
    });
    $('#check-minutes').click(function(e) {
        // Have to stop propagation here
        e.stopPropagation();
        input.clockpicker('show').clockpicker('toggleView', 'minutes');
    });
    if (/mobile/i.test(navigator.userAgent)) {
        $('input').prop('readOnly', true);
    }
    // Colorpicker
    $(".colorpicker").asColorPicker();
    $(".complex-colorpicker").asColorPicker({
        mode: 'complex'
    });
    $(".gradient-colorpicker").asColorPicker({
        mode: 'gradient'
    });
    // Date Picker
    jQuery('.mydatepicker, #datepicker').datepicker();
    jQuery('#datepicker-autoclose').datepicker({
        autoclose: true,
        todayHighlight: true
    });
    jQuery('#date-range').datepicker({
        toggleActive: true
    });
    jQuery('#datepicker-inline').datepicker({
        todayHighlight: true
    });

	// -------------------------------
	// Start Date Range Picker
	// -------------------------------

    // Basic Date Range Picker
    $('.daterange').daterangepicker();

    // Date & Time
    $('.datetime').daterangepicker({
        timePicker: true,
        timePickerIncrement: 30,
        locale: {
            format: 'MM/DD/YYYY h:mm A'
        }
    });

    //Calendars are not linked
    $('.timeseconds').daterangepicker({
        timePicker: true,
        timePickerIncrement: 30,
        timePicker24Hour: true,
        timePickerSeconds: true,
        locale: {
            format: 'MM-DD-YYYY h:mm:ss'
        }
    });

    // Single Date Range Picker
    $('.singledate').daterangepicker({
        singleDatePicker: true,
        showDropdowns: true
    });

    // Auto Apply Date Range
    $('.autoapply').daterangepicker({
        autoApply: true,
    });

    // Calendars are not linked
    $('.linkedCalendars').daterangepicker({
        linkedCalendars: false,
    });

    // Date Limit
    $('.dateLimit').daterangepicker({
        dateLimit: {
            days: 7
        },
    });

    // Show Dropdowns
    $('.showdropdowns').daterangepicker({
        showDropdowns: true,
    });

    // Show Week Numbers
    $('.showweeknumbers').daterangepicker({
        showWeekNumbers: true,
    });

     $('.dateranges').daterangepicker({
        ranges: {
            'Today': [moment(), moment()],
            'Yesterday': [moment().subtract(1, 'days'), moment().subtract(1, 'days')],
            'Last 7 Days': [moment().subtract(6, 'days'), moment()],
            'Last 30 Days': [moment().subtract(29, 'days'), moment()],
            'This Month': [moment().startOf('month'), moment().endOf('month')],
            'Last Month': [moment().subtract(1, 'month').startOf('month'), moment().subtract(1, 'month').endOf('month')]
        }
    });

    // Always Show Calendar on Ranges
    $('.shawCalRanges').daterangepicker({
        ranges: {
            'Today': [moment(), moment()],
            'Yesterday': [moment().subtract(1, 'days'), moment().subtract(1, 'days')],
            'Last 7 Days': [moment().subtract(6, 'days'), moment()],
            'Last 30 Days': [moment().subtract(29, 'days'), moment()],
            'This Month': [moment().startOf('month'), moment().endOf('month')],
            'Last Month': [moment().subtract(1, 'month').startOf('month'), moment().subtract(1, 'month').endOf('month')]
        },
        alwaysShowCalendars: true,
    });

    // Top of the form-control open alignment
    $('.drops').daterangepicker({
        drops: "up" // up/down
    });

    // Custom button options
    $('.buttonClass').daterangepicker({
        drops: "up",
        buttonClasses: "btn",
        applyClass: "btn-info",
        cancelClass: "btn-danger"
    });

	jQuery('#date-range').datepicker({
        toggleActive: true
    });
    jQuery('#datepicker-inline').datepicker({
        todayHighlight: true
    });

    // Daterange picker
    $('.input-daterange-datepicker').daterangepicker({
        buttonClasses: ['btn', 'btn-sm'],
        applyClass: 'btn-danger',
        cancelClass: 'btn-inverse'
    });
    $('.input-daterange-timepicker').daterangepicker({
        timePicker: true,
        format: 'MM/DD/YYYY h:mm A',
        timePickerIncrement: 30,
        timePicker12Hour: true,
        timePickerSeconds: false,
        buttonClasses: ['btn', 'btn-sm'],
        applyClass: 'btn-danger',
        cancelClass: 'btn-inverse'
    });
    $('.input-limit-datepicker').daterangepicker({
        format: 'MM/DD/YYYY',
        minDate: '06/01/2015',
        maxDate: '06/30/2015',
        buttonClasses: ['btn', 'btn-sm'],
        applyClass: 'btn-danger',
        cancelClass: 'btn-inverse',
        dateLimit: {
            days: 6
        }
    });




    </script>

    <!--=========================*
            This Page Scripts
    *===========================-->
    <!-- Fancy Box Js -->
    <script src="{{asset('public/assets/js/jquery.fancybox.pack.js')}}"></script>
    <script src="{{asset('public/assets/js/init/fancy.js')}}"></script>

    <!-- Data Table js -->
    <script src="{{asset('public/assets/vendors/data-table/js/jquery.dataTables.js')}}"></script>
    <script src="{{asset('public/assets/vendors/data-table/js/jquery.dataTables.min.js')}}"></script>
    <script src="{{asset('public/assets/vendors/data-table/js/dataTables.bootstrap4.min.js')}}"></script>
    <script src="{{asset('public/assets/vendors/data-table/js/dataTables.responsive.min.js')}}"></script>
    <script src="{{asset('public/assets/vendors/data-table/js/responsive.bootstrap.min.js')}}"></script>

    <!-- Data table Init -->
    <script src="{{asset('public/assets/js/init/data-table.js')}}"></script>
@endsection
