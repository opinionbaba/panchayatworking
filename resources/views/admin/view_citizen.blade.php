@extends('layouts.app')


{{-- Page Title --}}
@section('page-title')
  Accounts
@endsection
{{-- This Page Css --}}
@section('css')
<style>

.customtab li a.nav-link {
    padding: 15px 10px;
}


</style>
   <link rel="stylesheet" type="text/css"
        href="{{asset('public/assets/datatables.net-bs4/css/dataTables.bootstrap4.css')}}">
    <link rel="stylesheet" type="text/css"
        href="{{asset('public/assets/datatables.net-bs4/css/responsive.dataTables.min.css')}}">
    <!--=========================*
               Datatable
  
  *===========================-->
  <body class="skin-blue fixed-layout">
    <!-- ============================================================== -->
    <!-- Preloader - style you can find in spinners.css -->
    <!-- ============================================================== -->
    <div class="preloader">
        <div class="loader">
            <div class="loader__figure"></div>
        </div>
    </div>
	 <div id="main-wrapper">
	  
@endsection
@section('main-content')
        <div class="page-wrapper">
            <div class="container-fluid">
                <div class="row page-titles">
                    <div class="col-md-5 align-self-center">
                        <h4 class="text-themecolor">Datatable</h4>
                    </div>
                    <div class="col-md-7 align-self-center text-right">
                        <div class="d-flex justify-content-end align-items-center">
                            <ol class="breadcrumb">
                                <li class="breadcrumb-item"><a href="javascript:void(0)">Home</a></li>
                                <li class="breadcrumb-item active">Datatable</li>
                            </ol>
                            		
                        </div>
                    </div>
                </div>
				
				<div class="row">
                    <div class="col-md-12">
                        <div class="card">
                            <div class="card-body">
                                <h4 class="card-title">Default Tab</h4>
                                <h6 class="card-subtitle">Use default tab with class <code>nav-tabs & tabcontent-border </code></h6>
                                <!-- Nav tabs -->
                               <ul class="nav nav-tabs customtab" role="tablist">
                                   <li class="nav-item">
                                    <a class="nav-link active" id="home-tab" data-toggle="tab" href="#panchyat" role="tab" aria-controls="home" aria-selected="true">General </a>
                                </li>
                                <li class="nav-item">
                                    <a class="nav-link" id="profile-tab" data-toggle="tab" href="#asset" role="tab" aria-controls="profile" aria-selected="false">Assets </a>
                                </li>
								 <li class="nav-item">
                                    <a class="nav-link" id="profile-tab" data-toggle="tab" href="#employee" role="tab" aria-controls="profile" aria-selected="false">Documents </a>
                                </li>
								 <li class="nav-item">
                                    <a class="nav-link" id="profile-tab" data-toggle="tab" href="#citizen" role="tab" aria-controls="profile" aria-selected="false">Others </a>
                                </li>
								 <li class="nav-item">
                                    <a class="nav-link" id="profile-tab" data-toggle="tab" href="#bank_detail" role="tab" aria-controls="profile" aria-selected="false">Bank Details</a>
                                </li>
								 <li class="nav-item">
                                    <a class="nav-link" id="profile-tab" data-toggle="tab" href="#feedback" role="tab" aria-controls="profile" aria-selected="false">Feedback</a>
                                </li>
								<li class="nav-item">
                                    <a class="nav-link" id="profile-tab" data-toggle="tab" href="#certificate" role="tab" aria-controls="profile" aria-selected="false">Certificate</a>
                                </li>
								<li class="nav-item">
                                    <a class="nav-link" id="profile-tab" data-toggle="tab" href="#tax" role="tab" aria-controls="profile" aria-selected="false">Tax</a>
                                </li>
								<li class="nav-item">
                                    <a class="nav-link" id="profile-tab" data-toggle="tab" href="#rti" role="tab" aria-controls="profile" aria-selected="false">Rti Filled
									</a>
                                </li>
								<li class="nav-item">
                                    <a class="nav-link" id="profile-tab" data-toggle="tab" href="#sms" role="tab" aria-controls="profile" aria-selected="false">SMS Logs</a>
                                </li>
								<li class="nav-item">
                                    <a class="nav-link" id="profile-tab" data-toggle="tab" href="#push" role="tab" aria-controls="profile" aria-selected="false">Push Notifications
									</a>
                                </li>
								<li class="nav-item">
                                    <a class="nav-link" id="profile-tab" data-toggle="tab" href="#send_sms" role="tab" aria-controls="profile" aria-selected="false">Send SMS</a>
                                </li>
                                </ul>
								
								
                                <!-- Tab panes -->
                                <div class="tab-content tabcontent-border">
                                   <div class="tab-pane fade show active" id="panchyat" role="tabpanel" aria-labelledby="home-tab">
										<div class="col-12">
											<div class="card">
												<div class="card-body">
													<h4 class="card_title">Citizen Details</h4>
													  <div class="table-responsive m-t-40">
                                    <table id="example23"
                                        class="display nowrap table table-hover table-striped table-bordered"
                                        cellspacing="0" width="100%">
                                        <thead>
                                            <tr>
												<th>Sr No</th>
												<th>Ward Number</th>
												<th>Landline Number</th>
												<th>State</th>
												<th>City</th>
												<th>Area</th>
												<th>Locality</th>
												<th>Village</th>
												<th>Pin Code</th>
												<th>Date</th>
											</tr>
                                        </thead>
                                        <tfoot>
                                            <tr>
												<th>Sr No</th>
												<th>Ward Number</th>
												<th>Landline Number</th>
												<th>State</th>
												<th>City</th>
												<th>Area</th>
												<th>Locality</th>
												<th>Village</th>
												<th>Pin Code</th>
												<th>Date</th>
											</tr>
                                        </tfoot>
                                        <tbody>
                                           @php
											$num=1;
											@endphp
											@foreach($view_citizen as $value)
											<tr>
												<td>{{$num}}</td>
												<td>{{$value->ward_number}}</td>
												<td>{{$value->land_number}}</td>
												<td>{{$value->state}}</td>
												<td>{{$value->city}}</td>
												<td>{{$value->area}}</td>
												<td>{{$value->locality}}</td>
												<td>{{$value->village}}</td>
												<td>{{$value->pin_code}}</td>
												<td>{{$value->created_at}}</td>
											</tr>
											@php
											$num++;
											@endphp
											@endforeach
                                           
                                        </tbody>
                                    </table>
                                </div>
													
													
												</div>
											</div>
										</div>
										<!-- data table -->
									</div>
                 <div class="tab-pane fade" id="asset" role="tabpanel" aria-labelledby="profile-tab">
                   <div class="col-12">
							 <div class="table-responsive m-t-40">
                                    <table id="myTable" class="table table-bordered table-striped">
                                        <thead>
                                            <tr>
												<th>Sr No</th>
												<th>Ward Number</th>
												<th>Landline Number</th>
												<th>State</th>
												<th>City</th>
												<th>Area</th>
												<th>Locality</th>
												<th>Village</th>
												<th>Pin Code</th>
												<th>Date</th>
											</tr>
                                        </thead>
                                        <tbody>
                                            @php
											$num=1;
											@endphp
											@foreach($view_citizen as $value)
											<tr>
												<td>{{$num}}</td>
												<td>{{$value->ward_number}}</td>
												<td>{{$value->land_number}}</td>
												<td>{{$value->state}}</td>
												<td>{{$value->city}}</td>
												<td>{{$value->area}}</td>
												<td>{{$value->locality}}</td>
												<td>{{$value->village}}</td>
												<td>{{$value->pin_code}}</td>
												<td>{{$value->created_at}}</td>
											</tr>
											@php
											$num++;
											@endphp
											@endforeach
                                        </tbody>
                                    </table>
                                </div>
						
				</div>
				<!-- data table -->
		        </div>
		

		<div class="tab-pane fade" id="employee" role="tabpanel" aria-labelledby="profile-tab">
                   <div class="col-12">
					<div class="card">
					<!-- Modal -->
                    <div class="modal fade" id="add_doc">
                        <div class="modal-dialog modal-dialog-centered" role="document">
                            <div class="modal-content">
                                <div class="modal-header">
                                    <h5 class="modal-title">Add New Document</h5>
                                    <button type="button" class="close" data-dismiss="modal"><span>&times;</span></button>
                                </div>
                               <div class="modal-body">
								<form class="needs-validation" novalidate="">
								<div class="form-row">
									<div class="col-md-6 mb-3">
										<label for="validationCustom01"> Document Name/Number</label>
										<input type="text" name="f_name" class="form-control" id="validationCustom01" placeholder="Document name or number.."  required="">
										<div class="valid-feedback">
											Looks good!
										</div>
									</div>
									<div class="col-md-6 mb-3">
										<label for="validationCustom01">Upload Document</label>
										<input type="file" name="m_name" class="form-control" id="validationCustom01" placeholder="Middle Name.."  required="">
										<div class="valid-feedback">
											Looks good!
										</div>
									</div>
								 </div>
								  <button class="btn btn-primary" type="submit">Submit form</button>
								</form>
							</div>
                           </div>
                        </div>
                    </div>
						
						<div class="card border-warning">
                            <div class="card-header" style="margin-top:20px">
                                <h4 class="m-b-0 clearfix">Document 
								<a href="" data-target="#add_doc" data-toggle="modal" class="btn btn-success float-right">Add Document</a>
                               </h4>
							</div>
                            <div class="card-body">
                                <div class="table-responsive m-t-40">
                                    <table id="document" class="table table-bordered table-striped">
									
									<thead>
									<tr>
										<th>Sr No</th>
										<th>Document Name</th>
										<th>Show Document</th>
									</tr>
									</thead>
									<tbody>
									@php
									$num++;
									@endphp
									@foreach($Citizen_document as $value)
									<tr>
										<td>{{$num}}</td>
										<td>{{$value->aadhar_num}}</td>
										
										 <td> 
										<a href = "" class='forum-title' name = "abc" data-toggle="modal" data-target="#usermodal"><i class="ti-pencil mr-1 btn btn-success"></i></a>
										<a href = "" class='forum-title' name = "abc" data-toggle="modal" data-target="#usermodal"><i class="ti-eye mr-1 btn btn-success"></i></a>
										<a href = "" class='forum-title'><i class="ti-trash btn btn-danger"></i></a>
										</td>
									</tr>
									@php
									$num++;
									@endphp
									@endforeach
									</tbody>
								</table>
							</div>
                            </div>
                        </div>
						
						
						
					</div>
				</div>
				<!-- data table -->
		</div>
	


	    <div class="tab-pane fade" id="citizen" role="tabpanel" aria-labelledby="profile-tab">
			 <div class="card">
				<div class="card-header" style="margin-top:20px">
					<h4 class="m-b-0 clearfix">All Member
					<a href="" data-toggle="modal" data-target="#exampleModalCenter" class="btn btn-success float-right">Add Member</a>
				   </h4>
				</div>
							
				 <!-- Modal -->
                    <div class="modal fade" id="exampleModalCenter">
                        <div class="modal-dialog modal-dialog-centered" role="document">
                            <div class="modal-content">
                                <div class="modal-header">
                                    <h5 class="modal-title">Add New Member</h5>
                                    <button type="button" class="close" data-dismiss="modal"><span>&times;</span></button>
                                </div>
                                <div class="modal-body">
                                                    <form class="needs-validation" novalidate="">
													<div class="form-row">
														<div class="col-md-4 mb-3">
															<label for="validationCustom01"> First Name</label>
															<input type="text" name="f_name" class="form-control" id="validationCustom01" placeholder="First name.."  required="">
															<div class="valid-feedback">
																Looks good!
															</div>
														</div>
														<div class="col-md-4 mb-3">
															<label for="validationCustom01">Middle Name</label>
															<input type="text" name="m_name" class="form-control" id="validationCustom01" placeholder="Middle Name.."  required="">
															<div class="valid-feedback">
																Looks good!
															</div>
														</div>
														<div class="col-md-4 mb-3">
															<label for="validationCustom01">Last Name</label>
															<input type="text" name="m_name" class="form-control" id="validationCustom01" placeholder="Last Name.."  required="">
															<div class="valid-feedback">
																Looks good!
															</div>
														</div>
													 </div>
													 <div class="form-row">
														<div class="col-md-6 mb-3">
															<label for="validationCustom01">Mobile Number</label>
															<input type="number" name="f_name" class="form-control" id="validationCustom01" placeholder="Mobile Number.."  required="">
															<div class="valid-feedback">
																Looks good!
															</div>
														</div>
														<div class="col-md-6 mb-3">
															<label for="validationCustom01">Age</label>
															<input type="number" name="m_name" class="form-control" id="validationCustom01" placeholder="Age.."  required="">
															<div class="valid-feedback">
																Looks good!
															</div>
														</div>
													</div>
													 
													<button class="btn btn-primary" type="submit">Submit form</button>
													
												</form>
											</div>
                               
                            </div>
                        </div>
                    </div>
			
			
				<div class="card-body">
					<div class="table-responsive m-t-40">
						<table id="other" class="table table-bordered table-striped">
							<thead>
							<tr>
								<th>Sr No</th>
								<th>Name</th>
								<th>Number</th>
								<th>Address</th>
								<th>Email</th>
								<th>Domain</th>
								<th>Logo</th>
								<th>Action</th>
							</tr>
							</thead>
                            <tbody>
							  <tr>
								<td>Airi Satou</td>
								<td>Accountant</td>
								<td>Tokyo</td>
								<td>33</td>
								<td>33</td>
								<td>33</td>
								<td>2008/11/28</td>
								 <td> 
								<a href = "" class='forum-title' name = "abc" data-toggle="modal" data-target="#usermodal"><i class="ti-pencil mr-1 btn btn-success"></i></a>
								<a href = "{{route('view_secondry_mem')}}" class='forum-title' name = "abc" ><i class="ti-eye mr-1 btn btn-success"></i></a>
								<a href = "" class='forum-title'><i class="ti-trash btn btn-danger"></i></a>
								</td>
							</tr>
                                   
							</tbody>
						</table>
					</div>
				</div>
			</div>    
		</div>
		
		
		 <div class="tab-pane fade" id="bank_detail" role="tabpanel" aria-labelledby="profile-tab">
                   <div class="card">
				<div class="card-header" style="margin-top:20px">
					<h4 class="m-b-0 clearfix">All Bank Details</h4>
				</div>
							
				<div class="card-body">
					<div class="table-responsive m-t-40">
						<table id="bank-de" class="table table-bordered table-striped">
							<thead>
							<tr>
								<th>Bank Name</th>
								<th>Ac Number</th>
								<th>Branch</th>
								<th>IFSC</th>
								<th>UPI ID</th>
								<th>Phone Pay Num</th>
								<th>Google Pay Num</th>
								<th>Paytm Num</th>
							</tr>
							</thead>
                            <tbody>
							  @foreach($Citizen_bank_detail as $value)
									<tr>
										<td><span class="badge badge-warning">{{$value->bank_name}}</span></td>
										<td>{{$value->ac_number}}</td>
										<td>{{$value->branch_name}}</td>
										<td>{{$value->ifsc}}</td>
										<td>{{$value->upi_id}}</td>
										<td>{{$value->ph_pay_number}}</td>
										<td>{{$value->goo_pay_number}}</td>
										<td>{{$value->pytm_number}}</td>
									</tr>
									@endforeach
                                   
							</tbody>
						</table>
					</div>
				</div>
			</div>    
		</div>
		

		<div class="tab-pane fade" id="feedback" role="tabpanel" aria-labelledby="profile-tab">
           <div class="card">
				<div class="card-header" style="margin-top:20px">
					<h4 class="m-b-0 clearfix">All Feedback 
					<a href="" data-target="#myModal" class="btn btn-success float-right" data-toggle="modal">Add Feedback</a>
                    </h4>
					
					<div class="modal fade" id="myModal">
									<div class="modal-dialog modal-dialog-centered" role="document">
										<div class="modal-content">
											<div class="modal-header">
												<h5 class="modal-title">Update Form</h5>
												<button type="button" class="close" data-dismiss="modal"><span>&times;</span></button>
											</div>
											<div class="modal-body">
											 <form action="" method="post" class="needs-validation" novalidate enctype="multipart/form-data">
													@csrf
												   <div class="form-row">
														<div class="col-md-6">
															<label for="validationTooltip01">Subject</label>
															<input type="text" placeholder="Enter Subject..."name="name" class="form-control"  required>
															<div class="valid-tooltip">
																Looks good!
															</div>
														</div>
														 <div class="col-lg-12">
															<div class="card">
																<div class="card-body">
																	<h4 class="card_title"> Description</h4>
																	<textarea rows="2" name="editor1" class="ck-editor" placeholder="Enter Something.."></textarea>
																</div>
															</div>
														</div>
													</div>
													</div>
												<div class="modal-footer">
													<button class="btn btn-primary" type="submit">Submit form</button>
												<button type="button" class="btn btn-light" data-dismiss="modal">Close</button>
												</div>
												</form>
											</div>
										</div>
									</div>
									
				</div>
							
				<div class="card-body">
					<div class="table-responsive m-t-40">
						<table id="feedback-de" class="table table-bordered table-striped">
							<thead>
							<tr>
								<th>Sr No</th>
								<th>Subject</th>
								<th>Description</th>
								<th>Date</th>
							</tr>
							</thead>
                            <tbody>
							  @php
									$num=1;
									@endphp
										@foreach($Citizen_feedback as $value)
									<tr>
										<td>{{$num}}</td>
										<td>{{$value->subject}}</td>
										<td>{!!$value->description!!}</td>
										<td>{{$value->created_at}}</td>
									</tr>
									@php
									$num++;
									@endphp
									@endforeach
							</tbody>
						</table>
					</div>
				</div>
			</div>       
		</div>
	


	    <div class="tab-pane fade" id="certificate" role="tabpanel" aria-labelledby="profile-tab">
                 <div class="card">
				<div class="card-header" style="margin-top:20px">
					<h4 class="m-b-0 clearfix">All Certificate 
					<a href="" data-target="#myfeed" class="btn btn-success float-right" data-toggle="modal">Certificate Request</a>
                    </h4>
					
					<div class="modal fade" id="myfeed">
									<div class="modal-dialog modal-dialog-centered" role="document">
										<div class="modal-content">
											<div class="modal-header">
												<h5 class="modal-title">Update Form</h5>
												<button type="button" class="close" data-dismiss="modal"><span>&times;</span></button>
											</div>
											<div class="modal-body">
											 <form action="" method="post" class="needs-validation" novalidate enctype="multipart/form-data">
													@csrf
												   <div class="form-row">
														<div class="col-md-12">
															<label for="validationTooltip01">Subject</label>
															<input type="text" placeholder="Enter Subject..."name="name" class="form-control"  required>
															<div class="valid-tooltip">
																Looks good!
															</div>
														</div>
														
													</div>
													</div>
												<div class="modal-footer">
													<button class="btn btn-primary" type="submit">Submit form</button>
												<button type="button" class="btn btn-light" data-dismiss="modal">Close</button>
												</div>
												</form>
											</div>
										</div>
									</div>
									
				</div>
							
				<div class="card-body">
					<div class="table-responsive m-t-40">
						<table id="certificate-de" class="table table-bordered table-striped">
							<thead>
							<tr>
								<th>Sr No</th>
								<th>Name</th>
								<th>Certificate Type</th>
								<th>Request Date</th>
								<th>Issue Date</th>
							</tr>
							</thead>
                            <tbody>
							 <tr>
								<td>1</td>
								<td>Username</td>
								<td>Birth Certificate</td>
								<td>1/25/2020</td>
								<td>2008/11/28</td>
							</tr>
							</tbody>
						</table>
					</div>
				</div>
			</div>       
			</div>
				<!-- data table -->
		
		 <div class="tab-pane fade" id="tax" role="tabpanel" aria-labelledby="profile-tab">
              <div class="card">
				<div class="card-header" style="margin-top:20px">
					<h4 class="m-b-0 clearfix">All Pay Tax
					<a href="" data-target="#mytax" class="btn btn-success float-right" data-toggle="modal">Pay Tax</a>
                    </h4>
					
					<div class="modal fade" id="mytax">
									<div class="modal-dialog modal-dialog-centered" role="document">
										<div class="modal-content">
											<div class="modal-header">
												<h5 class="modal-title">Pay Tax</h5>
												<button type="button" class="close" data-dismiss="modal"><span>&times;</span></button>
											</div>
											<div class="modal-body">
											 <form action="" method="post" class="needs-validation" novalidate enctype="multipart/form-data">
													@csrf
												   <div class="form-row">
														<div class="col-md-12">
															<label for="validationTooltip01">Subject</label>
															<select class="form-control">
																<option value>--Choose House--</option>
																<option value>FZ007</option>
																<option value>AP004</option>
																<option value>PL521</option>
																<option value>JK548</option>
															</select>
															<div class="valid-tooltip">
																Looks good!
															</div>
														</div>
														<div class="col-md-12">
															<label for="validationTooltip01">From Date</label>
																<input type="date" class="form-control" name="start">
															<div class="valid-tooltip">
																Looks good!
															</div>
														</div>
														<div class="col-md-12">
															<label for="validationTooltip01">To Date</label>
															<input type="date" class="form-control" name="to">
															<div class="valid-tooltip">
																Looks good!
															</div>
														</div>
														
													</div>
													</div>
												<div class="modal-footer">
													<button class="btn btn-primary" type="submit">Submit form</button>
												<button type="button" class="btn btn-light" data-dismiss="modal">Close</button>
												</div>
												</form>
											</div>
										</div>
									</div>
									
				</div>
							
				<div class="card-body">
					<div class="table-responsive m-t-40">
						<table id="tax-de" class="table table-bordered table-striped">
							<thead>
							<tr>
								<th>Sr No</th>
								<th>Citizen Name</th>
								<th>House Number</th>
								<th>Amount</th>
								<th>Date</th>
							</tr>
							</thead>
                            <tbody>
							 <tr>
								<td>1</td>
								<td>Shubham</td>
								<td>FZ007</td>
								<td>1000</td>
								<td>2008/11/28</td>
							</tr>
							</tbody>
						</table>
					</div>
				</div>
			</div>            
		 </div>
          


		    <div class="tab-pane fade" id="rti" role="tabpanel" aria-labelledby="profile-tab">
                <div class="card">
				<div class="card-header" style="margin-top:20px">
					<h4 class="m-b-0 clearfix">Rti Filled</h4>
				</div>
							
				<div class="card-body">
					<div class="table-responsive m-t-40">
						<table id="rti-de" class="table table-bordered table-striped">
							<thead>
							<tr>
								<th>Sr No</th>
								<th>Title</th>
								<th>Description</th>
								<th>Amount</th>
								<th>Date</th>
							</tr>
							</thead>
                            <tbody>
							 <tr>
								<td>1</td>
								<td>Shubham</td>
								<td>FZ007</td>
								<td>1000</td>
								<td>2008/11/28</td>
							</tr>
							</tbody>
						</table>
					</div>
				</div>
			</div>        
			</div>
			
			<div class="tab-pane fade" id="sms" role="tabpanel" aria-labelledby="profile-tab">
              <div class="card">
				<div class="card-header" style="margin-top:20px">
					<h4 class="m-b-0 clearfix">SMS Logs</h4>
				</div>
							
				<div class="card-body">
					<div class="table-responsive m-t-40">
						<table id="sms-de" class="table table-bordered table-striped">
							<thead>
							<tr>
								<th>Sr No</th>
								<th>Description</th>
								<th>Date/Time</th>
							</tr>
							</thead>
                            <tbody>
							<tr>
								<td>1</td>
								<td>This is the sms logs</td>
								<td>2008/11/28 12:00</td>
							</tr>
							</tbody>
						</table>
					</div>
				</div>
			  </div>            
			</div>
			

			<div class="tab-pane fade" id="push" role="tabpanel" aria-labelledby="profile-tab">
               <div class="card">
				<div class="card-header" style="margin-top:20px">
					<h4 class="m-b-0 clearfix">Push Notificaion</h4>
				</div>
							
				<div class="card-body">
					<div class="table-responsive m-t-40">
						<table id="push-de" class="table table-bordered table-striped">
							<thead>
							<tr>
								<th>Sr No</th>
								<th>Description</th>
								<th>Date/Time</th>
							</tr>
							</thead>
							<tbody>
							<tr>
								<td>1</td>
								<td>This is the Push Notification</td>
								<td>2008/11/28 12:00</td>
							</tr>
							</tbody>
						</table>
					</div>
				</div>
			  </div>     
			</div>
			
			<div class="tab-pane fade" id="send_sms" role="tabpanel" aria-labelledby="profile-tab">
               <div class="card">
				<div class="card-header" style="margin-top:20px">
					<h4 class="m-b-0 clearfix">Send SMS</h4>
				</div>
							
				<div class="card-body">
					<div class="table-responsive m-t-40">
						<table id="sendsms-de" class="table table-bordered table-striped">
							<thead>
							<tr>
								<th>Sr No</th>
								<th>Description</th>
								<th>Date/Time</th>
							</tr>
							</thead>
							<tbody>
							<tr>
								<td>1</td>
								<td>This is the send sms</td>
								<td>2008/11/28 12:00</td>
							</tr>
							</tbody>
						</table>
					</div>
				</div>
			  </div>    
			</div>
								   
								   
                                </div>
								
                            </div>
                        </div>
						
						
                    </div>
                </div>
				
				
			
			</div>
		</div>
			
@endsection
@section('js')
 <script>
        $(function () {
            $('#myTable').DataTable();
            $('#document').DataTable();
            $('#other').DataTable();
            $('#bank-de').DataTable();
            $('#feedback-de').DataTable();
            $('#certificate-de').DataTable();
            $('#tax-de').DataTable();
            $('#rti-de').DataTable();
            $('#sms-de').DataTable();
            $('#push-de').DataTable();
            $('#sendsms-de').DataTable();
            var table = $('#example').DataTable({
                "columnDefs": [{
                    "visible": false,
                    "targets": 2
                }],
                "order": [
                    [2, 'asc']
                ],
                "displayLength": 25,
                "drawCallback": function (settings) {
                    var api = this.api();
                    var rows = api.rows({
                        page: 'current'
                    }).nodes();
                    var last = null;
                    api.column(2, {
                        page: 'current'
                    }).data().each(function (group, i) {
                        if (last !== group) {
                            $(rows).eq(i).before('<tr class="group"><td colspan="5">' + group + '</td></tr>');
                            last = group;
                        }
                    });
                }
            });
            // Order by the grouping
            $('#example tbody').on('click', 'tr.group', function () {
                var currentOrder = table.order()[0];
                if (currentOrder[0] === 2 && currentOrder[1] === 'asc') {
                    table.order([2, 'desc']).draw();
                } else {
                    table.order([2, 'asc']).draw();
                }
            });
            // responsive table
            $('#config-table').DataTable({
                responsive: true
            });
            $('#example23').DataTable({
                dom: 'Bfrtip',
                buttons: [
                    'copy', 'csv', 'excel', 'pdf', 'print'
                ]
            });
            $('.buttons-copy, .buttons-csv, .buttons-print, .buttons-pdf, .buttons-excel').addClass('btn btn-primary mr-1');
        });

    </script>
   
@endsection
