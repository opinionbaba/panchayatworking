@extends('layouts.app')


{{-- Page Title --}}
@section('page-title')
  Accounts
@endsection
{{-- This Page Css --}}
@section('css')
    <!--=========================*
               Datatable
    *===========================-->
    <!-- Start datatable css -->
    <link rel="stylesheet" type="text/css" href="{{asset('public/assets/vendors/data-table/css/jquery.dataTables.css')}}">
    <link rel="stylesheet" type="text/css" href="{{asset('public/assets/vendors/data-table/css/dataTables.bootstrap4.min.css')}}">
    <link rel="stylesheet" type="text/css" href="{{asset('public/assets/vendors/data-table/css/responsive.bootstrap.min.css')}}">
    <link rel="stylesheet" type="text/css" href="{{asset('public/assets/vendors/data-table/css/responsive.jqueryui.min.css')}}">
@endsection
@section('main-content')
 <div class="row">
			<a href=""><span class="badge badge-info mb-3">Home</span></a>&nbsp;
			<a href="{{route('admin.allnotice')}}"><span class="badge badge-info mb-3">Notice</span></a>&nbsp;
			<a href="{{route('admin.allevent')}}"><span class="badge badge-info mb-3">Events</span></a>&nbsp;
			<a href="{{route('admin.allgallery')}}"><span class="badge badge-info mb-3">Gallery</span></a>&nbsp;
			<a href="{{route('admin.alldoc')}}"><span class="badge badge-info mb-3">Forms</span></a>&nbsp;
			<a href="{{route('admin.contact')}}"><span class="badge badge-info mb-3">Contact Us</span></a>&nbsp;
			<a href="{{route('admin.subs')}}"><span class="badge badge-info mb-3">Subscribers</span></a>&nbsp;
			<a href=""><span class="badge badge-info mb-3">House tax</span></a>&nbsp;
			<a href="{{route('admin.allaudit')}}"><span class="badge badge-info mb-3">Audit</span></a>&nbsp;
         </div>
        <div class="vz_main_content" style="margin-top:-25px!important;">
            <div class="row">
                <!-- nav tab -->
                <div class="col-lg-12 stretched_card">
                    <div class="card">
                        <div class="card-body">
                            <div class="card_title">Basic Tab</div>
                            <ul class="nav nav-tabs" id="myTab" role="tablist">
                                <li class="nav-item">
                                    <a class="nav-link active" id="home-tab" data-toggle="tab" href="#panchyat" role="tab" aria-controls="home" aria-selected="true"><i class="ti-list"></i>General </a>
                                </li>
                                <li class="nav-item">
                                    <a class="nav-link" id="profile-tab" data-toggle="tab" href="#asset" role="tab" aria-controls="profile" aria-selected="false"><i class="ti-user"></i>Assets </a>
                                </li>
								 <li class="nav-item">
                                    <a class="nav-link" id="profile-tab" data-toggle="tab" href="#employee" role="tab" aria-controls="profile" aria-selected="false"><i class="ti-user"></i>Documents </a>
                                </li>
								 <li class="nav-item">
                                    <a class="nav-link" id="profile-tab" data-toggle="tab" href="#citizen" role="tab" aria-controls="profile" aria-selected="false"><i class="ti-user"></i>Others </a>
                                </li>
								 <li class="nav-item">
                                    <a class="nav-link" id="profile-tab" data-toggle="tab" href="#bank_detail" role="tab" aria-controls="profile" aria-selected="false"><i class="ti-user"></i>Bank Details</a>
                                </li>
								 <li class="nav-item">
                                    <a class="nav-link" id="profile-tab" data-toggle="tab" href="#feedback" role="tab" aria-controls="profile" aria-selected="false"><i class="ti-user"></i>Feedback</a>
                                </li>
								<li class="nav-item">
                                    <a class="nav-link" id="profile-tab" data-toggle="tab" href="#certificate" role="tab" aria-controls="profile" aria-selected="false"><i class="ti-user"></i>Certificate</a>
                                </li>
								<li class="nav-item">
                                    <a class="nav-link" id="profile-tab" data-toggle="tab" href="#tax" role="tab" aria-controls="profile" aria-selected="false"><i class="ti-user"></i>Tax</a>
                                </li>
								<li class="nav-item">
                                    <a class="nav-link" id="profile-tab" data-toggle="tab" href="#rti" role="tab" aria-controls="profile" aria-selected="false"><i class="ti-user"></i>Rti Filled
									</a>
                                </li>
								<li class="nav-item">
                                    <a class="nav-link" id="profile-tab" data-toggle="tab" href="#sms" role="tab" aria-controls="profile" aria-selected="false"><i class="ti-user"></i>SMS Logs</a>
                                </li>
								<li class="nav-item">
                                    <a class="nav-link" id="profile-tab" data-toggle="tab" href="#push" role="tab" aria-controls="profile" aria-selected="false"><i class="ti-user"></i>Push Notifications
									</a>
                                </li>
								<li class="nav-item">
                                    <a class="nav-link" id="profile-tab" data-toggle="tab" href="#send_sms" role="tab" aria-controls="profile" aria-selected="false"><i class="ti-user"></i>Send SMS</a>
                                </li>
                             </ul>
                            <div class="tab-content mt-3" id="myTabContent">
			<div class="tab-pane fade show active" id="panchyat" role="tabpanel" aria-labelledby="home-tab">
				<div class="col-12">
					<div class="card">
						<div class="card-body">
							<h4 class="card_title">Citizen Details</h4>
							<div class="table-responsive">
								<table id="dataTable" class="text-center">
									<thead class="bg-light text-capitalize">
									
									<tr>
										<th>Sr No</th>
										<th>Ward Number</th>
										<th>Landline Number</th>
										<th>State</th>
										<th>City</th>
										<th>Area</th>
										<th>Locality</th>
										<th>Village</th>
										<th>Pin Code</th>
										<th>Date</th>
									</tr>
									</thead>
									<tbody>
									@php
									$num=1;
									@endphp
									
									<tr>
										<td>{{$num}}</td>
										
									</tr>
									
									</tbody>
								</table>
							</div>
						</div>
					</div>
				</div>
				<!-- data table -->
			</div>
                 <div class="tab-pane fade" id="asset" role="tabpanel" aria-labelledby="profile-tab">
                   <div class="col-12">
					<div class="card">
						<div class="card-body">
							<h4 class="card_title">Asset</h4>
							<div class="table-responsive">
								<table id="dataTable2" class="text-center">
									<thead class="bg-light text-capitalize">
									<tr>
										<th>Sr No</th>
										<th>Asset Name</th>
									</tr>
									</thead>
									<tbody>
									@php
									$num=1;
									@endphp
								
									<tr>
										<td>{{$num}}</td>
										
									</tr>
									
									</tbody>
								</table>
							</div>
						</div>
					</div>
				</div>
				<!-- data table -->
		</div>
		 <div class="tab-pane fade" id="employee" role="tabpanel" aria-labelledby="profile-tab">
                   <div class="col-12">
					<div class="card">
					<!-- Modal -->
                    <div class="modal fade" id="add_doc">
                        <div class="modal-dialog modal-dialog-centered" role="document">
                            <div class="modal-content">
                                <div class="modal-header">
                                    <h5 class="modal-title">Modal title</h5>
                                    <button type="button" class="close" data-dismiss="modal"><span>&times;</span></button>
                                </div>
                               <div class="modal-body">
								<form class="needs-validation" novalidate="">
								<div class="form-row">
									<div class="col-md-6 mb-3">
										<label for="validationCustom01"> Document Name/Number</label>
										<input type="text" name="f_name" class="form-control" id="validationCustom01" placeholder="Document name or number.."  required="">
										<div class="valid-feedback">
											Looks good!
										</div>
									</div>
									<div class="col-md-6 mb-3">
										<label for="validationCustom01">Upload Document</label>
										<input type="file" name="m_name" class="form-control" id="validationCustom01" placeholder="Middle Name.."  required="">
										<div class="valid-feedback">
											Looks good!
										</div>
									</div>
								 </div>
								  <button class="btn btn-primary" type="submit">Submit form</button>
								</form>
							</div>
                           </div>
                        </div>
                    </div>
						<div class="card-body">
						<a href="" data-target="#add_doc" data-toggle="modal" class="badge badge-warning">Add Document</a>

							<h4 class="card_title">Documents</h4>
							<div class="table-responsive">
								<table id="dataTable3" class="text-center">
									<thead class="bg-light text-capitalize">
									<tr>
										<th>Sr No</th>
										<th>Document Name</th>
										<th>Show Document</th>
									</tr>
									</thead>
									<tbody>
									@php
									$num++;
									@endphp
								
									<tr>
										<td>{{$num}}</td>
										 <td> 
										<a href = "" class='forum-title' name = "abc" data-toggle="modal" data-target="#usermodal"><i class="ti-pencil mr-1 btn btn-success"></i></a>
										<a href = "" class='forum-title' name = "abc" data-toggle="modal" data-target="#usermodal"><i class="ti-eye mr-1 btn btn-success"></i></a>
										<a href = "" class='forum-title'><i class="ti-trash btn btn-danger"></i></a>
										</td>
									</tr>
								
									</tbody>
								</table>
							</div>
						</div>
					</div>
				</div>
				<!-- data table -->
		</div>
		 <div class="tab-pane fade" id="citizen" role="tabpanel" aria-labelledby="profile-tab">
                   <div class="col-12">
				   	<div class="card">
					 <div class="card-body">
						 <h4 class="card_title">All Member</h4>
							<div class="table-responsive">
								<table id="dataTable4" class="text-center">
									<thead class="bg-light text-capitalize">
									<tr>
										<th>Sr No</th>
										<th>Name</th>
										<th>Number</th>
										<th>Address</th>
										<th>Email</th>
										<th>Domain</th>
										<th>Logo</th>
										<th>Action</th>
									</tr>
									</thead>
									<tbody>
									<tr>
										<td>Airi Satou</td>
										<td>Accountant</td>
										<td>Tokyo</td>
										<td>33</td>
										<td>33</td>
										<td>33</td>
										<td>2008/11/28</td>
										 <td> 
										<a href = "" class='forum-title' name = "abc" data-toggle="modal" data-target="#usermodal"><i class="ti-pencil mr-1 btn btn-success"></i></a>
										<a href = "{{route('view_secondry_mem')}}" class='forum-title' name = "abc" data-toggle="modal" data-target="#usermodal"><i class="ti-eye mr-1 btn btn-success"></i></a> 
										<a href = "" class='forum-title'><i class="ti-trash btn btn-danger"></i></a>
										</td>
									</tr>
									</tbody>
								</table>
							</div>
						</div>
					</div>
				</div>
				<!-- data table -->
		</div>
		 <div class="tab-pane fade" id="bank_detail" role="tabpanel" aria-labelledby="profile-tab">
                   <div class="col-12">
					<div class="card">
						<div class="card-body">
							<h4 class="card_title">All Bank Details</h4>
							<div class="table-responsive">
								<table id="dataTable5" class="text-center">
									<thead class="bg-light text-capitalize">
									<tr>
										<th>Bank Name</th>
										<th>Ac Number</th>
										<th>Branch</th>
										<th>IFSC</th>
										<th>UPI ID</th>
										<th>Phone Pay Num</th>
										<th>Google Pay Num</th>
										<th>Paytm Num</th>
									</tr>
									</thead>
									<tbody>
									
									<tr>
										
									</tr>
									
									</tbody>
								</table>
							</div>
						</div>
					</div>
				</div>
				<!-- data table -->
		</div>
		 <div class="tab-pane fade" id="feedback" role="tabpanel" aria-labelledby="profile-tab">
                   <div class="col-12">
				   			 <!-- Vertically centered modal -->
								<div class="modal fade" id="myModal">
									<div class="modal-dialog modal-dialog-centered" role="document">
										<div class="modal-content">
											<div class="modal-header">
												<h5 class="modal-title">Update Form</h5>
												<button type="button" class="close" data-dismiss="modal"><span>&times;</span></button>
											</div>
											<div class="modal-body">
											 <form action="" method="post" class="needs-validation" novalidate enctype="multipart/form-data">
													@csrf
												   <div class="form-row">
														<div class="col-md-6">
															<label for="validationTooltip01">Subject</label>
															<input type="text" placeholder="Enter Subject..."name="name" class="form-control"  required>
															<div class="valid-tooltip">
																Looks good!
															</div>
														</div>
														 <div class="col-lg-12">
															<div class="card">
																<div class="card-body">
																	<h4 class="card_title"> Description</h4>
																	<textarea rows="2" name="editor1" class="ck-editor" placeholder="Enter Something.."></textarea>
																</div>
															</div>
														</div>
													</div>
													</div>
												<div class="modal-footer">
													<button class="btn btn-primary" type="submit">Submit form</button>
												<button type="button" class="btn btn-light" data-dismiss="modal">Close</button>
												</div>
												</form>
											</div>
										</div>
									</div>
					<div class="card">
						<div class="card-body">
							<a href="" data-target="#myModal" class="badge badge-warning" data-toggle="modal">Add Feedback</a>

							<h4 class="card_title">All Feedback</h4>
							<div class="table-responsive">
								<table id="dataTable6" class="text-center">
									<thead class="bg-light text-capitalize">
									<tr>
										<th>Sr No</th>
										<th>Subject</th>
										<th>Description</th>
										<th>Date</th>
									</tr>
									</thead>
									<tbody>
									@php
									$num=1;
									@endphp
										
									<tr>
										<td>{{$num}}</td>
										
									</tr>
									
									</tbody>
								</table>
							</div>
						</div>
					</div>
				</div>
				<!-- data table -->
		</div>
		 <div class="tab-pane fade" id="certificate" role="tabpanel" aria-labelledby="profile-tab">
                  <!-- Vertically centered modal -->
								<div class="modal fade" id="myfeed">
									<div class="modal-dialog modal-dialog-centered" role="document">
										<div class="modal-content">
											<div class="modal-header">
												<h5 class="modal-title">Update Form</h5>
												<button type="button" class="close" data-dismiss="modal"><span>&times;</span></button>
											</div>
											<div class="modal-body">
											 <form action="" method="post" class="needs-validation" novalidate enctype="multipart/form-data">
													@csrf
												   <div class="form-row">
														<div class="col-md-6">
															<label for="validationTooltip01">Subject</label>
															<input type="text" placeholder="Enter Subject..."name="name" class="form-control"  required>
															<div class="valid-tooltip">
																Looks good!
															</div>
														</div>
														
													</div>
													</div>
												<div class="modal-footer">
													<button class="btn btn-primary" type="submit">Submit form</button>
												<button type="button" class="btn btn-light" data-dismiss="modal">Close</button>
												</div>
												</form>
											</div>
										</div>
									</div>
					<div class="card">
						<div class="card-body">
							<a href="" data-target="#myfeed" class="badge badge-warning" data-toggle="modal">Certificate Request</a>
							<h4 class="card_title">Certificate</h4>
							<div class="table-responsive">
								<table id="dataTable7" class="text-center">
									<thead class="bg-light text-capitalize">
									<tr>
										<th>Sr No</th>
										<th>Name</th>
										<th>Certificate Type</th>
										<th>Request Date</th>
										<th>Issue Date</th>
									</tr>
									</thead>
									<tbody>
									<tr>
										<td>1</td>
										<td>Birth Certificate</td>
										<td>1/25/2020</td>
										<td>2008/11/28</td>
									</tr>
									</tbody>
								</table>
							</div>
						</div>
					</div>
				</div>
				<!-- data table -->
		
		 <div class="tab-pane fade" id="tax" role="tabpanel" aria-labelledby="profile-tab">
                   <div class="col-12">
				   <!-- Vertically centered modal -->
								<div class="modal fade" id="mytax">
									<div class="modal-dialog modal-dialog-centered" role="document">
										<div class="modal-content">
											<div class="modal-header">
												<h5 class="modal-title">Pay Tax</h5>
												<button type="button" class="close" data-dismiss="modal"><span>&times;</span></button>
											</div>
											<div class="modal-body">
											 <form action="" method="post" class="needs-validation" novalidate enctype="multipart/form-data">
													@csrf
												   <div class="form-row">
														<div class="col-md-4">
															<label for="validationTooltip01">Subject</label>
															<select class="form-control">
																<option value>--Choose House--</option>
																<option value>FZ007</option>
																<option value>AP004</option>
																<option value>PL521</option>
																<option value>JK548</option>
															</select>
															<div class="valid-tooltip">
																Looks good!
															</div>
														</div>
														<div class="col-md-4">
															<label for="validationTooltip01">From Date</label>
																<input type="date" class="form-control" name="start">
															<div class="valid-tooltip">
																Looks good!
															</div>
														</div>
														<div class="col-md-4">
															<label for="validationTooltip01">To Date</label>
															<input type="date" class="form-control" name="to">
															<div class="valid-tooltip">
																Looks good!
															</div>
														</div>
														
													</div>
													</div>
												<div class="modal-footer">
													<button class="btn btn-primary" type="submit">Submit form</button>
												<button type="button" class="btn btn-light" data-dismiss="modal">Close</button>
												</div>
												</form>
											</div>
										</div>
									</div>
					<div class="card">
						<div class="card-body">
							<a href="" data-target="#mytax" class="badge badge-warning" data-toggle="modal">Pay Tax</a>
					
							<h4 class="card_title">All Pay Tax</h4>
							<div class="table-responsive">
								<table id="dataTable8" class="text-center">
									<thead class="bg-light text-capitalize">
									<tr>
										<th>Sr No</th>
										<th>Citizen Name</th>
										<th>House Number</th>
										<th>Amount</th>
										<th>Date</th>
									</tr>
									</thead>
									<tbody>
									<tr>
										<td>1</td>
										<td>Shubham</td>
										<td>FZ007</td>
										<td>1000</td>
										<td>2008/11/28</td>
									</tr>
									</tbody>
								</table>
							</div>
						</div>
					</div>
				</div>
				<!-- data table -->
		</div>
            <div class="tab-pane fade" id="rti" role="tabpanel" aria-labelledby="profile-tab">
                 <div class="col-12">
				  <div class="card">
						<div class="card-body">					
							<h4 class="card_title">Rti Filled</h4>
							<div class="table-responsive">
								<table id="dataTable9" class="text-center">
									<thead class="bg-light text-capitalize">
									<tr>
										<th>Sr No</th>
										<th>Title</th>
										<th>Description</th>
										<th>Amount</th>
										<th>Date</th>
									</tr>
									</thead>
									<tbody>
									<tr>
										<td>1</td>
										<td>Shubham</td>
										<td>FZ007</td>
										<td>1000</td>
										<td>2008/11/28</td>
									</tr>
									</tbody>
								</table>
							</div>
						</div>
					</div>
				</div>
			</div>
			
			   <div class="tab-pane fade" id="sms" role="tabpanel" aria-labelledby="profile-tab">
                 <div class="col-12">
				  <div class="card">
						<div class="card-body">					
							<h4 class="card_title">SMS Logs</h4>
							<div class="table-responsive">
								<table id="dataTable_sms" class="text-center">
									<thead class="bg-light text-capitalize">
									<tr>
										<th>Sr No</th>
										<th>Description</th>
										<th>Date/Time</th>
									</tr>
									</thead>
									<tbody>
									<tr>
										<td>1</td>
										<td>This is the sms logs</td>
										<td>2008/11/28 12:00</td>
									</tr>
									</tbody>
								</table>
							</div>
						</div>
					</div>
				</div>
			</div>
			 <div class="tab-pane fade" id="push" role="tabpanel" aria-labelledby="profile-tab">
                 <div class="col-12">
				  <div class="card">
						<div class="card-body">					
							<h4 class="card_title">Push Notificaion</h4>
							<div class="table-responsive">
								<table id="dataTable_push" class="text-center">
									<thead class="bg-light text-capitalize">
									<tr>
										<th>Sr No</th>
										<th>Description</th>
										<th>Date/Time</th>
									</tr>
									</thead>
									<tbody>
									<tr>
										<td>1</td>
										<td>This is the Push Notification</td>
										<td>2008/11/28 12:00</td>
									</tr>
									</tbody>
								</table>
							</div>
						</div>
					</div>
				</div>
			</div>
			 <div class="tab-pane fade" id="send_sms" role="tabpanel" aria-labelledby="profile-tab">
                 <div class="col-12">
				  <div class="card">
						<div class="card-body">					
							<h4 class="card_title">Send SMS</h4>
							<div class="table-responsive">
								<table id="dataTable_send_sms" class="text-center">
									<thead class="bg-light text-capitalize">
									<tr>
										<th>Sr No</th>
										<th>Description</th>
										<th>Date/Time</th>
									</tr>
									</thead>
									<tbody>
									<tr>
										<td>1</td>
										<td>This is the send sms</td>
										<td>2008/11/28 12:00</td>
									</tr>
									</tbody>
								</table>
							</div>
						</div>
					</div>
				</div>
			</div>
                            </div>
                        </div>
                    </div>
                </div>
                <!-- nav tab -->
              
            </div>
            
        </div>
@endsection
@section('js')
<script>
$(document).ready(function () {
          $('#dataTable_push').DataTable();
          $('.dataTables_length').addClass('bs-select');
        });
		$(document).ready(function () {
          $('#dataTable_send_sms').DataTable();
          $('.dataTables_length').addClass('bs-select');
        });
$(document).ready(function () {
          $('#dataTable_sms').DataTable();
          $('.dataTables_length').addClass('bs-select');
        });
$(document).ready(function () {
          $('#dataTable4').DataTable();
          $('.dataTables_length').addClass('bs-select');
        });
		
$(document).ready(function () {
          $('#dataTable9').DataTable();
          $('.dataTables_length').addClass('bs-select');
        });

$(document).ready(function () {
          $('#dataTable5').DataTable();
          $('.dataTables_length').addClass('bs-select');
        });

$(document).ready(function () {
          $('#dataTable6').DataTable();
          $('.dataTables_length').addClass('bs-select');
        });

$(document).ready(function () {
          $('#dataTable7').DataTable();
          $('.dataTables_length').addClass('bs-select');
        });

$(document).ready(function () {
          $('#dataTable8').DataTable();
          $('.dataTables_length').addClass('bs-select');
        });
</script>
    <!--=========================*
            This Page Scripts
    *===========================-->
    <!-- Data Table js -->
    <script src="{{asset('public/assets/vendors/data-table/js/jquery.dataTables.js')}}"></script>
    <script src="{{asset('public/assets/vendors/data-table/js/jquery.dataTables.min.js')}}"></script>
    <script src="{{asset('public/assets/vendors/data-table/js/dataTables.bootstrap4.min.js')}}"></script>
    <script src="{{asset('public/assets/vendors/data-table/js/dataTables.responsive.min.js')}}"></script>
    <script src="{{asset('public/assets/vendors/data-table/js/responsive.bootstrap.min.js')}}"></script>

    <!-- Data table Init -->
    <script src="{{asset('public/assets/js/init/data-table.js')}}"></script>
	<!-- Ck Editor Js -->
    <script src="{{asset('public/assets/vendors/ck-editor/js/ckeditor.js')}}"></script>
    <script src="{{asset('public/assets/js/init/editors.js')}}"></script>
@endsection
