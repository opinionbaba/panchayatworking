@extends('layouts.app')


{{-- Page Title --}}
@section('page-title')
  Accounts
@endsection
{{-- This Page Css --}}
@section('css')
   <link rel="stylesheet" type="text/css"
        href="{{asset('public/assets/datatables.net-bs4/css/dataTables.bootstrap4.css')}}">
    <link rel="stylesheet" type="text/css"
        href="{{asset('public/assets/datatables.net-bs4/css/responsive.dataTables.min.css')}}">
 <body class="skin-blue fixed-layout">
  <div class="preloader">
	<div class="loader">
		<div class="loader__figure"></div>
	</div>
   </div>
  <div id="main-wrapper">
	  
@endsection
@section('main-content')

			 <div class="page-wrapper">
			  <div class="container-fluid">
                <div class="row page-titles">
                    <div class="col-md-5 align-self-center">
                        <h4 class="text-themecolor">Datatable</h4>
                    </div>
                    <div class="col-md-7 align-self-center text-right">
                        <div class="d-flex justify-content-end align-items-center">
                            <ol class="breadcrumb">
                                <li class="breadcrumb-item"><a href="javascript:void(0)">Home</a></li>
                                <li class="breadcrumb-item active">Datatable</li>
                            </ol>
                            <!--button type="button" class="btn btn-info d-none d-lg-block m-l-15" data-toggle="modal" data-target=".bs-example-modal-lg" ><i
                                    class="fa fa-plus-circle"></i> Create Ticket</button-->
									
                        </div>
                    </div>
                </div>
		<div class="row">
			<div class="col-12">
			 @if(Session::has('insrt'))
			<div class="alert alert-info alert-dismissible fade show" role="alert">
				<strong>{{Session::get('insrt')}}</strong>
				<button type="button" class="close" data-dismiss="alert" aria-label="Close"><span class="ti-close"></span>
				</button>
			</div>
			@endif
			@if(Session::has('edit'))
			<div class="alert alert-warning alert-dismissible fade show" role="alert">
				<strong> {{Session::get('edit')}}</strong>
				<button type="button" class="close" data-dismiss="alert" aria-label="Close"><span class="ti-close"></span>
				</button>
			</div>
			@endif
			@if(Session::has('delete'))
			<div class="alert alert-danger alert-dismissible fade show" role="alert">
				<strong> {{Session::get('delete')}}</strong>
				<button type="button" class="close" data-dismiss="alert" aria-label="Close"><span class="ti-close"></span>
				</button>
			</div>
			@endif
                       
                        <div class="card">
                            <div class="card-body">
                                <h4 class="card-title">Support Ticket List</h4>
                                <h6 class="card-subtitle">List of ticket opend by customers</h6>
                                <div class="row m-t-40">
                                    <!-- Column -->
                                    <div class="col-md-6 col-lg-3 col-xlg-3">
                                        <div class="card">
                                            <div class="box bg-info text-center">
                                                <h1 class="font-light text-white">2,064</h1>
                                                <h6 class="text-white">Total Tickets</h6>
                                            </div>
                                        </div>
                                    </div>
                                    <!-- Column -->
                                    <div class="col-md-6 col-lg-3 col-xlg-3">
                                        <div class="card">
                                            <div class="box bg-primary text-center">
                                                <h1 class="font-light text-white">1,738</h1>
                                                <h6 class="text-white">Responded</h6>
                                            </div>
                                        </div>
                                    </div>
                                    <!-- Column -->
                                    <div class="col-md-6 col-lg-3 col-xlg-3">
                                        <div class="card">
                                            <div class="box bg-success text-center">
                                                <h1 class="font-light text-white">1100</h1>
                                                <h6 class="text-white">Resolve</h6>
                                            </div>
                                        </div>
                                    </div>
                                    <!-- Column -->
                                    <div class="col-md-6 col-lg-3 col-xlg-3">
                                        <div class="card">
                                            <div class="box bg-dark text-center">
                                                <h1 class="font-light text-white">964</h1>
                                                <h6 class="text-white">Pending</h6>
                                            </div>
                                        </div>
                                    </div>
                                    <!-- Column -->
                                </div>
                                <div class="table-responsive">
                                    <table id="demo-foo-addrow" class="table m-t-30 table-hover no-wrap contact-list"
                                        data-paging="true" data-paging-size="7">
                                        <thead>
                                            <tr>
                                                <th>Sr No</th>
                                                <th>Ticket ID</th>
                                                <th>Panchayat ID</th>
                                                <th>Subject</th>
                                                <th>Status</th>
                                                <th>Priority</th>
                                                <th>Reply</th>
                                                <th>Date</th>
                                            </tr>
                                        </thead>
                                        <tbody>
										 @php
										 $num=1;
										 @endphp
										 @foreach($ticket as $value)
                                          <tr>
											<td>{{$num}}</td>
											<td>{{$value->ticket_id}}</td>
											<td>{{$value->panc_id}}</td>
											<td>{{$value->subject}}</td>
											<td>@if($value->status == 1)
												<span class="badge badge-info">Done</span>
												@else
												<span class="badge badge-danger">Pending</span>
												@endif
											</td>
											<td>@if($value->priority == 1)
												<span class="badge badge-info">High</span>
												@else
												@endif
												@if($value->priority == 2)
												<span class="badge badge-info">low</span>
												@else
												@endif
												@if($value->priority == 3)
												<span class="badge badge-info">Medium</span>
												@else
												@endif
											</td>
									<td> 
									<div class="btn-group">
                                       <button type="button" class="btn btn-info dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                            <i class="ti-eye"></i>
                                        </button>
                                       <div class="dropdown-menu animated flipInY" style="min-width:auto!important;top:20px!important;left:-20px!important;">
                                            <a class="dropdown-item" href="{{route('ticket-reply',$value->ticket_id)}}"> <i class="mdi mdi-send"></i> Reply</a>
                                        </div>
                                     </div> 
									</td>
                                      <td>{{$value->created_at}}</td>
                                            </tr>
											 @php
											 $num++;
											 @endphp
                                            @endforeach
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
			
			 <div class="modal bs-example-modal-lg" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true" style="display: none;">
				<div class="modal-dialog">
					<div class="modal-content">
						<div class="modal-header">
							<h4 class="modal-title" id="myLargeModalLabel">Add New Role</h4>
							<button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
						</div>
						<div class="modal-body">
							<form method="post"  action ="" class="needs-validation" novalidate="">
							@csrf
									<div class="form-row">
									   <div class="col-md-12 mb-3">
									    <label class="radio-inline">
										  <input type="radio" name="optradio" checked>Master Admin
										</label>
										<label class="radio-inline">
										  <input type="radio" name="optradio">Super Admin
										</label>
										</div>
									</div>
								    <div class="form-row">
									   <div class="col-md-6 mb-3">
										<label for="validationCustom01">Subject</label>
										<input type="text" class="form-control" id="validationCustom01" name="name" placeholder="Enter subject.."  required="">
										<div class="valid-feedback">
											Looks good!
										</div>
									  </div>
									   <div class="col-md-6 mb-3">
										<label for="validationCustom01">Attach File</label>
										<input type="file" class="form-control" id="validationCustom01" name="name" placeholder="Enter subject.."  required="">
										<div class="valid-feedback">
											Looks good!
										</div>
									  </div>
									</div>
									<div class="form-row">
									  <div class="col-md-12 mb-3">
										<label for="validationCustomUsername">Description </label>
										<div class="input-group">
											<textarea name="desc" class="form-control" placeholder="Enter Description..." rows="2"></textarea>
											<div class="invalid-feedback">
												Please choose a username.
											</div>
										</div>
									  </div>
									</div>
									
									<div class="form-row">
									  <div class="col-md-12 mb-3">
									    <button class="btn btn-primary" type="submit">Submit form</button>
									  </div>
									</div>
							</form>
						</div>
						
					</div>
					<!-- /.modal-content -->
				</div>
				<!-- /.modal-dialog -->
			</div>
            <!-- ============================================================== -->
            <!-- End Container fluid  -->
            <!-- ============================================================== -->
 
 </div>
@endsection
@section('js')
 <script>
        $(function () {
            $('#demo-foo-addrow').footable();
        });
    </script>
  <script>
        $(function () {
            $('#myTable').DataTable();
            var table = $('#example').DataTable({
                "columnDefs": [{
                    "visible": false,
                    "targets": 2
                }],
                "order": [
                    [2, 'asc']
                ],
                "displayLength": 25,
                "drawCallback": function (settings) {
                    var api = this.api();
                    var rows = api.rows({
                        page: 'current'
                    }).nodes();
                    var last = null;
                    api.column(2, {
                        page: 'current'
                    }).data().each(function (group, i) {
                        if (last !== group) {
                            $(rows).eq(i).before('<tr class="group"><td colspan="5">' + group + '</td></tr>');
                            last = group;
                        }
                    });
                }
            });
            // Order by the grouping
            $('#example tbody').on('click', 'tr.group', function () {
                var currentOrder = table.order()[0];
                if (currentOrder[0] === 2 && currentOrder[1] === 'asc') {
                    table.order([2, 'desc']).draw();
                } else {
                    table.order([2, 'asc']).draw();
                }
            });
            // responsive table
            $('#config-table').DataTable({
                responsive: true
            });
            $('#example23').DataTable({
                dom: 'Bfrtip',
                buttons: [
                    'copy', 'csv', 'excel', 'pdf', 'print'
                ]
            });
            $('.buttons-copy, .buttons-csv, .buttons-print, .buttons-pdf, .buttons-excel').addClass('btn btn-primary mr-1');
        });

    </script>
	
@endsection
