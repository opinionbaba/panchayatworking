@extends('layouts.app')


{{-- Page Title --}}
@section('page-title')
  Accounts
@endsection
{{-- This Page Css --}}
@section('css')
    <body class="skin-blue fixed-layout">
    <div class="preloader">
        <div class="loader">
            <div class="loader__figure"></div>
            <p class="loader__label">Elite admin</p>
        </div>
    </div>
    <div id="main-wrapper">  
@endsection
@section('main-content')
      <div class="page-wrapper">
          <div class="container-fluid">
                <div class="row page-titles">
                    <div class="col-md-5 align-self-center">
                        <h4 class="text-themecolor">Websites</h4>
                    </div>
                    <div class="col-md-7 align-self-center text-right">
                        <div class="d-flex justify-content-end align-items-center">
                            <ol class="breadcrumb">
                                <li class="breadcrumb-item"><a href="javascript:void(0)">Home</a></li>
                                <li class="breadcrumb-item active">Websites</li>
                            </ol>
                        </div>
                    </div>
                </div>
				
				
				<div class="card-group">
                    <div class="card">
                        <div class="card-body">
                            <div class="row">
                                <div class="col-md-12">
								   <h4 class="card_title">All Notice</h4>
                                   <div class="button-box">
                                    <a class="tst1 btn btn-info" href="#">Home</a>
                                    <a class="tst1 btn btn-info" href="{{route('admin.allnotice')}}">Notice</a>
                                    <a class="tst1 btn btn-info" href="{{route('admin.allevent')}}">Events</a>
                                    <a class="tst1 btn btn-info" href="{{route('admin.allgallery')}}">Gallery</a>
                                    <a class="tst1 btn btn-info" href="{{route('admin.alldoc')}}">Form</a>
                                    <a class="tst1 btn btn-info" href="{{route('admin.contact')}}">Contact Us</a>
                                    <a class="tst1 btn btn-info" href="{{route('admin.subs')}}">Subscribers</a>
                                    <a class="tst1 btn btn-info" href="#">House Tax</a>
                                    <a class="tst1 btn btn-info" href="{{route('admin.about-us')}}">About Us</a>
                                    <a class="tst1 btn btn-info" href="{{route('admin.allaudit')}}">Audit</a>
                                   </div>
                                </div>
								<div class="col-md-12" style="margin-top:40px;">
								  <form action="{{route('add-details')}}" method="post" class="needs-validation" novalidate="">
								@csrf
								<div class="form-row">
									<div class="col-md-6 mb-3">
										<label for="validationCustom01">Add Title</label>
										<input type="text" name="title" class="form-control" id="validationCustom01" placeholder="Add title..."  required="">
										<div class="valid-feedback">
											Looks good!
										</div>
									</div>
									<div class="col-md-6 mb-3">
										<label for="validationCustom01">Total Population</label>
										<input type="text" name="total_popu" class="form-control" id="validationCustom01" placeholder="Population.."  required="">
										<div class="valid-feedback">
											Looks good!
										</div>
									</div>
									<div class="col-md-6 mb-3">
										<label for="validationCustom02">Total Area(Hectares)</label>
										<input type="number" name="area" class="form-control" id="validationCustom02" placeholder="Enter Total Area(Hectares).." required="">
										<div class="valid-feedback">
											Looks good!
										</div>
									</div>
									<div class="col-md-6 mb-3">
										<label for="validationCustomUsername">No. of House Holder</label>
										<div class="input-group">
											<input name="house_hold" class="form-control" placeholder="Enter No. of House Holder.." rows="1">
											<div class="invalid-feedback">
												Please choose a username.
											</div>
										</div>
									</div>
								</div>
								
								<button class="btn btn-primary" type="submit">Submit form</button>
							</form>
							  </div>
							</div>
                        </div>
                    </div>
                </div>
              </div>
             </div>
		@endsection