@extends('layouts.app')


{{-- Page Title --}}
@section('page-title')
  Accounts
@endsection
{{-- This Page Css --}}
@section('css')
    <body class="skin-blue fixed-layout">
    <div class="preloader">
        <div class="loader">
            <div class="loader__figure"></div>
            <p class="loader__label">Elite admin</p>
        </div>
    </div>
    <div id="main-wrapper">  
@endsection
@section('main-content')
      <div class="page-wrapper">
            <!-- ============================================================== -->
            <!-- Container fluid  -->
            <!-- ============================================================== -->
            <div class="container-fluid">
                <!-- ============================================================== -->
                <!-- Bread crumb and right sidebar toggle -->
                <!-- ============================================================== -->
                <div class="row page-titles">
                    <div class="col-md-5 align-self-center">
                        <h4 class="text-themecolor">Websites</h4>
                    </div>
                    <div class="col-md-7 align-self-center text-right">
                        <div class="d-flex justify-content-end align-items-center">
                            <ol class="breadcrumb">
                                <li class="breadcrumb-item"><a href="javascript:void(0)">Home</a></li>
                                <li class="breadcrumb-item active">Websites</li>
                            </ol>
                        </div>
                    </div>
                </div>
				
				
				<div class="card-group">
                    <div class="card">
                        <div class="card-body">
                            <div class="row">
                                <div class="col-md-12">
								   <h4 class="card_title">Update Data</h4>
                                   <div class="button-box">
                                    <a class="tst1 btn btn-info" href="home">Home</a>
                                    <a class="tst1 btn btn-info" href="{{route('admin.allnotice')}}">Notice</a>
                                    <a class="tst1 btn btn-info" href="{{route('admin.allevent')}}">Events</a>
                                    <a class="tst1 btn btn-info" href="{{route('admin.allgallery')}}">Gallery</a>
                                    <a class="tst1 btn btn-info" href="{{route('admin.alldoc')}}">Form</a>
                                    <a class="tst1 btn btn-info" href="{{route('admin.contact')}}">Contact Us</a>
                                    <a class="tst1 btn btn-info" href="{{route('admin.subs')}}">Subscribers</a>
                                    <a class="tst1 btn btn-info" href="#">House Tax</a>
                                    <a class="tst1 btn btn-info" href="{{route('admin.about-us')}}">About Us</a>
                                    <a class="tst1 btn btn-info" href="{{route('admin.allaudit')}}">Audit</a>
                                   </div>
                                </div>
								@if(Session::has('insrt'))
								<div class="alert alert-info alert-dismissible fade show" role="alert">
									<strong></strong>  {{Session::get('insrt')}}
									<button type="button" class="close" data-dismiss="alert" aria-label="Close"><span class="ti-close"></span>
									</button>
								</div>
								@endif
								<div class="col-md-12" style="margin-top:40px;">
								   <form action="{{route('add-details')}}" method="post" class="needs-validation" novalidate="">
								@csrf
								<div class="form-row">
									<div class="col-md-6 mb-3">
										<label for="validationCustom01">Add Title</label>
										<input type="text" name="title" class="form-control" id="validationCustom01" placeholder="Add title..."  required="">
										<div class="valid-feedback">
											Looks good!
										</div>
									</div>
									<div class="col-md-6 mb-3">
										<label for="validationCustom01">Total Population</label>
										<input type="text" name="total_popu" class="form-control" id="validationCustom01" placeholder="Population.."  required="">
										<div class="valid-feedback">
											Looks good!
										</div>
									</div>
									<div class="col-md-6 mb-3">
										<label for="validationCustom02">Total Area(Hectares)</label>
										<input type="number" name="area" class="form-control" id="validationCustom02" placeholder="Enter Total Area(Hectares).." required="">
										<div class="valid-feedback">
											Looks good!
										</div>
									</div>
									<div class="col-md-6 mb-3">
										<label for="validationCustomUsername">No. of House Holder</label>
										<div class="input-group">
											<input name="house_hold" class="form-control" placeholder="Enter No. of House Holder.." rows="1">
											<div class="invalid-feedback">
												Please choose a username.
											</div>
										</div>
									</div>
								</div>
								<div class="form-row">
									<div class="col-md-6 mb-3">
										<label for="validationCustom01">Male Population</label>
										<input type="text" name="male_popu" class="form-control" id="validationCustom01" placeholder="Male Population.."  required="">
										<div class="valid-feedback">
											Looks good!
										</div>
									</div>
									<div class="col-md-6 mb-3">
										<label for="validationCustom02">Female Population</label>
										<input type="number" name="female_popu" class="form-control" id="validationCustom02" placeholder="Enter Female Population.." required="">
										<div class="valid-feedback">
											Looks good!
										</div>
									</div>
									<div class="col-md-6 mb-3">
										<label for="validationCustomUsername">Voters</label>
										<div class="input-group">
											<input type="text" name="voter" class="form-control" placeholder="Enter Voters.." rows="1">
											<div class="invalid-feedback">
												Please choose a username.
											</div>
										</div>
									</div>
								</div>
								<button class="btn btn-primary" type="submit">Submit form</button>
							</form>
								</div>
								
                            </div>
                        </div>
                    </div>
                </div>
								   
				
		
		
		  	<div class="vz_main_content" style="margin-top:-25px!important;">
				<div class="row">
				<div class="col-lg-6 mt-4 stretched_card">
                    <div class="card">
                        <div class="card-body">
                            <h4 class="card_title">Want To Widgets On Home Page</h4>
                            <ul class="list-group">
                                <li class="list-group-item d-flex justify-content-between align-items-center">
                                   Events

									<input type="checkbox" <?= $widgets[0]['events'] ==1 ? "checked" : ""?> id="events" url="widget_status" name="events" value="Bike" class="toggle_btn">
                                <li class="list-group-item d-flex justify-content-between align-items-center">
                                   Notice
                                  <input type="checkbox" <?= $widgets[0]['notice'] ==1 ? "checked" : ""?> class="toggle_btn" url="widget_status" id="notice" name="notice" value="Bike">
                                </li>
                                <li class="list-group-item d-flex justify-content-between align-items-center">
                                    Weather
                                   	<input type="checkbox" <?= $widgets[0]['weather'] ==1 ? "checked" : ""?> class="toggle_btn" url="widget_status" id="weather" name="weather" value="Bike">
                                </li>
                                <li class="list-group-item d-flex justify-content-between align-items-center">
                                    Forms
									<input type="checkbox" <?= $widgets[0]['forms'] ==1 ? 'checked' : ''?> class="toggle_btn" id="forms" url="widget_status" name="forms" value="Bike">
                                </li>
                                <li class="list-group-item d-flex justify-content-between align-items-center">
                                    Gallery
									<input type="checkbox" <?= $widgets[0]['gallery'] ==1 ? "checked" : ""?> class="toggle_btn" id="gallery" name="gallery" value="Bike" url="widget_status">
                                </li>
                                <li class="list-group-item d-flex justify-content-between align-items-center">
                                    Schemes
									<input type="checkbox" <?= $widgets[0]['schemes'] ==1 ? "checked" : ""?> class="toggle_btn" id="schemes" name="schemes" value="Bike" url="widget_status">
                                </li>
                               <li class="list-group-item d-flex justify-content-between align-items-center">
                                    Background Image
									<input type="checkbox"  <?= $widgets[0]['bg_img'] ==1 ? "checked" : ""?> id="bg_img" name="bg_img" url="widget_status" value="Bike">
                                </li>
                            </ul>
                        </div>
                    </div>
                </div>
				<div class="col-lg-6 mt-4 stretched_card">
                    <div class="card">
                        <div class="card-body">
                            <h4 class="card_title">Update Background Image</h4>
                              <form class="needs-validation ajaxform2" novalidate="" id="upimg">
								<div class="form-row">
									<div class="col-md-12 mb-3">
										<label for="validationCustom01">Choose Image</label>
										<input type="file" required="required" name="bg_img" class="form-control" id="validationCustom01" placeholder="Add title...">
										<div class="valid-feedback">
											Looks good!
										</div>
									</div>
								</div>
								@csrf
								<input type="hidden" name="action-url" value="update_bg_image">
								<button class="btn btn-primary" type="submit">Submit form</button>
							</form>
                        </div>
                    </div>
                </div>
              </div>
             </div>
           </div>
                <!-- With Badges -->

@endsection

