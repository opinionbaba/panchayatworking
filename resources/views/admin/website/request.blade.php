@extends('layouts.app')


{{-- Page Title --}}
@section('page-title')
   Contact
@endsection

{{-- This Page Css --}}
@section('css')
 <link rel="stylesheet" type="text/css"
        href="{{asset('public/assets/datatables.net-bs4/css/dataTables.bootstrap4.css')}}">
    <link rel="stylesheet" type="text/css"
        href="{{asset('public/assets/datatables.net-bs4/css/responsive.dataTables.min.css')}}">
    <style>
	  .selectpicker
	  {
		width: 100%;
		border: 1px solid #dedada;
		padding: 9px;
		border-radius: 4px;
		color: #a9a1a1; 
	  }
	</style>
  
 <body class="skin-blue fixed-layout">
    <!-- ============================================================== -->
    <!-- Preloader - style you can find in spinners.css -->
    <!-- ============================================================== -->
    <div class="preloader">
        <div class="loader">
            <div class="loader__figure"></div>
            <p class="loader__label">Elite admin</p>
        </div>
    </div>
@endsection

@section('main-content')
<div id="main-wrapper">
    <div class="page-wrapper">
        <div class="container-fluid">
			<div class="row page-titles">
				<div class="col-md-5 align-self-center">
					<h4 class="text-themecolor">Request List</h4>
				</div>
				<div class="col-md-7 align-self-center text-right">
					<div class="d-flex justify-content-end align-items-center">
						<ol class="breadcrumb">
							<li class="breadcrumb-item"><a href="javascript:void(0)">Home</a></li>
							<li class="breadcrumb-item active">Request</li>
						</ol>
					</div>
				</div>
            </div>
			
			 <div class="card">
                            <div class="card-body">
                                <h4 class="card-title">Request List</h4>
                                <h6 class="card-subtitle">Export data to Copy, CSV, Excel, PDF & Print</h6>
								<div class="row">
								  <div class="col-md-5">
									<form action="{{asset('panchayat/data')}}" method="post">
									{{ csrf_field() }}
									 <select class="selectpicker" name="status"  data-live-search="true" onchange='this.form.submit()'>
										<option>--select panchayat--</option>
										@foreach($panchyat as $value)
										<option value="{{$value->id}}" >{{$value->panc_name}}</option>
										@endforeach
									  </select>
									</form>
								  </div>
							    </div>
						<script type="text/javascript">
							$( "#status" ).change(function() { 
								//var id =$("#hid").val();
								var status =$("#status").val();
								//alert(status);
								console.log(status);
								$.ajax({
									type: "GET",
									url: "update/"+ id ,
									success: function(data){
									console.log(data);
									$("#update").val(data);
									},
								});
							});
						</script>
						
								@if(empty($panchyat_user))
                                <div class="table-responsive m-t-40">
                                    <table id="myTable"
                                        class="display nowrap table table-hover table-striped table-bordered"
                                        cellspacing="0" width="100%">
                                        <thead>
                                            <tr>
												<th>Sr No</th>
												<th>Applicant Name</th>
												<th>Ward No</th>
												<th>Mobile number</th>
												<th>Address</th>
												<th>Type Of Request</th>
											</tr>
                                        </thead>
                                        <tfoot>
                                            <tr>
												<th>Sr No</th>
												<th>Applicant Name</th>
												<th>Ward No</th>
												<th>Mobile number</th>
												<th>Address</th>
												<th>Type Of Request</th>
											</tr>
                                        </tfoot>
                                        <tbody>
                                            <tr>
											   <td>ff</td>
											   <td>ff</td>
											   <td>ff</td>
											   <td>ff</td>
											   <td>ff</td>
											   <td>ff</td>
											 </tr>
                                        </tbody>
                                    </table>
                                </div>
								@else
									
								 <div class="table-responsive m-t-40">
                                    <table id="example23"
                                        class="display nowrap table table-hover table-striped table-bordered"
                                        cellspacing="0" width="100%">
                                        <thead>
                                            <tr>
                                                <th>Sr No</th>
												<th>Applicant Name</th>
												<th>Ward No</th>
												<th>Mobile number</th>
												<th>Address</th>
												<th>Type Of Request</th>
                                            </tr>
                                        </thead>
                                        <tfoot>
                                            <tr>
                                                <th>Sr No</th>
												<th>Applicant Name</th>
												<th>Ward No</th>
												<th>Mobile number</th>
												<th>Address</th>
												<th>Type Of Request</th>
                                            </tr>
                                        </tfoot>
                                        <tbody>
                                            @php 
											$num=1;
											@endphp
											@foreach($panchyat_user  as $value)
											<tr>
												<td>{{$num}}</td>
												<td>{{$value->name}}</td>
												<td>{{$value->ward_number}}</td>
												<td>{{$value->mobile}}</td>
												<td>{{$value->name}}</td>
											   
											</tr>
											@php 
											$num++;
											@endphp
											@endforeach
                                        </tbody>
                                    </table>
                                </div>
								@endif	
                            </div>
                        </div>
			
			
			
			
			
        </div>
    </div>
</div>
@endsection

@section('js')
 <script>
        $(function () {
            $('#myTable').DataTable();
            var table = $('#example').DataTable({
                "columnDefs": [{
                    "visible": false,
                    "targets": 2
                }],
                "order": [
                    [2, 'asc']
                ],
                "displayLength": 25,
                "drawCallback": function (settings) {
                    var api = this.api();
                    var rows = api.rows({
                        page: 'current'
                    }).nodes();
                    var last = null;
                    api.column(2, {
                        page: 'current'
                    }).data().each(function (group, i) {
                        if (last !== group) {
                            $(rows).eq(i).before('<tr class="group"><td colspan="5">' + group + '</td></tr>');
                            last = group;
                        }
                    });
                }
            });
            // Order by the grouping
            $('#example tbody').on('click', 'tr.group', function () {
                var currentOrder = table.order()[0];
                if (currentOrder[0] === 2 && currentOrder[1] === 'asc') {
                    table.order([2, 'desc']).draw();
                } else {
                    table.order([2, 'asc']).draw();
                }
            });
            // responsive table
            $('#config-table').DataTable({
                responsive: true
            });
            $('#example23').DataTable({
                dom: 'Bfrtip',
                buttons: [
                    'copy', 'csv', 'excel', 'pdf', 'print'
                ]
            });
            $('.buttons-copy, .buttons-csv, .buttons-print, .buttons-pdf, .buttons-excel').addClass('btn btn-primary mr-1');
        });

    </script>
	<script>
/* When the user clicks on the button,
toggle between hiding and showing the dropdown content */
function myFunction() {
  document.getElementById("myDropdown").classList.toggle("show");
}

function filterFunction() {
  var input, filter, ul, li, a, i;
  input = document.getElementById("myInput");
  filter = input.value.toUpperCase();
  div = document.getElementById("myDropdown");
  a = div.getElementsByTagName("a");
  for (i = 0; i < a.length; i++) {
    txtValue = a[i].textContent || a[i].innerText;
    if (txtValue.toUpperCase().indexOf(filter) > -1) {
      a[i].style.display = "";
    } else {
      a[i].style.display = "none";
    }
  }
}
</script>
@endsection
