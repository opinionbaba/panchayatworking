@extends('layouts.app')


{{-- Page Title --}}
@section('page-title')
    Create Panchayat
@endsection
<body class="skin-blue fixed-layout">
    <!-- ============================================================== -->
    <!-- Preloader - style you can find in spinners.css -->
    <!-- ============================================================== -->
    <div class="preloader">
        <div class="loader">
            <div class="loader__figure"></div>
        </div>
    </div>
	 <div id="main-wrapper">
@section('main-content')
 
     <div class="page-wrapper">
            <!-- ============================================================== -->
            <!-- Container fluid  -->
            <!-- ============================================================== -->
            <div class="container-fluid">
                <!-- ============================================================== -->
                <!-- Bread crumb and right sidebar toggle -->
                <!-- ============================================================== -->
                <div class="row page-titles">
                    <div class="col-md-5 align-self-center">
                        <h4 class="text-themecolor">Panchayat Configuration</h4>
                    </div>
                    <div class="col-md-7 align-self-center text-right">
                        <div class="d-flex justify-content-end align-items-center">
                            <ol class="breadcrumb">
                                <li class="breadcrumb-item"><a href="javascript:void(0)">Home</a></li>
                                <li class="breadcrumb-item active"> Panchayat Configuration</li>
                            </ol>
                        </div>
                    </div>
                </div>
				
				
				 <!-- Validation wizard -->
                <div class="row" id="validation">
                    <div class="col-12">
                        <div class="card">
							<div class="card-header bg-info">
                                <h4 class="m-b-0 text-white">Panchayat Configuration</h4>
                            </div>
                            <div class="card-body wizard-content">
								@if(Session::has('msg'))
			<div class="alert alert-danger alert-dismissible fade show" role="alert">
				<strong> {{Session::get('msg')}}</strong>
				<button type="button" class="close" data-dismiss="alert" aria-label="Close"><span class="ti-close"></span>
				</button>
			</div>
			@endif
                                @if ($errors->any())
									<div class="alert alert-danger">
										<ul>
											@foreach ($errors->all() as $error)
												<li>{{ $error }}</li>
											@endforeach
										</ul>
									</div>
								@endif
                                <!-- post_step_first -->
                                <form method="post" action="{{route('')}}" class="needs-validation" novalidate="" enctype="multipart/form-data">
                                    <!-- Step 1 -->
                                   @csrf
								   
                                        <div class="row">
                                            <div class="col-md-6">
                                                <div class="form-group">
                                                    <label for="validationCustom01">Panchyat name</label>
															<input type="text" name="panc_name" class="form-control" id="validationCustom01" placeholder="Panchyat name.."  required=""> </div>
                                            </div>
                                            <div class="col-md-6">
                                                <div class="form-group">
                                                    <label for="validationCustom02">Panchyat Number</label>
															<input type="number" name="panc_number" class="required form-control" id="validationCustom02" placeholder="Enter number.." required=""> </div>
                                            </div>
                                        </div>
                                        <div class="row">
                                            
                                            <div class="col-md-6">
                                                <div class="form-group">
                                                    <label for="validationCustom03">Panchyat Email</label>
															<input type="email" name="panc_email" class="required form-control" id="validationCustom03" placeholder="Enter Email" required="">
												</div>
											</div>
											<div class="col-md-6">
                                                <div class="form-group">
                                                    <label for="validationCustom04">Panchyat Domain</label>
															<input type="text" name="panc_domain" class="required form-control" id="validationCustom04" placeholder="Enter Domain.." required="">
                                                </div>
                                            </div>
											
                                        </div>
                                        <div class="row">
                                            <div class="col-md-6">
                                                <div class="form-group">
                                                    <label for="validationCustomUsername">Panchyat Address</label>
															<div class="input-group">
																<textarea name="panc_address" class="required form-control" placeholder="Enter Panchyat Address.." id="validationCustomUsername" rows="2"></textarea>
																
															</div> 
												</div>
                                            </div>
											
											<div class="col-md-6">
                                                <div class="form-group">
                                                    <label for="validationCustomUsername">Panchyat City</label>
													<input type="text" name="panc_city" class="required form-control" id="validationCustom04" placeholder="Enter City.." required="">
												</div>
                                            </div>
											

                                       </div>
									   
									   <div class="row">
										<div class="col-md-6">
                                                <div class="form-group">
                                                    <label for="validationCustom04">Panchyat Pincode</label>
															<input type="number" name="panc_pincode" class="required form-control" id="validationCustom04" placeholder="Enter Pincode.." required="">
                                                </div>
                                            </div>
											<div class="col-md-6">
                                                <div class="form-group">
                                                    <label for="validationCustom01">Panchyat Logo</label>
															<input type="file" name="panc_logo" class="form-control"  required="">
															
                                            </div>
											</div>
                                            
                                            
                                       </div>
									   <div class="row">
									   <div class="col-md-6">
                                                <div class="form-group">
                                                    <label for="validationCustom02">Panchyat App Logo</label>
															<input type="file" name="panc_app_logo" class="form-control">
                                                </div>
                                            </div>
											
                                            
                                            
                                       </div>
									   
								
                                   
                                    <!-- Step 2 -->
                                   <div class="form-group text-right">
                                       <button class="btn btn-success " type="submit">Create Panchayat</button>
									   
                                    </div>
                                   
							   </form>
                            </div>
                        </div>
                    </div>

                </div>
                <!-- vertical wizard -->
               
				
				
			</div>
	</div>
@endsection

@section('js')
    <!--=========================*
        This Page Scripts
*===========================-->
    <!-- Validation Script -->
	 <script>
        //Custom design form example
        $(".tab-wizard").steps({
            headerTag: "h6",
            bodyTag: "section",
            transitionEffect: "fade",
            titleTemplate: '<span class="step">#index#</span> #title#',
            labels: {
                finish: "Submit"
            },
            onFinished: function (event, currentIndex) {
                Swal.fire("Form Submitted!", "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Sed lorem erat eleifend ex semper, lobortis purus sed.");

            }
        });


        var form = $(".validation-wizard").show();

        $(".validation-wizard").steps({
            headerTag: "h6",
            bodyTag: "section",
            transitionEffect: "fade",
            titleTemplate: '<span class="step">#index#</span> #title#',
            labels: {
                finish: "Submit"
            },
            onStepChanging: function (event, currentIndex, newIndex) {
                return currentIndex > newIndex || !(3 === newIndex && Number($("#age-2").val()) < 18) && (currentIndex < newIndex && (form.find(".body:eq(" + newIndex + ") label.error").remove(), form.find(".body:eq(" + newIndex + ") .error").removeClass("error")), form.validate().settings.ignore = ":disabled,:hidden", form.valid())
            },
            onFinishing: function (event, currentIndex) {
                return form.validate().settings.ignore = ":disabled", form.valid()
            },
            onFinished: function (event, currentIndex) {
                Swal.fire("Form Submitted!", "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Sed lorem erat eleifend ex semper, lobortis purus sed.");
            }
        }), $(".validation-wizard").validate({
            ignore: "input[type=hidden]",
            errorClass: "text-danger",
            successClass: "text-success",
            highlight: function (element, errorClass) {
                $(element).removeClass(errorClass)
            },
            unhighlight: function (element, errorClass) {
                $(element).removeClass(errorClass)
            },
            errorPlacement: function (error, element) {
                error.insertAfter(element)
            },
            rules: {
                email: {
                    email: !0
                }
            }
        })
    </script>

    <script>
        //  Form Validation
        window.addEventListener('load', function() {
            // Fetch all the forms we want to apply custom Bootstrap validation styles to
            var forms = document.getElementsByClassName('needs-validation');
            // Loop over them and prevent submission
            var validation = Array.prototype.filter.call(forms, function(form) {
                form.addEventListener('submit', function(event) {
                    if (form.checkValidity() === false) {
                        event.preventDefault();
                        event.stopPropagation();
                    }
                    form.classList.add('was-validated');
                }, false);
            });
        }, false);
    </script>
@endsection
