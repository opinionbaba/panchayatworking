@extends('layouts.f_dashboard.main')
{{-- Page Title --}}
@section('page-title')
    Citizen
@endsection
{{-- This Page Css --}}
@section('css')
@endsection
@section('main-content')
          <!-- Page Heading -->
	<div class="d-sm-flex align-items-center justify-content-between mb-4">
	  <h1 class="h3 mb-0 text-gray-800">Advertise Details</h1>
	</div>
<div class="">
 
  <div class="" >
    <div class="card shadow mb-4">
      <div class="card-header py-3 clearfix">
        <h6 class="m-0 font-weight-bold text-primary clearfix">Advertise Price
         <a href="{{route('new-ad')}}"><h1 style="float: right;"class="btn btn-warning">Create New Ad</h1></a>
        </h6>
  </div>
    <div class="card-body">
      <div class="table-responsive ">
        <table class="table table-bordered" id="dataTable" width="100%" cellspacing="0">
          <thead>
            <tr>
              <th>Ad size</th>
              <th>Slots</th>
              <th>Amount</th>
              <th>Price Type</th>
              <th>Panchayat</th>
            </tr>
          </thead>
          <tfoot>
            <tr>
              <th>Ad size</th>
              <th>Slots</th>
              <th>Amount</th>
              <th>Price Type</th>
              <th>Panchayat</th>
            </tr>
          </tfoot>
          <tbody>
            @foreach($price as $value)
            <tr>
              <td> {{$value->height.' X '.$value->width}}
          	    <div class="dropdown no-arrow">
          			   <a class="dropdown-toggle" href="#" role="button" id="dropdownMenuLink" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"><i class="fas fa-exclamation-circle"></i></a>
          			   <div class="dropdown-menu dropdown-menu-right shadow animated--fade-in" aria-labelledby="dropdownMenuLink">
          			     <div class="dropdown-header">Dropdown Header:</div>
          			      <a class="dropdown-item" href="#">250*250</a>
          				    <a class="dropdown-item" href="#">300*250</a>
          				    <a class="dropdown-item" href="#">728*90</a>
          			   </div>
          		  </div>
          	  </td>
					    <td>{{$value->name}} 
					      <div class="dropdown no-arrow">
							  <a class="dropdown-toggle" href="#" role="button" id="dropdownMenuLink1" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
							   <i class="fas fa-exclamation-circle"></i>
							  </a>
  							<div class="dropdown-menu dropdown-menu-right shadow animated--fade-in" aria-labelledby="dropdownMenuLink1">
  							  <div class="dropdown-header">Dropdown Header:</div>
  							      <a class="dropdown-item" href="#">Right</a>
  								  <a class="dropdown-item" href="#">Top</a>
  								  <a class="dropdown-item" href="#">Bottom</a>
  							  </div>
  						  </div>
					    </td>
              <td>{{$value->price}}</td>
              <td>{{$value->price_type}}</td>
              <td>{{$value->panc_name}}</td>
					     </td>
               </tr>
              @endforeach
            </tbody>
          </table>
        </div>
        </div>
      </div>
      </div>
    
        <!-- table 2 -->


  <div>  
    <div class="card shadow mb-4">
    <div class="card-header py-3 clearfix">
        <h6 class="m-0 font-weight-bold text-primary clearfix"> Booked Advertisements  </h6>
  </div>
    <div class="card-body">
      <div class="table-responsive ">
        <table class="table table-bordered" id="dataTable" width="100%" cellspacing="0">
          <thead>
            <tr>
              <th>Ad size</th>
              <th>Slots</th>
              <th>Price</th>
              <th>Price_type</th>
              <th>Start date</th>
              <th>End date</th>
              <th>Panchayt</th>
              <th>Status</th>
              <!-- <th>Report</th>
              <th>Expend</th> -->
            </tr>
          </thead>
          <tfoot>
            <tr>
              <th>Ad size</th>
              <th>Slots</th>
              <th>Price</th>
              <th>Price_type</th>
              <th>Start date</th>
              <th>End date</th>
              <th>Panchayt</th>
              <th>Status</th>
              <!-- <th>Report</th>
              <th>Expend</th> -->
            </tr>
          </tfoot>
          <tbody>
            @foreach($advertises as $value)
            <tr>
              <td> {{$value->height.' X '.$value->width}} 
                <div class="dropdown no-arrow">
                   <a class="dropdown-toggle" href="#" role="button" id="dropdownMenuLink" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"><i class="fas fa-exclamation-circle"></i></a>
                   <div class="dropdown-menu dropdown-menu-right shadow animated--fade-in" aria-labelledby="dropdownMenuLink">
                       <div class="dropdown-header">Dropdown Header:</div>
                      <a class="dropdown-item" href="#">250*250</a>
                      <a class="dropdown-item" href="#">300*250</a>
                      <a class="dropdown-item" href="#">728*90</a>
                   </div>
                </div>
              </td>
              <td>{{$value->name}}
                <div class="dropdown no-arrow">
                <a class="dropdown-toggle" href="#" role="button" id="dropdownMenuLink1" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                 <i class="fas fa-exclamation-circle"></i>
                </a>
                <div class="dropdown-menu dropdown-menu-right shadow animated--fade-in" aria-labelledby="dropdownMenuLink1">
                  <div class="dropdown-header">Dropdown Header:</div>
                      <a class="dropdown-item" href="#">Right</a>
                    <a class="dropdown-item" href="#">Top</a>
                    <a class="dropdown-item" href="#">Bottom</a>
                  </div>
                </div>
              </td>
              <td>{{$value->price}}</td>
              <td>{{$value->price_type}}</td>
              <td>{{$value->from_date}}</td>
              <td>{{$value->last_date}}</td>
              <td>{{$value->panc_name}}</td>
              <td>{{$value->status}}</td>
              <!-- <td><span class="badge badge-info">Active</span></td>
    
              <td> 
                <a href="#" class="btn btn-success btn-user btn-block"> Generate PDF </a>
              </td> -->
               </tr>
               @endforeach
            </tbody>
          </table>
        </div>
      </div>
    </div>
 </div> 
</div>

       <!-- end -->
      
@endsection
@section('js')

@endsection
