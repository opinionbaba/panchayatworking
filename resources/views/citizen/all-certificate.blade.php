@extends('layouts.f_dashboard.main')
{{-- Page Title --}}
@section('page-title')
    Citizen
@endsection
{{-- This Page Css --}}
@section('css')
@endsection
@section('main-content')
          <!-- Page Heading -->
	<div class="d-sm-flex align-items-center justify-content-between mb-4">
	  <h1 class="h3 mb-0 text-gray-800">Document Details</h1>
	</div>
	
	
   
  <div class="card shadow mb-4">
            <div class="card-header py-3">
              <h6 class="m-0 font-weight-bold text-primary">DataTables Example
			   <a href="{{route('certificate-request')}}"><h1 style="float: right;"class="btn btn-warning">Request New Certificate</h1></a>
			  </h6>
            </div>
            <div class="card-body">
              <div class="table-responsive">
                <table class="table table-bordered" id="dataTable" width="100%" cellspacing="0">
                  <thead>
                    <tr>
                      <th>Sr No</th>
                      <th>Certificate Name</th>
                      <th>Certificate Type</th>
                      <th>Submit Doc</th>
                      <th>Requested Date</th>
                      <th>Issue Date</th>
                      <th>Status</th>
                    </tr>
                  </thead>
                  <tbody>
                    <tr>
                      <td>1</td>
                      <td>Income</td>
                      <td>Type</td>
                      <td>Aadhar Card <div class="dropdown no-arrow">
							<a class="dropdown-toggle" href="#" role="button" id="dropdownMenuLink" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
							 <i class="fas fa-exclamation-circle"></i>
							</a>
							<div class="dropdown-menu dropdown-menu-right shadow animated--fade-in" aria-labelledby="dropdownMenuLink">
							  <div class="dropdown-header">Valid Doc:</div>
							      <a class="dropdown-item" href="#">Pan Card</a>
								  <a class="dropdown-item" href="#">Ration Card</a>
							  </div>
						  </div></td>
                      <td>12/21/2020</td>
                      <td>09/08/2020</td>
                      <td><span class="badge badge-info">Success</span></td>
                      
                    </tr>
                   </tbody>
                </table>
              </div>
            </div>
     @endsection
