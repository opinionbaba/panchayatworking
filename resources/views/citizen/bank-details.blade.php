@extends('layouts.f_dashboard.main')
{{-- Page Title --}}
@section('page-title')
    Citizen
@endsection
{{-- This Page Css --}}
@section('css')
@endsection
@section('main-content')
          <!-- Page Heading -->
  <div class="d-sm-flex align-items-center justify-content-between mb-4">
	<h1 class="h3 mb-0 text-gray-800">Bank Details</h1>
  </div>
    <div class="card o-hidden border-0 shadow-lg my-5">
      <div class="card-body p-0">
        <!-- Nested Row within Card Body -->
        <div class="row">
          <!--<div class="col-lg-5 d-none d-lg-block bg-register-image"></div>-->
          <div class="col-lg-12">
            <div class="p-5">
              <div class="text-left">
			   <div class="row">
			    <div class="col-md-6">
                 <h1 class="h4 text-gray-900 mb-4">Fill Bank Details</h1>
				</div>
				<div class="col-md-6">
				<a href="{{route('citizen-bank-detail')}}"><h1 style="float: right;"class="btn btn-info">View Bank Detail</h1></a>
				</div>
				</div>
              </div>
			  
			  <form id="myform"  action="{{route('save-details')}}" method="post" class="needs-validation user" novalidate="">
		@csrf
		<div class="form-row">
			<div class="col-md-6 mb-3">
				<label for="validationCustom01">Account Number</label>
				<input type="text" name="ac_num" class="form-control form-control-user" id="validationCustom01" placeholder="Account Number.."  required="">
			</div>
			<div class="col-md-6 mb-3">
				<label for="validationCustom01">Confirm Account Number</label>
				<input type="text" name="con_ac_num" class="form-control form-control-user" id="validationCustom01" placeholder="Confirm Account Number.."  required="">
			</div>
			
		 </div>
		 <div class="form-row">
		    <div class="col-md-6 mb-3">
				<label for="validationCustom01">IFSC Code</label>
				<input type="text" name="ifsc" class="form-control form-control-user"  id="input-ifsc" placeholder="IFSC Code.."  required="">
				</div>
			<div class="col-md-6 mb-3">
				<label for="validationCustom01">Bank Name</label>
				<input type="text" name="bank_name" class="form-control form-control-user" id="validationCustom01" placeholder="Bank Name.."  required="">
				
			</div>
			
		 </div>
		  <div class="form-row">
		     <div class="col-md-6 mb-3">
				<label for="validationCustom01">Branch</label>
				<input type="text" name="br_name" class="form-control form-control-user" id="validationCustom01" placeholder="Branch Name.."  required="">
				
			</div>
			
			<div class="col-md-6 mb-3">
				<label for="validationCustom01">UPI Id</label>
				<input type="text" name="upi" class="form-control form-control-user" id="validationCustom01" placeholder="UPI Id.."  required="">
				
			</div>
			
		 </div>
		  <div class="form-row">
		    <div class="col-md-6 mb-3">
				<label for="validationCustom01">Phone Pay Number</label>
				<input type="text" name="p_pay" class="form-control form-control-user" id="validationCustom01" placeholder="Phone Pay Number.."  required="">
				
			</div>
			
			<div class="col-md-6 mb-3">
				<label for="validationCustom01">Google Pay Number</label>
				<input type="text" name="g_pay" class="form-control form-control-user" id="validationCustom01" placeholder="Google Pay Number.."  required="">
				
			</div>
			<div class="col-md-6 mb-3">
				<label for="validationCustom01">Paytm Number</label>
				<input type="text" name="paytm_num" class="form-control form-control-user" id="validationCustom01" placeholder="Paytm Number.."  required="">
				
			</div>
		 </div>
		 <div class="row">
		   <div class="col-md-4 mb-3"></div>
		    <div class="col-md-4 mb-3">
		       <button class="btn btn-primary btn-user btn-block" type="submit">Submit form</button>
		    </div>
		  </div>
	</form>
             
              
            </div>
          </div>
        </div>
      </div>

        </div>
		 <script type="text/javascript">
	 $(document).ready(function() {
  $.validator.addMethod("ifsc", function(value, element) {
    var reg = /^[A-Za-z]{4}[0-9]{6,7}$/;
    if (this.optional(element)) {
      console.log(value);
      console.log(element);
      return true;
    }
    if (value.match(reg)) {
      return true;
    } else {
      return false;
    }
  }, "Please specify a valid IFSC CODE");

  $('#myform').validate({ // initialize the plugin
    rules: {
      ifsc: {
        required: true,
        ifsc: true
      }

    },
    submitHandler: function(form) {
      //alert('valid form submitted');
      return false;
    }
  });

});
	</script>

       @endsection


  