@extends('layouts.f_dashboard.main')
{{-- Page Title --}}
@section('page-title')
    Citizen
@endsection
{{-- This Page Css --}}
@section('css')
@endsection
@section('main-content')
          <!-- Page Heading -->
          <div class="d-sm-flex align-items-center justify-content-between mb-4">
            <h1 class="h3 mb-0 text-gray-800">Certificates</h1>
          </div>
		  
		 
			 
 <div class="card o-hidden border-0 shadow-lg my-5">
      <div class="card-body p-0">
        <!-- Nested Row within Card Body -->
        <div class="row">
          <!--<div class="col-lg-5 d-none d-lg-block bg-register-image"></div>-->
          <div class="col-lg-12">
            <div class="p-5">
              <div class="text-left">
                <h1 class="h4 text-gray-900 mb-4">Request Certificate</h1>
              </div>
			  
			  <div class="row">
			     <div class="col-md-6">
					<form action="{{asset('get-certi/data')}}" method="post" style="margin-bottom:10px">
						{{ csrf_field() }}
					 <select class="selectpicker" name="status"  class="form-control" data-live-search="true" onchange='this.form.submit()'>
						<option>--select panchayat--</option>
						@foreach($certificate as $value)
						<option value="{{$value->id}}" >{{$value->cer_name}}</option>
						@endforeach
					  </select>
					</form>
					
					<h5>Required Document : </h5>
					 
					 
				</div>
					
			 </div>
				
				<form action="{{route('add-certificate')}}" method="post" class="needs-validation" novalidate="">
				@csrf
				   <input type="hidden" name="_token" id="mytoken1" value="{{ csrf_token() }}" />
					<div class="form-row">
					    <div class="col-md-3 mb-3">
						   <label class="labl1">
								<input type="radio" name="optradio" data-toggle="collapse" data-target="#collapsible" />
								<div>For You</div>
							</label>
					    </div>
						
						  <div class="col-md-3 mb-3">
								<label class="labl1">
									<input type="radio" name="optradio" id="off"  />
									<div>For Family Member</div>
								</label>
						  </div>
					</div>
					
					
					<div class="form-row well collapse" id="collapsible"> 
						<div class="col-md-6 mb-3">
							<label for="validationCustom01">Select Member</label>
							<select name="" class="certi form-control form-control-user">
							 <option value=""  >--Select Member--</option>
							    <option value="">Main</option>
								<option value="">Sub member</option>
								<option value="">Sub member</option>
								<option value="">Sub member</option>
								<option value="">Sub member</option>
							 </select>
						</div>
					  </div>
					<div class="form-row">
					    <div class="col-md-6 mb-3">
							<label for="validationCustom01">Choose Certificate</label>
							<select name="" class="certi form-control form-control-user">
							 <option value=""  >--Select certificate--</option>
							   @foreach($certificate as $value)
								<option value="{{$value->id}}">{{$value->cer_name}}</option>
							   @endforeach
							</select>
						</div>
					</div>
					<div class="form-row">
						<div class="col-md-6 mb-3 green-color">
							<b>Required Doc : </b>Pan card , Aadhar Card <br>
						</div>
					</div>
					<div class="form-row">
						<div class="col-md-6 mb-3 red-color">
							( <b>Pending Doc : </b>Ration card , Passport )<br>
						</div>
					</div>
					
					<div class="form-row">
						<div class="col-md-6 mb-3">
							<label for="validationCustom01">Upload Pending Doc</label>
							 <input type="file" class=" form-control attachment_upload1" name="" style="height: 43px;" multiple>
						</div>
					</div>
		          <div class="row">
				   <div class="col-md-6 mb-3">
					   <button class="btn btn-primary btn-user btn-block" id="submit" type="button">Submit form</button>
					</div>
				  </div>
			   </form>
			    
             </div>
          </div>
        </div>
      </div>
	  
	  <style>
	    .selectpicker
		{
		   width: 100%;
			color: #9a9393;
			padding: 8px;
			border-radius: 5px;
			border: 1px solid #dcd7d7;
		}
	  </style>
	  <script type="text/javascript">
		$( "#status" ).change(function() { 
			//var id =$("#hid").val();
			var status =$("#status").val();
			$.ajax({
				type: "GET",
				url: "get-certi/"+ id ,
				success: function(data){
				console.log(data);
				$("#update").val(data);
				},
			});
		});
	</script>
<script>
$('.off').click(function() {
	alert('You want to off this ad');
	});

</script>
        </div>
        <!-- /.container-fluid -->
@endsection
 

 