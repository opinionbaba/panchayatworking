@extends('layouts.f_dashboard.main')
{{-- Page Title --}}
@section('page-title')
    Citizen
@endsection
{{-- This Page Css --}}
@section('css')
@endsection
@section('main-content')
<style>
.pta{
	height: 262px;
}
</style>
  <div class="d-sm-flex align-items-center justify-content-between mb-4">
	  <h1 class="h3 mb-0 text-gray-800">All Tax</h1>
	</div>
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>

	 <!-- DataTales Example -->
          <div class="card shadow mb-4">
            <div class="card-header py-3">
			  <div class="row">
			     <div class="col-md-6">
				    <a href="" class="btn btn-warning"   style="float:right;">Add Asset</a> 
				  </div>
			   </div>
			  </div>
		
           <div class="card-body">
			<div class="row">
				 <div class="col-md-4">
				 <div class="card" style="width: 20rem;">
				  <img class="card-img-top" src="{{asset('public/upload/h1.jpg')}}" alt="Card image cap">
				  <div class="card-body">
					<h4 class="card-title">Card title</h4>
					<p class="card-text">Tax : 500</p>
					<a href="{{route('tax-details')}}" class="btn btn-primary">More Information</a>
					<a href="" class="btn btn-primary">Pay</a>
				  </div>     
				 </div>
				</div>
		<div class="col-md-4">
			<div class="card" style="width: 20rem;">
			  <img class="card-img-top" src="{{asset('public/upload/h2.jpg')}}" alt="Card image cap">
			  <div class="card-body">
				<h4 class="card-title">Card title</h4>
				<p class="card-text">Tax : 700</p>
				<a href="{{route('tax-details')}}" class="btn btn-primary">More Information</a>
				<a href="" class="btn btn-primary">Pay</a>
			  </div>
			</div>
		</div>
		<div class="col-md-4">
			<div class="card" style="width: 20rem;">
			  <img class="card-img-top" src="{{asset('public/upload/h2.jpg')}}" alt="Card image cap">
			  <div class="card-body">
				<h4 class="card-title">Card title</h4>
				<p class="card-text">Tax : 700</p>
				 <input type="hidden" id ="status" value="1" >
				  <input type="hidden" name="_token" id="mytoken1" value="{{ csrf_token() }}" />
				  <input type="button" class="btn btn-info" value="update data" id="update_tax">
				<a href="{{route('tax-details')}}" class="btn btn-primary">More Information</a>
				<a href="" class="btn btn-primary">Pay</a>
				</div>
			</div>
		</div>
	</div>
	
	<script type="text/javascript">
  $(document).ready(function() 
{
$("#update_tax").click(function() 
{
var status = $('#status').val();
 var mytoken1 = $("#mytoken1").val();
	if(status!="" )
	{
		jQuery.ajax({
		type: "POST",
		url: "{{route('update-tax')}}",
		dataType: 'html',
		data: {status: status  , _token:mytoken1},
		success: function(res) 
		{
			if(res==1)
			{
			alert('Data saved successfully');	
			}
			else
			{
			alert('Data not saved');	
			}
			
		},
		error:function()
		{
		alert('data not saved');	
		}
		});
	}
	else
	{
	alert("pls fill all fields first");
	}

});
});
</script>
            </div>
          </div>
	   </div>
	   <div class="modal bs-example-modal-lg" id="addasset" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true" style="display: none;">
 <div class="modal-dialog ">
	<div class="modal-content">
		<div class="modal-header">
			<h4 class="modal-title" id="myLargeModalLabel">Edit Timing</h4>
			<button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
		</div>
		<div class="modal-body">
			<form action="" method ="post" class="needs-validation" novalidate="" enctype="multipart/form-data">
			  @csrf
				<div class="form-row">
				  <div class="col-md-6">
					<label for="validationCustom01">Enter Check In Time</label>
					<input type="text" name="name"  class="form-control" id="validationCustom01" placeholder="check in time..."  required="">
				  </div>
					<div class="col-md-6 mb-3">
						<label for="validationCustom01">Enter Check Out Time</label>
					<input type="text" name="name"  class="form-control" id="validationCustom01" placeholder="check out time..."  required="">
					</div>
				</div>
				<div class="form-row">
				  <div class="col-md-6">
					<label for="validationCustom01">Late Time</label>
					<input type="text" name="name"  class="form-control" id="validationCustom01" placeholder="late time..."  required="">
				  </div>
					<div class="col-md-6 mb-3">
						<label for="validationCustom01">Absent Time</label>
					<input type="text" name="name"  class="form-control" id="validationCustom01" placeholder="Absent time..."  required="">
					</div>
				</div>
			<button class="btn btn-primary" type="submit">Submit form</button>
		  </form>
		</div>
		<div class="modal-footer">
			<button type="button" class="btn btn-danger waves-effect text-left" data-dismiss="modal">Close</button>
		</div>
	</div>
	<!-- /.modal-content -->
</div>
</div>

 @endsection
