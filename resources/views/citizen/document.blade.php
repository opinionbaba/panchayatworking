@extends('layouts.f_dashboard.main')
{{-- Page Title --}}
@section('page-title')
    Citizen
@endsection
{{-- This Page Css --}}
@section('css')
@endsection
@section('main-content')
          <!-- Page Heading -->
	<div class="d-sm-flex align-items-center justify-content-between mb-4">
	  <h1 class="h3 mb-0 text-gray-800">Document Details</h1>
	</div>
     <div class="card o-hidden border-0 shadow-lg my-5">
      <div class="card-body p-0">
        <!-- Nested Row within Card Body -->
        <div class="row">
          <!--<div class="col-lg-5 d-none d-lg-block bg-register-image"></div>-->
          <div class="col-lg-12">
            <div class="p-5">
              <div class="text-left">
                <h1 class="h4 text-gray-900 mb-4">Fill Document Details</h1>
              </div>
			 <form method="post" action="{{route('save-doc')}}" class="needs-validation user" novalidate="" enctype="multipart/form-data">
	            @csrf
				<div class="form-row">
				<div class="col-md-4 mb-3">
					<label for="validationCustom01">Aadhar Number</label>
					<input type="text" name="aadhar_num" class="form-control form-control-user" id="validationCustom01" placeholder="Aadhar Number.."  required="">
					<div class="valid-feedback">
						Looks good!
					</div>
				</div>
				<div class="col-md-4 mb-3">
					<label for="validationCustom01">Aadhar Front Image</label>
					<input type="file" name="aadhar_front" class="form-control form-control-user"   required="">
					<div class="valid-feedback">
						Looks good!
					</div>
				</div>
				<div class="col-md-4 mb-3">
					<label for="validationCustom01">Aadhar Back Image</label>
					<input type="file" name="aadhar_back" class="form-control form-control-user"   required="">
					<div class="valid-feedback">
						Looks good!
					</div>
				</div>
			</div>
			
			
			<div class="form-row">
		<div class="col-md-4 mb-3">
			<label for="validationCustom01">Ration Card Number</label>
			<input type="text" name="ration_card" class="form-control form-control-user" id="validationCustom01" placeholder="Ration Card Number.."  required="">
			<div class="valid-feedback">
				Looks good!
			</div>
		</div>
		<div class="col-md-4 mb-3">
			<label for="validationCustom02">Ration Card Front</label>
			<input type="file" name="ration_front"  class="form-control form-control-user"  required="">
			<div class="valid-feedback">
				Looks good!
			</div>
		</div>
		<div class="col-md-4 mb-3">
			<label for="validationCustom02">Ration Card Back</label>
			<input type="file" name="ration_back"  class="form-control form-control-user"  required="">
			<div class="valid-feedback">
				Looks good!
			</div>
		</div>
	</div>
	
	
		<div class="form-row">
		<div class="col-md-4 mb-3">
			<label for="validationCustom01">Passport Number</label>
			<input type="text" name="passport" class="form-control form-control-user" id="validationCustom01" placeholder="Landlilne number..."  required="">
			<div class="valid-feedback">
				Looks good!
			</div>
		</div>
		<div class="col-md-4 mb-3">
			<label for="validationCustom02">Passport Front Image</label>
			<input type="file" name="passport_front" class="form-control form-control-user"  required="">
			<div class="valid-feedback">
				Looks good!
			</div>
		</div>
		<div class="col-md-4 mb-3">
			<label for="validationCustom02">Passport Back Image</label>
			<input type="file" name="passport_back" class="form-control form-control-user"  required="">
			<div class="valid-feedback">
				Looks good!
			</div>
		</div>
	</div>
	<div class="form-row">
		<div class="col-md-4 mb-3">
			<label for="validationCustom01">Voter Id</label>
			<input type="text" name="voter_id" class="form-control form-control-user" id="validationCustom01" placeholder="Enter Voter Id.."  required="">
			<div class="valid-feedback">
				Looks good!
			</div>
		</div>
		<div class="col-md-4 mb-3">
			<label for="validationCustom02">Voter Card Front</label>
			<input type="file" name="voter_front" class="form-control form-control-user"  required="">
			<div class="valid-feedback">
				Looks good!
			</div>
		</div>
		<div class="col-md-4 mb-3">
			<label for="validationCustom02">Voter Card Back</label>
			<input type="file" name="voter_back" class="form-control form-control-user"  required="">
			<div class="valid-feedback">
				Looks good!
			</div>
		</div>
	</div>
	<div class="form-row">
		<div class="col-md-6 mb-3">
			<label for="validationCustom01">Pan Number</label>
			<input type="text" name="pan_num" class="form-control form-control-user" id="validationCustom01" placeholder="Pan Number.."  required="">
			<div class="valid-feedback">
				Looks good!
			</div>
		</div>
		<div class="col-md-6 mb-3">
			<label for="validationCustom02">Pan Card Image</label>
			<input type="file" name="pan_image" class="form-control form-control-user"  required="">
			<div class="valid-feedback">
				Looks good!
			</div>
		</div>
	</div>
	<div class="form-row">
		<div class="col-md-6 mb-3">
			<label for="validationCustom01">Health Card Number</label>
			<input type="text" name="health_card" class="form-control form-control-user" id="validationCustom01" placeholder="Enter Health Card Number.." required="">
			<div class="valid-feedback">
				Looks good!
			</div>
		</div>
		<div class="col-md-6 mb-3">
			<label for="validationCustom01">Health Card Image</label>
			<input type="file" name="health_image" class="form-control form-control-user" required="">
			<div class="valid-feedback">
				Looks good!
			</div>
		</div>
	</div>
	<div class="form-row">
		<div class="col-md-6 mb-3">
			<label for="validationCustom01">Birth Certificate Number</label>
			<input type="text" name="birth_num" class="form-control form-control-user" id="validationCustom01" placeholder="Enter Birth Certificate Number.." required="">
			<div class="valid-feedback">
				Looks good!
			</div>
		</div>
		<div class="col-md-6 mb-3">
			<label for="validationCustom01">Birth Certificate Image</label>
			<input type="file" name="birth_image" class="form-control form-control-user" required="">
			<div class="valid-feedback">
				Looks good!
			</div>
		</div>
	</div>
	<div class="form-row">
		<div class="col-md-6 mb-3">
			<label for="validationCustom01">Driving Licence Number</label>
			<input type="text" name="driving" class="form-control form-control-user" id="validationCustom01" placeholder="Driving Licence Number.." required="">
			<div class="valid-feedback">
				Looks good!
			</div>
		</div>
		<div class="col-md-6 mb-3">
			<label for="validationCustom01">Driving Licence Image</label>
			<input type="file" name="driving_image" class="form-control form-control-user"   required="">
			<div class="valid-feedback">
				Looks good!
			</div>
		</div>
	</div>	
		<div class="row">
		   <div class="col-md-4 mb-3"></div>
		    <div class="col-md-4 mb-3">
		       <button class="btn btn-primary btn-user btn-block" type="submit">Submit form</button>
		    </div>
		  </div>
	</form>
             
              
            </div>
          </div>
        </div>
      </div>
   </div>
         </div>
     @endsection
