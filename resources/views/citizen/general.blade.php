@extends('layouts.f_dashboard.main')
{{-- Page Title --}}
@section('page-title')
    Citizen
@endsection
{{-- This Page Css --}}
@section('css')
@endsection
@section('main-content')
          <!-- Page Heading -->
	<div class="d-sm-flex align-items-center justify-content-between mb-4">
	  <h1 class="h3 mb-0 text-gray-800">Document Details</h1>
	</div>
     <div class="card o-hidden border-0 shadow-lg my-5">
      <div class="card-body p-0">
        <!-- Nested Row within Card Body -->
        <div class="row">
          <!--<div class="col-lg-5 d-none d-lg-block bg-register-image"></div>-->
          <div class="col-lg-12">
            <div class="p-5">
              <div class="text-left">
                <h1 class="h4 text-gray-900 mb-4">General Details</h1>
              </div>       
                            
			  <div class="row">
			   @foreach($citizen as $value)
				 <div class="col-md-6">
					   <ul class="list-group list-group-flush">
						  <li class="list-group-item"><b>Citizen Type : </b>@if($value->citizen_type == '1')
						   <span class="badge badge-info">Individual</span>
					   @else
						    <span class="badge badge-info">Business</span>
						@endif
						 </li>
						  <li class="list-group-item"><b>Citizen Name : </b>{{$value->f_name}}</li>
						  <li class="list-group-item"><b>Ward Number : </b>{{$value->ward_number}}</li>
						  <li class="list-group-item"><b>Mobile : </b>{{$value->mobile}}</li>
						  <li class="list-group-item"><b>Land Line Num : </b>{{$value->land_number}}</li>
						  <li class="list-group-item"><b>Family Member : </b>{{$value->family_number}}</li>
						  <li class="list-group-item"><b>Citizen Email : </b>{{$value->email}}</li>
						 </ul>
				   </div>
					<div class="col-md-6">
						<ul class="list-group list-group-flush">
						  <li class="list-group-item"><b>House Number : </b>{{$value->house_number}}</li>
						  <li class="list-group-item"><b>Area: </b>{{$value->area}}</li>
						  <li class="list-group-item"><b>Locality : </b>{{$value->locality}}</li>
						  <li class="list-group-item"><b>Village : </b>{{$value->village}}</li>
						  <li class="list-group-item"><b>City : </b>{{$value->city}}</li>
						  <li class="list-group-item"><b>State : </b>{{$value->state}}</li>
						  <li class="list-group-item"><b>Pin Code : </b>{{$value->pin_code}}</li>
					   </ul>
					</div>
				@endforeach
			  </div>
             </div>
          </div>
        </div>
      </div>
   </div>
         </div>
     @endsection
