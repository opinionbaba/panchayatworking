@extends('layouts.f_dashboard.main')
{{-- Page Title --}}
@section('page-title')
    Citizen
@endsection
{{-- This Page Css --}}
@section('css')
@endsection
@section('main-content')
 <div class="row">
		      <div class="col-md-6">
			     <div class="hello_div">
					<h1 class="h3 mb-0">Hello, Shubham!</h1> 
					<p>Welcome To Village Panchayat Bethoda</p>			
				  </div>
				  

                <div id="calendar"></div>				  
				 
				    <!-- Dropdown Card Example -->
              <div class="card shadow mb-4">
                <!-- Card Header - Dropdown -->
                <div class="card-header py-3 d-flex flex-row align-items-center justify-content-between">
                  <h6 class="m-0 font-weight-bold text-primary">ALl Request</h6>
                  <div class="dropdown no-arrow">
                    <a class="dropdown-toggle" href="#" role="button" id="dropdownMenuLink" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                      <i class="fas fa-ellipsis-v fa-sm fa-fw text-gray-400"></i>
                    </a>
                    <div class="dropdown-menu dropdown-menu-right shadow animated--fade-in" aria-labelledby="dropdownMenuLink">
                      <div class="dropdown-header">Dropdown Header:</div>
                      <a class="dropdown-item" href="#">Action</a>
                      <a class="dropdown-item" href="#">Another action</a>
                      <div class="dropdown-divider"></div>
                      <a class="dropdown-item" href="#">Something else here</a>
                    </div>
                  </div>
                </div>
                <!-- Card Body -->
                <div class="card-body">
                   	<div class="table-container">
							<table class="table table-filter">
								<tbody>
									<tr data-status="pagado">
										<td>
											<div class="ckbox">
												<input type="checkbox" id="checkbox1">
												<label for="checkbox1"></label>
											</div>
										</td>
										
										<td>
											<div class="media">
												<a href="#" class="pull-left">
													<img src="https://s3.amazonaws.com/uifaces/faces/twitter/fffabs/128.jpg" class="media-photo">
												</a>
												<div class="media-body">
													<span class="media-meta pull-right">Febrero 13, 2016</span>
													<h4 class="title">
														Lorem Impsum
														<span class="pull-right pagado">(Pagado)</span>
													</h4>
													<p class="summary">Ut enim ad minim veniam, quis nostrud exercitation...</p>
												</div>
											</div>
										</td>
									</tr>
									<tr data-status="pendiente">
										<td>
											<div class="ckbox">
												<input type="checkbox" id="checkbox3">
												<label for="checkbox3"></label>
											</div>
										</td>
										
										<td>
											<div class="media">
												<a href="#" class="pull-left">
													<img src="https://s3.amazonaws.com/uifaces/faces/twitter/fffabs/128.jpg" class="media-photo">
												</a>
												<div class="media-body">
													<span class="media-meta pull-right">Febrero 13, 2016</span>
													<h4 class="title">
														Lorem Impsum
														<span class="pull-right pendiente">(Pendiente)</span>
													</h4>
													<p class="summary">Ut enim ad minim veniam, quis nostrud exercitation...</p>
												</div>
											</div>
										</td>
									</tr>
									<tr data-status="cancelado">
										<td>
											<div class="ckbox">
												<input type="checkbox" id="checkbox2">
												<label for="checkbox2"></label>
											</div>
										</td>
										
										<td>
											<div class="media">
												<a href="#" class="pull-left">
													<img src="https://s3.amazonaws.com/uifaces/faces/twitter/fffabs/128.jpg" class="media-photo">
												</a>
												<div class="media-body">
													<span class="media-meta pull-right">Febrero 13, 2016</span>
													<h4 class="title">
														Lorem Impsum
														<span class="pull-right cancelado">(Cancelado)</span>
													</h4>
													<p class="summary">Ut enim ad minim veniam, quis nostrud exercitation...</p>
												</div>
											</div>
										</td>
									</tr>
									<tr data-status="pagado" class="selected">
										<td>
											<div class="ckbox">
												<input type="checkbox" id="checkbox4" checked>
												<label for="checkbox4"></label>
											</div>
										</td>
										
										<td>
											<div class="media">
												<a href="#" class="pull-left">
													<img src="https://s3.amazonaws.com/uifaces/faces/twitter/fffabs/128.jpg" class="media-photo">
												</a>
												<div class="media-body">
													<span class="media-meta pull-right">Febrero 13, 2016</span>
													<h4 class="title">
														Lorem Impsum
														<span class="pull-right pagado">(Pagado)</span>
													</h4>
													<p class="summary">Ut enim ad minim veniam, quis nostrud exercitation...</p>
												</div>
											</div>
										</td>
									</tr>
									<tr data-status="pendiente">
										<td>
											<div class="ckbox">
												<input type="checkbox" id="checkbox5">
												<label for="checkbox5"></label>
											</div>
										</td>
										
										<td>
											<div class="media">
												<a href="#" class="pull-left">
													<img src="https://s3.amazonaws.com/uifaces/faces/twitter/fffabs/128.jpg" class="media-photo">
												</a>
												<div class="media-body">
													<span class="media-meta pull-right">Febrero 13, 2016</span>
													<h4 class="title">
														Lorem Impsum
														<span class="pull-right pendiente">(Pendiente)</span>
													</h4>
													<p class="summary">Ut enim ad minim veniam, quis nostrud exercitation...</p>
												</div>
											</div>
										</td>
									</tr>
								</tbody>
							</table>
						</div>
                </div>
              </div>
				   
			  </div>
			  
		    <div class="col-md-6 side-right">
			    <!-- Earnings (Monthly) Card Example -->
            <div class="row">
				<div class="col-xl-6 col-md-6 mb-4">
				  <div class="card h-100">
					<div class="card-body">
					  <div class="row no-gutters align-items-center">
					    <div class="icon-box b1">
						  <i class="fas fa-calendar fa-2x text-white"></i>
						  <i class="fas fa-ellipsis-v text-white float-right mt-2"></i>
						</div>
						<div class="col">
						  <div class="font-weight-bold text-primary text-uppercase mb-1">Opened tickets</div>
						  <div class="mb-0 font-weight-bold text-gray-800">40,000</div>
						</div>
						
					  </div>
					</div>
				  </div>
				</div>
				
				
				<div class="col-xl-6 col-md-6 mb-4">
				  <div class="card h-100">
					<div class="card-body">
					  <div class="row no-gutters align-items-center">
					    <div class="icon-box b2">
						  <i class="fas fa-calendar fa-2x text-white"></i>
						   <i class="fas fa-ellipsis-v text-white float-right mt-2"></i>
						</div>
						<div class="col">
						  <div class="font-weight-bold text-primary text-uppercase mb-1">closed tickets</div>
						  <div class="mb-0 font-weight-bold text-gray-800">40,000</div>
						</div>
						
					  </div>
					</div>
				  </div>
				</div>
				
				
				<div class="col-xl-6 col-md-6 mb-4">
				  <div class="card  h-100">
					<div class="card-body">
					  <div class="row no-gutters align-items-center">
					    <div class="icon-box b3">
						  <i class="fas fa-calendar fa-2x text-white"></i>
						   <i class="fas fa-ellipsis-v text-white float-right mt-2"></i>
						</div>
						<div class="col">
						  <div class="text-xs font-weight-bold text-primary text-uppercase mb-1">certificates issued</div>
						  <div class="mb-0 font-weight-bold text-gray-800">40,000</div>
						</div>
						
					  </div>
					</div>
				  </div>
				</div>
				
				<div class="col-xl-6 col-md-6 mb-4">
				  <div class="card h-100">
					<div class="card-body">
					  <div class="row no-gutters align-items-center">
					    <div class="icon-box b4">
						  <i class="fas fa-calendar fa-2x text-white"></i>
						   <i class="fas fa-ellipsis-v text-white float-right mt-2"></i>
						</div>
						<div class="col">
						  <div class="font-weight-bold text-primary text-uppercase mb-1">certificates pending</div>
						  <div class="mb-0 font-weight-bold text-gray-800">40,000</div>
						</div>
						
					  </div>
					</div>
				  </div>
				</div>
             </div>
			 
              <div class="card shadow mb-4">
                <div class="card-header py-3">
                  <h6 class="m-0 font-weight-bold text-primary">Status About Information</h6>
                </div>
                <div class="card-body">
                  <h4 class="small font-weight-bold">approved docs <span class="float-right">20%</span></h4>
                  <div class="progress mb-4">
                    <div class="progress-bar bg-danger" role="progressbar" style="width: 20%" aria-valuenow="20" aria-valuemin="0" aria-valuemax="100"></div>
                  </div>
                  <h4 class="small font-weight-bold">pending docs <span class="float-right">40%</span></h4>
                  <div class="progress mb-4">
                    <div class="progress-bar bg-warning" role="progressbar" style="width: 40%" aria-valuenow="40" aria-valuemin="0" aria-valuemax="100"></div>
                  </div>
                  <h4 class="small font-weight-bold">Re-submit docs <span class="float-right">60%</span></h4>
                  <div class="progress mb-4">
                    <div class="progress-bar" role="progressbar" style="width: 60%" aria-valuenow="60" aria-valuemin="0" aria-valuemax="100"></div>
                  </div>
                 
                </div>
              </div>
           
		     <div class="row">
				<div class="col-xl-6 col-md-6 mb-4">
				  <div class="card h-100">
					<div class="card-body">
					  <div class="row no-gutters align-items-center">
					    <div class="icon-box b1">
						  <i class="fas fa-calendar fa-2x text-white"></i>
						  <i class="fas fa-ellipsis-v text-white float-right mt-2"></i>
						</div>
						<div class="col">
						  <div class="font-weight-bold text-primary text-uppercase mb-1">Total tax paid</div>
						  <div class="mb-0 font-weight-bold text-gray-800">40,000</div>
						</div>
						
					  </div>
					</div>
				  </div>
				</div>
				
				
				<div class="col-xl-6 col-md-6 mb-4">
				  <div class="card h-100">
					<div class="card-body">
					  <div class="row no-gutters align-items-center">
					    <div class="icon-box b2">
						  <i class="fas fa-calendar fa-2x text-white"></i>
						   <i class="fas fa-ellipsis-v text-white float-right mt-2"></i>
						</div>
						<div class="col">
						  <div class="font-weight-bold text-primary text-uppercase mb-1">Total amount paid</div>
						  <div class="mb-0 font-weight-bold text-gray-800">40,000</div>
						</div>
						
					  </div>
					</div>
				  </div>
				</div>
				</div>
				
				   
		     <div class='calc'>
				 <div id='disp'></div>
				 <button id='1'>1</button>
				 <button id='2'>2</button>
				 <button id='3'>3</button>
				 <button id='4'>4</button>
				 <button id='5'>5</button>
				 <button id='6'>6</button>
				 <button id='7'>7</button>
				 <button id='8'>8</button>
				 <button id='9'>9</button>				 
				 <button id='0'>0</button>
				 
				 <button id='subtr'>-</button>
				 <button id='add'>+</button>
				 <button id='mult'>x</button>
				 <button id='clear'>CE</button>
				 <button id='dec'>.</button>
				 <button id='division'>/</button>
				 <button id='ans'>=</button>
			  </div>
             </div>
		   </div>
          </div>
		  
	

		  @endsection