
@extends('layouts.f_dashboard.main')
{{-- Page Title --}}
@section('page-title')
    Citizen
@endsection
{{-- This Page Css --}}
@section('css')
@endsection
@section('main-content')
<style>
.pta{
	height: 262px;
}
</style>
<div class="d-sm-flex align-items-center justify-content-between mb-4">
	  <h1 class="h3 mb-0 text-gray-800">Create Advertise</h1>
</div>
   
	 
<form id="regForm" action="{{route('adsrequest')}}" method="post" class="user" enctype="multipart/form-data">
	
	@csrf
	<div class="tab">
	     <div class="row">
			<div class="col-md-12">
			  <h4 class="card-title">Advertise Size in particular size image</h4>
			</div>
			@foreach($ads as $value)
			<div class="col-md-4">
				<div class="form-group">
				   <p class="help-block">* Upload Image Size : {{$value->height}} X {{$value->width}} </p>
				  <div class="input-group">
					<input id="fakeUploadLogo{{$value->id}}" class="form-control fake-shadow" placeholder="Choose File" disabled="disabled">
					<div class="input-group-btn">
					  <div class="fileUpload btn btn-danger fake-shadow">
						<span>Upload Image</span>
						<input id="logo-id1" name="ads_img_{{$value->id}}" type="file"  previd ="img-preview{{$value->id}}" fakeid="fakeUploadLogo{{$value->id}}" class="attachment_upload1 me">
					  </div>
					</div>
				  </div>
				   <div class="main-img-preview">
					<img class="img-preview1" id="img-preview{{$value->id}}" src="{{asset('public/citizens/img/unnamed.png')}}" title="Preview Logo">
				  </div>
				  
				   <input type="hidden" name="slot[]" value="{{$value->id}}">
				   <input type="text" name="ads_url_{{$value->id}}"  value = "moofth" class="weblink form-control form-control-user"  placeholder="Redirect Link.."  required="">
				  
				   <input type="text" name="ads_date_{{$value->id}}" value = "180" class="days form-control form-control-user"  placeholder="Number of Days.."  required="">
				   
				</div>
			</div>
			@endforeach
			
		 </div>
	</div>
	<div class="tab">
		<div class="form-check">
		  <div class="row">
			<div class="col-md-12"  style="margin-top:40px;margin-bottom:40px;">
			  <h5>Select Slots</h5>
			</div>
			@foreach($panchayat as $value)
		    <div class="col-md-4 ad-box">
			   <label class="labl">
				<input type="checkbox" name="panchayat[]"  value="{{$value->id}}" website="{{$value->panc_name}}" class="website" />
				<div>
				    <div class="hovereffect">
						<img class="img-responsive" src="{{asset('public/citizens/img/3.png')}}" alt="">
						<div class="overlay">
						   <h2>{{$value->panc_name}}</h2>
						   <div class="info" >
						      <h3> {{$value->panc_name}}</h3>
							  <a href="{{$value->panc_domain}}" target="blank">{{$value->panc_domain}}</a>
							  @foreach($panchyat_ads as $ads)
							  	@if($ads->panchayat_id == $value->id)
							  		<h6>Size {{$ads->height}} X {{$ads->width}} : Rs. {{$ads->price}}/ {{$ads->price_type}}</h6>
							  		<span style="display: none" class="ads_{{$value->id}}" size="{{$ads->height}} X {{$ads->width}}" price="{{$ads->price}}" price_type="{{$ads->price_type}}" price_id="{{$ads->id}}"></span>
							  	@endif
							  @endforeach
						   </div>
						</div>
					</div>
				</div>
			  </label>
			 
			</div>
		   	@endforeach
		  	</div>
		</div>
	</div>

	<!-- tab -->
	<script type="text/javascript">
		
	</script>
	<div class="tab">
		<div class="form-check">
			<div class="row">
				<div class="col-md-12">
				  <h5 style="margin-bottom:30px;">Selected Slot's Details</h5>
				</div>
				<div id="pastehere" class="row" style="width: 100%;">

				</div>
			</div>
		</div>
	</div>
<!-- end tab -->
<!-- new tab -->
    <div class="tab">
	   	 
		  <table id="cart" class="table table-hover table-condensed">
		<thead>
			<tr>
				<th style="width:58%">Advertise Name</th>
				<th style="width:10%">Price</th>
				<th style="width:18%" class="text-center">Subtotal</th>
			</tr>
		</thead>
		<tbody id="invoice">
			
		</tbody>
		<tfoot>
			<tr>
				<td class="text-right" colspan="3"><strong>Total <span class="subt" id="subtotal-1"></span></strong></td>
			</tr>
			<tr>
				<td class="hidden-xs text-right" colspan="2"><strong>Total Rs:<span class="subt" id="subtotal-2"></span></strong></td>
				<td><a href="#" class="btn btn-success btn-block">Checkout <i class="fa fa-angle-right"></i></a></td>
			</tr>
		</tfoot>
	</table>
	</div>

	<div style="overflow:auto;">
	  <div style="float:right;">
		<button type="button" class="btn btn-primary btn-user" id="prevBtn" onclick="nextPrev(-1)">Previous</button>
		<button type="button" class="btn btn-primary btn-user" id="nextBtn" onclick="nextPrev(1)">Next</button>
	  </div>
	</div>

	<!-- Circles which indicates the steps of the form: -->
	<div style="text-align:center;margin-top:40px;display:none;">
	  <span class="step"></span>
	  <span class="step"></span>
	  <span class="step"></span>
	  <span class="step"></span>
	</div>
	<!-- imformation -->
</form>
</div>
@endsection
