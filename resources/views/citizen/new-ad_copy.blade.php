
@extends('layouts.f_dashboard.main')
{{-- Page Title --}}
@section('page-title')
    Citizen
@endsection
{{-- This Page Css --}}
@section('css')
@endsection
@section('main-content')
<style>
.pta{
	height: 262px;
}
</style>
<div class="d-sm-flex align-items-center justify-content-between mb-4">
	  <h1 class="h3 mb-0 text-gray-800">Create Advertise</h1>
</div>
     
	 
<form id="regForm" action="{{route('adsrequest')}}" method="post" class="user ajaxform">
 
 
	@csrf
	<input type="hidden" name="action-url" value="{{route('ads-update')}}">
	<div class="tab">
	     <div class="row">
			<div class="col-md-12">
			  <h4 class="card-title">Advertise Size in particular size image</h4>
			</div>
						
			<div class="col-md-4">
				<div class="form-group">
				   <p class="help-block">* Upload Image Size : 250*250 </p>
				  <div class="input-group">
					<input id="fakeUploadLogo1" class="form-control fake-shadow" placeholder="Choose File" disabled="disabled">
					<div class="input-group-btn">
					  <div class="fileUpload btn btn-danger fake-shadow">
						<span>Upload Image</span>
						<input id="logo-id1" name="logo_1" type="file" class="attachment_upload1">
					  </div>
					</div>
				  </div>
				   <div class="main-img-preview">
					<img class="img-preview1" src="{{asset('public/citizens/img/unnamed.png')}}" title="Preview Logo">
				  </div>
				  
				  
				   <input type="text" name="ration_card" class="form-control form-control-user" value="moofth.com"  placeholder="Redirect Link.."  required="">
				  
				   <input type="text" name="ration_card" class="form-control form-control-user"  value="moofth.com" placeholder="Number of Days.."  required="">
				   
				</div>
			</div>
			
			<div class="col-md-4">
				<div class="form-group">
				  <p class="help-block">* Upload Image Size : 300*250 </p>						
				  <div class="input-group">
					<input id="fakeUploadLogo2" class="form-control fake-shadow" placeholder="Choose File" disabled="disabled">
					<div class="input-group-btn">
					  <div class="fileUpload btn btn-danger fake-shadow">
						<span>Upload Image</span>
						<input id="logo-id2" name="logo_2" type="file" class="attachment_upload2">
					  </div>
					</div>
				  </div>
				  
					<div class="main-img-preview">
					<img class="img-preview2" src="{{asset('public/citizens/img/unnamed.gif')}}" title="Preview Logo">
				  </div>
				  
				  
				   <input type="text" name="ration_card" class="form-control form-control-user" value="moofth.com" placeholder="Redirect Link.."  required="">
				   
				  
				   <input type="text" name="ration_card" class="form-control form-control-user" value="moofth.com" placeholder="Number of Days.."  required="">
				   
				</div>
			</div>
			
			
			<div class="col-md-4">
				<div class="form-group">
				  <p class="help-block">* Upload Image Size : 728*90 </p>
				 
				  <div class="input-group">
					<input id="fakeUploadLogo3" class="form-control fake-shadow" placeholder="Choose File" disabled="disabled">
					<div class="input-group-btn">
					  <div class="fileUpload btn btn-danger fake-shadow ">
						<span>Upload Image</span>
						<input id="logo-id3" name="logo_3" type="file" class="attachment_upload3">
					  </div>
					</div>
				  </div>
				   <div class="main-img-preview">
					<img class="thumbnail img-preview3" src="{{asset('public/citizens/img/category-thumbnail-1554122903370.jpg')}}" title="Preview Logo">
				  </div>
				  
				  
				   <input type="text" name="ration_card" class="form-control form-control-user" value="moofth.com"  placeholder="Redirect Link.."  required="">
				   
				  
				   <input type="text" name="ration_card" class="form-control form-control-user" value="180"  placeholder="Number of Days.."  required="">
				
				</div>
			</div>
		 </div>
	</div>

	
	<div class="tab">
	<div class="form-check">
	  <div class="row">
	    <div class="col-md-6">
		  <h5>Select And Search Slots </h5>
		  
		    <select class="form-control select2">
	           <option>Select</option> 
	           <option>Car</option> 
	           <option>Bike</option> 
	           <option>Scooter</option> 
	           <option>Cycle</option> 
	           <option>Horse</option> 
	        </select>
		</div>
		<div class="col-md-12"  style="margin-top:40px;margin-bottom:40px;">
		  <h5>Select Slots</h5>
		</div>
	    <div class="col-md-4 ad-box">
		   <label class="labl">
			<input type="checkbox" name="radioname" value="one_value" checked="checked"/>
			<div>
			    <div class="hovereffect">
					<img class="img-responsive" src="{{asset('public/citizens/img/category-thumbnail-1554122903370.jpg')}}" alt="">
					<div class="overlay">
					   <h2> Website 1</h2>
					   <div class="info" >
					      <h3> Website 1</h3>
						  <a href="http://kbeesolutions.in/new-ad">www.google.com</a>
					     <h6>Size 250*250 : Rs. 200/Day</h6>
					     <h6>Size 300*250 : Rs. 300/Day</h6>
					     <h6>Size 728*90 : Rs. 400/Day</h6>
					   </div>
					</div>
				</div>
			</div>
		  </label>
		 
		</div>
	   
	    <div class="col-md-4 ad-box">
		   <label class="labl">
			<input type="checkbox" name="radioname" value="second" />
			<div>
			    <div class="hovereffect">
					<img class="img-responsive" src="{{asset('public/citizens/img/category-thumbnail-1554122903370.jpg')}}" alt="">
					<div class="overlay">
					   <h2> Website 2</h2>
					   <div class="info">
					     <h3> Website 1</h3>
						  <a href="http://kbeesolutions.in/new-ad">www.google.com</a>
					     <h6>Size 250*250 : Rs. 200/Day</h6>
					     <h6>Size 300*250 : Rs. 300/Day</h6>
					     <h6>Size 728*90 : Rs. 400/Day</h6>
					   </div>
					</div>
				</div>			 
			</div>
		   </label>
		  
		</div>
	   
	    <div class="col-md-4 ad-box">
		   <label class="labl">
			<input type="checkbox" name="radioname" value="third" />
			<div>
			    <div class="hovereffect">
					<img class="img-responsive" src="{{asset('public/citizens/img/category-thumbnail-1554122903370.jpg')}}" alt="">
					<div class="overlay">
					   <h2> Website 3</h2>
					   <div class="info">
                         <h3> Website 1</h3>
						  <a href="http://kbeesolutions.in/new-ad">www.google.com</a>
					     <h6>Size 250*250 : Rs. 200/Day</h6>
					     <h6>Size 300*250 : Rs. 300/Day</h6>
					     <h6>Size 728*90 : Rs. 400/Day</h6>
					   </div>
					</div>
				</div>			 
			</div>
		   </label>		  
		</div>
		
		 <div class="col-md-4 ad-box">
		   <label class="labl">
			<input type="checkbox" name="radioname" value="third2" />
			<div>
			    <div class="hovereffect">
					<img class="img-responsive" src="{{asset('public/citizens/img/category-thumbnail-1554122903370.jpg')}}" alt="">
					<div class="overlay">
					   <h2> Website 4</h2>
					   <div class="info">
					      <h3> Website 1</h3>
						  <a href="http://kbeesolutions.in/new-ad">www.google.com</a>
					     <h6>Size 250*250 : Rs. 200/Day</h6>
					     <h6>Size 300*250 : Rs. 300/Day</h6>
					     <h6>Size 728*90 : Rs. 400/Day</h6>
					   </div>
					</div>
				</div>			 
			</div>
		   </label>		  
		</div>
	  </div>
	  </div>
	</div>


	
	
	<div class="tab">
	    <div class="form-check">
		  <div class="row">
			<div class="col-md-12">
			  <h5 style="margin-bottom:30px;">Selected Slot's Details</h5>
			</div>
			<div class="col-md-4">
			    <div class="slot-detail">
				  <img class="img-responsive" src="{{asset('public/citizens/img/category-thumbnail-1554122903370.jpg')}}" alt="">
				   <ul>
				     <li>
					   <h6>Name Of Website : <span> Websites 1</span></h6>
					 </li>
					 
					  <li>
					   <h6>Redirect To : <span> http://kbeesolutions.in/new-ad</span></h6>
					 </li>
					 
					  <li>
					   <h6>Number Of Days : <span>  20 Days</span></h6>
					 </li>
					 
					  <li>
					   <h6>Advertise Details</span></h6>
					 </li>
					 
					  <li>
					    <h6>Post 250*250 : <span>4000 Rs.</span></h6>
					  </li>
					  
					    <li>
					    <h6>Post 300*250 : <span>6000 Rs.</span></h6>
					  </li>
					  
					    <li>
					    <h6>Post 728*90 : <span>8000 Rs.</span></h6>
					  </li>
				   </ul>	
				</div>
			</div>
		   
			<div class="col-md-4 ad-box">
			   <div class="slot-detail">
				  <img class="img-responsive" src="{{asset('public/citizens/img/category-thumbnail-1554122903370.jpg')}}" alt="">
				   <ul>
				     <li>
					   <h6>Name Of Website : <span> Websites 1</span></h6>
					 </li>
					 
					  <li>
					   <h6>Redirect To : <span> http://kbeesolutions.in/new-ad</span></h6>
					 </li>
					 
					  <li>
					   <h6>Number Of Days : <span>  20 Days</span></h6>
					 </li>
					 
					  <li>
					   <h6>Advertise Details</span></h6>
					 </li>
					 
					  <li>
					    <h6>Post 250*250 : <span>4000 Rs.</span></h6>
					  </li>
					  
					    <li>
					    <h6>Post 300*250 : <span>6000 Rs.</span></h6>
					  </li>
					  
					    <li>
					    <h6>Post 728*90 : <span>8000 Rs.</span></h6>
					  </li>
				   </ul>	
				</div>
			</div>
			
			<div class="col-md-4 ad-box">
			    <div class="slot-detail">
				  <img class="img-responsive" src="{{asset('public/citizens/img/category-thumbnail-1554122903370.jpg')}}" alt="">
				   <ul>
				     <li>
					   <h6>Name Of Website : <span> Websites 1</span></h6>
					 </li>
					 
					  <li>
					   <h6>Redirect To : <span> http://kbeesolutions.in/new-ad</span></h6>
					 </li>
					 
					  <li>
					   <h6>Number Of Days : <span>  20 Days</span></h6>
					 </li>
					 
					  <li>
					   <h6>Advertise Details</span></h6>
					 </li>
					 
					  <li>
					    <h6>Post 250*250 : <span>4000 Rs.</span></h6>
					  </li>
					  
					    <li>
					    <h6>Post 300*250 : <span>6000 Rs.</span></h6>
					  </li>
					  
					    <li>
					    <h6>Post 728*90 : <span>8000 Rs.</span></h6>
					  </li>
				   </ul>	
				</div>
			</div>
			
			
		 </div>
		</div>
	   
	 
	</div>


    <div class="tab">
	   	 
		  <table id="cart" class="table table-hover table-condensed">
		<thead>
			<tr>
				<th style="width:58%">Advertise Name</th>
				<th style="width:10%">Price</th>
				<th style="width:18%" class="text-center">Subtotal</th>
			</tr>
		</thead>
		<tbody>
			<tr>
				<td data-th="Product">
					<div class="row">
						<div class="col-sm-2 hidden-xs"><img src="http://placehold.it/100x100" alt="..." class="img-responsive"/></div>
						<div class="col-sm-10">
							<h4 class="nomargin">Product 1</h4>
							<p>Quis aute iure reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Lorem ipsum dolor sit amet.</p>
						</div>
					</div>
				</td>
				<td data-th="Price">$1.99</td>
				<td data-th="Subtotal" class="text-center">1.99</td>
				
			</tr>
		</tbody>
		<tfoot>
			<tr>
				<td class="text-right" colspan="3"><strong>Total 1.99</strong></td>
			</tr>
			<tr>
				<td class="hidden-xs text-right" colspan="2"><strong>Total $1.99</strong></td>
				<td><a href="#" class="btn btn-success btn-block">Checkout <i class="fa fa-angle-right"></i></a></td>
			</tr>
		</tfoot>
	</table>
	</div>

	<div style="overflow:auto;">
	  <div style="float:right;">
		<button type="button" class="btn btn-primary btn-user" id="prevBtn" onclick="nextPrev(-1)">Previous</button>
		<button type="button" class="btn btn-primary btn-user" id="nextBtn" onclick="nextPrev(1)">Next</button>
	  </div>
	</div>

	<!-- Circles which indicates the steps of the form: -->
	<div style="text-align:center;margin-top:40px;display:none;">
	  <span class="step"></span>
	  <span class="step"></span>
	  <span class="step"></span>
	  <span class="step"></span>
	</div>
</form>
	 
	 
</div>


<script>


</script>

     @endsection
