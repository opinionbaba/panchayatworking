@extends('layouts.f_dashboard.main')
{{-- Page Title --}}
@section('page-title')
    Citizen
@endsection
{{-- This Page Css --}}
@section('css')
@endsection
@section('main-content')
 <div class="card o-hidden border-0 shadow-lg my-5">
      <div class="card-body p-0">
        <!-- Nested Row within Card Body -->
        <div class="row">
          <!--<div class="col-lg-5 d-none d-lg-block bg-register-image"></div>-->
          <div class="col-lg-12">
            <div class="p-5">
              <div class="text-left">
                <h1 class="h4 text-gray-900 mb-4">RTI</h1>
              </div>
			 <form action="{{route('add-certificate')}}" method="post" class="needs-validation" novalidate="">
				@csrf
				   <input type="hidden" name="_token" id="mytoken1" value="{{ csrf_token() }}" />
					 <div class="form-row">
					   <div class="col-md-6 mb-3">
							<label for="validationCustom01">Select Type</label>
							<select name="" class="certi form-control form-control-user">
							 <option value=""  >--Select RTI--</option>
								<option value="">Main</option>
								<option value="">RTI</option>
								<option value="">RTI</option>
								<option value="">RTI</option>
							</select>
						  </div>
						 <div class="col-md-6 mb-3">
							<label for="validationCustom01">Subject</label>
							<input type="text" class="form-control" name="" >
						</div>
					</div>
					<div class="form-row">
					   <div class="col-md-6 mb-3">
							<label for="validationCustom01">Attachment</label>
							<input type="file" class=" form-control attachment_upload1" name="" multiple >
						  </div>
						 <div class="col-md-6 mb-3">
							<label for="validationCustom01">Description</label>
							<textarea  class="form-control" name="" rows="2"></textarea>
						</div>
					</div>
					
		          <div class="row">
				   <div class="col-md-6 mb-3">
					   <button class="btn btn-primary btn-user btn-block" id="submit" type="button">Submit </button>
					</div>
				  </div>
			   </form>
			    
             </div>
          </div>
        </div>
      </div>
	</div>
        <!-- /.container-fluid -->
@endsection
 

 