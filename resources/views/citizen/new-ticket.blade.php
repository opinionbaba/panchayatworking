@extends('layouts.f_dashboard.main')
{{-- Page Title --}}
@section('page-title')
    Citizen
@endsection
{{-- This Page Css --}}
@section('css')
@endsection
@section('main-content')
 <div class="card o-hidden border-0 shadow-lg my-5">
      <div class="card-body p-0">
        <!-- Nested Row within Card Body -->
        <div class="row">
          <!--<div class="col-lg-5 d-none d-lg-block bg-register-image"></div>-->
          <div class="col-lg-12">
            <div class="p-5">
              <div class="text-left">
                <h1 class="h4 text-gray-900 mb-4">Add Ticket</h1>
              </div>
			 <form action="{{route('add-ticket')}}" method="post" class="needs-validation" novalidate="" enctype="multipart/form-data">
				@csrf
				   <input type="hidden" name="user_id"  value="{{ Auth::user()->id}}" />
				   <input type="hidden" name="panc_id"  value="{{ Auth::user()->panc_id}}" />
					 <div class="form-row">
					  <div class="col-md-4 mb-3">
							<label for="validationCustom01">Subject</label>
							<input type="text" name="subject" class="form-control" name="" >
						</div>
						<div class="col-md-4 mb-3">
						<label for="validationCustom01">Priority</label>
						 <select  class="form-control" name="priority" rows="2">
						  <option value="">--Select Priority--</option>
						  <option value="1">High</option>
						  <option value="2"> Medium</option>
						  <option value="3">Low</option>
						 </select>
						 </div>
						 <div class="col-md-4 mb-3">
							<label for="validationCustom01">Attachment</label>
							<input type="file" class=" form-control attachment_upload1" name="attach" multiple >
						 </div>
					</div>
					<div class="form-row">
					   
						<div class="col-md-6 mb-3">
							<label for="validationCustom01">Category</label>
							 <select  class="form-control" name="category" rows="2">
							  <option value="">--Select Category--</option>
							  <option value="1">Default</option>
							  <option value="2">Technical Support</option>
							  <option value="3">User Support</option>
							  <option value="4">Media</option>
							 </select>
						</div>
						 <div class="col-md-6 mb-3">
							<label for="validationCustom01">Description</label>
							<textarea  class="form-control" name="desc" rows="2"></textarea>
						</div>
					</div>
					
					<div class="col-md-6 mb-3">
					   <button class="btn btn-primary btn-user btn-block" id="submit" type="submit">Submit </button>
					</div>
					</div>
				 </form>
			  </div>
          </div>
        </div>
      </div>
	</div>
        <!-- /.container-fluid -->
@endsection
 

 