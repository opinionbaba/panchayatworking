@extends('layouts.f_dashboard.main')
{{-- Page Title --}}
@section('page-title')
    Citizen
@endsection
{{-- This Page Css --}}
@section('css')
@endsection
@section('main-content')
<div class="card shadow mb-4">
            <div class="card-header py-3">
              <h6 class="m-0 font-weight-bold text-primary">All RTI 
			  </h6>
			  <a class="btn btn-info" style="float:right;"href="{{route('new-rti')}}" >New RTI</a>
            </div>
            <div class="card-body">
              <div class="table-responsive">
                <table class="table table-bordered" id="dataTable" width="100%" cellspacing="0">
                  <thead>
                    <tr>
                      <th>Sr No</th>
                      <th>Date</th>
                      <th>Type</th>
                      <th>Subject</th>
                      <th>Status</th>
                      <th>Action</th>
                    </tr>
                  </thead>
                  <tbody>
                    <tr>
                      <td>1</td>
                      <td>21/21/2020</td>
                      <td>Type</td>
                      <td>Against to --</td>
                      <td><span class="badge badge-info">Succes</span></td>
                      <td> 
						<a href="">
						 <i class="fas fa-eye"></i>
						</a>
					   </td>
                      </tr>
                   </tbody>
                </table>
              </div>
            </div>
     @endsection
