@extends('layouts.f_dashboard.main')
{{-- Page Title --}}
@section('page-title')
    Citizen
@endsection
{{-- This Page Css --}}
@section('css')
@endsection
@section('main-content')
<script type="text/javascript" src="http://ajax.googleapis.com/ajax/libs/jquery/1.3.2/jquery.min.js"></script>
          <!-- Page Heading -->
  <div class="d-sm-flex align-items-center justify-content-between mb-4">
	<h1 class="h3 mb-0 text-gray-800">Bank Details</h1>
  </div>
    <div class="card o-hidden border-0 shadow-lg my-5">
      <div class="card-body p-0">
        <!-- Nested Row within Card Body -->
        <div class="row">
          <!--<div class="col-lg-5 d-none d-lg-block bg-register-image"></div>-->
          <div class="col-lg-12">
            <div class="p-5">
              <div class="text-left">
			   <div class="row">
			    <div class="col-md-6">
                 <h1 class="h4 text-gray-900 mb-4">Fill Bank Details</h1>
				</div>
				<div class="col-md-6">
				<a data-toggle="modal" data-target="#myModal"><h1 style="float: right;"class="btn btn-primary">Edit</h1></a>
				</div>
				</div>
              </div>
			   <div class="row">
			    @foreach($bank as $value)
			    <div class="col-md-6">
				   <ul class="list-group list-group-flush">
					  <li class="list-group-item"><b>Account No : </b>{{$value->ac_number}}</li>
					  <li class="list-group-item"><b>IFSC : </b>{{$value->ifsc}}</li>
					  <li class="list-group-item"><b>Bank Name : </b>{{$value->bank_name}}</li>
					  <li class="list-group-item"><b>Branch Name : </b>{{$value->branch_name}}</li>
					  
				   </ul>
			   </div>
			    <div class="col-md-6">
					<ul class="list-group list-group-flush">
					  <li class="list-group-item"><b>Phone Pay Num : </b>{{$value->ph_pay_number}}</li>
					  <li class="list-group-item"><b>Google Pay Num : </b>{{$value->goo_pay_number}}</li>
					  <li class="list-group-item"><b>Paytm Num : </b>{{$value->pytm_number}}</li>
					  <li class="list-group-item"><b>UPI ID : </b>{{$value->upi_id}}</li>
				   </ul>
				</div>
  <div class="modal fade bd-example-modal-lg" id="myModal" role="dialog">
    <div class="modal-dialog">
    
      <!-- Modal content-->
      <div class="modal-content modal-lg">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal">&times;</button>
         
        </div>
        <div class="modal-body">
           <form id="myform"  action="{{route('save-details')}}" method="post" class="needs-validation user" novalidate="">
		@csrf
		<div class="form-row">
			<div class="col-md-6 mb-3">
				<label for="validationCustom01">Account Number</label>
				<input type="text" value="{{$value->ac_number}}"name="ac_num" class="form-control form-control-user" id="validationCustom01" placeholder="Account Number.."  required="">
			</div>
			<div class="col-md-6 mb-3">
				<label for="validationCustom01">Paytm Number</label>
				<input type="text" value="{{$value->pytm_number}}" name="paytm_num" class="form-control form-control-user" id="validationCustom01" placeholder="Paytm Number.."  required="">
				
			</div>
			
			
		 </div>
		 <div class="form-row">
		    <div class="col-md-6 mb-3">
				<label for="validationCustom01">IFSC Code</label>
				<input type="text" value="{{$value->ifsc}}" name="ifsc" class="form-control form-control-user"  id="input-ifsc" placeholder="IFSC Code.."  required="">
				</div>
			<div class="col-md-6 mb-3">
				<label for="validationCustom01">Bank Name</label>
				<input type="text" value="{{$value->branch_name}}" name="bank_name" class="form-control form-control-user" id="validationCustom01" placeholder="Bank Name.."  required="">
				
			</div>
			
		 </div>
		  <div class="form-row">
		     <div class="col-md-6 mb-3">
				<label for="validationCustom01">Branch</label>
				<input type="text" value="{{$value->ac_number}}" name="br_name" class="form-control form-control-user" id="validationCustom01" placeholder="Branch Name.."  required="">
				
			</div>
			
			<div class="col-md-6 mb-3">
				<label for="validationCustom01">UPI Id</label>
				<input type="text" value="{{$value->upi_id}}" name="upi" class="form-control form-control-user" id="validationCustom01" placeholder="UPI Id.."  required="">
				
			</div>
			
		 </div>
		  <div class="form-row">
		    <div class="col-md-6 mb-3">
				<label for="validationCustom01">Phone Pay Number</label>
				<input type="text" value="{{$value->ph_pay_number}}" name="p_pay" class="form-control form-control-user" id="validationCustom01" placeholder="Phone Pay Number.."  required="">
				
			</div>
			
			<div class="col-md-6 mb-3">
				<label for="validationCustom01">Google Pay Number</label>
				<input type="text" value="{{$value->goo_pay_number}}" name="g_pay" class="form-control form-control-user" id="validationCustom01" placeholder="Google Pay Number.."  required="">
				
			</div>
			
		 </div>
		 <div class="row">
		   <div class="col-md-4 mb-3"></div>
		    <div class="col-md-4 mb-3">
		       <button class="btn btn-primary btn-user btn-block" type="submit">Submit form</button>
		    </div>
		  </div>
	</form>
        </div>
       
      </div>
      
    </div>
  </div>
  
              @endforeach
            </div>
          </div>
		  
        </div>
      </div>

        </div>
	
  
       @endsection


  