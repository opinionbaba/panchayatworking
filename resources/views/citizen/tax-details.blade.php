@extends('layouts.f_dashboard.main')
{{-- Page Title --}}
@section('page-title')
    Citizen
@endsection
{{-- This Page Css --}}
@section('css')
@endsection
@section('main-content')
 <div class="card shadow mb-4">
    <div class="card-body">
	<div class="card-header py-3">
              <h6 class="m-0 font-weight-bold text-primary">Details Of Tax
			 
			  </h6>
            </div>
		  <div class="row">
				<div class="form-group row mb0  col-sm-6">
					<label class="control-label col-sm-5">
					<strong>House Number :</strong></label>
					<div class="col-sm-7 ">
						<p class="form-control-static">EMP-856985</p>
                    </div>
				</div>
				
				<div class="form-group row mb0  col-sm-6">
					<label class="control-label col-sm-5">
					<strong>Area :</strong></label>
					<div class="col-sm-7 ">
						<p class="form-control-static">Jakkur</p>
					</div>
				</div>
				
				<div class="form-group mb0 row col-sm-6">
					<label class="control-label col-sm-5"><strong>Locality :</strong></label>
					<div class="col-sm-7 ">
						<p class="form-control-static">Bengaluru</p>

					</div>
				</div>
				
				<div class="form-group mb0 row col-sm-6">
					<label class="control-label col-sm-5"><strong>Village :</strong></label>
					<div class="col-sm-7 ">
						<p class="form-control-static">Baroda
						</p>
					</div>
				</div>
				
				<div class="form-group mb0 row col-sm-6">
					<label class="col-sm-5 control-label"><strong>City : </strong></label>
					<div class="col-sm-7">
					  <p class="form-control-static">Bengaluru
						</p>
					</div>
				</div>
				
				<div class="form-group mb0 row col-sm-6">
					<label class="col-sm-5 control-label"><strong>State : </strong></label>
					<div class="col-sm-7">
					<p class="form-control-static">Karnataka
						</p>
													</div>
				</div>
				
				<div class="form-group mb0 row col-sm-6">

					<label class="col-sm-5 control-label"><strong>Pin Code  :</strong> </label>
					<div class="col-sm-7">
					<p class="form-control-static">560064
						</p>
													</div>
				</div>
				
				<div class="form-group mb0 row col-sm-6">
					<label class="col-sm-5 control-label"><strong>Upload Previous Tax Slip : </strong></label>
					<div class="col-sm-7">
					   <input type="file" class="form-control" name="slip">
					</div>
				</div>
				
				<div class="form-group mb0 row col-sm-6">
					<label class="col-sm-5 control-label"><strong>Pay Tax : </strong></label>
					<span class="badge badge-info">Clic Here!
						</span>
				</div>
				</div>									 
		</div>
		</div>
	@endsection