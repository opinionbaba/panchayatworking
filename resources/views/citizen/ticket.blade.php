@extends('layouts.f_dashboard.main')
{{-- Page Title --}}
@section('page-title')
    Citizen
@endsection
{{-- This Page Css --}}
@section('css')
@endsection
@section('main-content')
<style>
.pta{
	height: 262px;
}
</style>
  <!-- Page Heading -->
	<div class="d-sm-flex align-items-center justify-content-between mb-4">
	  <h1 class="h3 mb-0 text-gray-800">All Tickets</h1>
	</div>
	<!-- DataTales Example -->
          <div class="card shadow mb-4">
            <div class="card-header py-3">
              <h4 class="m-0 font-weight-bold text-primary">List Of Tickets <a href="{{route('new-ticket')}}"><h1 style="float: right;" class="btn btn-warning">Create New Tickets</h1></a></h4>
            </div>
            <div class="card-body">
              <div class="table-responsive">
                <table class="table table-bordered" id="dataTable" width="100%" cellspacing="0">
                  <thead>
                    <tr>
                      <th>Sr No</th>
                      <th>Ticket ID</th>
                      <th>Subject</th>
                      <th>File</th>
                      <th>Date</th>
                      <th>Status</th>
                    </tr>
                  </thead>
                 <tbody>
				 @php
				 $num=1;
				 @endphp
                    @foreach($ticket as $value)
                    <tr>
                      <td>{{$num}}</td>
                      <td><a href="{{route('show-reply',$value->ticket_id)}}">{{$value->ticket_id}}</td>
                      <td>{{$value->subject}}</td>
                      <td>{{$value->file}}</td>
                      <td>{{$value->created_at}}</td>
                      <td>@if($value->status == 1)
						<span class="badge badge-info">Done</span>
						@else
						<span class="badge badge-danger">Pending</span>
					  @endif
					  </td>
                     </tr>
					  @php
					 $num++;
					 @endphp
					@endforeach
                  </tbody>
                </table>
              </div>
            </div>
          </div>
	
	

    </div>


     @endsection
