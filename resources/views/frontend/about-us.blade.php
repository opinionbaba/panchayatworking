@extends('layouts.frontend.main')
{{-- Page Title --}}
@section('page-title')
    Form Validation
@endsection

@section('main-content')
<style>

.member{
	    color: #fff;
    border-color: #6ab165;
    background-color: #6ab165;
    float: right;
    border-radius: 6px;
    padding: 5px 10px;
    margin-top: -6px;
    text-decoration: none!important;
}
</style>
<!-- CORE : begin -->
		<div id="core">
			<div class="c-container">
				<div class="row">
					<div class="middle-column col-md-6 col-md-push-3">
						<div id="page-header" class="m-has-breadcrumbs">
							<div class="page-title">
								<h1>About US</h1>
							</div>
							<div class="breadcrumbs">
								<ul>
									<li class="home"><a href="{{('/')}}">Home</a></li>
									<li class="home" ><a class="member" href="{{('/')}}">Members</a></li>
									
								</ul>
							</div>
							</div>
						<div id="page-content">
							<div class="page-content-inner">
								<div class="c-content-box">

									<h2>About US</h2>
									<p>The Right to Information Act (RTI) is an Act of the Parliament of India “to provide for setting out the practical regime of right to information for citizens” and replaces the erstwhile Freedom of information Act, 2002. The Act applies to all States and Union Territories of India except Jammu & Kashmir. Under the provisions of the Act, any citizen may request information from a “public authority” (a body of Government or “instrumentality of State”) which is required to reply expeditiously or within thirty days. The Act also requires every public authority to computerise their records for wide dissemination and to proactively certain categories of information so that the citizens need minimum recourse to request for information formally. This law was passed by Parliament on 15 June 2005 and came fully into force on 12 October 2005, which was Vijayadashmi. The first application was given to a Pune police station. Information disclosure in India was restricted by the Official Secrets Act 1923 and various other special laws, which the new RTI Act relaxes. It codifies a fundamental right of citizens.</p>
								<blockquote style="font-size: 17px !important;">

								Name Of PIO: Gokuldas Kudalkar<br>
							    Designation: VP Secetary
								</blockquote>

								</div>
							</div>
						</div>
					</div>
				  <div class="left-column col-md-3 col-md-pull-6">
					<nav class="side-menu m-left-side m-show-submenu">
						 @include('frontend.menu')
						</nav>
						<aside class="sidebar">
							<div class="widget-list">
								<div class="widget featured-gallery-widget">
									<div class="widget-inner">
										<h3 class="widget-title m-has-ico"><i class="widget-ico tp tp-pictures"></i>Featured Gallery</h3>
										<div class="widget-content">
											<div class="gallery-image" title="Streets of TownPress">
												<a href="gallery-detail.html"><img src="images/featured-gallery-01.jpg" alt=""></a>
											</div>
											<p class="show-all-btn"><a href="gallery-list.html">See All Galleries</a></p>
            							</div>
									</div>
								</div>
								<!-- FEATURED GALLERY WIDGET : end -->

								<!-- DOCUMENTS WIDGET : begin -->
								<div class="widget documents-widget">
									<div class="widget-inner">
										<h3 class="widget-title m-has-ico"><i class="widget-ico tp tp-papers"></i>Documents</h3>
										<div class="widget-content">
											<ul class="document-list m-has-icons">

												<!-- DOCUMENT : begin -->
												<li class="document">
													<div class="document-inner">
														<div class="document-icon" title="PDF File"><i class="fa fa-file-pdf-o"></i></div>
														<h4 class="document-title">
															<a href="documents/dummy-document2.pdf" target="_blank">Council Agenda April 24, 2014</a>
															<span class="document-filesize">(27 kB)</span>
														</h4>
													</div>
												</li>
												<!-- DOCUMENT : end -->

												<!-- DOCUMENT : begin -->
												<li class="document">
													<div class="document-inner">
														<div class="document-icon" title="PDF File"><i class="fa fa-file-pdf-o"></i></div>
														<h4 class="document-title">
															<a href="documents/dummy-document.pdf" target="_blank">Town Report 2014</a>
															<span class="document-filesize">(24 kB)</span>
														</h4>
													</div>
												</li>
												<!-- DOCUMENT : end -->

												<!-- DOCUMENT : begin -->
												<li class="document">
													<div class="document-inner">
														<div class="document-icon" title="Word Document"><i class="fa fa-file-word-o"></i></div>
														<h4 class="document-title">
															<a href="documents/dummy-document2.pdf" target="_blank">Police Contract</a>
															<span class="document-filesize">(27 kB)</span>
														</h4>
													</div>
												</li>
												<!-- DOCUMENT : end -->

												<!-- DOCUMENT : begin -->
												<li class="document">
													<div class="document-inner">
														<div class="document-icon" title="Word Document"><i class="fa fa-file-word-o"></i></div>
														<h4 class="document-title">
															<a href="documents/dummy-document2.pdf" target="_blank">Public Schools Contract</a>
															<span class="document-filesize">(27 kB)</span>
														</h4>
													</div>
												</li>
												<!-- DOCUMENT : end -->

											</ul>
											<p class="show-all-btn"><a href="document-list.html">See All Documents</a></p>
										</div>
									</div>
								</div>
								<!-- DOCUMENTS WIDGET : end -->

							</div>
						</aside>
						<!-- LEFT SIDEBAR : end -->

					</div>
					<!-- LEFT COLUMN : end -->

					<!-- RIGHT COLUMN : begin -->
					<div class="right-column col-md-3">

						<!-- RIGHT SIDEBAR : begin -->
						<aside class="sidebar">
							<div class="widget-list">

								<!-- CATEGORIES WIDGET : begin -->
								<div class="widget categories-widget">
									<div class="widget-inner">
										<h3 class="widget-title m-has-ico">
										<i class="widget-ico tp tp-list4"></i>Categories</h3>
										<div class="widget-content">
											<ul>
												<li><a href="event-list.html">Community</a> (2)</li>
												<li><a href="event-list.html">Culture</a> (2)</li>
												<li><a href="event-list.html">Relax</a> (2)</li>
												<li><a href="event-list.html">Sport</a> (1)</li>
											</ul>
										</div>
									</div>
								</div>
								<!-- CATEGORIES WIDGET : end -->

								<!-- IMAGE WIDGET : begin -->
								<div class="widget image-widget">
									<div class="widget-inner">
										<div class="widget-content">
											<a href="#"><img src="images/poster-01.jpg" alt=""></a>
										</div>
									</div>
								</div>
								<!-- IMAGE WIDGET : end -->

							</div>
						</aside>
						<!-- RIGHT SIDEBAR : end -->

					</div>
					<!-- RIGHT COLUMN : end -->

				</div>
			</div>
		</div>
		<!-- CORE : end -->
@endsection
