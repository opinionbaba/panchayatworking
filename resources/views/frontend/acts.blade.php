@extends('layouts.frontend.main')
{{-- Page Title --}}
@section('page-title')
    Form Validation
@endsection

@section('main-content')
<!-- CORE : begin -->
		<div id="core">
			<div class="c-container">
				<div class="row">

					<!-- MIDDLE COLUMN : begin -->
					<div class="middle-column col-md-6 col-md-push-3">

						<!-- PAGE HEADER : begin -->
						<div id="page-header" class="m-has-breadcrumbs">

							<!-- PAGE TITLE : begin -->
							<div class="page-title">
								<h1>Acts</h1>
							</div>
							<!-- PAGE TITLE : end -->

							<!-- BREADCRUMBS : begin -->
							<div class="breadcrumbs">
								<ul>
									<li class="home"><a href="{{('/')}}">Home</a></li>
									<li>Acts</li>
								</ul>
							</div>
							<!-- BREADCRUMBS : end -->

						</div>
						<!-- PAGE HEADER : end -->

						<!-- PAGE CONTENT : begin -->
						<div id="page-content">
							<div class="page-content-inner">

								<!-- NOTICE LIST PAGE : begin -->
								<div class="notice-list-page notice-page">

									<!-- NOTICE : begin -->
									<article class="notice">
										<div class="notice-inner c-content-box m-no-padding">

											<!-- NOTICE CORE : begin -->
											
											<div class="notice-core">
												<div class="row">
												  <div class="col-sm-10">
													<!-- NOTICE TITLE : begin -->
													<h2 class="notice-title">The Goa Panchayat Raj Ordinance 1994</h2>
													<!-- NOTICE TITLE : end -->
													</div>
													<div class="col-sm-2">
													  <a href="{{asset('public/acts/The Goa Panchayat Raj Ordinance 1994')}}" target="_blank" ><i class="fa fa-download"></i></a>
													</div>
												</div>
											</div>
											<div class="notice-core">
												<div class="row">
												  <div class="col-sm-10">
													<!-- NOTICE TITLE : begin -->
													<h2 class="notice-title">The Goa Panchayat Raj Act, 1994.pdf</h2>
													<!-- NOTICE TITLE : end -->
													</div>
													<div class="col-sm-2">
													  <a href="{{asset('public/acts/The Goa Panchayat Raj Act, 1994.pdf')}}" target="_blank" ><i class="fa fa-download"></i></a>
													</div>
												</div>
											</div>
											<div class="notice-core">
												<div class="row">
												  <div class="col-sm-10">
													<!-- NOTICE TITLE : begin -->
													<h2 class="notice-title">The Goa, Daman and Diu Mundkars Act, 1975 and Rules, 1977.pdf</h2>
													<!-- NOTICE TITLE : end -->
													</div>
													<div class="col-sm-2">
													  <a href="{{asset('public/acts/The Goa, Daman and Diu Mundkars Act, 1975 and Rules, 1977.pdf')}}" target="_blank" ><i class="fa fa-download"></i></a>
													</div>
												</div>
											</div>
											<div class="notice-core">
												<div class="row">
												  <div class="col-sm-10">
													<!-- NOTICE TITLE : begin -->
													<h2 class="notice-title">The Goa Regulation of Land Development and Building Construction Act, 2008.pdf</h2>
													<!-- NOTICE TITLE : end -->
													</div>
													<div class="col-sm-2">
													  <a href="{{asset('public/acts/The Goa Regulation of Land Development and Building Construction Act, 2008.pdf')}}" target="_blank" ><i class="fa fa-download"></i></a>
													</div>
												</div>
											</div>
											<div class="notice-core">
												<div class="row">
												  <div class="col-sm-10">
													<!-- NOTICE TITLE : begin -->
													<h2 class="notice-title">The Goa, Daman and Diu Public Health Act, 1985 and Rules, 1987.pdf</h2>
													<!-- NOTICE TITLE : end -->
													</div>
													<div class="col-sm-2">
													  <a href="{{asset('public/acts/The Goa, Daman and Diu Public Health Act, 1985 and Rules, 1987.pdf')}}" target="_blank" ><i class="fa fa-download"></i></a>
													</div>
												</div>
											</div>
											<div class="notice-core">
												<div class="row">
												  <div class="col-sm-10">
													<!-- NOTICE TITLE : begin -->
													<h2 class="notice-title">The Goa, Daman and Diu Town and Country Planning Act, 1974 and Rules, 1976.pdf</h2>
													<!-- NOTICE TITLE : end -->
													</div>
													<div class="col-sm-2">
													  <a href="{{asset('public/acts/The Goa, Daman and Diu Town and Country Planning Act, 1974 and Rules, 1976.pdf')}}" target="_blank" ><i class="fa fa-download"></i></a>
													</div>
												</div>
											</div>
											<div class="notice-core">
												<div class="row">
												  <div class="col-sm-10">
													<!-- NOTICE TITLE : begin -->
													<h2 class="notice-title">The Registration Of Births And Deaths Act, 1969.pdf</h2>
													<!-- NOTICE TITLE : end -->
													</div>
													<div class="col-sm-2">
													  <a href="{{asset('public/acts/The Registration Of Births And Deaths Act, 1969.pdf')}}" target="_blank" ><i class="fa fa-download"></i></a>
													</div>
												</div>
											</div>
											<div class="notice-core">
												<div class="row">
												  <div class="col-sm-10">
													<!-- NOTICE TITLE : begin -->
													<h2 class="notice-title">The Right To Information Act, 2005.pdf</h2>
													<!-- NOTICE TILE : end -->
													</div>
													<div class="col-sm-2">
													  <a href="{{asset('public/acts/The Right To Information Act, 2005.pdf')}}" target="_blank" ><i class="fa fa-download"></i></a>
													</div>
												</div>
											</div>
											
											<!-- NOTICE CORE : end -->
										</div>
									</article>
									<!-- NOTICE : end -->
								</div>
								<!-- NOTICE LIST PAGE : begin -->
							</div>
						</div>
						<!-- PAGE CONTENT : end -->

						<hr class="c-separator m-margin-top-small m-margin-bottom-small m-transparent hidden-lg hidden-md">

					</div>
					<!-- MIDDLE COLUMN : end -->

					<!-- LEFT COLUMN : begin -->
					<div class="left-column col-md-3 col-md-pull-6">

						<!-- SIDE MENU : begin -->
						<nav class="side-menu m-left-side m-show-submenu">
						 @include('frontend.menu')
						</nav>
						<!-- SIDE MENU : end -->

						<!-- LEFT SIDEBAR : begin -->
						<aside class="sidebar">
							<div class="widget-list">

								<!-- FEATURED GALLERY WIDGET : begin -->
								<div class="widget featured-gallery-widget">
									<div class="widget-inner">
										<h3 class="widget-title m-has-ico"><i class="widget-ico tp tp-pictures"></i>Featured Gallery</h3>
										<div class="widget-content">
											<div class="gallery-image" title="Streets of TownPress">
												<a href="gallery-detail.html"><img src="images/featured-gallery-01.jpg" alt=""></a>
											</div>
											<p class="show-all-btn"><a href="gallery-list.html">See All Galleries</a></p>
            							</div>
									</div>
								</div>
								<!-- FEATURED GALLERY WIDGET : end -->

								<!-- DOCUMENTS WIDGET : begin -->
								<div class="widget documents-widget">
									<div class="widget-inner">
										<h3 class="widget-title m-has-ico"><i class="widget-ico tp tp-papers"></i>Documents</h3>
										<div class="widget-content">
											<ul class="document-list m-has-icons">

												<!-- DOCUMENT : begin -->
												<li class="document">
													<div class="document-inner">
														<div class="document-icon" title="PDF File"><i class="fa fa-file-pdf-o"></i></div>
														<h4 class="document-title">
															<a href="documents/dummy-document2.pdf" target="_blank">Council Agenda April 24, 2014</a>
															<span class="document-filesize">(27 kB)</span>
														</h4>
													</div>
												</li>
												<!-- DOCUMENT : end -->

												<!-- DOCUMENT : begin -->
												<li class="document">
													<div class="document-inner">
														<div class="document-icon" title="PDF File"><i class="fa fa-file-pdf-o"></i></div>
														<h4 class="document-title">
															<a href="documents/dummy-document.pdf" target="_blank">Town Report 2014</a>
															<span class="document-filesize">(24 kB)</span>
														</h4>
													</div>
												</li>
												<!-- DOCUMENT : end -->

												<!-- DOCUMENT : begin -->
												<li class="document">
													<div class="document-inner">
														<div class="document-icon" title="Word Document"><i class="fa fa-file-word-o"></i></div>
														<h4 class="document-title">
															<a href="documents/dummy-document2.pdf" target="_blank">Police Contract</a>
															<span class="document-filesize">(27 kB)</span>
														</h4>
													</div>
												</li>
												<!-- DOCUMENT : end -->

												<!-- DOCUMENT : begin -->
												<li class="document">
													<div class="document-inner">
														<div class="document-icon" title="Word Document"><i class="fa fa-file-word-o"></i></div>
														<h4 class="document-title">
															<a href="documents/dummy-document2.pdf" target="_blank">Public Schools Contract</a>
															<span class="document-filesize">(27 kB)</span>
														</h4>
													</div>
												</li>
												<!-- DOCUMENT : end -->

											</ul>
											<p class="show-all-btn"><a href="document-list.html">See All Documents</a></p>
										</div>
									</div>
								</div>
								<!-- DOCUMENTS WIDGET : end -->

							</div>
						</aside>
						<!-- LEFT SIDEBAR : end -->

					</div>
					<!-- LEFT COLUMN : end -->

					<!-- RIGHT COLUMN : begin -->
					<div class="right-column col-md-3">

						<!-- RIGHT SIDEBAR : begin -->
						<aside class="sidebar">
							<div class="widget-list">

								<!-- CATEGORIES WIDGET : begin -->
								<div class="widget categories-widget">
									<div class="widget-inner">
										<h3 class="widget-title m-has-ico">
										<i class="widget-ico tp tp-list4"></i>Categories</h3>
										<div class="widget-content">
											<ul>
												<li><a href="event-list.html">Community</a> (2)</li>
												<li><a href="event-list.html">Culture</a> (2)</li>
												<li><a href="event-list.html">Relax</a> (2)</li>
												<li><a href="event-list.html">Sport</a> (1)</li>
											</ul>
										</div>
									</div>
								</div>
								<!-- CATEGORIES WIDGET : end -->

								<!-- IMAGE WIDGET : begin -->
								<div class="widget image-widget">
									<div class="widget-inner">
										<div class="widget-content">
											<a href="#"><img src="images/poster-01.jpg" alt=""></a>
										</div>
									</div>
								</div>
								<!-- IMAGE WIDGET : end -->

							</div>
						</aside>
						<!-- RIGHT SIDEBAR : end -->

					</div>
					<!-- RIGHT COLUMN : end -->

				</div>
			</div>
		</div>
		<!-- CORE : end -->
@endsection
