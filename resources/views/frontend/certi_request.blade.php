@extends('layouts.frontend.main')
{{-- Page Title --}}
@section('page-title')
    Form Validation
@endsection

@section('main-content')
<!-- CORE : begin -->
 <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/css/bootstrap.min.css">
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
		<div id="core">
			<div class="c-container">
				<div class="row">

					<!-- MIDDLE COLUMN : begin -->
					<div class="middle-column col-md-6 col-md-push-3">

						<!-- PAGE HEADER : begin -->
						<div id="page-header" class="m-has-breadcrumbs">

							<!-- PAGE TITLE : begin -->
							<div class="page-title">
								<h1>Forms</h1>
							</div>
							<!-- PAGE TITLE : end -->

							<!-- BREADCRUMBS : begin -->
							<div class="breadcrumbs">
								<ul>
									<li class="home"><a href="{{('/')}}">Home</a></li>
									<li>forms</li>
								</ul>
							</div>
							<!-- BREADCRUMBS : end -->

						</div>
						<!-- PAGE HEADER : end -->

						<!-- PAGE CONTENT : begin -->
						<div id="page-content">
							<div class="page-content-inner">

								<!-- NOTICE LIST PAGE : begin -->
								<div class="notice-list-page notice-page">

									<!-- NOTICE : begin -->
									<article class="notice">
										<div class="notice-inner c-content-box m-no-padding">

											<!-- NOTICE CORE : begin -->
										
											<div class="notice-core">
											  <input class="form-control" id="myInput" type="text" placeholder="Search..">
													  <br>
													  <table class="table table-bordered table-striped">
														<thead>
														  <tr>
															<th>Sr No</th>
															<th>Name</th>
															<th>Request Date</th>
															<th>Issue Date</th>
															<th>Certificate</th>
														  </tr>
														</thead>
														<tbody id="myTable">
														  <tr>
															<td>1</td>
															<td>John</td>
															<td>12/2/2020</td>
															<td>12/2/2020</td>
															<td>Income</td>
														  </tr>
														 </tbody>
													  </table>
													
											</div>
										
											<!-- NOTICE CORE : end -->
										</div>
									</article>
									<!-- NOTICE : end -->
								</div>
								<!-- NOTICE LIST PAGE : begin -->
							</div>
						</div>
						<!-- PAGE CONTENT : end -->

						<hr class="c-separator m-margin-top-small m-margin-bottom-small m-transparent hidden-lg hidden-md">

					</div>
					<!-- MIDDLE COLUMN : end -->

					<!-- LEFT COLUMN : begin -->
					<div class="left-column col-md-3 col-md-pull-6">

						<!-- SIDE MENU : begin -->
						<nav class="side-menu m-left-side m-show-submenu">
						 @include('frontend.menu')
						</nav>
						<!-- SIDE MENU : end -->

						<!-- LEFT SIDEBAR : begin -->
						<aside class="sidebar">
							<div class="widget-list">

								<!-- FEATURED GALLERY WIDGET : begin -->
								<div class="widget featured-gallery-widget">
									<div class="widget-inner">
										<h3 class="widget-title m-has-ico"><i class="widget-ico tp tp-pictures"></i>Featured Gallery</h3>
										<div class="widget-content">
											<div class="gallery-image" title="Streets of TownPress">
												<a href="gallery-detail.html"><img src="images/featured-gallery-01.jpg" alt=""></a>
											</div>
											<p class="show-all-btn"><a href="gallery-list.html">See All Galleries</a></p>
            							</div>
									</div>
								</div>
								<!-- FEATURED GALLERY WIDGET : end -->

								<!-- DOCUMENTS WIDGET : begin -->
								<div class="widget documents-widget">
									<div class="widget-inner">
										<h3 class="widget-title m-has-ico"><i class="widget-ico tp tp-papers"></i>Documents</h3>
										<div class="widget-content">
											<ul class="document-list m-has-icons">

												<!-- DOCUMENT : begin -->
												<li class="document">
													<div class="document-inner">
														<div class="document-icon" title="PDF File"><i class="fa fa-file-pdf-o"></i></div>
														<h4 class="document-title">
															<a href="documents/dummy-document2.pdf" target="_blank">Council Agenda April 24, 2014</a>
															<span class="document-filesize">(27 kB)</span>
														</h4>
													</div>
												</li>
												<!-- DOCUMENT : end -->

												<!-- DOCUMENT : begin -->
												<li class="document">
													<div class="document-inner">
														<div class="document-icon" title="PDF File"><i class="fa fa-file-pdf-o"></i></div>
														<h4 class="document-title">
															<a href="documents/dummy-document.pdf" target="_blank">Town Report 2014</a>
															<span class="document-filesize">(24 kB)</span>
														</h4>
													</div>
												</li>
												<!-- DOCUMENT : end -->

												<!-- DOCUMENT : begin -->
												<li class="document">
													<div class="document-inner">
														<div class="document-icon" title="Word Document"><i class="fa fa-file-word-o"></i></div>
														<h4 class="document-title">
															<a href="documents/dummy-document2.pdf" target="_blank">Police Contract</a>
															<span class="document-filesize">(27 kB)</span>
														</h4>
													</div>
												</li>
												<!-- DOCUMENT : end -->

												<!-- DOCUMENT : begin -->
												<li class="document">
													<div class="document-inner">
														<div class="document-icon" title="Word Document"><i class="fa fa-file-word-o"></i></div>
														<h4 class="document-title">
															<a href="documents/dummy-document2.pdf" target="_blank">Public Schools Contract</a>
															<span class="document-filesize">(27 kB)</span>
														</h4>
													</div>
												</li>
												<!-- DOCUMENT : end -->

											</ul>
											<p class="show-all-btn"><a href="document-list.html">See All Documents</a></p>
										</div>
									</div>
								</div>
								<!-- DOCUMENTS WIDGET : end -->

							</div>
						</aside>
						<!-- LEFT SIDEBAR : end -->

					</div>
					<!-- LEFT COLUMN : end -->

					<!-- RIGHT COLUMN : begin -->
					<div class="right-column col-md-3">

						<!-- RIGHT SIDEBAR : begin -->
						<aside class="sidebar">
							<div class="widget-list">

								<!-- CATEGORIES WIDGET : begin -->
								<div class="widget categories-widget">
									<div class="widget-inner">
										<h3 class="widget-title m-has-ico">
										<i class="widget-ico tp tp-list4"></i>Categories</h3>
										<div class="widget-content">
											<ul>
												<li><a href="event-list.html">Community</a> (2)</li>
												<li><a href="event-list.html">Culture</a> (2)</li>
												<li><a href="event-list.html">Relax</a> (2)</li>
												<li><a href="event-list.html">Sport</a> (1)</li>
											</ul>
										</div>
									</div>
								</div>
								<!-- CATEGORIES WIDGET : end -->

								<!-- IMAGE WIDGET : begin -->
								<div class="widget image-widget">
									<div class="widget-inner">
										<div class="widget-content">
											<a href="#"><img src="images/poster-01.jpg" alt=""></a>
										</div>
									</div>
								</div>
								<!-- IMAGE WIDGET : end -->

							</div>
						</aside>
						<!-- RIGHT SIDEBAR : end -->

					</div>
					<!-- RIGHT COLUMN : end -->

				</div>
			</div>
		</div>
		<!-- CORE : end -->
<script>
$(document).ready(function(){
  $("#myInput").on("keyup", function() {
    var value = $(this).val().toLowerCase();
    $("#myTable tr").filter(function() {
      $(this).toggle($(this).text().toLowerCase().indexOf(value) > -1)
    });
  });
});
</script>
@endsection
