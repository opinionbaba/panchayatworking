@extends('layouts.frontend.main')
{{-- Page Title --}}
@section('page-title')
    Form Validation
@endsection

@section('main-content')
<!-- CORE : begin -->
		<div id="core">
			<div class="c-container">
				<div class="row">
<!-- MIDDLE COLUMN : begin -->
					<div class="middle-column col-md-6 col-md-push-3">

						<!-- PAGE HEADER : begin -->
						<div id="page-header" class="m-has-breadcrumbs">

							<!-- PAGE TITLE : begin -->
							<div class="page-title">
								<h1>Galleries</h1>
							</div>
							<!-- PAGE TITLE : end -->

							<!-- BREADCRUMBS : begin -->
							<div class="breadcrumbs">
								<ul>
									<li class="home"><a href="{{('/')}}">Home</a></li>
									<li>Galleries</li>
								</ul>
							</div>
							<!-- BREADCRUMBS : end -->

						</div>
						<!-- PAGE HEADER : end -->

						<!-- PAGE CONTENT : begin -->
						<div id="page-content">
							<div class="page-content-inner">

								<!-- GALLERY LIST PAGE : begin -->
								<div class="gallery-list-page gallery-page">
									<div class="c-gallery">
										<!-- You can change the number of columns by changing "m-3-columns" class
										in the following element to m-2-columns | m-4-columns | m-5-columns : begin -->
										<ul class="gallery-images m-layout-masonry m-3-columns">

											<!-- GALLERY IMAGE : begin -->
											<li class="gallery-image">
												<div class="gallery-image-inner">
													<a href="gallery-detail.html">
														<img src="images/gallery-01.jpg" alt="">
													</a>
													<h2 class="gallery-title"><a href="gallery-detail.html">Summer Camp for Kids</a></h2>
												</div>
											</li>
											<!-- GALLERY IMAGE : end -->

											<!-- GALLERY IMAGE : begin -->
											<li class="gallery-image">
												<div class="gallery-image-inner">
													<a href="gallery-detail.html">
														<img src="images/gallery-02.jpg" alt="">
													</a>
													<h2 class="gallery-title"><a href="gallery-detail.html">Annual Marathon</a></h2>
												</div>
											</li>
											<!-- GALLERY IMAGE : end -->

											<!-- GALLERY IMAGE : begin -->
											<li class="gallery-image">
												<div class="gallery-image-inner">
													<a href="gallery-detail.html">
														<img src="images/gallery-03.jpg" alt="">
													</a>
													<h2 class="gallery-title"><a href="gallery-detail.html">Summer Rock Festival</a></h2>
												</div>
											</li>
											<!-- GALLERY IMAGE : end -->

											<!-- GALLERY IMAGE : begin -->
											<li class="gallery-image">
												<div class="gallery-image-inner">
													<a href="gallery-detail.html">
														<img src="images/gallery-04.jpg" alt="">
													</a>
													<h2 class="gallery-title"><a href="gallery-detail.html">TownPress State Park</a></h2>
												</div>
											</li>
											<!-- GALLERY IMAGE : end -->

											<!-- GALLERY IMAGE : begin -->
											<li class="gallery-image">
												<div class="gallery-image-inner">
													<a href="gallery-detail.html">
														<img src="images/gallery-05.jpg" alt="">
													</a>
													<h2 class="gallery-title"><a href="gallery-detail.html">Friendly Soccer Match</a></h2>
												</div>
											</li>
											<!-- GALLERY IMAGE : end -->

											<!-- GALLERY IMAGE : begin -->
											<li class="gallery-image">
												<div class="gallery-image-inner">
													<a href="gallery-detail.html">
														<img src="images/gallery-06.jpg" alt="">
													</a>
													<h2 class="gallery-title"><a href="gallery-detail.html">TownPress Sightseeing</a></h2>
												</div>
											</li>
											<!-- GALLERY IMAGE : end -->

											<!-- GALLERY IMAGE : begin -->
											<li class="gallery-image">
												<div class="gallery-image-inner">
													<a href="gallery-detail.html">
														<img src="images/gallery-07.jpg" alt="">
													</a>
													<h2 class="gallery-title"><a href="gallery-detail.html">Streets of TownPress</a></h2>
												</div>
											</li>
											<!-- GALLERY IMAGE : end -->

											<!-- GALLERY IMAGE : begin -->
											<li class="gallery-image">
												<div class="gallery-image-inner">
													<a href="gallery-detail.html">
														<img src="images/gallery-08.jpg" alt="">
													</a>
													<h2 class="gallery-title"><a href="gallery-detail.html">Campgrounds</a></h2>
												</div>
											</li>
											<!-- GALLERY IMAGE : end -->

											<!-- GALLERY IMAGE : begin -->
											<li class="gallery-image">
												<div class="gallery-image-inner">
													<a href="gallery-detail.html">
														<img src="images/gallery-09.jpg" alt="">
													</a>
													<h2 class="gallery-title"><a href="gallery-detail.html">New Housing</a></h2>
												</div>
											</li>
											<!-- GALLERY IMAGE : end -->

										</ul>
									</div>
								</div>
								<!-- GALLERY LIST PAGE : begin -->

								
							</div>
						</div>
						<!-- PAGE CONTENT : end -->

						<hr class="c-separator m-margin-top-small m-margin-bottom-small m-transparent hidden-lg hidden-md">

					</div>
					<!-- MIDDLE COLUMN : end -->

					<!-- LEFT COLUMN : begin -->
					<div class="left-column col-md-3 col-md-pull-6">

						<!-- SIDE MENU : begin -->
						<nav class="side-menu m-left-side m-show-submenu">
						 @include('frontend.menu')
						</nav>
						<!-- SIDE MENU : end -->

						<!-- LEFT SIDEBAR : begin -->
						<aside class="sidebar">
							<div class="widget-list">

								<!-- FEATURED GALLERY WIDGET : begin -->
								<div class="widget featured-gallery-widget">
									<div class="widget-inner">
										<h3 class="widget-title m-has-ico"><i class="widget-ico tp tp-pictures"></i>Featured Gallery</h3>
										<div class="widget-content">
											<div class="gallery-image" title="Streets of TownPress">
												<a href="gallery-detail.html"><img src="images/featured-gallery-01.jpg" alt=""></a>
											</div>
											<p class="show-all-btn"><a href="gallery-list.html">See All Galleries</a></p>
            							</div>
									</div>
								</div>
								<!-- FEATURED GALLERY WIDGET : end -->

								<!-- DOCUMENTS WIDGET : begin -->
								<div class="widget documents-widget">
									<div class="widget-inner">
										<h3 class="widget-title m-has-ico"><i class="widget-ico tp tp-papers"></i>Documents</h3>
										<div class="widget-content">
											<ul class="document-list m-has-icons">

												<!-- DOCUMENT : begin -->
												<li class="document">
													<div class="document-inner">
														<div class="document-icon" title="PDF File"><i class="fa fa-file-pdf-o"></i></div>
														<h4 class="document-title">
															<a href="documents/dummy-document2.pdf" target="_blank">Council Agenda April 24, 2014</a>
															<span class="document-filesize">(27 kB)</span>
														</h4>
													</div>
												</li>
												<!-- DOCUMENT : end -->

												<!-- DOCUMENT : begin -->
												<li class="document">
													<div class="document-inner">
														<div class="document-icon" title="PDF File"><i class="fa fa-file-pdf-o"></i></div>
														<h4 class="document-title">
															<a href="documents/dummy-document.pdf" target="_blank">Town Report 2014</a>
															<span class="document-filesize">(24 kB)</span>
														</h4>
													</div>
												</li>
												<!-- DOCUMENT : end -->

												<!-- DOCUMENT : begin -->
												<li class="document">
													<div class="document-inner">
														<div class="document-icon" title="Word Document"><i class="fa fa-file-word-o"></i></div>
														<h4 class="document-title">
															<a href="documents/dummy-document2.pdf" target="_blank">Police Contract</a>
															<span class="document-filesize">(27 kB)</span>
														</h4>
													</div>
												</li>
												<!-- DOCUMENT : end -->

												<!-- DOCUMENT : begin -->
												<li class="document">
													<div class="document-inner">
														<div class="document-icon" title="Word Document"><i class="fa fa-file-word-o"></i></div>
														<h4 class="document-title">
															<a href="documents/dummy-document2.pdf" target="_blank">Public Schools Contract</a>
															<span class="document-filesize">(27 kB)</span>
														</h4>
													</div>
												</li>
												<!-- DOCUMENT : end -->

											</ul>
											<p class="show-all-btn"><a href="document-list.html">See All Documents</a></p>
										</div>
									</div>
								</div>
								<!-- DOCUMENTS WIDGET : end -->

							</div>
						</aside>
						<!-- LEFT SIDEBAR : end -->

					</div>
					<!-- LEFT COLUMN : end -->

					<!-- RIGHT COLUMN : begin -->
					<div class="right-column col-md-3">

						<!-- RIGHT SIDEBAR : begin -->
						<aside class="sidebar">
							<div class="widget-list">

								<!-- CATEGORIES WIDGET : begin -->
								<div class="widget categories-widget">
									<div class="widget-inner">
										<h3 class="widget-title m-has-ico">
										<i class="widget-ico tp tp-list4"></i>Categories</h3>
										<div class="widget-content">
											<ul>
												<li><a href="event-list.html">Community</a> (2)</li>
												<li><a href="event-list.html">Culture</a> (2)</li>
												<li><a href="event-list.html">Relax</a> (2)</li>
												<li><a href="event-list.html">Sport</a> (1)</li>
											</ul>
										</div>
									</div>
								</div>
								<!-- CATEGORIES WIDGET : end -->

								<!-- IMAGE WIDGET : begin -->
								<div class="widget image-widget">
									<div class="widget-inner">
										<div class="widget-content">
											<a href="#"><img src="images/poster-01.jpg" alt=""></a>
										</div>
									</div>
								</div>
								<!-- IMAGE WIDGET : end -->

							</div>
						</aside>
						<!-- RIGHT SIDEBAR : end -->

					</div>
					<!-- RIGHT COLUMN : end -->

				</div>
			</div>
		</div>
		<!-- CORE : end -->
@endsection
