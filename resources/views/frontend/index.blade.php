@extends('layouts.frontend.main')
{{-- Page Title --}}
@section('page-title')
    Form Validation
@endsection
@section('main-content')
<!-- CORE : begin -->

		<div id="core">
			<div class="c-container">
				<div class="row">
					<!-- MIDDLE COLUMN : begin -->
					<div class="middle-column col-md-6 col-md-push-3">

						<!-- PAGE HEADER : begin -->
						<div id="page-header">

							<!-- PAGE TITLE : begin -->
							<div class="page-title">
								<h1>Welcome to {{$panchayat_home_page[0]['title']}},<br><em>the Most Exciting Town of Northeast!</em></h1>
							</div>
							<!-- PAGE TITLE : end -->

						</div>
						<!-- PAGE HEADER : end -->

						<!-- PAGE CONTENT : begin -->
						<div id="page-content">
							<div class="page-content-inner">

								<!-- DIRECTORY : begin -->
								<!-- You can choose to have 2, 3 or 4 columns in Directory element.
								To change the number of columns, change the class in the following element from m-3-columns to m-2-columns or m-4-columns -->
								<div class="c-directory m-has-icon m-3-columns">
									<div class="c-content-box">
										<div class="directory-inner">
											<i class="ico-shadow tp tp-road-sign"></i>
											<h2 class="directory-title"><i class="ico tp tp-road-sign"></i>Choose <strong>Your Interest</strong></h2>
											<div class="directory-content">
												<nav class="directory-menu">
													<ul>
														<li><a href="town-hall.html">Government</a>
															<ul>
																<li><a href="town-hall.html">Town Hall</a></li>
																<li><a href="town-council.html">Town Council</a></li>
																<li><a href="home-2.html">Alternative Home</a></li>
															</ul>
														</li>
														<li><a href="post-list.html">Community</a>
															<ul>
																<li><a href="post-list.html">TownPress News</a></li>
																<li><a href="post-list.html">Public Notices</a></li>
																<li><a href="document-list.html">Town Documents</a></li>
															</ul>
														</li>
														<li><a href="statistics.html">About Our Town</a>
															<ul>
																<li><a href="statistics.html">Statistics</a></li>
																<li><a href="town-history.html">Town History</a></li>
																<li><a href="virtual-tour.html">Virtual Tour</a></li>
															</ul>
														</li>
														<li><a href="event-list.html">Relax</a>
															<ul>
																<li><a href="event-list.html">Upcoming Events</a></li>
																<li><a href="gallery-list-html.html">Photo Galleries</a></li>
																<li><a href="#">Facebook Page</a></li>
															</ul>
														</li>
														<li><a href="contact.html">Get In Touch</a>
															<ul>
																<li><a href="contact.html">Write To Mayor</a></li>
																<li><a href="phone-numbers-2.html">Phone Numbers</a></li>
																<li><a href="#">Twitter Profile</a></li>
															</ul>
														</li>
														<li><a href="http://themeforest.net/user/LSVRthemes/portfolio">TownPress Template</a>
															<ul>
																<li><a href="http://themeforest.net/user/LSVRthemes/portfolio">Purchase TownPress</a></li>
																<li><a href="http://themeforest.net/user/LSVRthemes/portfolio">WordPress Version</a></li>
																<li><a href="http://demos.lsvr.sk/townpress.html/documentation/">Documentation</a></li>
															</ul>
														</li>
													</ul>
												</nav>
											</div>
										</div>
									</div>
								</div>
								<!-- DIRECTORY : end -->

								<!-- POST LIST : begin -->
								<div class="c-post-list m-has-icon">
									<div class="c-content-box">
										<div class="post-list-inner">
											<i class="ico-shadow tp tp-reading"></i>
											<h2 class="post-list-title"><i class="ico tp tp-reading"></i><a href="post-list.html">TownPress <strong>News</strong></a></h2>
											<div class="post-list-content">

												<!-- FEATURED POST : begin -->
												<article class="featured-post m-has-thumb">
													<div class="post-image">
														<a href="post-detail.html"><img src="{{asset('public/frontend/images/post-01-cropped.jpg')}}" alt=""></a>
													</div>
													<div class="post-core">
														<h3 class="post-title">
															<a href="post-detail.html">Report from Friendly Soccer Match between TownPress and PressTown</a>
														</h3>
														<div class="post-date"><i class="ico tp tp-clock2"></i>August 23, 2015</div>
														<div class="post-excerpt">
															<p>Football refers to a number of sports that involve, to varying degrees, kicking a ball with the foot to score a goal. Unqualified, the word football is understood to refer to whichever form of football is the most popular in the regional context in which the word appears.</p>
														</div>
													</div>
												</article>
												<!-- FEATURED POST : end -->

												<!-- POST : begin -->
												<article class="post">
													<h3 class="post-title">
														<a href="post-detail.html">New Housing Complex in TownPress Nearly Complete</a>
													</h3>
													<div class="post-date">July 6, 2015</div>
												</article>
												<!-- POST : end -->

												<!-- POST : begin -->
												<article class="post">
													<h3 class="post-title">
														<a href="post-detail.html">This Year’s Summer Rock Festival Draws More Than 1000 Fans</a>
													</h3>
													<div class="post-date">June 15, 2015</div>
												</article>
												<!-- POST : end -->

												<!-- POST : begin -->
												<article class="post">
													<h3 class="post-title">
														<a href="post-detail.html">10 Things You Didn’t Know About our Town</a>
													</h3>
													<div class="post-date">June 2, 2015</div>
												</article>
												<!-- POST : end -->

												<!-- POST : begin -->
												<article class="post">
													<h3 class="post-title">
														<a href="post-detail.html">Report From Monday’s Financial Town Meeting</a>
													</h3>
													<div class="post-date">May 14, 2015</div>
												</article>
												<!-- POST : end -->

												<p class="more-btn-holder"><a href="post-list.html">Read All Posts</a></p>

											</div>
										</div>
									</div>
								</div>
								<!-- POST LIST : end -->

							</div>
						</div>
						<!-- PAGE CONTENT : end -->

						<hr class="c-separator m-margin-top-small m-margin-bottom-small m-transparent hidden-lg hidden-md">

					</div>
					<!-- MIDDLE COLUMN : end -->

					<!-- LEFT COLUMN : begin -->
					<div class="left-column col-md-3 col-md-pull-6">

						<!-- SIDE MENU : begin -->
						<nav class="side-menu m-left-side m-show-submenu">
							@include('frontend.menu')
						</nav>
						<!-- SIDE MENU : end -->

						<!-- LEFT SIDEBAR : begin -->
						<aside class="sidebar">
							<div class="widget-list">

								<!-- DOCUMENTS WIDGET : begin -->
								<div class="widget documents-widget">
									<div class="widget-inner">
										<h3 class="widget-title m-has-ico"><i class="widget-ico tp tp-papers"></i>Documents</h3>
										<div class="widget-content">
											<ul class="document-list m-has-icons">

												<!-- DOCUMENT : begin -->
												<li class="document">
													<div class="document-inner">
														<div class="document-icon" title="PDF File"><i class="fa fa-file-pdf-o"></i></div>
														<h4 class="document-title">
															<a href="documents/dummy-document2.pdf" target="_blank">Council Agenda April 24, 2015</a>
															<span class="document-filesize">(27 kB)</span>
														</h4>
													</div>
												</li>
												<!-- DOCUMENT : end -->

												<!-- DOCUMENT : begin -->
												<li class="document">
													<div class="document-inner">
														<div class="document-icon" title="PDF File"><i class="fa fa-file-pdf-o"></i></div>
														<h4 class="document-title">
															<a href="documents/dummy-document.pdf" target="_blank">Town Report 2015</a>
															<span class="document-filesize">(24 kB)</span>
														</h4>
													</div>
												</li>
												<!-- DOCUMENT : end -->

												<!-- DOCUMENT : begin -->
												<li class="document">
													<div class="document-inner">
														<div class="document-icon" title="Word Document"><i class="fa fa-file-word-o"></i></div>
														<h4 class="document-title">
															<a href="documents/dummy-document2.pdf" target="_blank">Police Contract</a>
															<span class="document-filesize">(27 kB)</span>
														</h4>
													</div>
												</li>
												<!-- DOCUMENT : end -->

												<!-- DOCUMENT : begin -->
												<li class="document">
													<div class="document-inner">
														<div class="document-icon" title="Word Document"><i class="fa fa-file-word-o"></i></div>
														<h4 class="document-title">
															<a href="documents/dummy-document2.pdf" target="_blank">Public Schools Contract</a>
															<span class="document-filesize">(27 kB)</span>
														</h4>
													</div>
												</li>
												<!-- DOCUMENT : end -->

											</ul>
											<p class="show-all-btn"><a href="document-list.html">See All Documents</a></p>
										</div>
									</div>
								</div>
								<!-- DOCUMENTS WIDGET : end -->

								<!-- LOCALE INFO WIDGET : begin -->
								<!-- to remove background image from this widget, simply remove "m-has-bg" class from the following element -->
								<div class="widget locale-info-widget m-has-bg">
									<div class="widget-inner">
										<h3 class="widget-title m-has-ico"><i class="widget-ico tp tp-map-marker"></i>Locale Info</h3>
										<div class="widget-content">
											<ul>
												<li>
													<div class="row-title"><h4>Country</h4></div>
													<div class="row-value">United States</div>
												</li>
												<li>
													<div class="row-title"><h4>State</h4></div>
													<div class="row-value">Vermont</div>
												</li>
												<li>
													<div class="row-title"><h4>County</h4></div>
													<div class="row-value">Lamoille</div>
												</li>
												<li>
													<div class="row-title"><h4>Area</h4></div>
													<div class="row-value">72.7 sq mi (188.4 km<sup>2</sup>)</div>
												</li>
												<li>
													<div class="row-title"><h4>Population</h4></div>
													<div class="row-value">4,314</div>
												</li>
												<li>
													<div class="row-title"><h4>Coordinates</h4></div>
													<div class="row-value">44°28′31″N<br>72°42′8″W</div>
												</li>
												<li>
													<div class="row-title"><h4>Time zone</h4></div>
													<div class="row-value">Eastern (EST) (UTC-5)</div>
												</li>
												<li>
													<div class="row-title"><h4>ZIP code</h4></div>
													<div class="row-value">05672</div>
												</li>
											</ul>
            							</div>
									</div>
								</div>
								<!-- LOCALE INFO WIDGET : end -->

								<!-- FEATURED GALLERY WIDGET : begin -->
								<div class="widget featured-gallery-widget">
									<div class="widget-inner">
										<h3 class="widget-title m-has-ico"><i class="widget-ico tp tp-pictures"></i>Featured Gallery</h3>
										<div class="widget-content">
											<div class="gallery-image" title="Streets of TownPress">
												<a href="gallery-detail.html"><img src="{{asset('public/frontend/images/featured-gallery-01.jpg')}}" alt=""></a>
											</div>
											<p class="show-all-btn"><a href="gallery-list.html">See All Galleries</a></p>
            							</div>
									</div>
								</div>
								<!-- FEATURED GALLERY WIDGET : end -->

							</div>
						</aside>
						<!-- LEFT SIDEBAR : end -->

					</div>
					<!-- LEFT COLUMN : end -->

					<!-- RIGHT COLUMN : begin -->
					<div class="right-column col-md-3">

						<!-- RIGHT SIDEBAR : begin -->
						<aside class="sidebar">
							<div class="widget-list">

								<!-- NOTICES WIDGET : begin -->
								<div class="widget notices-widget">
									<div class="widget-inner">
										<h3 class="widget-title m-has-ico"><i class="widget-ico tp tp-bullhorn"></i>Town Notices</h3>
										<div class="widget-content">
											<ul class="notice-list">

												<!-- NOTICE : begin -->
												<li class="notice">
													<div class="notice-inner">
														<h4 class="notice-title"><a href="notice-detail.html">Annual Marathon Registration</a></h4>
														<span class="notice-date">May 14, 2015</span>
													</div>
												</li>
												<!-- NOTICE : end -->

												<!-- NOTICE : begin -->
												<li class="notice">
													<div class="notice-inner">
														<h4 class="notice-title"><a href="notice-detail.html">Traffic Safety Notice</a></h4>
														<span class="notice-date">July 19, 2015</span>
													</div>
												</li>
												<!-- NOTICE : end -->

												<!-- NOTICE : begin -->
												<li class="notice">
													<div class="notice-inner">
														<h4 class="notice-title"><a href="notice-detail.html">Municipal Waste</a></h4>
														<span class="notice-date">August 24, 2015</span>
													</div>
												</li>
												<!-- NOTICE : end -->

												<!-- NOTICE : begin -->
												<li class="notice">
													<div class="notice-inner">
														<h4 class="notice-title"><a href="notice-detail.html">Upcoming Public Meetings</a></h4>
														<span class="notice-date">October 18, 2015</span>
													</div>
												</li>
												<!-- NOTICE : end -->

											</ul>
											<p class="show-all-btn"><a href="notice-list.html">See All Notices</a></p>
										</div>
									</div>
								</div>
								<!-- NOTICES WIDGET : end -->

								<!-- IMAGE WIDGET : begin -->
								<div class="widget image-widget">
									<div class="widget-inner">
										<div class="widget-content">
											<a id="rsadsurl" title="Ads by Sunil" href=""><img alt ="SUNIL" id="rsads" src="{{asset('public/frontend/images/poster-01.jpg')}}" alt=""></a>
										</div>
									</div>
								</div>
								<!-- IMAGE WIDGET : end -->

								<!-- EVENTS WIDGET : begin -->
								<div class="widget events-widget">
									<div class="widget-inner">
										<h3 class="widget-title m-has-ico"><i class="widget-ico tp tp-calendar-full"></i>Upcoming Events</h3>
										<div class="widget-content">
											<ul class="event-list">

												<!-- EVENT : begin -->
												<li class="event m-has-date">
													<div class="event-inner">
														<div class="event-date" title="June 3, 2016">
															<span class="event-month">Jun</span>
															<span class="event-day">3</span>
														</div>
														<h4 class="event-title"><a href="event-detail.html">Movie Premiere: Reflex Revenge (Action Drama)</a></h4>
													</div>
												</li>
												<!-- EVENT : end -->

												<!-- EVENT : begin -->
												<li class="event m-has-date">
													<div class="event-inner">
														<div class="event-date" title="June 24, 2016">
															<span class="event-month">Jun</span>
															<span class="event-day">24</span>
														</div>
														<h4 class="event-title"><a href="event-detail.html">Summer Rock Festival: Intrepid Pioneers, Filament Kingdom, Messenger Encounter &amp; More!</a></h4>
													</div>
												</li>
												<!-- EVENT : end -->

												<!-- EVENT : begin -->
												<li class="event m-has-date">
													<div class="event-inner">
														<div class="event-date" title="July 9, 2016">
															<span class="event-month">Jul</span>
															<span class="event-day">9</span>
														</div>
														<h4 class="event-title"><a href="event-detail.html">Theatre Premiere: Antigone with Maximiliano Simonas</a></h4>
													</div>
												</li>
												<!-- EVENT : end -->

												<!-- EVENT : begin -->
												<li class="event m-has-date">
													<div class="event-inner">
														<div class="event-date" title="July 15, 2016">
															<span class="event-month">Jul</span>
															<span class="event-day">15</span>
														</div>
														<h4 class="event-title"><a href="event-detail.html">Annual Spring Marathon: Around the TownPress</a></h4>
													</div>
												</li>
												<!-- EVENT : end -->

											</ul>
											<p class="show-all-btn"><a href="event-list.html">See All Events</a></p>
										</div>
									</div>
								</div>
								<!-- EVENTS WIDGET : end -->

							</div>
						</aside>
						<!-- RIGHT SIDEBAR : end -->

					</div>
					<!-- RIGHT COLUMN : end -->
				</div>
			</div>
		</div>
		<!-- CORE : end -->
@endsection
