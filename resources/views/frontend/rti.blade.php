@extends('layouts.frontend.main')
{{-- Page Title --}}
@section('page-title')
    Form Validation
@endsection

@section('main-content')
<!-- CORE : begin -->
		<div id="core">
			<div class="c-container">
				<div class="row">

					<!-- MIDDLE COLUMN : begin -->	<!-- MIDDLE COLUMN : begin -->
					<div class="middle-column col-md-6 col-md-push-3">

						<!-- PAGE HEADER : begin -->
						<div id="page-header" class="m-has-breadcrumbs">

							<!-- PAGE TITLE : begin -->
							<div class="page-title">
								<h1>RTI</h1>
							</div>
							<!-- PAGE TITLE : end -->

							<!-- BREADCRUMBS : begin -->
							<div class="breadcrumbs">
								<ul>
									<li class="home"><a href="{{('/')}}">Home</a></li>
									<li>R.T.I</li>
								</ul>
							</div>
							<!-- BREADCRUMBS : end -->

						</div>
						<!-- PAGE HEADER : end -->

						<!-- PAGE CONTENT : begin -->
						<div id="page-content">
							<div class="page-content-inner">
								<div class="c-content-box">

									<h2>VILLAGE PANCHAYAT</h2>
									<p>The Right to Information Act (RTI) is an Act of the Parliament of India “to provide for setting out the practical regime of right to information for citizens” and replaces the erstwhile Freedom of information Act, 2002. The Act applies to all States and Union Territories of India except Jammu & Kashmir. Under the provisions of the Act, any citizen may request information from a “public authority” (a body of Government or “instrumentality of State”) which is required to reply expeditiously or within thirty days. The Act also requires every public authority to computerise their records for wide dissemination and to proactively certain categories of information so that the citizens need minimum recourse to request for information formally. This law was passed by Parliament on 15 June 2005 and came fully into force on 12 October 2005, which was Vijayadashmi. The first application was given to a Pune police station. Information disclosure in India was restricted by the Official Secrets Act 1923 and various other special laws, which the new RTI Act relaxes. It codifies a fundamental right of citizens.</p>
								<blockquote style="font-size: 17px !important;">

								Name Of PIO: Gokuldas Kudalkar<br>
							    Designation: VP Secetary
								</blockquote>

									<!-- TABS : begin
									<div class="c-tabs">
										<div class="c-content-box">
											<div class="tabs-inner">
												<ul class="tab-list">
													<li class="tab m-active">
														<span class="tab-label">From 1731 to 1734</span>
													</li>
													<li class="tab">
														<span class="tab-label">Through the 1763</span>
													</li>
													<li class="tab">
														<span class="tab-label">On March 20, 1764</span>
													</li>
												</ul>
												<ul class="content-list">
													<li class="tab-content m-active">
														<p>From 1731 to 1734, the French constructed Fort St. Frédéric, which gave the French control of the New France/Vermont frontier region in the Lake Champlain Valley. With the outbreak of the French and Indian War in 1754, the North American front of the Seven Years’ War between the French 8and English, the French began construction of Fort Carillon at present-day Ticonderoga, New York in 1755. The British failed to take Fort St. Frédéric or Fort Carillon between 1755 and 1758. In 1759, a combined force of 12,000 British regular and provincial troops under Sir Jeffery Amherst captured Carillon, after which the French abandoned Fort St. Frédéric. Amherst constructed Fort Crown Point next to the remains of the Fort St. Frédéric, securing British control over the area.</p>
													</li>
													<li class="tab-content">
														<p>Following France’s loss in the French and Indian War, through the 1763 Treaty of Paris they ceded control of the land to the British. Colonial settlement was limited by the Crown to lands east of the Appalachians, in order to try to end encroachment on Native American lands. The territory of Vermont was divided nearly in half in a jagged line running from Fort William Henry in Lake George diagonally north-eastward to Lake Memphremagog. With the end of the war, new settlers arrived in Vermont. Ultimately, Massachusetts, New Hampshire and New York all claimed this frontier area.</p>
													</li>
													<li class="tab-content">
														<p>On March 20, 1764, King George III established the boundary between New Hampshire and New York along the west bank of the Connecticut River, north of Massachusetts, and south of 45 degrees north latitude. In 1770, Ethan Allen, his brothers Ira and Levi, and Seth Warner, recruited an informal militia known as the Green Mountain Boys to protect the interests of the original New Hampshire settlers against newcomers from New York.</p>
													</li>
												</ul>
											</div>
										</div>
									</div-->
								</div>
							</div>
						</div>
						<!-- PAGE CONTENT : end -->

					

					</div>
				
					<div class="left-column col-md-3 col-md-pull-6">

						<!-- SIDE MENU : begin -->
						<nav class="side-menu m-left-side m-show-submenu">
						 @include('frontend.menu')
						</nav>
						<!-- SIDE MENU : end -->

						<!-- LEFT SIDEBAR : begin -->
						<aside class="sidebar">
							<div class="widget-list">

								<!-- FEATURED GALLERY WIDGET : begin -->
								<div class="widget featured-gallery-widget">
									<div class="widget-inner">
										<h3 class="widget-title m-has-ico"><i class="widget-ico tp tp-pictures"></i>Featured Gallery</h3>
										<div class="widget-content">
											<div class="gallery-image" title="Streets of TownPress">
												<a href="gallery-detail.html"><img src="images/featured-gallery-01.jpg" alt=""></a>
											</div>
											<p class="show-all-btn"><a href="gallery-list.html">See All Galleries</a></p>
            							</div>
									</div>
								</div>
								<!-- FEATURED GALLERY WIDGET : end -->

								<!-- DOCUMENTS WIDGET : begin -->
								<div class="widget documents-widget">
									<div class="widget-inner">
										<h3 class="widget-title m-has-ico"><i class="widget-ico tp tp-papers"></i>Documents</h3>
										<div class="widget-content">
											<ul class="document-list m-has-icons">

												<!-- DOCUMENT : begin -->
												<li class="document">
													<div class="document-inner">
														<div class="document-icon" title="PDF File"><i class="fa fa-file-pdf-o"></i></div>
														<h4 class="document-title">
															<a href="documents/dummy-document2.pdf" target="_blank">Council Agenda April 24, 2014</a>
															<span class="document-filesize">(27 kB)</span>
														</h4>
													</div>
												</li>
												<!-- DOCUMENT : end -->

												<!-- DOCUMENT : begin -->
												<li class="document">
													<div class="document-inner">
														<div class="document-icon" title="PDF File"><i class="fa fa-file-pdf-o"></i></div>
														<h4 class="document-title">
															<a href="documents/dummy-document.pdf" target="_blank">Town Report 2014</a>
															<span class="document-filesize">(24 kB)</span>
														</h4>
													</div>
												</li>
												<!-- DOCUMENT : end -->

												<!-- DOCUMENT : begin -->
												<li class="document">
													<div class="document-inner">
														<div class="document-icon" title="Word Document"><i class="fa fa-file-word-o"></i></div>
														<h4 class="document-title">
															<a href="documents/dummy-document2.pdf" target="_blank">Police Contract</a>
															<span class="document-filesize">(27 kB)</span>
														</h4>
													</div>
												</li>
												<!-- DOCUMENT : end -->

												<!-- DOCUMENT : begin -->
												<li class="document">
													<div class="document-inner">
														<div class="document-icon" title="Word Document"><i class="fa fa-file-word-o"></i></div>
														<h4 class="document-title">
															<a href="documents/dummy-document2.pdf" target="_blank">Public Schools Contract</a>
															<span class="document-filesize">(27 kB)</span>
														</h4>
													</div>
												</li>
												<!-- DOCUMENT : end -->

											</ul>
											<p class="show-all-btn"><a href="document-list.html">See All Documents</a></p>
										</div>
									</div>
								</div>
								<!-- DOCUMENTS WIDGET : end -->

							</div>
						</aside>
						<!-- LEFT SIDEBAR : end -->

					</div>
					<!-- LEFT COLUMN : end -->

					<!-- RIGHT COLUMN : begin -->
					<div class="right-column col-md-3">

						<!-- RIGHT SIDEBAR : begin -->
						<aside class="sidebar">
							<div class="widget-list">

								<!-- CATEGORIES WIDGET : begin -->
								<div class="widget categories-widget">
									<div class="widget-inner">
										<h3 class="widget-title m-has-ico">
										<i class="widget-ico tp tp-list4"></i>Categories</h3>
										<div class="widget-content">
											<ul>
												<li><a href="event-list.html">Community</a> (2)</li>
												<li><a href="event-list.html">Culture</a> (2)</li>
												<li><a href="event-list.html">Relax</a> (2)</li>
												<li><a href="event-list.html">Sport</a> (1)</li>
											</ul>
										</div>
									</div>
								</div>
								<!-- CATEGORIES WIDGET : end -->

								<!-- IMAGE WIDGET : begin -->
								<div class="widget image-widget">
									<div class="widget-inner">
										<div class="widget-content">
											<a href="#"><img src="images/poster-01.jpg" alt=""></a>
										</div>
									</div>
								</div>
								<!-- IMAGE WIDGET : end -->

							</div>
						</aside>
						<!-- RIGHT SIDEBAR : end -->

					</div>
					<!-- RIGHT COLUMN : end -->

				</div>
			</div>
		</div>
		<!-- CORE : end -->
@endsection
