<!DOCTYPE html>
<html class="no-js" lang="zxx">

    <head>

        @include('layouts.head')
        <link rel="stylesheet" href="{{asset('public/assets/css/flag-icon.min.css')}}">

        @yield('css')

    </head>

    <body>
    <!--[if lt IE 8]>
    <p class="browserupgrade">You are using an <strong>outdated</strong> browser. Please <a href="http://browsehappy.com/">upgrade your browser</a> to improve your experience.</p>
    <![endif]-->

    <!--=========================*
            Page Content
    *===========================-->
    <div class="vz_main_sec">
        <!--==================================*
                Header And Sidebar Section
        *====================================-->
        @include('layouts.header')
        <!--==================================*
               End Header Sidebar Section
        *====================================-->

        <!--=========================*
               Main Section
       *===========================-->
        <div class="vz_main_container">
            <div class="vz_main_content">
                @yield('main-content')
            </div>
            <!--=========================*
                        Footer
           *===========================-->
            @include('layouts.footer')
            <!--=========================*
                    End Footer
           *===========================-->
        </div>
        <!--=========================*
                End Main Section
       *===========================-->

    </div>
   
	@include('sweetalert::alert')

    @include('layouts.scripts')

    @yield('js')

    </body>
</html>
