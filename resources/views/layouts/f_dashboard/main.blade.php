
<!DOCTYPE html>
<html lang="en">

<head>

  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
  <title>@yield('title')</title>
  @include('layouts.f_dashboard.head_css')
</head>
<body id="page-top">
<div id="wrapper">
   <ul class="navbar-nav bg-gradient-primary sidebar sidebar-dark accordion" id="accordionSidebar">
      @include('layouts.f_dashboard.sidebar')
	</ul>
    <div id="content-wrapper" class="d-flex flex-column">
      <div id="content">
        <nav class="navbar navbar-expand navbar-light bg-white topbar mb-4 static-top shadow">
          @include('layouts.f_dashboard.header')
        </nav>
        <!-- End of Topbar -->

        <!-- Begin Page Content -->
        <div class="container-fluid">
          @yield('main-content')
		</div>
    </div>
    @include('layouts.f_dashboard.footer')     
<!-- Bootstrap core JavaScript-->
 
   @include('layouts.f_dashboard.footer_js')

</body>

</html>
