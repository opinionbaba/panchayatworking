 <a class="sidebar-brand d-flex align-items-center justify-content-center" href="/">        
        <div class="sidebar-brand-text">Citizen<sup>2</sup></div>
     </a>
      <li class="nav-item active">
        <a class="nav-link" href="{{route('citizen-login')}}">
          <i class="fas fa-fw fa-tachometer-alt"></i>
		  <span>Dashboard</span>
		</a>
      </li>
     <!-- Nav Item - Pages Collapse Menu -->
	 <li class="nav-item">
        <a class="nav-link collapsed" href="{{route('profile')}}">
          <i class="fas fa-fw fa-user"></i>
		  <span>Profile</span>
        </a>
       </li>
      <li class="nav-item">
        <a class="nav-link collapsed" href="{{route('assets')}}">
          <i class="fas fa-fw fa-cog"></i>
		  <span>Assets</span>
        </a> 
      </li>
	   
	    <li class="nav-item">
        <a class="nav-link collapsed" href="{{route('advertise')}}">
          <i class="fas fa-fw fa-list"></i>
		  <span>Advertise</span>
        </a>
       </li>

      <!-- Nav Item - Utilities Collapse Menu -->
      <li class="nav-item">
        <a class="nav-link collapsed" href="{{route('tax')}}" >
          <i class="fas fa-chalkboard"></i>
		  <span>Tax</span>
        </a>
         </li>

      <!-- Nav Item - Pages Collapse Menu -->
      <li class="nav-item">
        <a class="nav-link" href="{{route('all-ertificate')}}">
          <i class="fas fa-fw fa-folder"></i>
		  <span>Certificates</span>
        </a>       
      </li>

      <!-- Nav Item - Charts -->
      <li class="nav-item">
        <a class="nav-link" href="{{route('citi-rti')}}">
         <i class="fas fa-university"></i>
		  <span>RTI</span>  
		</a>
      </li>

      <!-- Nav Item - Tables -->
      <li class="nav-item">
        <a class="nav-link" href="{{route('notification')}}">
          <i class="fas fa-calculator"></i>
		  <span>Notification</span>
		</a>		  
      </li>
	  
	  
	  <!-- Nav Item - Tables -->
      <li class="nav-item">
        <a class="nav-link" href="{{route('ticket')}}">
          <i class="fas fa-award"></i>
		  <span>Tickets</span>
		</a>		  
      </li> 

	  <!-- Nav Item - Tables -->
      <li class="nav-item">
        <a class="nav-link" href="{{route('other')}}">
          <i class="fas fa-book-reader"></i>
		  <span>Others</span>
		</a>		  
      </li>

