<!-- FOOTER : end -->

<!-- SCRIPTS : begin -->
<script src="{{asset('public/frontend/library/js/jquery-1.9.1.min.js')}}" type="text/javascript"></script>
<script src="{{asset('public/frontend/library/js/third-party.js')}}" type="text/javascript"></script>
<script src="{{asset('public/frontend/library/js/library.js')}}" type="text/javascript"></script>
<script src="{{asset('public/frontend/library/js/scripts.js')}}" type="text/javascript"></script>
<script>

$(document).ready(function(){
	var rsads = [
		@foreach($advertises as $img)
			'{{$img->ads_img}}',
		@endforeach
	];
	var rsadsurl = [
		@foreach($advertises as $img)
			'{{$img->ads_url}}',
		@endforeach
	];
	setInterval(
		function(){
			let index = (Math.floor(Math.random() * rsads.length));
			$("#rsads").attr('src',rsads[index]);
			$("#rsadsurl").attr('href',rsadsurl[index]);
		},3000
	)
	$('.button').click(function(){
		var buttonId = $(this).attr('id');
		$('#modal-container').removeAttr('class').addClass(buttonId);
		$('body').addClass('modal-active');
	})

	$('#close').click(function(){
		$("#modal-container").addClass('out');
		$('body').removeClass('modal-active');
		});
});
</script>
		<!-- SCRIPTS : end -->

	</body>
</html>