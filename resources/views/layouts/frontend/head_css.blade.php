<!DOCTYPE html>
<html>
	
<!-- Mirrored from demos.lsvr.sk/townpress.html/demo/index.html by HTTrack Website Copier/3.x [XR&CO'2014], Fri, 14 Feb 2020 06:28:30 GMT -->
<head>

		<meta http-equiv="content-type" content="text/html; charset=utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
		<meta name="csrf_token" content="{{ csrf_token() }}" />
        <title>TownPress - Municipality HTML Template</title>
        <link rel="shortcut icon" href="{{asset('public/frontend/images/favicon.png')}}">
		
		<!-- GOOGLE FONTS : begin -->
		<link href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:400,400italic,700,700italic&amp;subset=latin,latin-ext" rel="stylesheet" type="text/css">
		<!-- GOOGLE FONTS : end -->

        <!-- STYLESHEETS : begin -->
		<link rel="stylesheet" type="text/css" href="{{asset('public/frontend/library/css/style.css')}}">
		<!-- To change to a different pre-defined color scheme, change "red.css" in the following element to blue.css | bluegray.css | green.css | orange.css
		Please refer to the documentation to learn how to create your own color schemes -->
        
		<link rel="stylesheet" type="text/css" href="{{asset('public/frontend/library/css/skin/red.css')}}">
		<link rel="stylesheet" type="text/css" href="{{asset('public/frontend/library/css/custom.css')}}">
		<!--[if lte IE 9]>
		<link rel="stylesheet" type="text/css" href="library/css/oldie.css">
		<![endif]-->
		<!-- STYLESHEETS : end -->

	</head>