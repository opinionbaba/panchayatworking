	<style>
	.submit{
    background: #ec5237;
    height: 39px;
    width: 88px;
	color: #fff;
	}
	.otp{
		height: 32px;
		background: green;
		width: 104px;
		border-radius: 13px;
		color: #fff;
		margin-top: 22px;
		float: right;
	}
	</style>
	<!-- HEADER : end -->
	<script src="http://ajax.googleapis.com/ajax/libs/jquery/1.9.1/jquery.js"></script>
<div id="modal-container">
  <div class="modal-background">
    <div class="modal">
	 <!---content---------------->
	   
 <div class="login-wrap">
	<div class="login-html">
	 <button type="button" id="close">&times;</button>
		<input id="tab-1" type="radio" name="tab" class="sign-in" checked><label for="tab-1" class="tab">Sign In</label>
		<input id="tab-2" type="radio" name="tab" class="sign-up"><label for="tab-2" class="tab">Sign Up</label>
		<div class="login-form">
			<div class="sign-in-htm">
			 <form action ="{{route('citizen-login')}}" method="post">
			  @csrf
				<div class="group">
					<label for="user" class="label">Email</label>
					<input id="user" name="email" type="text" class="input">
				</div>
				<div class="group">
					<label for="pass" class="label">Password</label>
					<input id="pass" name="password" type="password" class="input" data-type="password">
				</div>
				<div class="group">
					<input id="check" type="checkbox" class="check" checked>
					<label for="check"><span class="icon"></span> Keep me Signed in</label>
				</div>
				<div class="group">
					<input type="submit" class="button" value="Sign In">
				</div>
				<div class="hr"></div>
				<div class="foot-lnk">
					<a href="#forgot">Forgot Password?</a>
				</div>
			</form>
			</div>
			<div class="sign-up-htm">
			
		<form action ="{{route('add-citizen')}}"  method="post">
			 <input type="hidden" name="_token" id="mytoken1" value="{{ csrf_token() }}" />
			  <div class="row">
			    <div class="col-md-4">
					<div class="group">
						<label for="user" class="label">First Name</label>
						<input id="user" name="f_name" type="text" class="input">
					</div>
				</div>
				 <div class="col-md-4">
					<div class="group">
						<label for="user" class="label">Middle Name</label>
						<input id="user" name="m_name" type="text" class="input">
					</div>
				 </div>
				  <div class="col-md-4">
					<div class="group">
						<label for="user" class="label">Last Name</label>
						<input id="user" name="l_name" type="text" class="input">
					</div>
				  </div>
			  </div>
			   <div class="row">
			    <div class="col-md-6">
					<div class="group">
						<label for="user" class="label">Mobile</label>
						<input type="text" name="moblie" class="input phone">
					</div>
				</div>
				  <div class="col-md-6">
					<div class="group">
					<button type="button"  class="otp" id="getotp">Get Otp</button>
				   </div>
				 </div>
				 
			<div class="row hide_otp">
				<div class="group col-md-3">
					<label>Enter OTP</label>
				</div>
				<div class="group col-md-9">
				   <input type="number" class="input" name="otp" id="otp" >
				</div>
			 </div>
			  </div>
			  
			   <div class="row">
			    <div class="col-md-6">
					<div class="group">
						<label for="user" class="label">Email</label>
						<input id="user" name="email" type="email" class="input">
					</div>
				</div>
			 
			    <div class="col-md-6">
					<div class="group">
						<label for="user" class="label">Password</label>
						<input id="user" name="password" type="password" class="input">
					</div>
				</div>
			 </div>
				<div class="group">
					<input type="submit" class="button" value="Sign Up">
				</div>
				<div class="foot-lnk">
					<label for="tab-1" style="color:#fff">Already Member?</a>
				</div>
				</form>
			</div>
		</div>
	</div>
</div>
<script type="text/javascript">
  $(document).ready(function(){
		$('.hide_otp').hide();
		$('#getotp').click(function(event){
		    var phone = $('.phone').val();
		    var mytoken1 = $("#mytoken1").val();
		    var dataString = { phone: phone , _token:mytoken1 };
			if(phone=='') 
			{
			 alert("Please Enter Your Mobile Number");
			}
			else
			{
			$.ajax({
				method: 'POST',
				url: "{{ url('ajax-form-submit')}}",
				data: dataString,
				cache: false,
				success: function(result){
				   if(result == 'error')
					{
						alert('Mobile Number Already Registered...');
					}
					else
					{
						$('.hide_otp').show();
					}
				}
				});
			}
		});
	});
</script>
	 <!---end of content---------------->
    </div>
  </div>
</div>
  <body>
	<header id="header" class="m-has-header-tools m-has-gmap">
		<div class="header-inner">
         <div class="header-content">
		  <div class="c-container">
			<div class="header-content-inner">
             <div class="header-branding m-large-logo">
				<a href="{{('/')}}"><span>
					<img src="{{asset('public/frontend/images/header-logo.png')}}"
						data-hires="images/header-logo.2x.png"
						alt="TownPress - Municipality HTML Template">
				</span></a>
			 </div>
				<div class="header-toggle-holder">
                 <button type="button" class="header-toggle">
					<i class="ico-open tp tp-menu"></i>
					<i class="ico-close tp tp-cross"></i>
					<span>Menu</span>
				</button>
					<button type="button" class="header-gmap-switcher" title="Show on Map">
						<i class="ico-open tp tp-map2"></i>
						<i class="ico-close tp tp-cross"></i>
					</button>
				</div>
					<nav class="header-menu">
					  <ul>
						<li><a href="index-2.html">HOME</a></li>
						<li><a href="town-hall.html">ABOUT US</a></li>
						<li><a href="town-hall.html">SCHEMES</a></li>
						<li><a href="town-council.html">ACTS</a></li>
						<li><a href="phone-numbers.html">R.T.I</a></li>
						<li><a href="document-list.html">FORMS</a></li>
						<li><a href="contact.html">PHOTO GALLERY</a></li>
						<li><a href="post-list.html">AUDIT</a></li>
						<li><a href="post-list.html">CERTIFICATES</a></li>
						<li><a href="notice-list.html">CONTACT US</a></li>
					  </ul>
					</nav>
					<div class="header-tools">
						<div class="header-search">
							<form method="get" action="" class="c-search-form">
								<div class="form-fields">
									<input type="text" value="" placeholder="Search this site..." name="s">
									<button type="submit" class="submit-btn"><i class="tp tp-magnifier"></i></button>
								</div>
							</form>
						</div>
					    <button type="button" class="header-gmap-switcher" title="Show on Map">
							<i class="ico-open tp tp-map2"></i>
							<i class="ico-close tp tp-cross"></i>
							<span>Map</span>
						</button>
						<button  id="one" class="button" >
							<i class="ico-open tp tp-map2"></i>
							
							<span>Login</span>
						</button>
					</div>
				  </div>
				</div>
			   </div>
				<!-- Add your address into "data-address" attribute
				Change zoom level with "data-zoom" attribute, bigger number = bigger zoom
				Change map type with "data-maptype" attribute, values: roadmap | terrain | hybrid | satellite
				API KEY IS REQUIRED! More info https://developers.google.com/maps/documentation/javascript/get-api-key -->
				<div class="header-gmap">
					<div class="gmap-canvas"
						data-google-api-key="AIzaSyCHctnM3xR2JUhnaQ5gLrIveomGIIbCS2w"
						data-address="Jakkur, Bengaluru, Karnataka "
						data-zoom="17"
						data-maptype="hybrid"
						data-enable-mousewheel="true">
					</div>
				</div>
			</div>
		</header>
	<div class="header-bg">
     <div class="header-image" data-autoplay="8">
		<div class="image-layer" style="background-image: url( '{{asset('public/frontend/images/header-01.jpg')}}' );"></div>
			</div>
	</div>
		