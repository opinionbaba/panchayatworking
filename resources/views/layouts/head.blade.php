 
<meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <!-- Tell the browser to be responsive to screen width -->
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>@yield('page-title') | VpCurti</title>
    <link rel="shortcut icon" type="image/x-icon" href="{{asset('public/assets/images/favicon.png')}}">
    <link href="{{asset('public/assets/morrisjs/morris.css')}}" rel="stylesheet">
    <link href="{{asset('public/assets/toast-master/css/jquery.toast.css')}}" rel="stylesheet">
    <link href="{{asset('public/assets/dist/css/style.min.css')}}" rel="stylesheet">
    <link href="{{asset('public/assets/dist/css/pages/dashboard1.css')}}" rel="stylesheet">
    <link href="{{asset('public/assets/switchery/dist/switchery.min.css')}}" rel="stylesheet">
    <link href="{{asset('public/assets/dist/css/pages/tab-page.css')}}" rel="stylesheet">
    <link href="{{asset('public/assets/footable/css/footable.bootstrap.min.css')}}" rel="stylesheet">
	<link href="{{asset('public/assets/dist/css/pages/contact-app-page.css')}}" rel="stylesheet">
    <link href="{{asset('public/dist/css/pages/footable-page.css')}}" rel="stylesheet">
    <link href="{{asset('public/assets/bootstrap-daterangepicker/daterangepicker.css')}}" rel="stylesheet">
    <link href="{{asset('public/assets/timepicker/bootstrap-timepicker.min.css')}}" rel="stylesheet">
    <link href="{{asset('public/assets/bootstrap-datepicker/bootstrap-datepicker.min.css')}}" rel="stylesheet">
    <link href="{{asset('public/assets/bootstrap-material-datetimepicker/css/bootstrap-material-datetimepicker.css')}}" rel="stylesheet">
	 <link href="{{asset('public/assets/sweetalert2/dist/sweetalert2.min.css')}}" rel="stylesheet">
	 <link href="{{asset('public/assets/wizard/steps.css')}}" rel="stylesheet">

     <link rel="stylesheet" type="text/css" href="{{asset('public/assets/datatables.net-bs4/css/dataTables.bootstrap4.css')}}">
    <link rel="stylesheet" type="text/css" href="{{asset('public/assets/datatables.net-bs4/css/responsive.dataTables.min.css')}}">
    <link rel="stylesheet" type="text/css" href="{{asset('public/assets/toggle/bootstrap-toggle.css')}}">
    <link rel="stylesheet" type="text/css" href="{{asset('public/assets/dist/css/pages/footable-page.css')}}">
    <link rel="stylesheet" type="text/css" href="{{asset('public/assets/footable/css/footable.bootstrap.min.css')}}">
    <link rel="stylesheet" type="text/css" href="{{asset('public/assets/dist/css/pages/other-pages.css')}}">
    <link rel="stylesheet" type="text/css" href="{{asset('public/assets/dist/css/pages/other-pages.css')}}">
	