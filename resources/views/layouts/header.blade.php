 <style>
 .custom{
	 margin-top: 10px;
    margin-right: 21px;
    width: auto;
 }
 </style>
  <div class="modal bs-example-modal-lg" id="validate" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true" style="display: none;">
		<div class="modal-dialog">
			<div class="modal-content">
				<div class="modal-header">
					<h4 class="modal-title" id="myLargeModalLabel">Check</h4>
					<button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
				</div>
				<div class="modal-body">
					<form method="post"  action ="" class="needs-validation" novalidate="">
					@csrf
							<div class="form-row">
							   <div class="col-md-12 mb-3">
							    <label class="radio-inline">Enter Number To Validate</label>
								 
								  <input type="text" class="form-control" name="optradio">
								
								</div>
							</div>
						  <div class="form-row">
							  <div class="col-md-12 mb-3">
							    <a href="{{route('timeline')}}"class="btn btn-primary" type="submit">Submit form</a>
							  </div>
							</div>
					</form>
				</div>
				
			</div>
			<!-- /.modal-content -->
		</div>
		<!-- /.modal-dialog -->
	</div>
 <header class="topbar">
            <nav class="navbar top-navbar navbar-expand-md navbar-dark">
               <div class="navbar-header">
                    <a class="navbar-brand" href="#">
                        <!-- Logo icon --><b>
                          <img src="{{asset('public/assets/images/logo-icon.png')}}" alt="homepage" class="dark-logo" />
                            <!-- Light Logo icon -->
                            <img src="{{asset('public/assets/images/logo-light-icon.png')}}" alt="homepage" class="light-logo" />
                        </b>
                        <span>
                         <img src="{{asset('public/assets/images/logo-light-text.png')}}" alt="homepage" class="dark-logo" />
                         <!-- Light Logo text -->    
                         <img src="{{asset('public/assets/images/logo-light-text.png')}}" class="light-logo" alt="homepage" /></span> </a>
                </div>
                <div class="navbar-collapse">
                     <ul class="navbar-nav mr-auto">
                        <!-- This is  -->
                        <li class="nav-item"> <a class="nav-link nav-toggler d-block d-md-none waves-effect waves-dark" href="javascript:void(0)"><i class="ti-menu"></i></a> </li>
                        <li class="nav-item"> <a class="nav-link sidebartoggler d-none d-lg-block d-md-block waves-effect waves-dark" href="javascript:void(0)"><i class="icon-menu"></i></a> </li>
                        <li class="nav-item">
                            <form class="app-search d-none d-md-block d-lg-block">
                                <input type="text" class="form-control" placeholder="Search & enter">
                            </form>
                        </li>&nbsp;&nbsp;&nbsp;
						 <li class="nav-item">
                            <button type="button" id="#validate" class="btn btn-danger m-b-20 p-10 btn-block waves-effect waves-light custom" data-toggle="modal" data-target="#validate">Validate</button>
                        </li>
						
						<li class="nav-item pt-3">
                         <?php 
                        	$protocol = ((!empty($_SERVER['HTTPS']) && $_SERVER['HTTPS'] != 'off') || $_SERVER['SERVER_PORT'] == 443) ? "https://" : "http://";  
                        	$CurPageURL = $protocol . $_SERVER['HTTP_HOST'] . $_SERVER['REQUEST_URI']; 
                        	$panc_id=str_replace($CurPageURL,'/',$CurPageURL);
                        	//echo $url;
                         ?>
							<form action="<?php echo $panc_id;?>" method="get">
									<select class="form-control custom-select-value" name="panc_id" onchange='this.form.submit()'>
										<option>Select Panchyat</option>
										<option value="4">My Panchyat</option>
										<option value="2">Panchyat 1</option>
									</select>
									
								</form>
                        </li>
						
						<li class="nav-item">
                            <a href="{{route('sms-credit')}}" class="btn btn-danger m-b-20 p-10 btn-block waves-effect waves-light custom" >SMS Credit</a>
                        </li>
						
                    </ul>
                    <ul class="navbar-nav my-lg-0">
                        <li class="nav-item dropdown">
                            <a class="nav-link dropdown-toggle waves-effect waves-dark" href="" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"> <i class="ti-email"></i>
                                <div class="notify"> <span class="heartbit"></span> <span class="point"></span> </div>
                            </a>
                            <div class="dropdown-menu dropdown-menu-right mailbox animated bounceInDown">
                                <ul>
                                    <li>
                                        <div class="drop-title">Notifications</div>
                                    </li>
                                    <li>
                                        <div class="message-center">
                                            <!-- Message -->
                                            <a href="javascript:void(0)">
                                                <div class="btn btn-danger btn-circle"><i class="fa fa-link"></i></div>
                                                <div class="mail-contnet">
                                                    <h5>Luanch Admin</h5> <span class="mail-desc">Just see the my new admin!</span> <span class="time">9:30 AM</span> </div>
                                            </a>
                                            <!-- Message -->
                                            <a href="javascript:void(0)">
                                                <div class="btn btn-success btn-circle"><i class="ti-calendar"></i></div>
                                                <div class="mail-contnet">
                                                    <h5>Event today</h5> <span class="mail-desc">Just a reminder that you have event</span> <span class="time">9:10 AM</span> </div>
                                            </a>
                                            <!-- Message -->
                                            <a href="javascript:void(0)">
                                                <div class="btn btn-info btn-circle"><i class="ti-settings"></i></div>
                                                <div class="mail-contnet">
                                                    <h5>Settings</h5> <span class="mail-desc">You can customize this template as you want</span> <span class="time">9:08 AM</span> </div>
                                            </a>
                                            <!-- Message -->
                                            <a href="javascript:void(0)">
                                                <div class="btn btn-primary btn-circle"><i class="ti-user"></i></div>
                                                <div class="mail-contnet">
                                                    <h5>Pavan kumar</h5> <span class="mail-desc">Just see the my admin!</span> <span class="time">9:02 AM</span> </div>
                                            </a>
                                        </div>
                                    </li>
                                    <li>
                                        <a class="nav-link text-center link" href="javascript:void(0);"> <strong>Check all notifications</strong> <i class="fa fa-angle-right"></i> </a>
                                    </li>
                                </ul>
                            </div>
                        </li>
                       
                        <li class="nav-item dropdown">
                            <a class="nav-link dropdown-toggle waves-effect waves-dark" href="" id="2" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"> <i class="icon-note"></i>
                                <div class="notify"> <span class="heartbit"></span> <span class="point"></span> </div>
                            </a>
                            <div class="dropdown-menu mailbox dropdown-menu-right animated bounceInDown" aria-labelledby="2">
                                <ul>
                                    <li>
                                        <div class="drop-title">You have 4 new messages</div>
                                    </li>
                                    <li>
                                        <div class="message-center">
                                            <!-- Message -->
                                            <a href="javascript:void(0)">
                                                <div class="user-img"> <img src="{{asset('public/assets/images/users/1.jpg')}}" alt="user" class="img-circle"> <span class="profile-status online pull-right"></span> </div>
                                                <div class="mail-contnet">
                                                    <h5>Pavan kumar</h5> <span class="mail-desc">Just see the my admin!</span> <span class="time">9:30 AM</span> </div>
                                            </a>
                                            <!-- Message -->
                                            <a href="javascript:void(0)">
                                                <div class="user-img"> <img src="{{asset('public/assets/images/users/2.jpg')}}" alt="user" class="img-circle"> <span class="profile-status busy pull-right"></span> </div>
                                                <div class="mail-contnet">
                                                    <h5>Sonu Nigam</h5> <span class="mail-desc">I've sung a song! See you at</span> <span class="time">9:10 AM</span> </div>
                                            </a>
                                            <!-- Message -->
                                            <a href="javascript:void(0)">
                                                <div class="user-img"> <img src="{{asset('public/assets/images/users/3.jpg')}}" alt="user" class="img-circle"> <span class="profile-status away pull-right"></span> </div>
                                                <div class="mail-contnet">
                                                    <h5>Arijit Sinh</h5> <span class="mail-desc">I am a singer!</span> <span class="time">9:08 AM</span> </div>
                                            </a>
                                            <!-- Message -->
                                            <a href="javascript:void(0)">
                                                <div class="user-img"> <img src="{{asset('public/assets/images/users/4.jpg')}}" alt="user" class="img-circle"> <span class="profile-status offline pull-right"></span> </div>
                                                <div class="mail-contnet">
                                                    <h5>Pavan kumar</h5> <span class="mail-desc">Just see the my admin!</span> <span class="time">9:02 AM</span> </div>
                                            </a>
                                        </div>
                                    </li>
                                    <li>
                                        <a class="nav-link text-center link" href="javascript:void(0);"> <strong>See all e-Mails</strong> <i class="fa fa-angle-right"></i> </a>
                                    </li>
                                </ul>
                            </div>
                        </li>
                         <li class="nav-item dropdown mega-dropdown"> <a class="nav-link dropdown-toggle waves-effect waves-dark" href="" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"><i class="ti-layout-width-default"></i></a>
                            <div class="dropdown-menu animated bounceInDown">
                                <ul class="mega-dropdown-menu row">
                                    <li class="col-lg-3 col-xlg-2 m-b-30">
                                        <h4 class="m-b-20">CAROUSEL</h4>
                                        <!-- CAROUSEL -->
                                        <div id="carouselExampleControls" class="carousel slide" data-ride="carousel">
                                            <div class="carousel-inner" role="listbox">
                                                <div class="carousel-item active">
                                                    <div class="container"> <img class="d-block img-fluid" src="{{asset('public/assets/images/big/img1.jpg')}}" alt="First slide"></div>
                                                </div>
                                                <div class="carousel-item">
                                                    <div class="container"><img class="d-block img-fluid" src="{{asset('public/assets/images/big/img2.jpg')}}" alt="Second slide"></div>
                                                </div>
                                                <div class="carousel-item">
                                                    <div class="container"><img class="d-block img-fluid" src="{{asset('public/assets/images/big/img3.jpg')}}" alt="Third slide"></div>
                                                </div>
                                            </div>
                                            <a class="carousel-control-prev" href="#carouselExampleControls" role="button" data-slide="prev"> <span class="carousel-control-prev-icon" aria-hidden="true"></span> <span class="sr-only">Previous</span> </a>
                                            <a class="carousel-control-next" href="#carouselExampleControls" role="button" data-slide="next"> <span class="carousel-control-next-icon" aria-hidden="true"></span> <span class="sr-only">Next</span> </a>
                                        </div>
                                        <!-- End CAROUSEL -->
                                    </li>
                                    <li class="col-lg-3 m-b-30">
                                        <h4 class="m-b-20">ACCORDION</h4>
                                         <!-- Accordian -->
                                        <div class="accordion" id="accordionExample">
                                            <div class="card m-b-0">
                                                <div class="card-header bg-white p-0" id="headingOne">
                                                    <h5 class="mb-0">
                                                        <button class="btn btn-link" type="button" data-toggle="collapse" data-target="#collapseOne" aria-expanded="true" aria-controls="collapseOne">
                                                            Collapsible Group Item #1
                                                        </button>
                                                    </h5>
                                                </div>

                                                <div id="collapseOne" class="collapse show" aria-labelledby="headingOne" data-parent="#accordionExample">
                                                    <div class="card-body">
                                                        Anim pariatur cliche reprehenderit, enim eiusmod high.
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="card m-b-0">
                                                <div class="card-header bg-white p-0" id="headingTwo">
                                                    <h5 class="mb-0">
                                                        <button class="btn btn-link collapsed" type="button" data-toggle="collapse" data-target="#collapseTwo" aria-expanded="false"
                                                            aria-controls="collapseTwo">
                                                            Collapsible Group Item #2
                                                        </button>
                                                    </h5>
                                                </div>
                                                <div id="collapseTwo" class="collapse" aria-labelledby="headingTwo" data-parent="#accordionExample">
                                                    <div class="card-body">
                                                        Anim pariatur cliche reprehenderit, enim eiusmod high.
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="card m-b-0">
                                                <div class="card-header bg-white p-0" id="headingThree">
                                                    <h5 class="mb-0">
                                                        <button class="btn btn-link collapsed" type="button" data-toggle="collapse" data-target="#collapseThree" aria-expanded="false"
                                                            aria-controls="collapseThree">
                                                            Collapsible Group Item #3
                                                        </button>
                                                    </h5>
                                                </div>
                                                <div id="collapseThree" class="collapse" aria-labelledby="headingThree" data-parent="#accordionExample">
                                                    <div class="card-body">
                                                        Anim pariatur cliche reprehenderit, enim eiusmod high.
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </li>
                                    <li class="col-lg-3  m-b-30">
                                        <h4 class="m-b-20">CONTACT US</h4>
                                        <!-- Contact -->
                                        <form>
                                            <div class="form-group">
                                                <input type="text" class="form-control" id="exampleInputname1" placeholder="Enter Name"> </div>
                                            <div class="form-group">
                                                <input type="email" class="form-control" placeholder="Enter email"> </div>
                                            <div class="form-group">
                                                <textarea class="form-control" id="exampleTextarea" rows="3" placeholder="Message"></textarea>
                                            </div>
                                            <button type="submit" class="btn btn-info">Submit</button>
                                        </form>
                                    </li>
                                    <li class="col-lg-3 col-xlg-4 m-b-30">
                                        <h4 class="m-b-20">List style</h4>
                                        <!-- List style -->
                                        <ul class="list-style-none">
                                            <li><a href="javascript:void(0)"><i class="fa fa-check text-success"></i> You can give link</a></li>
                                            <li><a href="javascript:void(0)"><i class="fa fa-check text-success"></i> Give link</a></li>
                                            <li><a href="javascript:void(0)"><i class="fa fa-check text-success"></i> Another Give link</a></li>
                                            <li><a href="javascript:void(0)"><i class="fa fa-check text-success"></i> Forth link</a></li>
                                            <li><a href="javascript:void(0)"><i class="fa fa-check text-success"></i> Another fifth link</a></li>
                                        </ul>
                                    </li>
                                </ul>
                            </div>
                        </li>
                       <li class="nav-item dropdown u-pro">
                            <a class="nav-link dropdown-toggle waves-effect waves-dark profile-pic" href="" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
							<img src="{{asset('public/assets/images/users/1.jpg')}}" alt="user" class=""> <span class="hidden-md-down">@if(Auth::check())
								 Welcome {{Auth::user()->username}}  
								 @else
								 not    
								 @endif&nbsp;
							<i class="fa fa-angle-down"></i></span> </a>
                            <div class="dropdown-menu dropdown-menu-right animated flipInY">
                                <!-- text-->
                                <a href="javascript:void(0)" class="dropdown-item"><i class="ti-user"></i> My Profile</a>
                                <!-- text-->
                                <a href="javascript:void(0)" class="dropdown-item"><i class="ti-wallet"></i> My Balance</a>
                                <!-- text-->
                                <a href="javascript:void(0)" class="dropdown-item"><i class="ti-email"></i> Inbox</a>
                                <!-- text-->
                                <div class="dropdown-divider"></div>
                                <!-- text-->
                                <a href="javascript:void(0)" class="dropdown-item"><i class="ti-settings"></i> Account Setting</a>
                                <!-- text-->
                                <div class="dropdown-divider"></div>
                                <!-- text-->
								@if(Auth::check())
								 <a href="{{route('logout')}}" class="dropdown-item"><i class="fa fa-power-off"></i> Logout</a>
								</a>
								@else
								@endif
								
                                <!-- text-->
                            </div>
                        </li>
                        <!-- ============================================================== -->
                        <!-- End User Profile -->
                        <!-- ============================================================== -->
                        <li class="nav-item right-side-toggle"> <a class="nav-link  waves-effect waves-light" href="javascript:void(0)"><i class="ti-settings"></i></a></li>
                    </ul>
                </div>
            </nav>
        </header>
      
        <aside class="left-sidebar">
            <!-- Sidebar scroll-->
            <div class="scroll-sidebar">
                <!-- Sidebar navigation-->
                <nav class="sidebar-nav">
                    <ul id="sidebarnav">
                        <li class="user-pro"> 
						<a class="has-arrow waves-effect waves-dark" href="javascript:void(0)" aria-expanded="false">
						<img src="{{asset('public/assets/images/users/1.jpg')}}" alt="user-img" class="img-circle">
						<span class="hide-menu">Mark Jeckson</span>
						</a>
                            <ul aria-expanded="false" class="collapse">
                                <li><a href="#"><i class="ti-user"></i> My Profile</a></li>
                                <li><a href="#"><i class="ti-wallet"></i> My Balance</a></li>
                                <li><a href="#"><i class="ti-email"></i> Inbox</a></li>
                                <li><a href="#"><i class="ti-settings"></i> Account Setting</a></li>
                                <li><a href="#"><i class="fa fa-power-off"></i> Logout</a></li>
                            </ul>
                        </li>
                        
                        <li> 
						 <a class="waves-effect waves-dark" href="/" aria-expanded="false"><i class="far fa-circle text-danger"></i><span class="hide-menu">Dashboard</span></a>
						</li>
                        
                        <li> 
                        <a class="waves-effect waves-dark" href="javascript:void(0)" aria-expanded="false"><i class="far fa-circle text-info"></i><span class="hide-menu">Websites</span></a>

                            <ul aria-expanded="false" class="collapse">
                                
                                <li> 
                                    <a class="waves-effect waves-dark" href="{{route('admin.home')}}" aria-expanded="false"><i class="far text-info"></i><span class="hide-menu">Home</span></a>
                                </li>

                                <li> 
                                    <a class="waves-effect waves-dark" href="{{route('admin.allaudit')}}" aria-expanded="false"><i class="far text-info"></i><span class="hide-menu">Audit</span></a>
                                </li>
                                
                                
                                <li> 
                                    <a class="waves-effect waves-dark" href="{{route('admin.alldoc')}}" aria-expanded="false"><i class="far text-info"></i><span class="hide-menu">Forms</span></a>
                                </li>
                                
                                <li> 
                                    <a class="waves-effect waves-dark" href="{{route('admin.allgallery')}}" aria-expanded="false"><i class="far text-info"></i><span class="hide-menu">Gallery</span></a>
                                </li>
                                
                                <li> 
                                    <a class="waves-effect waves-dark" href="{{route('admin.allevent')}}" aria-expanded="false"><i class="far text-info"></i><span class="hide-menu">Events</span></a>
                                </li>
                                
                                <li> 
                                    <a class="waves-effect waves-dark" href="{{route('admin.allnotice')}}" aria-expanded="false"><i class="far text-info"></i><span class="hide-menu">Notice</span></a>
                                </li>
                                
                                <li> 
                                    <a class="waves-effect waves-dark" href="{{route('admin.contact')}}" aria-expanded="false"><i class="far text-info"></i><span class="hide-menu">Contact us</span></a>
                                </li>
                                
                                <li> 
                                    <a class="waves-effect waves-dark" href="{{route('admin.subs')}}" aria-expanded="false"><i class="far  text-info"></i><span class="hide-menu">Subscribes</span></a>
                                </li>  
                           </ul>
                        </li>

                        <li> 
                            <a class="waves-effect waves-dark" href="javascript:void(0)" aria-expanded="false"><i class="far fa-circle text-info"></i><span class="hide-menu">Roles & Permission</span></a>
                            <ul aria-expanded="false" class="collapse">
                                <li>
                                    <a href="{{route('admin.role')}}">List of roles</a>
                                </li>
                                <li>
                                    <a href="{{route('admin.set-permission')}}">Set Permission</a>
                                </li>
                            </ul>
                        </li>
                        <li>
                            <a class="waves-effect waves-dark" href="{{route('admin.account')}}" aria-expanded="false"><i class="far fa-circle text-success"></i><span class="hide-menu">Panchyat</span></a>
                        </li>
                        
                        <li> 
                            <a class="waves-effect waves-dark" href="{{route('admin.user')}}" aria-expanded="false"><i class="far fa-circle text-info"></i><span class="hide-menu">User</span></a>
                        </li>
                        <li> 
                            <a class="waves-effect waves-dark" href="{{route('admin.tickets')}}" aria-expanded="false"><i class="far fa-circle text-info"></i><span class="hide-menu">Support Ticket</span></a>
                        </li>
                        
                        <li> 
                            <a class="waves-effect waves-dark" href="{{route('setting')}}" aria-expanded="false"><i class="far fa-circle text-info"></i><span class="hide-menu">Setting</span></a>
                        </li>
                        
                        <li>
                            <a class="waves-effect waves-dark" href="{{route('admin.email')}}" aria-expanded="false"><i class="far fa-circle text-info"></i><span class="hide-menu">Email</span></a>
                        </li>
                        <li>
                            <a class="waves-effect waves-dark" href="{{route('file-manager')}}" aria-expanded="false"><i class=" text-info far fa-circle"></i><span class="hide-menu">File Manager</span></a>
                        </li>
                        
                        <li> 
                             <a class="waves-effect waves-dark" href="{{route('ads')}}" aria-expanded="false"><i class="text-info far fa-circle"></i><span class="hide-menu">Advertise</span></a>
                        </li>
                        
                        <li> 
                            <a class="waves-effect waves-dark" href="{{route('admin.add-certificate')}}" aria-expanded="false"><i class="far fa-circle text-info"></i><span class="hide-menu">Add Certificates</span></a>
                        </li>
                        
                        <li> 
                            <a class="waves-effect waves-dark" href="{{route('admin.attendence')}}" aria-expanded="false"><i class="text-info far fa-circle"></i><span class="hide-menu">Attendence</span></a>
                        </li>
                        
                        <li> 
                            <a class="waves-effect waves-dark" href="{{route('admin.request')}}" aria-expanded="false"><i class="far fa-circle text-info"></i><span class="hide-menu">Request</span></a>
                        </li>

						
                        <li> 
                            <a class="waves-effect waves-dark" href="{{route('admin.main_cat')}}" aria-expanded="false"><i class="far fa-circle text-info"></i><span class="hide-menu">Add Category</span></a>
                        </li>
						
						<li> 
						<a class="waves-effect waves-dark" href="{{route('admin.citizens')}}" aria-expanded="false"><i class="far fa-circle text-info"></i><span class="hide-menu">Citizens</span></a>
						</li>
						
						
						
						
                    </ul>
                </nav>
                <!-- End Sidebar navigation -->
            </div>
            <!-- End Sidebar scroll-->
        </aside>