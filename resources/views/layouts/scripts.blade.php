<!--=========================*
          All JS
*===========================-->
 <script src="{{asset('public/assets/jquery/jquery-3.2.1.min.js')}}"></script>
    <script src="{{asset('public/assets/popper/popper.min.js')}}"></script>
    <script src="{{asset('public/assets/bootstrap/dist/js/bootstrap.min.js')}}"></script>
    <script src="{{asset('public/assets/dist/js/perfect-scrollbar.jquery.min.js')}}"></script>
    <script src="{{asset('public/assets/dist/js/waves.js')}}"></script>
    <script src="{{asset('public/assets/dist/js/sidebarmenu.js')}}"></script>
    <script src="{{asset('public/assets/dist/js/custom.min.js')}}"></script>
    <script src="{{asset('public/assets/raphael/raphael-min.js')}}"></script>
    <script src="{{asset('public/assets/morrisjs/morris.min.js')}}"></script>
    <script src="{{asset('public/assets/jquery-sparkline/jquery.sparkline.min.js')}}"></script>
    <script src="{{asset('public/assets/toast-master/js/jquery.toast.js')}}"></script>
    <script src="{{asset('public/assets/dist/js/dashboard1.js')}}"></script>
    <script src="{{asset('public/assets/toast-master/js/jquery.toast.js')}}"></script>
    <script src="{{asset('public/assets/switchery/dist/switchery.min.js')}}"></script>
	<script src="{{asset('public/assets/footable/js/moment.js')}}"></script>
    <script src="{{asset('public/assets/bootstrap-daterangepicker/daterangepicker.js')}}"></script>
    <script src="{{asset('public/assets/bootstrap-daterangepicker/daterangepicker.js')}}"></script>
    <script src="{{asset('public/assets/bootstrap-datepicker/bootstrap-datepicker.min.js')}}"></script>
    <script src="{{asset('public/assets/bootstrap-material-datetimepicker/js/bootstrap-material-datetimepicker.js')}}"></script>
    <script src="{{asset('public/assets/bootstrap-datepicker/bootstrap-datepicker.min.js')}}"></script>
    <script src="{{asset('public/assets/wizard/jquery.steps.min.js')}}"></script>
    <script src="{{asset('public/assets/wizard/jquery.validate.min.js')}}"></script>
	 <script src="{{asset('public/assets/Magnific-Popup-master/dist/jquery.magnific-popup.min.js')}}"></script>
    <script src="{{asset('public/assets/Magnific-Popup-master/dist/jquery.magnific-popup-init.js')}}"></script>
   
 
  <script src="{{asset('public/assets/datatables.net/js/jquery.dataTables.min.js')}}"></script>
    <script src="{{asset('public/assets/datatables.net-bs4/js/dataTables.responsive.min.js')}}"></script>
    <!-- start - This is for export functionality only -->
    <script src="https://cdn.datatables.net/buttons/1.5.1/js/dataTables.buttons.min.js"></script>
    <script src="https://cdn.datatables.net/buttons/1.5.1/js/buttons.flash.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jszip/3.1.3/jszip.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.32/pdfmake.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.32/vfs_fonts.js"></script>
    <script src="https://cdn.datatables.net/buttons/1.5.1/js/buttons.html5.min.js"></script>
    <script src="https://cdn.datatables.net/buttons/1.5.1/js/buttons.print.min.js"></script>
	<script src="{{asset('public/assets/sweetalert2/dist/sweetalert2.all.min.js')}}"></script>
    <script src="{{asset('public/assets/sweetalert2/sweet-alert.init.js')}}"></script>
    <script src="{{asset('public/assets/ajax/ajaxrequest.js')}}"></script>
    <script src="{{asset('public/assets/toggle/bootstrap-toggle.js')}}"></script>
    <script src="{{asset('public/assets/calendar/dist/fullcalendar.min.js')}}"></script>
    <script src="{{asset('public/assets/calendar/dist/cal-init.js')}}"></script>
   
    <!-- end - This is for export functionality only -->
    
	