<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

	/*Route::get('/', function () {
	    return view('frontend.index');
	});*/
	Route::get('/', 'PublicController@index')->name('/');
	Route::get('/super', function () {
	    return view('index');
	});


	//Main website routes are starts here

	
	Route::post('panchayat/data','WebsiteController@panc_data');

		
	Route::prefix('sa')->group(function () 
	{
			Route::post('/login', 'LoginController@login_superadmin')->name('superadmin-login');
			Route::get('/', function () {
					return view('admin.login');
			});
				
	});
	Route::group(['middleware' => 'superadmin'], function()
	{

		Route::post('round/update','AdminController@status_update');
		Route::get('/dashboard', 'AdminController@index')->name('superadmin');
		Route::get('all-account','AdminController@add_account')->name('admin.account');

		Route::get('home','AdminController@home')->name('admin.home');

		//Super Admin
		Route::get('all-details','AdminController@add_super_account')->name('admin.super_admin');
		// Audit Route
		Route::get('all-audit','AdminController@allaudit')->name('admin.allaudit');
		Route::get('add-audit','AdminController@addaudit')->name('admin.addaudit');

		//Document Route
		Route::get('all-doc','AdminController@alldocument')->name('admin.alldoc');
		Route::get('add-doc','AdminController@add_document')->name('admin.adddoc');
		Route::post('insert-doc','AdminController@save_document')->name('admin.insert-form');
		Route::post('edit-doc','AdminController@save_document')->name('admin.update-form');
		Route::get('form-delete/{id}','AdminController@deletedoc');

		//Gallery Route
		Route::get('all-gallery','AdminController@allgallery')->name('admin.allgallery');
		Route::get('add-gallery','AdminController@add_gallery')->name('admin.addgallery');

		//Event Route
		Route::get('all-event','AdminController@allevent')->name('admin.allevent');
		Route::get('add-event','AdminController@add_event')->name('admin.addevent');
		Route::post('save-event','AdminController@save_event')->name('admin.save_event');
		Route::post('edit-event','AdminController@edit_event')->name('admin.update-event');
		Route::get('full_event/{id}','AdminController@full_event');
		Route::get('delete_event/{id}','AdminController@delete_event')->name('admin.event_delete');

		//Notice Route
		Route::get('all-notice','AdminController@allnotice')->name('admin.allnotice');
		Route::get('add-notice','AdminController@add_notice')->name('admin.addnotice');
		Route::post('save-notice','AdminController@save_notice')->name('admin.notice');
		Route::get('delete-notice/{id}','AdminController@delete_notice')->name('admin.notice_delete');
		Route::post('edit-notice','AdminController@edit_notice')->name('admin.update-notice');
		Route::get('full_notice/{id}','AdminController@parti_notice');

		//Contact Us Route
		Route::get('contact','AdminController@contact_us')->name('admin.contact');
		Route::get('contact-delete/{id}','AdminController@contact_delete')->name('contact-delete');

		//Subscribe Route
		Route::get('subscribe','AdminController@subscriber')->name('admin.subs');
		Route::get('subscribe-delete/{id}','AdminController@subs_delete')->name('subs-delete');

		//All category Routes
		Route::get('main-cat','AdminController@maincat')->name('admin.main_cat');
		Route::post('add-main-cat','AdminController@add_maincat')->name('admin.insert-main-cat');
		Route::get('all-category','AdminController@all_maincat')->name('admin.all-cat');
		Route::get('sub-category/{id}','AdminController@sub_cat')->name('sub-cat');
		Route::post('add-sub-category','AdminController@add_sub_cat')->name('admin.insert_sub_cat');
		Route::get('delete-main-category/{id}','AdminController@delete_main_cat')->name('delete-main-cat');
		Route::get('delete-sub-category/{id}','AdminController@delete_sub_cat')->name('sub-cat-delete');

		Route::get('show-panchayat/{id}','AdminController@show_panchayat')->name('show_panchayat');
		Route::post('edit-panchayat','AdminController@edit_panchayat')->name('edit_panchyat');
		Route::get('edit-panchayat/{id}','AdminController@delete_panchayat')->name('delete-panchayat');

		//All Users Routes
		Route::get('all-user','AdminController@users')->name('admin.user');
		Route::post('add-user','AdminController@add_user')->name('add-panachyat-user');
		Route::get('panchayat-user/{id}','AdminController@view_user')->name('user-view');
		Route::post('edit-user/{id}','AdminController@edit_user')->name('edit-user');
		Route::post('new-user','AdminController@add_user')->name('new-user');
		Route::get('delete-user/{id}','AdminController@delete_user')->name('user-delete');

		//Roles Route
		Route::get('role','AdminController@role')->name('admin.role');
		Route::post('add-role','AdminController@add_role')->name('add-role');
		Route::post('edit-role/{id}','AdminController@edit_role')->name('edit-role');
		Route::get('delete-role/{id}','AdminController@delete_role')->name('role-delete');
		Route::get('citizens','AdminController@citizens')->name('admin.citizens');
		Route::get('view-citizen/{id}','AdminController@view_citizen')->name('citizen.view');
		Route::get('view-secondry-member','AdminController@view_secondry_mem')->name('view_secondry_mem');


		//Ads Routes
		Route::get('ads','AdsController@ads')->name('ads');
		//Attendence Route
		Route::get('attendence','AdminController@attendence')->name('admin.attendence');
		Route::get('secretary-attendence','AdminController@sec_attendence')->name('attdence.secretary');

		# Set Permissions
		Route::get('set-permission','PermissionController@index')->name('admin.set-permission');

		// Tickets Routes
		Route::get('tickets','AdminController@tickets')->name('admin.tickets');
		Route::get('ticket-reply/{id}','AdminController@ticket_reply')->name('ticket-reply');
		Route::get('email','AdminController@email')->name('admin.email');
		Route::get('show-email','AdminController@show_email')->name('show-email');
		Route::get('compose-email','AdminController@compose_email')->name('compose-email');
		Route::get('timeline','AdminController@timeline')->name('timeline');
		Route::get('sms-credit','AdminController@sms_credit')->name('sms-credit');
		Route::get('file-manager','AdminController@file_manager')->name('file-manager');
		Route::post('reply-to-citizen','AdminController@reply_to_citizen')->name('reply-to-citizen');
		Route::get('widget_status/{status}/{id}','AdminController@widget_status')->name('widget_status');
		Route::post('update_bg_image','AdminController@update_bg_image')->name('update_bg_image');



	});
	//Citizen Route Start here

	
	Route::group(['middleware' => 'citizen'], function()
	{
		Route::get('superadminchek',function(){
			echo 'superadmin';
		});
		Route::get('profile','CitizenController@profile')->name('profile');
		Route::get('citizen','CitizenController@index')->name('citizen');
		Route::get('certificates','CitizenController@document')->name('citizen-add-doc');
		Route::post('save-doc','CitizenController@save_document')->name('save-doc');
		Route::match(array('GET','POST'),'ajax-form-submit','CitizenController@get_otp');
		Route::get('citizen-logout','LoginController@citizen_logout')->name('citizen-logout');
		Route::get('bank-detail','CitizenController@bank_details')->name('bank-details');
		Route::post('save-detail','CitizenController@save_details')->name('save-details');
		Route::get('certificate-request','CitizenController@certificate_request')->name('certificate-request');
		Route::post('ad-certificate','CitizenController@ad_certificate')->name('add-certificate');
		Route::get('general','CitizenController@general')->name('general');
		
		Route::get('citizen-bank-detail','CitizenController@bank_detail')->name('citizen-bank-detail');
		Route::match(array('GET','POST'),'check-certificate','CitizenController@check_certificate');
		
		Route::get('assets','CitizenController@assets')->name('assets');
		Route::get('tax','CitizenController@tax')->name('tax');
		
		Route::get('notification','CitizenController@notification')->name('notification');
		Route::get('ticket','CitizenController@ticket')->name('ticket');
		Route::get('other','CitizenController@other')->name('other');
		Route::get('pay-tax','CitizenController@pay_tax')->name('pay-tax');
		Route::get('tax-details','CitizenController@tax_details')->name('tax-details');
		Route::get('all-ertificate','CitizenController@all_ertificate')->name('all-ertificate');
		Route::get('new-rti','CitizenController@new_rti')->name('new-rti');
		Route::get('new-ticket','CitizenController@new_ticket')->name('new-ticket');
		Route::post('update-tax','CitizenController@update_tax')->name('update-tax');
		Route::post('add-ticket','CitizenController@ad_ticket')->name('add-ticket');
		Route::get('show-reply/{id}','CitizenController@show_reply')->name('show-reply');
	
	});

	/*other route */
	Route::get('/logout', 'LoginController@logout')->name('logout');
	
	

	
	//Accounts and Wizard Routes
	Route::post('admin/wizard/step-second','WizardController@post_step_first')->name('post_step_first');
	Route::get('admin/wizard/step-first-panchayat','WizardController@get_step_first')->name('get_step_first');
	Route::get('admin/wizard/step-second-panchayat','WizardController@get_step_second')->name('get_step_second');
	Route::post('admin/wizard/step-second','WizardController@post_step_second')->name('post_step_second');
	Route::get('admin/wizard/step-third-panchayat','WizardController@get_step_third')->name('get_step_third');
	
	
	
	
	

	//Setting Routes
	//Route::get('setting.attdence','SettingController@index')->name('admin.setting');
	Route::get('website','WebsiteController@website')->name('admin.website');
	Route::get('request','WebsiteController@request')->name('admin.request');
	Route::get('setting','WebsiteController@setting')->name('setting');

	//Certificate Routes
	Route::get('certificate','WebsiteController@certificate')->name('admin.add-certificate');
	Route::post('add-certificate','WebsiteController@add_certificate')->name('add-certificate');
	Route::get('delete-certificate/{id}','WebsiteController@delete_certificate')->name('delete-certificate');
	Route::get('view-certificate/{id}','WebsiteController@view_certificate')->name('view-certificate');


	//Websites Routes
	Route::post('add-details','WebsiteController@add_home_detail')->name('add-details');
	Route::get('rti','WebsiteController@rti')->name('citi-rti');
	Route::get('salary-pdf','AdminController@sallery_pdf')->name('sallery-pdf');
	Route::get('about-us','WebsiteController@admin_about_us')->name('admin.about-us');
	Route::get('admin/about-us','WebsiteController@admin_about_us')->name('admin.about-us');
	
	
	//Frontend Routes
	Route::get('scheme','WebsiteController@scheme')->name('scheme');
	Route::get('acts','WebsiteController@acts')->name('acts');
	Route::get('web-rti','CitizenController@web_rti')->name('exit-rti');
	Route::get('forms','WebsiteController@home_forms')->name('forms');
	Route::get('gallery','WebsiteController@gallery')->name('gallery');
	Route::get('audit','WebsiteController@audit')->name('audit');
	Route::get('certi-request','WebsiteController@certi_request')->name('certi_request');
	Route::get('contact-us','WebsiteController@contact_us')->name('contact_us');
	Route::post('add-citizen','CitizenController@add_citizen')->name('add-citizen');
	Route::get('about-us','CitizenController@about_us')->name('about-us');
	Route::match(array('GET','POST'),'citi-dashboard','LoginController@login')->name('citizen-login');
	
	//Citizen routes for frontend
	

