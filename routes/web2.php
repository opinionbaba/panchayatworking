<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web2 Custom Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web2 routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web2" middleware group. Now create something great!
|
*/
#Route::get('tet','CitizenController@test')->name('test');
//Ads Routes
/*for admin /super admin */
Route::group(['middleware' => 'superadmin'], function(){

	Route::get('ads','AdsController@ads')->name('ads');
	Route::get('ads-delete/{id}','AdsController@ads_delete')->name('ads-delete');
	Route::get('adspricedelete/{id}','AdsController@price_delete')->name('adspricedelete');
	Route::get('adsstatus/{status}/{id}','AdsController@ads_status')->name('adsstatus');
	Route::get('pricestatus/{status}/{id}','AdsController@price_status')->name('pricestatus');
	/*rendering webdata */
	Route::get('adsdeletes/{data}','AdsController@renderweb')->name('adsdeletes');
	/*
	*/
	Route::post('adsupdate','AdsController@ads_update')->name('ads-update');
	Route::post('adsprice','AdsController@ads_price')->name('ads-price');

});
Route::group(['middleware' => 'citizen'], function(){

	/*new ads*/
	Route::post('adsrequest','AdsController@adsrequest')->name('adsrequest');
	/* for citizen2*/
	Route::get('advertise','AdsController@advertise')->name('advertise');
	Route::get('new-ad','AdsController@new_ad')->name('new-ad');

});

