<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web3 Custom Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web3 routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web3" middleware group. Now create something great!
|
*/
#Route::get('tet','CitizenController@test')->name('test');

Route::get('/hello', function () {
    return "hello";
});
Route::group(['middleware' => 'superadmin'], function(){

	Route::post('get-certi/data','CitizenController@get_certi');
	
	
	Route::get('/add-permission','PermissionController@add_permission')->name('add_permission');

});
Route::group(['middleware' => 'citizen'], function(){

	

});
	//Shubham 
	Route::group(['prefix' => '{panc_id}'], function(){
	return "success";
	Route::get('/heloo','WizardController@heloo')->name('heloo');
	Route::get('/hi','AdminController@hi')->name('hi');
});